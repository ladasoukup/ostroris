using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySQLDriverCS;
using ComponentFactory.Krypton.Toolkit;
using PSTaskDialog;

namespace CTK_reader
{
    public partial class frm_profile_select : KryptonForm
    {
        private MainForm opener;
        public bool AllowAutoLogin = false;

        public frm_profile_select(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            OKclicked();
        }

        private void OKclicked()
        {
            string profile_sql = "SELECT * FROM  " + opener.sql_db_table + "_profil WHERE ctk_user='" + txt_user.Text + "' AND ctk_pass='" + txt_pass.Text + "'";
            loadingCircle_login.Visible = true;
            loadingCircle_login.Active = true;
            txt_user.Enabled = false;
            btn_login.Enabled = false;
            lbl_error.Text = "";

            try
            {
                MySQLCommand profile_cmd = new MySQLCommand(profile_sql, opener.MYSQL);
                MySQLDataReader profile_reader = profile_cmd.ExecuteReaderEx();
                bool UserPwdOK = profile_reader.Read();

                if (UserPwdOK == true)
                {
                    if ((profile_reader["ctk_user"].ToString() == txt_user.Text) && (profile_reader["ctk_pass"].ToString() == txt_pass.Text))
                    {
                        opener.ActualProfile = profile_reader["ctk_user"].ToString();
                        opener.Visible = true;
                        this.Focus();
                        Application.DoEvents();

                        opener.CTK_profileLoad();
                        lbl_error.Text = "OK";
                        this.Close();
                    }
                }
                else
                {
                    lbl_error.Text = "Neplatn� heslo!";
                    opener.ShowAppNotify("P�ihl�en� k profilu se nezda�ilo, Va�e heslo nen� platn�.", 2000, ToolTipIcon.Warning);
                    loadingCircle_login.Visible = false;
                    loadingCircle_login.Active = false;
                }

            }
            catch (Exception ex)
            {
                lbl_error.Text = "Chyba profilu!";
                opener.LogDebug(ex.ToString(), "PROFILE-LOAD");
                loadingCircle_login.Visible = false;
                loadingCircle_login.Active = false;
            }
            txt_pass.SelectAll();
            txt_user.Enabled = true;
            btn_login.Enabled = true;
        }

        private void frm_profile_select_Load(object sender, EventArgs e)
        {
            opener.Visible = false;
            txt_user.Enabled = false;
            txt_pass.Enabled = false;
            timer_open.Interval = 500;
            timer_open.Enabled = true;
            this.TextExtra = "verze: " + Application.ProductVersion.ToString();
            loadingCircle_login.Visible = false;
            loadingCircle_login.Active = false;
        }

        private void profile_LoadUsers()
        {
            loadingCircle_login.Visible = true;
            loadingCircle_login.Active = true;
            string S = null;
            string profile_sql = "SELECT ctk_user FROM  " + opener.sql_db_table + "_profil ORDER BY ctk_user";
            Application.DoEvents();
            txt_user.Enabled = false;
            btn_login.Enabled = false;
            lbl_error.Text = "";

            try
            {
                MySQLCommand profile_cmd = new MySQLCommand(profile_sql, opener.MYSQL);
                MySQLDataReader profile_reader = profile_cmd.ExecuteReaderEx();
                opener.LogDebug(profile_sql, "SQL");
                while (profile_reader.Read())
                {
                    S = profile_reader["ctk_user"].ToString();
                    txt_user.AutoCompleteCustomSource.Add(S);
                }
                txt_user.Enabled = true;
                txt_pass.Enabled = true;
                txt_user.Focus();
            }
            catch (Exception ex)
            {
                // DialogResult res = MessageBox.Show("P�ipojen� k MySQL datab�zi se nezda�ilo.\nZkontrolujte nastaven� a restartujte aplikaci.\nPokud pot�e p�etrvaj�, kontaktujte spr�vce syst�mu.\r\n\r\nJe mo�n�, �e se jedn� jen o do�asnou nedostupnost serveru.\r\nChcete pokus o p�ipojen� opakovat?", "MySQL - p�ipojen� selhalo", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                DialogResult res = cTaskDialog.ShowTaskDialogBox(opener.Text, "P�ipojen� k datab�zi selhalo.", "M��e se jednat jen o do�asnou nedostupnost datab�ze, nebo chybnou konfiguraci aplikace.", "", "", "", "", "Zkusit se p�ipojit znovu|Zm�nit nastaven�", eTaskDialogButtons.OK, eSysIcons.Error, eSysIcons.Error);
                int res_id = cTaskDialog.CommandButtonResult;

                if (res_id == 0)
                {
                    opener.MySQL_connect();
                    Application.DoEvents();
                    timer_open.Interval = 250;
                    timer_open.Enabled = true;
                }
                else if (res_id == 1)
                {
                    this.Close();
                    this.Dispose();
                    opener.cfg_OpenMySQL_config();

                }
                else
                {
                    opener.LogDebug(ex.ToString(), "SQL");
                    this.Close();
                    this.Dispose();
                    opener.ChangeStatusBar("Spojen� s datab�z� nen� nav�z�no!", false);
                }
            }

            Application.DoEvents();
            this.Focus();
            loadingCircle_login.Visible = false;
            loadingCircle_login.Active = false;
        }

        private void txt_pass_textChanged(object sender, EventArgs e)
        {
            lbl_error.Text = "";
        }

        private void txt_pass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { this.Close(); }
            if (e.KeyCode == Keys.Enter) { OKclicked(); }
        }

        private void txt_user_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape) { this.Close(); }
            if (e.KeyCode == Keys.Enter) { txt_pass.Focus(); }
        }

        private void profile_DoAutoLogin()
        {
            if (AllowAutoLogin == true)
            {
                string S = null;
                S = opener.ini.IniReadValue("AutoLogin", "enable");
                if (S == "True")
                {
                    txt_user.Text = opener.ini.IniReadEncValue("AutoLogin", "user");
                    txt_pass.Text = opener.ini.IniReadEncValue("AutoLogin", "password");
                    OKclicked();
                }
            }
        }

        private void timer_open_Tick(object sender, EventArgs e)
        {
            timer_open.Enabled = false;
            profile_DoAutoLogin();
            if (lbl_error.Text != "OK")
            {
                profile_LoadUsers();
                //txt_user.Text = "";
                txt_pass.Text = "";
                if (txt_user.Text != "") txt_pass.Focus();
            }
        }

        private void txt_user_TextChanged(object sender, EventArgs e)
        {
            btn_login.Enabled = true;
            lbl_error.Text = "";
            txt_pass.Text = "";
        }

        private void frm_profile_select_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.DoEvents();
            loadingCircle_login.Visible = false;
            loadingCircle_login.Active = false;
            opener.Visible = true;
            opener.Focus();
        }

        private void timer_hide_app_Tick(object sender, EventArgs e)
        {
            opener.Visible = false;
            this.Focus();
        }
    }
}