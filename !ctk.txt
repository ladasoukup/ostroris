> REGION�LN� K�DY SERVISU
> 
>     Region�ln� lze vyhled�vat podle k�d� servisu. Ty, kter� za��naj� 
> p�smenem "m", ozna�uj� v�eobecn� zpr�vy ze zahrani��, ty, kter� 
> za��naj� p�smenem "d", ozna�uj� v�eobecn� zpr�vy z �R, ty, kter� 
> za��naj� p�smenem "e", ozna�uj� ekonomick� zpr�vy a ty, kter� za��naj� 
> p�smenem "s", ozna�uj� sportovn� zpr�vy.
> 
>     Evropa
>     mes
>     ees
>     ses
> 
>     Spole�enstv� (nez�visl�ch st�t�)
>     msp
>     esp
>     ssp
> 
>     Bl�zk� (v�chod)
>     mbv
>     ebv
>     sbv
> 
>     Afrika
>     maf
>     eaf
>     saf
> 
>     Asie
>     mas
>     eas
>     sas
> 
>     Amerika
>     mus
>     eus
>     sus
> 
>     Latinsk� (Amerika)
>     mla
>     ela
>     sla
> 
>     Zpr�vy z Prahy maj� k�dy:
>     dce
>     ece
>     sce
> 
>     Zpr�vy z kraj�:
> 
>     St�edo�esk�
>     dsc
>     esc
>     ssc
> 
>     Jiho�esk�
>     dcb
>     ecb
>     scb
> 
>     Plze�sk�
>     dpm
>     epm
>     spm
> 
>     Karlovarsk�
>     dkv
>     ekv
>     skv
> 
>     �steck�
>     dul
>     eul
>     sul
> 
>     Libereck�
>     dli
>     eli
>     sli
> 
>     Kr�lov�hradeck�
>     dhk
>     ehk
>     shk
> 
>     Pardubick�
>     dpu
>     epu
>     spu
> 
>     Vyso�ina
>     dji
>     eji
>     sji
> 
>     Jihomoravsk�
>     dbn
>     ebn
>     sbn
> 
>     Zl�nsk�
>     dzl
>     ezl
>     szl
> 
>     Olomouck�
>     dol
>     eol
>     sol
> 
>     Moravskoslezsk�
>     dov
>     eov
>     sov
