using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using MySQLDriverCS;

namespace CTK_reader
{
    partial class MainForm
    {
        public void CustNews_init()
        {
            LogDebug("CustNews_init()", "SYSTEM");
            CustNews_cleanup();
            // start

            // Load CustNews groups
            CustNews_sel_group.Items.Add("-v�echy-");
            sql_query = "SELECT group_id FROM " + sql_db_table + "_custnews_groups ORDER BY id";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    CustNews_sel_group.Items.Add(sql_reader.GetString(0));
                }
                sql_reader.Close();
                if (CustNews_sel_group.Items.Count > 0) CustNews_sel_group.SelectedIndex = 0;
            }
            catch { LogDebug("Load CustNews groups FAILED", "ERROR"); }

            // Load CustNews categories
            CustNews_sel_kategorie.Items.Add("-v�echy-");
            sql_query = "SELECT category_id FROM " + sql_db_table + "_custnews_categories ORDER BY category_id";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    CustNews_sel_kategorie.Items.Add(sql_reader.GetString(0));
                }
                sql_reader.Close();
                if (CustNews_sel_kategorie.Items.Count > 0) CustNews_sel_kategorie.SelectedIndex = 0;
            }
            catch { LogDebug("Load CustNews categories FAILED", "ERROR"); }
            CustNews_sel_kategorie.Items.Add("- ostatn� -");

            // Load CustNews oblasti
            CustNews_sel_oblast.Items.Add("-v�echy-");
            sql_query = "SELECT DISTINCT oblast FROM " + sql_db_table + "_custnews_oblasti ORDER BY oblast ASC";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    CustNews_sel_oblast.Items.Add(sql_reader[0].ToString());
                }
                if (CustNews_sel_oblast.Items.Count > 0) CustNews_sel_oblast.SelectedIndex = 0;
            }
            catch { LogDebug("Load CustNews oblasti FAILED", "ERROR"); }


            //end

            CustNews_dg_Load_List();
        }

        public void CustNews_cleanup()
        {
            CustNews_txt_detail.Text = "";
            dg_CustNews_list.Rows.Clear();
            CustNews_sel_group.Items.Clear();
            CustNews_sel_kategorie.Items.Clear();
            CustNews_sel_oblast.Items.Clear();

        }

        private void CustNews_timer_dg_reload_Tick(object sender, EventArgs e)
        {
            CustNews_dg_Load_List(true);
        }

        public void CustNews_dg_Load_List()
        {
            LogDebug("CustNews_dg_Load_List()", "SYSTEM");
            CustNews_dg_Load_List(false);
        }
        public void CustNews_dg_Load_List(bool onlyAdd)
        {
            LogDebug("CustNews_dg_Load_List(" + onlyAdd.ToString() + ")", "SYSTEM");
            string datum_sel_start;
            string datum_sel_end;
            string cn_datum;
            string CustNews_NowDate = "20070101";
            string CustNews_NowTime = "00:00:00";
            string[] new_row = new string[] { "", "", "", "", "" };
            Bitmap cn_repo_flag = Properties.Resources.flag_empty;

            ChangeStatusBar("Na��t�m data...", true);
            CustNews_timer_dg_reload.Enabled = false;
            try
            {
                datum_sel_start = CustNews_Calendar.SelectionStart.Year.ToString("0000") + CustNews_Calendar.SelectionStart.Month.ToString("00") + CustNews_Calendar.SelectionStart.Day.ToString("00");
                datum_sel_end = CustNews_Calendar.SelectionEnd.Year.ToString("0000") + CustNews_Calendar.SelectionEnd.Month.ToString("00") + CustNews_Calendar.SelectionEnd.Day.ToString("00");

                sql_query = "SELECT id, base_id, cn_datum, cn_cas, cn_okres, cn_nazev, cn_klicova_slova, cn_id_repa";
                sql_query += " FROM " + sql_db_table + "_custnews";
                sql_query += " WHERE (cn_datum BETWEEN '" + datum_sel_start + "' AND '" + datum_sel_end + "')";

                CustNews_NowDate = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00");
                CustNews_NowTime = DateTime.Now.AddSeconds(-1).Hour.ToString("00") + ":" + DateTime.Now.AddSeconds(-1).Minute.ToString("00") + ":" + DateTime.Now.AddSeconds(-1).Second.ToString("00");

                if (btn_CustNews_showAll.Checked == true)
                {
                    //sql_query += " AND ((cn_datum<='" + CustNews_NowDate + "' AND cn_cas<='" + CustNews_NowTime + "') OR cn_datum<'" + CustNews_NowDate + "' OR vlozil='" + ActualProfile + "' )";
                    sql_query += "";
                } else {
                    sql_query += " AND ((cn_datum<='" + CustNews_NowDate + "' AND cn_cas<='" + CustNews_NowTime + "') OR cn_datum<'" + CustNews_NowDate + "')";
                }


                if (onlyAdd == true)
                {
                    sql_query += " AND cn_datum >= '" + CustNews_LastDate + "'";
                    sql_query += " AND cn_cas >= '" + CustNews_LastTime + "'";
                }
                CustNews_LastDate = CustNews_NowDate;
                CustNews_LastTime = CustNews_NowTime;

                if (CustNews_sel_group.SelectedIndex != 0) sql_query += " AND group_id LIKE '%*" + CustNews_sel_group.Text + "*%'";
                if (CustNews_sel_oblast.SelectedIndex != 0) sql_query += " AND cn_oblast='" + CustNews_sel_oblast.Text + "'";
                if (CustNews_sel_kategorie.SelectedIndex != 0) sql_query += " AND cn_kategorie='" + CustNews_sel_kategorie.Text + "'";
                if (CustNews_sel_text.Text != "") sql_query += " AND (cn_nazev LIKE '%" + CustNews_sel_text.Text + "%' OR cn_klicova_slova LIKE '%" + CustNews_sel_text.Text + "%' OR cn_text LIKE '%" + CustNews_sel_text.Text + "%')";

                sql_query += " ORDER BY cn_datum ASC, cn_cas ASC, verze DESC";

                if (onlyAdd == false) dg_CustNews_list.Rows.Clear();
                
                LogDebug(sql_query, "SQL");
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    cn_datum = sql_reader.GetString(2).Substring(6, 2) + "." + sql_reader.GetString(2).Substring(4, 2) + "." + sql_reader.GetString(2).Substring(0, 4);
                    cn_repo_flag = Properties.Resources.flag_empty;

                    if (sql_reader.GetString(7) != "") cn_repo_flag = Properties.Resources.CustNews_repo;
                    new_row = new string[] { sql_reader["id"].ToString(), cn_datum, sql_reader.GetString(3).Substring(0, 5), sql_reader.GetString(4), sql_reader.GetString(5), sql_reader.GetString(6), null, sql_reader.GetString(7) };

                    dg_CustNews_list.Rows.Insert(0, new_row);
                    CustNews_list_MarkUnReaded(0, sql_reader["id"].ToString());

                    dg_CustNews_list.Rows[0].Cells[6].Value = cn_repo_flag;
                    dg_CustNews_list.Columns[6].Width = 20;
                }
                sql_reader.Close();
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "ERROR");
                ctk_status_label.Text = "Nen� nav�z�no p�ipojen� k MySQL datab�zi.";
                MYSQL.Close();
                Application.DoEvents();
                MySQL_connect();
            }
            LogDebug("CustNews_timer_dg_reload timer started - time: " + CustNews_reload_time.ToString() + " s", "TIMER");
            CustNews_timer_dg_reload.Interval = CustNews_reload_time * 1000;
            if (btn_CustNews_showAll.Checked != true) CustNews_timer_dg_reload.Enabled = true;
            ChangeStatusBar("P�ipraven", false);
            CustNews_LoadDetail();
        }


        public void CustNews_LoadDetail()
        {
            LogDebug("CustNews_LoadDetail()", "SYSTEM");
            ChangeStatusBar("Na��t�m data...", true);
            CustNews_list_MarkReaded();

            int dg_currID = 0; // dg_CustNews_list.CurrentRow.Index;
            string cn_datum;
            string datum_vlozil;
            string cn_id_repa = "";
            string VersionEditHistory = "";

            if (dg_CustNews_list.Rows.Count > 0)
            {
                try
                {
                    dg_currID = dg_CustNews_list.CurrentRow.Index;
                }
                catch { }

                try
                {
                    sql_query2 = "SELECT * FROM " + sql_db_table + "_custnews WHERE id=" + dg_CustNews_list.Rows[dg_currID].Cells[0].Value + " LIMIT 1";
                    sql_cmd2 = new MySQLCommand(sql_query2, MYSQL);
                    sql_reader2 = sql_cmd2.ExecuteReaderEx();
                    while (sql_reader2.Read())
                    {
                        string sql_query3 = "SELECT vlozil, vlozil_datum, vlozil_cas, verze FROM " + sql_db_table + "_custnews WHERE base_id=" + sql_reader2.GetString(19) + " AND verze < '" + sql_reader2.GetString(20) + "' ORDER BY verze ASC";
                        MySQLCommand sql_cmd3 = new MySQLCommand(sql_query3, MYSQL);
                        MySQLDataReader sql_reader3 = sql_cmd3.ExecuteReaderEx();
                        while (sql_reader3.Read())
                        {
                            datum_vlozil = sql_reader3.GetString(1).Substring(6, 2) + "." + sql_reader3.GetString(1).Substring(4, 2) + "." + sql_reader3.GetString(1).Substring(0, 4);
                            VersionEditHistory += sql_reader3.GetString(3) + ". verze: " + sql_reader3.GetString(0); // vlozil
                            VersionEditHistory += " (" + datum_vlozil + " " + sql_reader3.GetString(2) + ")";
                            VersionEditHistory += "\r\n";
                        }


                        cn_datum = sql_reader2["cn_datum"].ToString().Substring(6, 2) + "." + sql_reader2["cn_datum"].ToString().Substring(4, 2) + "." + sql_reader2["cn_datum"].ToString().Substring(0, 4);
                        datum_vlozil = sql_reader2["vlozil_datum"].ToString().Substring(6, 2) + "." + sql_reader2["vlozil_datum"].ToString().Substring(4, 2) + "." + sql_reader2["vlozil_datum"].ToString().Substring(0, 4);


                        if (sql_reader2.GetString(12) != "") cn_id_repa = sql_reader2.GetString(12); // id_repa


                        CustNews_txt_detail.Text = sql_reader2.GetString(11) + "\r\n"; //keywords
                        CustNews_txt_detail.Text += "Nadpis: " + sql_reader2.GetString(9) + "\r\n\r\n";

                        CustNews_txt_detail.Text += sql_reader2.GetString(10) + "\r\n\r\n"; // popis

                        if (cn_id_repa != "")
                        {
                            CustNews_txt_detail.Text += "Repo: " + cn_id_repa + "\r\n";
                            CustNews_txt_detail.Text += "D�lka: " + sql_reader2.GetString(13) + "\r\n";
                            CustNews_txt_detail.Text += sql_reader2.GetString(14) + " ... " + sql_reader2.GetString(15) + "\r\n\r\n";

                        }

                        CustNews_txt_detail.Text += "Datum: " + cn_datum + "\r\n";
                        CustNews_txt_detail.Text += "�as: " + sql_reader2.GetString(4) + "\r\n";
                        
                        CustNews_txt_detail.Text += "-------------------------------------------------------\r\n";
                        CustNews_txt_detail.Text += VersionEditHistory;
                        CustNews_txt_detail.Text += "Tato verze: " + sql_reader2.GetString(16); // vlozil
                        CustNews_txt_detail.Text += " (" + datum_vlozil + " " + sql_reader2.GetString(18) + ")";
                    }
                    sql_reader2.Close();
                }
                catch (Exception ex)
                {
                    LogDebug(ex.ToString(), "ERROR");
                    ctk_status_label.Text = "Nen� nav�z�no p�ipojen� k MySQL datab�zi.";
                    MYSQL.Close();
                    Application.DoEvents();
                    MySQL_connect();
                }
            }
            else
            {
                CustNews_txt_detail.Text = "";
            }
            ChangeStatusBar("P�ipraven", false);
        }

        private void CustNews_list_MarkReaded()
        {
            try
            {
                dg_CustNews_list.Rows[dg_CustNews_list.CurrentRow.Index].DefaultCellStyle.ForeColor = UIcolors[0];
                dg_CustNews_list.Rows[dg_CustNews_list.CurrentRow.Index].DefaultCellStyle.BackColor = UIcolors[1];

                int IdxVal = Convert.ToInt32(dg_CustNews_list.Rows[dg_CustNews_list.CurrentRow.Index].Cells[0].Value.ToString());
                object obj1 = IdxVal;
                int IntRet = Array.IndexOf(CustNews_list_Readed, obj1);
                if (IntRet < 0)
                {
                    CustNews_list_Readed[CustNews_list_Readed_idx] = IdxVal;
                    CustNews_list_Readed_idx++;
                    if (CustNews_list_Readed_idx >= CustNews_list_Readed.Length) CustNews_list_Readed_idx = 0;
                }

            }
            catch
            {
            }
        }

        private void CustNews_list_MarkUnReaded(int RowId, string IdxVal)
        {
            object obj1 = Convert.ToInt32(IdxVal);
            int IntRet = Array.IndexOf(CustNews_list_Readed, obj1);
            if (IntRet < 0)
            {
                dg_CustNews_list.Rows[RowId].DefaultCellStyle.ForeColor = UIcolors[2];
                dg_CustNews_list.Rows[RowId].DefaultCellStyle.BackColor = UIcolors[3];
            }
            else
            {
                dg_CustNews_list.Rows[RowId].DefaultCellStyle.ForeColor = UIcolors[0];
                dg_CustNews_list.Rows[RowId].DefaultCellStyle.BackColor = UIcolors[1];
            }
        }


        private void dg_CustNews_list_SelectionChanged(object sender, EventArgs e)
        {
            LogDebug("dg_CustNews_list_SelectionChanged", "USER");
            CustNews_LoadDetail();
        }

        private void btn_CustNews_sel_set_Click(object sender, EventArgs e)
        {
            LogDebug("btn_CustNews_sel_set_Click()", "USER");
            CustNews_dg_Load_List();
        }

        private void btn_CustNews_sel_drop_Click(object sender, EventArgs e)
        {
            LogDebug("btn_CustNews_sel_drop_Click()", "USER");

            if (CustNews_sel_group.Items.Count > 0) CustNews_sel_group.SelectedIndex = 0;
            if (CustNews_sel_oblast.Items.Count > 0) CustNews_sel_oblast.SelectedIndex = 0;
            if (CustNews_sel_kategorie.Items.Count > 0) CustNews_sel_kategorie.SelectedIndex = 0;
            CustNews_sel_text.Text = "";
            
            CustNews_dg_Load_List();
        }

        private void btn_CustNews_new_Click(object sender, EventArgs e)
        {
            LogDebug("btn_CustNews_new_Click()", "USER");
            CustNews_new_record wnd = new CustNews_new_record(this);
            wnd.Show();
        }

        private void btn_CustNews_edit_Click(object sender, EventArgs e)
        {
            LogDebug("btn_CustNews_edit_Click()", "USER");
            CustNews_new_record wnd = new CustNews_new_record(this);
            wnd.EditMode = true;
            wnd.EditMode_id = dg_CustNews_list.Rows[dg_CustNews_list.CurrentRow.Index].Cells[0].Value.ToString();
            wnd.Show();
        }

        private void btn_CustNews_showAll_Click(object sender, EventArgs e)
        {
            CustNews_dg_Load_List(false);
        }
    }
}