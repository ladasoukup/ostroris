/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 18.7.2005
 * Time: 22:35
 * 
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
	/// <summary>
	/// Description of about.
	/// </summary>
    public class about : KryptonForm
    {
        private PictureBox pictureBox1;
        private Label lbl_appversion;
		private System.Windows.Forms.TextBox txt_license;
		public about()
		{
			// The InitializeComponent() call is required for Windows Forms designer support.
			InitializeComponent();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(about));
            this.txt_license = new System.Windows.Forms.TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_appversion = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // txt_license
            // 
            this.txt_license.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(177)))), ((int)(((byte)(211)))), ((int)(((byte)(255)))));
            this.txt_license.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_license.ForeColor = System.Drawing.Color.Black;
            this.txt_license.Location = new System.Drawing.Point(-1, 199);
            this.txt_license.Multiline = true;
            this.txt_license.Name = "txt_license";
            this.txt_license.ReadOnly = true;
            this.txt_license.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_license.Size = new System.Drawing.Size(500, 191);
            this.txt_license.TabIndex = 0;
            this.txt_license.Text = "textBox1";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CTK_reader.Properties.Resources.OstroRis_logo;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // lbl_appversion
            // 
            this.lbl_appversion.BackColor = System.Drawing.Color.Black;
            this.lbl_appversion.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_appversion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(127)))), ((int)(((byte)(62)))));
            this.lbl_appversion.Location = new System.Drawing.Point(12, 69);
            this.lbl_appversion.Name = "lbl_appversion";
            this.lbl_appversion.Size = new System.Drawing.Size(241, 26);
            this.lbl_appversion.TabIndex = 5;
            this.lbl_appversion.Text = "<VERSION>";
            // 
            // about
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(499, 390);
            this.Controls.Add(this.lbl_appversion);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txt_license);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "about";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "O aplikaci";
            this.Load += new System.EventHandler(this.AboutLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		
	
		void Btn_okClick(object sender, System.EventArgs e)
		{
			this.Close();
		}
		
		void AboutLoad(object sender, System.EventArgs e)
		{
			lbl_appversion.Text = "Verze: " + Application.ProductVersion.ToString();
            txt_license.Text = Properties.Resources.license;
            txt_license.Select(0, 0);
		}
		
	}
}
