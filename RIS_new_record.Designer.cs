namespace CTK_reader
{
    partial class RIS_new_record
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RIS_new_record));
            this.btn_vlozit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.e_kategorie = new System.Windows.Forms.ComboBox();
            this.RIS_new_header_misto = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_okres = new System.Windows.Forms.ComboBox();
            this.e_oblast = new System.Windows.Forms.ComboBox();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_misto = new System.Windows.Forms.TextBox();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_mesto = new System.Windows.Forms.TextBox();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.RIS_new_header = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.btn_MasterEditMode = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.btn_delete = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.RIS_header_popis = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_groups = new System.Windows.Forms.CheckedListBox();
            this.e_popis = new System.Windows.Forms.TextBox();
            this.RIS_header_title = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_promo_vysilani = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.e_nazev = new System.Windows.Forms.TextBox();
            this.e_medialni_partner = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.RIS_header_datum = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_datum_do = new System.Windows.Forms.DateTimePicker();
            this.e_datum_od = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.btn_vlozit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header_misto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header_misto.Panel)).BeginInit();
            this.RIS_new_header_misto.Panel.SuspendLayout();
            this.RIS_new_header_misto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header.Panel)).BeginInit();
            this.RIS_new_header.Panel.SuspendLayout();
            this.RIS_new_header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_MasterEditMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_popis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_popis.Panel)).BeginInit();
            this.RIS_header_popis.Panel.SuspendLayout();
            this.RIS_header_popis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title.Panel)).BeginInit();
            this.RIS_header_title.Panel.SuspendLayout();
            this.RIS_header_title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.e_promo_vysilani)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_medialni_partner)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_datum)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_datum.Panel)).BeginInit();
            this.RIS_header_datum.Panel.SuspendLayout();
            this.RIS_header_datum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_vlozit
            // 
            this.btn_vlozit.DirtyPaletteCounter = 4;
            this.btn_vlozit.Location = new System.Drawing.Point(524, 467);
            this.btn_vlozit.Name = "btn_vlozit";
            this.btn_vlozit.Size = new System.Drawing.Size(100, 25);
            this.btn_vlozit.TabIndex = 50;
            this.btn_vlozit.Text = "Vlo�it";
            this.btn_vlozit.Values.ExtraText = "";
            this.btn_vlozit.Values.Image = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_vlozit.Values.Text = "Vlo�it";
            this.btn_vlozit.Click += new System.EventHandler(this.btn_vlozit_Click);
            // 
            // e_kategorie
            // 
            this.e_kategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.e_kategorie.FormattingEnabled = true;
            this.e_kategorie.Location = new System.Drawing.Point(85, 39);
            this.e_kategorie.Name = "e_kategorie";
            this.e_kategorie.Size = new System.Drawing.Size(222, 21);
            this.e_kategorie.TabIndex = 32;
            this.e_kategorie.SelectedIndexChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // RIS_new_header_misto
            // 
            this.RIS_new_header_misto.DirtyPaletteCounter = 4;
            this.RIS_new_header_misto.Dock = System.Windows.Forms.DockStyle.Top;
            this.RIS_new_header_misto.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_new_header_misto.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.RIS_new_header_misto.HeaderVisibleSecondary = false;
            this.RIS_new_header_misto.Location = new System.Drawing.Point(0, 0);
            this.RIS_new_header_misto.Name = "RIS_new_header_misto";
            // 
            // RIS_new_header_misto.Panel
            // 
            this.RIS_new_header_misto.Panel.Controls.Add(this.e_okres);
            this.RIS_new_header_misto.Panel.Controls.Add(this.e_oblast);
            this.RIS_new_header_misto.Panel.Controls.Add(this.kryptonLabel8);
            this.RIS_new_header_misto.Panel.Controls.Add(this.e_misto);
            this.RIS_new_header_misto.Panel.Controls.Add(this.kryptonLabel7);
            this.RIS_new_header_misto.Panel.Controls.Add(this.kryptonLabel5);
            this.RIS_new_header_misto.Panel.Controls.Add(this.e_mesto);
            this.RIS_new_header_misto.Panel.Controls.Add(this.kryptonLabel6);
            this.RIS_new_header_misto.Size = new System.Drawing.Size(628, 91);
            this.RIS_new_header_misto.TabIndex = 10;
            this.RIS_new_header_misto.Text = "M�sto kon�n�";
            this.RIS_new_header_misto.ValuesPrimary.Description = "";
            this.RIS_new_header_misto.ValuesPrimary.Heading = "M�sto kon�n�";
            this.RIS_new_header_misto.ValuesPrimary.Image = null;
            this.RIS_new_header_misto.ValuesSecondary.Description = "";
            this.RIS_new_header_misto.ValuesSecondary.Heading = "Description";
            this.RIS_new_header_misto.ValuesSecondary.Image = null;
            // 
            // e_okres
            // 
            this.e_okres.FormattingEnabled = true;
            this.e_okres.Items.AddRange(new object[] {
            "-zvolte-"});
            this.e_okres.Location = new System.Drawing.Point(363, 11);
            this.e_okres.Name = "e_okres";
            this.e_okres.Size = new System.Drawing.Size(250, 21);
            this.e_okres.TabIndex = 12;
            this.e_okres.Text = "-zvolte-";
            this.e_okres.SelectedIndexChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_oblast
            // 
            this.e_oblast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.e_oblast.FormattingEnabled = true;
            this.e_oblast.Items.AddRange(new object[] {
            "-zvolte-"});
            this.e_oblast.Location = new System.Drawing.Point(57, 11);
            this.e_oblast.Name = "e_oblast";
            this.e_oblast.Size = new System.Drawing.Size(250, 21);
            this.e_oblast.TabIndex = 11;
            this.e_oblast.SelectedIndexChanged += new System.EventHandler(this.e_oblast_SelectedIndexChanged);
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.DirtyPaletteCounter = 4;
            this.kryptonLabel8.Location = new System.Drawing.Point(7, 11);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.Size = new System.Drawing.Size(48, 20);
            this.kryptonLabel8.TabIndex = 14;
            this.kryptonLabel8.Text = "Oblast:";
            this.kryptonLabel8.Values.ExtraText = "";
            this.kryptonLabel8.Values.Image = null;
            this.kryptonLabel8.Values.Text = "Oblast:";
            // 
            // e_misto
            // 
            this.e_misto.Location = new System.Drawing.Point(363, 38);
            this.e_misto.Name = "e_misto";
            this.e_misto.Size = new System.Drawing.Size(250, 21);
            this.e_misto.TabIndex = 14;
            this.e_misto.TextChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.DirtyPaletteCounter = 4;
            this.kryptonLabel7.Location = new System.Drawing.Point(316, 11);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.Size = new System.Drawing.Size(45, 20);
            this.kryptonLabel7.TabIndex = 7;
            this.kryptonLabel7.Text = "Okres:";
            this.kryptonLabel7.Values.ExtraText = "";
            this.kryptonLabel7.Values.Image = null;
            this.kryptonLabel7.Values.Text = "Okres:";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.DirtyPaletteCounter = 4;
            this.kryptonLabel5.Location = new System.Drawing.Point(316, 38);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel5.TabIndex = 11;
            this.kryptonLabel5.Text = "M�sto:";
            this.kryptonLabel5.Values.ExtraText = "";
            this.kryptonLabel5.Values.Image = null;
            this.kryptonLabel5.Values.Text = "M�sto:";
            // 
            // e_mesto
            // 
            this.e_mesto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.e_mesto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.e_mesto.Location = new System.Drawing.Point(57, 38);
            this.e_mesto.Name = "e_mesto";
            this.e_mesto.Size = new System.Drawing.Size(250, 21);
            this.e_mesto.TabIndex = 13;
            this.e_mesto.TextChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.DirtyPaletteCounter = 4;
            this.kryptonLabel6.Location = new System.Drawing.Point(7, 38);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(47, 20);
            this.kryptonLabel6.TabIndex = 9;
            this.kryptonLabel6.Text = "M�sto:";
            this.kryptonLabel6.Values.ExtraText = "";
            this.kryptonLabel6.Values.Image = null;
            this.kryptonLabel6.Values.Text = "M�sto:";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.DirtyPaletteCounter = 4;
            this.kryptonLabel1.Location = new System.Drawing.Point(18, 41);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(66, 20);
            this.kryptonLabel1.TabIndex = 6;
            this.kryptonLabel1.Text = "Kategorie:";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "Kategorie:";
            // 
            // RIS_new_header
            // 
            this.RIS_new_header.DirtyPaletteCounter = 4;
            this.RIS_new_header.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_new_header.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_new_header.Location = new System.Drawing.Point(0, 0);
            this.RIS_new_header.Name = "RIS_new_header";
            // 
            // RIS_new_header.Panel
            // 
            this.RIS_new_header.Panel.Controls.Add(this.btn_MasterEditMode);
            this.RIS_new_header.Panel.Controls.Add(this.btn_delete);
            this.RIS_new_header.Panel.Controls.Add(this.RIS_header_popis);
            this.RIS_new_header.Panel.Controls.Add(this.RIS_header_title);
            this.RIS_new_header.Panel.Controls.Add(this.btn_vlozit);
            this.RIS_new_header.Panel.Controls.Add(this.RIS_header_datum);
            this.RIS_new_header.Panel.Controls.Add(this.RIS_new_header_misto);
            this.RIS_new_header.Size = new System.Drawing.Size(630, 551);
            this.RIS_new_header.TabIndex = 6;
            this.RIS_new_header.Text = "Vlo�it nov� z�znam";
            this.RIS_new_header.ValuesPrimary.Description = "RIS";
            this.RIS_new_header.ValuesPrimary.Heading = "Vlo�it nov� z�znam";
            this.RIS_new_header.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.copy;
            this.RIS_new_header.ValuesSecondary.Description = "kl��ov� slova";
            this.RIS_new_header.ValuesSecondary.Heading = "";
            this.RIS_new_header.ValuesSecondary.Image = null;
            // 
            // btn_MasterEditMode
            // 
            this.btn_MasterEditMode.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Cluster;
            this.btn_MasterEditMode.DirtyPaletteCounter = 7;
            this.btn_MasterEditMode.Location = new System.Drawing.Point(8, 467);
            this.btn_MasterEditMode.Name = "btn_MasterEditMode";
            this.btn_MasterEditMode.Size = new System.Drawing.Size(157, 25);
            this.btn_MasterEditMode.TabIndex = 52;
            this.btn_MasterEditMode.Text = "Administr�torsk� m�d";
            this.btn_MasterEditMode.Values.ExtraText = "";
            this.btn_MasterEditMode.Values.Image = null;
            this.btn_MasterEditMode.Values.Text = "Administr�torsk� m�d";
            this.btn_MasterEditMode.Click += new System.EventHandler(this.btn_MasterEditMode_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.DirtyPaletteCounter = 4;
            this.btn_delete.Enabled = false;
            this.btn_delete.Location = new System.Drawing.Point(374, 467);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(100, 25);
            this.btn_delete.TabIndex = 51;
            this.btn_delete.Text = "Zru�eno";
            this.btn_delete.Values.ExtraText = "";
            this.btn_delete.Values.Image = null;
            this.btn_delete.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_delete.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_delete.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_delete.Values.Text = "Zru�eno";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // RIS_header_popis
            // 
            this.RIS_header_popis.DirtyPaletteCounter = 4;
            this.RIS_header_popis.Dock = System.Windows.Forms.DockStyle.Top;
            this.RIS_header_popis.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_header_popis.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.RIS_header_popis.HeaderVisibleSecondary = false;
            this.RIS_header_popis.Location = new System.Drawing.Point(0, 253);
            this.RIS_header_popis.Name = "RIS_header_popis";
            // 
            // RIS_header_popis.Panel
            // 
            this.RIS_header_popis.Panel.Controls.Add(this.e_groups);
            this.RIS_header_popis.Panel.Controls.Add(this.e_popis);
            this.RIS_header_popis.Size = new System.Drawing.Size(628, 208);
            this.RIS_header_popis.TabIndex = 40;
            this.RIS_header_popis.Text = "Popis akce";
            this.RIS_header_popis.ValuesPrimary.Description = "Okruhy";
            this.RIS_header_popis.ValuesPrimary.Heading = "Popis akce";
            this.RIS_header_popis.ValuesPrimary.Image = null;
            this.RIS_header_popis.ValuesSecondary.Description = "";
            this.RIS_header_popis.ValuesSecondary.Heading = "Description";
            this.RIS_header_popis.ValuesSecondary.Image = null;
            // 
            // e_groups
            // 
            this.e_groups.CheckOnClick = true;
            this.e_groups.FormattingEnabled = true;
            this.e_groups.Location = new System.Drawing.Point(479, 3);
            this.e_groups.Name = "e_groups";
            this.e_groups.Size = new System.Drawing.Size(144, 180);
            this.e_groups.TabIndex = 12;
            this.e_groups.Click += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_popis
            // 
            this.e_popis.Location = new System.Drawing.Point(2, 3);
            this.e_popis.Multiline = true;
            this.e_popis.Name = "e_popis";
            this.e_popis.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.e_popis.Size = new System.Drawing.Size(471, 180);
            this.e_popis.TabIndex = 11;
            // 
            // RIS_header_title
            // 
            this.RIS_header_title.DirtyPaletteCounter = 4;
            this.RIS_header_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.RIS_header_title.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_header_title.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.RIS_header_title.HeaderVisibleSecondary = false;
            this.RIS_header_title.Location = new System.Drawing.Point(0, 159);
            this.RIS_header_title.Name = "RIS_header_title";
            // 
            // RIS_header_title.Panel
            // 
            this.RIS_header_title.Panel.Controls.Add(this.e_promo_vysilani);
            this.RIS_header_title.Panel.Controls.Add(this.e_nazev);
            this.RIS_header_title.Panel.Controls.Add(this.e_medialni_partner);
            this.RIS_header_title.Panel.Controls.Add(this.kryptonLabel4);
            this.RIS_header_title.Panel.Controls.Add(this.kryptonLabel1);
            this.RIS_header_title.Panel.Controls.Add(this.e_kategorie);
            this.RIS_header_title.Size = new System.Drawing.Size(628, 94);
            this.RIS_header_title.TabIndex = 30;
            this.RIS_header_title.Text = "Informace o akci";
            this.RIS_header_title.ValuesPrimary.Description = "";
            this.RIS_header_title.ValuesPrimary.Heading = "Informace o akci";
            this.RIS_header_title.ValuesPrimary.Image = null;
            this.RIS_header_title.ValuesSecondary.Description = "";
            this.RIS_header_title.ValuesSecondary.Heading = "Description";
            this.RIS_header_title.ValuesSecondary.Image = null;
            // 
            // e_promo_vysilani
            // 
            this.e_promo_vysilani.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.NavigatorStack;
            this.e_promo_vysilani.DirtyPaletteCounter = 4;
            this.e_promo_vysilani.Location = new System.Drawing.Point(500, 39);
            this.e_promo_vysilani.Name = "e_promo_vysilani";
            this.e_promo_vysilani.Size = new System.Drawing.Size(115, 22);
            this.e_promo_vysilani.TabIndex = 35;
            this.e_promo_vysilani.Text = "Promo ve vys�l�n�";
            this.e_promo_vysilani.Values.ExtraText = "";
            this.e_promo_vysilani.Values.Image = null;
            this.e_promo_vysilani.Values.Text = "Promo ve vys�l�n�";
            this.e_promo_vysilani.Click += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_nazev
            // 
            this.e_nazev.Location = new System.Drawing.Point(85, 12);
            this.e_nazev.Name = "e_nazev";
            this.e_nazev.Size = new System.Drawing.Size(531, 21);
            this.e_nazev.TabIndex = 31;
            this.e_nazev.TextChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_medialni_partner
            // 
            this.e_medialni_partner.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.NavigatorStack;
            this.e_medialni_partner.DirtyPaletteCounter = 4;
            this.e_medialni_partner.Location = new System.Drawing.Point(379, 39);
            this.e_medialni_partner.Name = "e_medialni_partner";
            this.e_medialni_partner.Size = new System.Drawing.Size(115, 22);
            this.e_medialni_partner.TabIndex = 34;
            this.e_medialni_partner.Text = "Medi�ln� partner";
            this.e_medialni_partner.Values.ExtraText = "";
            this.e_medialni_partner.Values.Image = null;
            this.e_medialni_partner.Values.Text = "Medi�ln� partner";
            this.e_medialni_partner.Click += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.DirtyPaletteCounter = 4;
            this.kryptonLabel4.Location = new System.Drawing.Point(10, 14);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(75, 20);
            this.kryptonLabel4.TabIndex = 0;
            this.kryptonLabel4.Text = "N�zev akce:";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "N�zev akce:";
            // 
            // RIS_header_datum
            // 
            this.RIS_header_datum.DirtyPaletteCounter = 4;
            this.RIS_header_datum.Dock = System.Windows.Forms.DockStyle.Top;
            this.RIS_header_datum.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_header_datum.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.RIS_header_datum.HeaderVisibleSecondary = false;
            this.RIS_header_datum.Location = new System.Drawing.Point(0, 91);
            this.RIS_header_datum.Name = "RIS_header_datum";
            // 
            // RIS_header_datum.Panel
            // 
            this.RIS_header_datum.Panel.Controls.Add(this.kryptonLabel3);
            this.RIS_header_datum.Panel.Controls.Add(this.kryptonLabel2);
            this.RIS_header_datum.Panel.Controls.Add(this.e_datum_do);
            this.RIS_header_datum.Panel.Controls.Add(this.e_datum_od);
            this.RIS_header_datum.Size = new System.Drawing.Size(628, 68);
            this.RIS_header_datum.TabIndex = 20;
            this.RIS_header_datum.Text = "Datum kon�n�";
            this.RIS_header_datum.ValuesPrimary.Description = "";
            this.RIS_header_datum.ValuesPrimary.Heading = "Datum kon�n�";
            this.RIS_header_datum.ValuesPrimary.Image = null;
            this.RIS_header_datum.ValuesSecondary.Description = "";
            this.RIS_header_datum.ValuesSecondary.Heading = "Description";
            this.RIS_header_datum.ValuesSecondary.Image = null;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.DirtyPaletteCounter = 4;
            this.kryptonLabel3.Location = new System.Drawing.Point(283, 15);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(114, 20);
            this.kryptonLabel3.TabIndex = 14;
            this.kryptonLabel3.Text = "Datum konce akce:";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "Datum konce akce:";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.DirtyPaletteCounter = 4;
            this.kryptonLabel2.Location = new System.Drawing.Point(10, 15);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(123, 20);
            this.kryptonLabel2.TabIndex = 13;
            this.kryptonLabel2.Text = "Datum za��tku akce:";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "Datum za��tku akce:";
            // 
            // e_datum_do
            // 
            this.e_datum_do.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.e_datum_do.Location = new System.Drawing.Point(403, 14);
            this.e_datum_do.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.e_datum_do.Name = "e_datum_do";
            this.e_datum_do.Size = new System.Drawing.Size(134, 21);
            this.e_datum_do.TabIndex = 22;
            this.e_datum_do.ValueChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_datum_od
            // 
            this.e_datum_od.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.e_datum_od.Location = new System.Drawing.Point(139, 14);
            this.e_datum_od.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.e_datum_od.Name = "e_datum_od";
            this.e_datum_od.Size = new System.Drawing.Size(129, 21);
            this.e_datum_od.TabIndex = 21;
            this.e_datum_od.ValueChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // RIS_new_record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(630, 551);
            this.Controls.Add(this.RIS_new_header);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RIS_new_record";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RIS - Region�ln� Informa�n� Syst�m";
            this.Load += new System.EventHandler(this.RIS_new_record_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_vlozit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header_misto.Panel)).EndInit();
            this.RIS_new_header_misto.Panel.ResumeLayout(false);
            this.RIS_new_header_misto.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header_misto)).EndInit();
            this.RIS_new_header_misto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header.Panel)).EndInit();
            this.RIS_new_header.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_new_header)).EndInit();
            this.RIS_new_header.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_MasterEditMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_popis.Panel)).EndInit();
            this.RIS_header_popis.Panel.ResumeLayout(false);
            this.RIS_header_popis.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_popis)).EndInit();
            this.RIS_header_popis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title.Panel)).EndInit();
            this.RIS_header_title.Panel.ResumeLayout(false);
            this.RIS_header_title.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title)).EndInit();
            this.RIS_header_title.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.e_promo_vysilani)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.e_medialni_partner)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_datum.Panel)).EndInit();
            this.RIS_header_datum.Panel.ResumeLayout(false);
            this.RIS_header_datum.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_datum)).EndInit();
            this.RIS_header_datum.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_vlozit;
        private System.Windows.Forms.ComboBox e_kategorie;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_header_datum;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_new_header_misto;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_new_header;
        private System.Windows.Forms.TextBox e_misto;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private System.Windows.Forms.TextBox e_mesto;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private System.Windows.Forms.DateTimePicker e_datum_do;
        private System.Windows.Forms.DateTimePicker e_datum_od;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_header_title;
        private System.Windows.Forms.TextBox e_nazev;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton e_medialni_partner;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_header_popis;
        private System.Windows.Forms.TextBox e_popis;
        private System.Windows.Forms.CheckedListBox e_groups;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        private System.Windows.Forms.ComboBox e_oblast;
        private System.Windows.Forms.ComboBox e_okres;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton e_promo_vysilani;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_delete;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton btn_MasterEditMode;
        


    }
}