using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using MySQLDriverCS;

namespace CTK_reader
{
    //public partial class CustNews_new_record : Form
    public partial class CustNews_new_record : KryptonForm
    {
        MainForm opener;
        public bool EditMode = false;
        public bool MasterEditMode = false;
        public string EditMode_id = "0";
        private string baseID = "";
        private int e_nazev_VER = 0;
        private int e_nazev_VER_new = 1;
        private string edit_base_nazev = "";
        private string edit_base_vlozil = "";
        
        public CustNews_new_record(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void CustNews_new_record_Load(object sender, EventArgs e)
        {
            string ctk_sql = "";
            btn_delete.Visible = false;
            btn_MasterEditMode.Visible = false;

            e_embargo_do.Value = DateTime.Now;

            // kategorie
            e_kategorie.Items.Clear();
            e_kategorie.Items.Add("-zvolte-");
            for (int loop = 1; loop < opener.CustNews_sel_kategorie.Items.Count; loop++)
            {
                e_kategorie.Items.Add(opener.CustNews_sel_kategorie.Items[loop]);
            }
            try { e_kategorie.SelectedIndex = 0; }
            catch { }

            // okruhy
            e_groups.Items.Clear();
            for (int loop = 1; loop < opener.CustNews_sel_group.Items.Count; loop++)
            {
                e_groups.Items.Add(opener.CustNews_sel_group.Items[loop]);
            }

            // oblasti
            e_oblast.Items.Clear();
            e_oblast.Items.Add("-zvolte-");
            for (int loop = 1; loop < opener.CustNews_sel_oblast.Items.Count; loop++)
            {
                e_oblast.Items.Add(opener.CustNews_sel_oblast.Items[loop]);
            }
            e_oblast.Items.Add("-jin�-");
            try { e_oblast.SelectedIndex = 0; }
            catch { }

            // mesto - AutoComplete
            ctk_sql = "SELECT DISTINCT cn_mesto FROM " + opener.sql_db_table + "_custnews ORDER BY cn_mesto ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                e_mesto.AutoCompleteCustomSource.Clear();
                while (ctk_reader.Read())
                {
                    e_mesto.AutoCompleteCustomSource.Add(ctk_reader[0].ToString());
                }
            }
            catch { }

            try
            {
                if (EditMode) CustNews_new_record_Load_editmode();
            }
            catch (Exception ex)
            {
                opener.LogDebug(ex.ToString(), "Error");
                MessageBox.Show("Do�lo k nezn�m� chyb�\r\nEditace nem��e b�t provedena.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }


        private bool CustNews_edit_checkRights()
        {
            bool ret = false;
            /*
            string sql_query = "";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            sql_query = "SELECT DISTINCT(vlozil) FROM " + opener.sql_db_table + "_custnews WHERE (id=" + EditMode_id + " OR base_id=" + EditMode_id + ")";
            sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
            sql_reader = sql_cmd.ExecuteReaderEx();
            opener.LogDebug(sql_query, "SQL");
            while (sql_reader.Read())
            {
                if (sql_reader.GetString(0).ToString() == opener.ActualProfile)
                {
                    ret = true;
                }
            }
            if (ret == false) ret = CustNews_edit_checkMasterRights();
            */

            ret = true;
            return ret;
        }

        private bool CustNews_edit_checkMasterRights()
        {
            bool ret = false;
            string MasterEdit = opener.ini.IniReadEncValue("CustNews", "MasterEdit");
            string[] A;

            if (MasterEdit != null)
            {
                A = MasterEdit.Split(',');
                foreach (string MasterEditor in A)
                {
                    if (MasterEditor == opener.ActualProfile)
                    {
                        ret = true;
                    }

                    if (MasterEditor == "***ALL***")
                    {
                        ret = true;
                    }
                }
            }
            return ret;
        }


        private void CustNews_new_record_Load_editmode()
        {
            bool HaltEditor = false;
            string S;
            string[] A;
            string sql_query = "";
            string sql_date = "00000000";
            string sql_time = "00:00:00";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            // set values and blosk fields...
            //e_groups.Enabled = false;
            e_kategorie.Enabled = false;
            e_mesto.Enabled = false;
            e_nazev.Enabled = false;
            e_oblast.Enabled = false;
            e_okres.Enabled = false;
            btn_delete.Enabled = true;
            btn_delete.Visible = true;
            group_custnews_main.ValuesPrimary.Heading = "Editace zpr�vy";

            bool CanEdit = CustNews_edit_checkRights();

            if (CanEdit == false)
            {
                MessageBox.Show("Pro editaci t�to zpr�vy nem�te dostate�n� pr�va.\r\nB�n� u�ivatel m��e editovat jen sv� vlastn� zpr�vy.", "Nem�te pr�va editovat tuto zpr�vu", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.DoEvents();
                this.Close();
            }
            else
            {
                if (CustNews_edit_checkMasterRights()) { btn_MasterEditMode.Visible = true; }

                sql_query = "SELECT base_id FROM " + opener.sql_db_table + "_custnews WHERE id=" + EditMode_id + " LIMIT 1";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                opener.LogDebug(sql_query, "SQL");
                while (sql_reader.Read())
                {
                    try { baseID = sql_reader.GetInt32(0).ToString(); }
                    catch { baseID = EditMode_id; }
                }
                sql_query = "SELECT * FROM " + opener.sql_db_table + "_custnews WHERE base_id=" + baseID + " ORDER BY verze DESC LIMIT 1";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                opener.LogDebug(sql_query, "SQL");

                while (sql_reader.Read())
                {
                    try { e_nazev_VER = sql_reader.GetInt32(20); }
                    catch { e_nazev_VER = 1; }
                    e_nazev_VER_new = e_nazev_VER + 1;

                    if (e_nazev_VER_new > 9999)
                    {
                        if (CustNews_edit_checkMasterRights() == true)
                        {
                            MessageBox.Show("Pr�v� editujete zru�enou zpr�vu.\r\nAby byla editace �sp�n�, je vhodn� se p�epnout do m�du Administr�tora!", "Editace zru�en� zpr�vy", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            HaltEditor = false;
                        }
                        else
                        {
                            // AKCE ZRUSENA
                            MessageBox.Show("Tuto zpr�vu nem��ete editovat - zpr�va byla zru�ena!", "Akce zru�ena", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            HaltEditor = true;
                            this.Close();
                        }
                    }

                    if (HaltEditor == false)
                    {
                        // e_groups -1-
                        S = sql_reader.GetString(1);
                        A = S.Split('*');
                        int I = e_groups.Items.Count;
                        for (int loop = 0; loop < I; loop++)
                        {
                            foreach (string item in A)
                            {
                                if (item == e_groups.Items[loop].ToString()) e_groups.SetItemChecked(loop, true);
                            }
                        }

                        S = sql_reader.GetString(5);
                        e_oblast.SelectedItem = S;
                        if (e_oblast.SelectedItem.ToString() != S) e_oblast.Enabled = true;

                        if (e_oblast.SelectedItem.ToString() != "-jin�-")
                        {
                            S = sql_reader.GetString(6);
                            e_okres.SelectedItem = S;
                            if (e_okres.SelectedItem.ToString() != S)
                            {
                                e_okres.Items.Add(S);
                                e_okres.SelectedItem = S;
                            }
                        }
                        else
                        {
                            S = sql_reader.GetString(6);
                            e_okres.Items.Add(S);
                            e_okres.SelectedItem = S;
                            if (e_okres.SelectedItem.ToString() != S)
                            {
                                e_okres.Items.Add(S);
                                e_okres.SelectedItem = S;
                            }
                        }

                        e_mesto.Text = sql_reader.GetString(7);

                        S = sql_reader.GetString(8);
                        e_kategorie.SelectedItem = S;
                        if (e_kategorie.SelectedItem.ToString() != S) e_kategorie.Enabled = true;

                        S = sql_reader.GetString(9);
                        A = S.Split('|');
                        edit_base_nazev = A[0].Trim();
                        S = edit_base_nazev + " | " + e_nazev_VER_new + ". verze";
                        e_nazev.Text = S;

                        e_popis.Text = sql_reader.GetString(10);

                        // embargovat do...
                        sql_date = sql_reader.GetString(3);
                        sql_time = sql_reader.GetString(4);
                        try
                        {
                            e_embargo_do.Value = new DateTime(Convert.ToInt32(sql_date.Substring(0, 4)), Convert.ToInt32(sql_date.Substring(4, 2)), Convert.ToInt32(sql_date.Substring(6, 2)), Convert.ToInt32(sql_time.Substring(0, 2)), Convert.ToInt32(sql_time.Substring(3, 2)), Convert.ToInt32(sql_time.Substring(6, 2)));
                        }
                        catch { }
                        e_id_repa.Text = sql_reader.GetString(12);
                        e_delka_repa.Text = sql_reader.GetString(13);
                        e_zacatek_repa.Text = sql_reader.GetString(14);
                        e_konec_repa.Text = sql_reader.GetString(15);

                        edit_base_vlozil = sql_reader.GetString(16);
                    }
                }
                sql_reader.Close();
            }
        }



        private void e_oblast_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ctk_sql;
            ctk_sql = "SELECT DISTINCT okres FROM " + opener.sql_db_table + "_custnews_oblasti WHERE oblast='" + e_oblast.Text + "' ORDER BY okres ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                e_okres.Items.Clear();
                e_okres.Items.Add("-zvolte-");
                while (ctk_reader.Read())
                {
                    e_okres.Items.Add(ctk_reader[0].ToString());
                }
                try { e_okres.SelectedIndex = 0; }
                catch { }
            }
            catch { }
            Keywords_create();
        }

        private void data_changed__rebuild_keywords(object sender, EventArgs e)
        {
            if (e_embargo_do.Value < DateTime.Now) e_embargo_do.Value = DateTime.Now;
            
            Keywords_create();
        }

        private string Keywords_create()
        {
            string ret = "";
            string S_nazev = "";

            S_nazev = e_nazev.Text;
            string[] A = S_nazev.Split('|');
            S_nazev = A[0].Trim();

            ret = e_oblast.Text + ", " + e_mesto.Text + ", " + e_kategorie.Text + ", " + S_nazev;

            group_custnews_main.ValuesSecondary.Heading = ret;
            return ret;
        }


        private int CheckValues()
        {
            int ret = 0;
            string keywords = Keywords_create();
            string sql_query = "";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            e_groups.BackColor = Color.White;
            e_popis.BackColor = Color.White;
            e_kategorie.BackColor = Color.White;
            e_nazev.BackColor = Color.White;
            e_mesto.BackColor = Color.White;
            e_okres.BackColor = Color.White;
            e_oblast.BackColor = Color.White;

            if (e_groups.CheckedItems.Count < 1) ret = 8;
            if (e_popis.Text == "") ret = 7;
            if (e_kategorie.Text == "-zvolte-") ret = 6;
            if (e_nazev.Text == "") ret = 5;
            if (e_mesto.Text == "") ret = 3;
            if (e_okres.Text == "-zvolte-") ret = 2;
            if (e_oblast.Text == "-zvolte-") ret = 1;


            if (EditMode == false)
            {
                // check KeyWords
                sql_query = "SELECT DISTINCT(cn_klicova_slova) FROM " + opener.sql_db_table + "_custnews WHERE (id!=" + EditMode_id + " OR base_id!=" + EditMode_id + ")";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                opener.LogDebug(sql_query, "SQL");
                while (sql_reader.Read())
                {
                    if (keywords == sql_reader.GetString(0))
                    {
                        DialogResult question = MessageBox.Show("V datab�zi byla nalezena zpr�va, kter� m� stejn� kl��ov� slova!\r\nMohlo by se jednat o tu samou zpr�vu.\r\n\r\nOpravdu chcete zpr�vu vlo�it do datab�ze?", "Duplicita", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (question == DialogResult.No) ret = 10;
                    }
                }
            }

            return ret;
        }

        private void CustNews_new_ShowCheckErrorMsg(int err_code)
        {
            string ErrorString = "";
            switch (err_code)
            {
                case 1:
                    ErrorString = "Pros�m zvolte oblast.";
                    e_oblast.BackColor = Color.Yellow;
                    e_oblast.Focus();
                    break;

                case 2:
                    ErrorString = "Pros�m zvolte okres.";
                    e_okres.BackColor = Color.Yellow;
                    e_okres.Focus();
                    break;

                case 3:
                    ErrorString = "Pros�m zadejte m�sto.";
                    e_mesto.BackColor = Color.Yellow;
                    e_mesto.Focus();
                    break;

                case 4:
                    break;

                case 5:
                    ErrorString = "Pros�m zadejte titulek zpr�vy.";
                    e_nazev.BackColor = Color.Yellow;
                    e_nazev.Focus();
                    break;

                case 6:
                    ErrorString = "Pros�m zvolte kategorie dann� zpr�vy.";
                    e_kategorie.BackColor = Color.Yellow;
                    e_kategorie.Focus();
                    break;

                case 7:
                    ErrorString = "Pros�m napi�te tetx zpr�vy.";
                    e_popis.BackColor = Color.Yellow;
                    e_popis.Focus();
                    break;

                case 8:
                    ErrorString = "Pros�m zvolte alespo� jeden okruh (m��ete i v�ce).";
                    e_groups.BackColor = Color.Yellow;
                    e_groups.Focus();
                    break;


                case 10:
                    ErrorString = "Do�lo k duplicit� kl��ov�ch slov - zpr�va ji� v datab�zi existuje.";
                    break;

                default:
                    ErrorString = "Nezn�m� chyba :(";
                    break;
            }
            opener.ShowAppNotify("Vlo�en� zpr�vy se nezda�ilo.\r\n\r\n" + ErrorString, 5, ToolTipIcon.Warning);

        }


        private int CustNews_saveRecord()
        {
            return CustNews_saveRecord(false);
        }
        private int CustNews_saveRecord(bool SkipChecks)
        {
            int ret = 9999;
            string keywords = Keywords_create();
            string group_id = "*";
            string custnews_vlozil = opener.ActualProfile;

            int ValueCheck = CheckValues();
            if ((ValueCheck == 0) || (SkipChecks == true))
            {
                for (int loop = 0; loop < e_groups.CheckedItems.Count; loop++)
                {
                    group_id += e_groups.CheckedItems[loop] + "*";
                }

                if (MasterEditMode)
                {
                    // EditMode_id
                    object[,] sql_delete = { { "id", "=", EditMode_id } };
                    new MySQLDeleteCommand(opener.MYSQL, opener.sql_db_table + "_custnews", sql_delete, null);
                    custnews_vlozil = edit_base_vlozil;
                    e_nazev_VER_new = e_nazev_VER;
                }
                else
                {
                    EditMode_id = null;
                }

                if (e_embargo_do.Value < DateTime.Now) e_embargo_do.Value = DateTime.Now;
                object[,] sql_insert = {
                        {"id", EditMode_id},
                        {"group_id", group_id},
                        {"cn_all_groups", 0},
                        {"cn_datum", e_embargo_do.Value.Year.ToString("0000") + e_embargo_do.Value.Month.ToString("00") + e_embargo_do.Value.Day.ToString("00")},
                        {"cn_cas", e_embargo_do.Value.Hour.ToString("00") + ":" + e_embargo_do.Value.Minute.ToString("00") + ":" + e_embargo_do.Value.Second.ToString("00")}, 
                        {"cn_oblast", e_oblast.Text},
                        {"cn_okres", e_okres.Text},
                        {"cn_mesto", e_mesto.Text},
                        {"cn_kategorie", e_kategorie.SelectedItem.ToString()},
                        {"cn_nazev", e_nazev.Text},
                        {"cn_text", e_popis.Text},
                        {"cn_klicova_slova", keywords},
                        {"cn_id_repa", e_id_repa.Text},
                        {"cn_delka_repa", e_delka_repa.Text},
                        {"cn_zacatek_repa", e_zacatek_repa.Text},
                        {"cn_konec_repa", e_konec_repa.Text},
                        {"vlozil", custnews_vlozil},
                        {"vlozil_datum", DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")},
                        {"vlozil_cas", DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00")}, 
                        {"base_id", baseID}, 
                        {"verze", e_nazev_VER_new}
                    };
                try
                {
                    new MySQLDriverCS.MySQLInsertCommand(opener.MYSQL, sql_insert, opener.sql_db_table + "_custnews");
                    CustNews_FixEmptyBaseID();
                    opener.LogDebug("Zpr�va vlo�ena...", "CustNews");
                    ret = 0;
                }
                catch (Exception e)
                {
                    opener.LogDebug(e.ToString(), "Error");
                    ret = 9999;
                }
            }
            else
            {
                ret = ValueCheck;
                CustNews_new_ShowCheckErrorMsg(ret);
            }
            return ret;
        }



        private void CustNews_FixEmptyBaseID()
        {
            try
            {
                string sql_query = "UPDATE " + opener.sql_db_table + "_custnews SET base_id=id WHERE base_id=0";
                opener.LogDebug(sql_query, "SQL");
                MySQLDriverCS.MySQLCommand sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_cmd.ExecuteNonQuery();
            }
            catch (Exception err) { opener.LogDebug(err.ToString(), "ERROR"); }
        }

        private void btn_vlozit_Click(object sender, EventArgs e)
        {
            int RetCode = CustNews_saveRecord();
            if (RetCode == 0)
            {
                opener.ShowAppNotify("Zpr�va byla �sp�n� vlo�ena do datab�ze.", 3, ToolTipIcon.Info);
                
                // TODO: CHANGE THIS LINE !!!
                Application.DoEvents();
                opener.CustNews_dg_Load_List();

                this.Close();
            }
            else
            {
                opener.LogDebug("check failed: " + RetCode.ToString(), "user error");
            }
        }

        private void btn_MasterEditMode_Click(object sender, EventArgs e)
        {
            if (CustNews_edit_checkMasterRights())
            {

                if (btn_MasterEditMode.Checked == true)
                {
                    if (e_nazev_VER < 2)
                    {
                        e_nazev.Text = edit_base_nazev;
                    }
                    else
                    {
                        e_nazev.Text = edit_base_nazev + " | " + e_nazev_VER + ". verze";
                    }
                    MasterEditMode = true;
                    group_custnews_main.ValuesPrimary.Heading = "Administr�torsk� m�d";
                }
                else
                {
                    e_nazev.Text = edit_base_nazev + " | " + e_nazev_VER_new + ". verze";
                    MasterEditMode = false;
                    group_custnews_main.ValuesPrimary.Heading = "Editace zpr�vy";
                }
            }
            else
            {
                MessageBox.Show("Nem�te pr�va na vstup do Administr�torsk�ho m�du", "P��stup odep�en", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            int RetCode = -1;

            DialogResult question;
            question = MessageBox.Show("Tato operace nastav� p��znak ZRU�ENO pro tuto zpr�vu.\r\nZpr�va nebude odstran�na z datab�ze.\r\n\r\nTuto akci NELZE vz�t zp�t.\r\nOpravdu chcete zpr�vu zru�it?", "Zru�it zpr�vu", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

            if (question == DialogResult.Yes)
            {
                string[] A;
                string S;

                e_nazev_VER_new = 9999;
                A = e_nazev.Text.Split('|');
                S = A[0].Trim() + " | ZRU�ENO";
                e_nazev.Text = S;
                Keywords_create();

                if (MasterEditMode)
                {
                    try
                    {
                        object[,] sql_delete = { { "id", "=", EditMode_id } };
                        new MySQLDeleteCommand(opener.MYSQL, opener.sql_db_table + "_custnews", sql_delete, null);
                        RetCode = 0;
                    }
                    catch { }
                }
                else
                {
                    RetCode = CustNews_saveRecord(true);
                }

                if (RetCode == 0)
                {
                    opener.ShowAppNotify("Zpr�va byla �sp�n� ZRU�ENA.", 3, ToolTipIcon.Info);
                    opener.CustNews_dg_Load_List();
                    this.Close();
                }
                else
                {
                    opener.LogDebug("unknown error", "error");
                }
            }
        }

    }
}