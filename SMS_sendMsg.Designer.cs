namespace CTK_reader
{
    partial class SMS_sendMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SMS_sendMsg));
            this.label1 = new System.Windows.Forms.Label();
            this.SMS_sendTo = new System.Windows.Forms.MaskedTextBox();
            this.SMS_text = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.SMS_charCount = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SMS_charCountLeft = new System.Windows.Forms.Label();
            this.btn_SMS_send = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_send)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "P��jemce SMS:";
            // 
            // SMS_sendTo
            // 
            this.SMS_sendTo.Location = new System.Drawing.Point(111, 6);
            this.SMS_sendTo.Mask = "+000000000000";
            this.SMS_sendTo.Name = "SMS_sendTo";
            this.SMS_sendTo.Size = new System.Drawing.Size(267, 22);
            this.SMS_sendTo.TabIndex = 1;
            this.SMS_sendTo.Text = "420";
            // 
            // SMS_text
            // 
            this.SMS_text.Location = new System.Drawing.Point(15, 34);
            this.SMS_text.MaxLength = 160;
            this.SMS_text.Multiline = true;
            this.SMS_text.Name = "SMS_text";
            this.SMS_text.Size = new System.Drawing.Size(363, 68);
            this.SMS_text.TabIndex = 2;
            this.SMS_text.TextChanged += new System.EventHandler(this.SMS_text_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Po�et znak�:";
            // 
            // SMS_charCount
            // 
            this.SMS_charCount.AutoSize = true;
            this.SMS_charCount.Location = new System.Drawing.Point(86, 114);
            this.SMS_charCount.Name = "SMS_charCount";
            this.SMS_charCount.Size = new System.Drawing.Size(28, 14);
            this.SMS_charCount.TabIndex = 5;
            this.SMS_charCount.Text = "000";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(120, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "Zb�v�:";
            // 
            // SMS_charCountLeft
            // 
            this.SMS_charCountLeft.AutoSize = true;
            this.SMS_charCountLeft.Location = new System.Drawing.Point(160, 114);
            this.SMS_charCountLeft.Name = "SMS_charCountLeft";
            this.SMS_charCountLeft.Size = new System.Drawing.Size(28, 14);
            this.SMS_charCountLeft.TabIndex = 7;
            this.SMS_charCountLeft.Text = "160";
            // 
            // btn_SMS_send
            // 
            this.btn_SMS_send.Location = new System.Drawing.Point(289, 108);
            this.btn_SMS_send.Name = "btn_SMS_send";
            this.btn_SMS_send.Size = new System.Drawing.Size(89, 26);
            this.btn_SMS_send.TabIndex = 8;
            this.btn_SMS_send.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SMS_send.Values.Image")));
            this.btn_SMS_send.Values.Text = "Odeslat";
            this.btn_SMS_send.Click += new System.EventHandler(this.btn_SMS_send_Click);
            // 
            // SMS_sendMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(390, 143);
            this.Controls.Add(this.btn_SMS_send);
            this.Controls.Add(this.SMS_charCountLeft);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.SMS_charCount);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SMS_text);
            this.Controls.Add(this.SMS_sendTo);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SMS_sendMsg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Odeslat SMS";
            this.WindowActive = true;
            this.Load += new System.EventHandler(this.SMS_sendMsg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_send)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.MaskedTextBox SMS_sendTo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label SMS_charCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label SMS_charCountLeft;
        public System.Windows.Forms.TextBox SMS_text;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SMS_send;
    }
}