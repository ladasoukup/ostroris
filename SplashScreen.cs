/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 21.7.2005
 * Time: 20:53
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace CTK_reader
{
	/// <summary>
	/// Description of SplashScreen.
	/// </summary>
	public class SplashScreen : System.Windows.Forms.Form
    {
        private Label lbl_app_version;
        private PictureBox pictureBox1;
		public SplashScreen()
		{
			InitializeComponent();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbl_app_version = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::CTK_reader.Properties.Resources.OstroRis_logo;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(1, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 200);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.DoubleClick += new System.EventHandler(this.pictureBox1_DoubleClick);
            // 
            // lbl_app_version
            // 
            this.lbl_app_version.BackColor = System.Drawing.Color.Black;
            this.lbl_app_version.Font = new System.Drawing.Font("Tahoma", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_app_version.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(127)))), ((int)(((byte)(62)))));
            this.lbl_app_version.Location = new System.Drawing.Point(12, 71);
            this.lbl_app_version.Name = "lbl_app_version";
            this.lbl_app_version.Size = new System.Drawing.Size(225, 27);
            this.lbl_app_version.TabIndex = 4;
            this.lbl_app_version.Text = "<VERSION>";
            // 
            // SplashScreen
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(502, 200);
            this.Controls.Add(this.lbl_app_version);
            this.Controls.Add(this.pictureBox1);
            this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashScreen";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SplashScreen";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SplashScreenLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		void SplashScreenLoad(object sender, System.EventArgs e)
		{
			lbl_app_version.Text = "Verze: " + Application.ProductVersion.ToString();
		}

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
	}
}
