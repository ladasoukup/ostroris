/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 17.7.2005
 * Time: 19:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using Ini;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
	/// <summary>
	/// Description of sqlcfg.
	/// </summary>
    public class sqlcfg : KryptonForm
	{
		private System.Windows.Forms.TextBox frm_sql_db_table;
		private System.Windows.Forms.Button btn_ok;
		private System.Windows.Forms.TextBox frm_sql_server;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox frm_sql_db;
		private System.Windows.Forms.TextBox frm_sql_password;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox frm_sql_user;
		// Global variables
		MainForm opener;
		bool cfg_saved = false;
		// Functions
		public sqlcfg(MainForm sender)
		{
			opener = sender;
			// The InitializeComponent() call is required for Windows Forms designer support.
			InitializeComponent();
		}		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(sqlcfg));
            this.frm_sql_user = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.frm_sql_password = new System.Windows.Forms.TextBox();
            this.frm_sql_db = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.frm_sql_server = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.frm_sql_db_table = new System.Windows.Forms.TextBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // frm_sql_user
            // 
            this.frm_sql_user.Location = new System.Drawing.Point(128, 60);
            this.frm_sql_user.Name = "frm_sql_user";
            this.frm_sql_user.Size = new System.Drawing.Size(224, 20);
            this.frm_sql_user.TabIndex = 5;
            this.frm_sql_user.Text = "textBox2";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(24, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 4;
            this.label4.Text = "SQL heslo:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(24, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tabulka:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(24, 32);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 16);
            this.label6.TabIndex = 2;
            this.label6.Text = "Databáze:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(368, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nastavení připojení k MySQL";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(24, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "SQL host:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(24, 64);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "SQL uživatel:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frm_sql_password
            // 
            this.frm_sql_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.frm_sql_password.Location = new System.Drawing.Point(128, 92);
            this.frm_sql_password.Name = "frm_sql_password";
            this.frm_sql_password.PasswordChar = '#';
            this.frm_sql_password.Size = new System.Drawing.Size(224, 22);
            this.frm_sql_password.TabIndex = 5;
            // 
            // frm_sql_db
            // 
            this.frm_sql_db.Location = new System.Drawing.Point(128, 28);
            this.frm_sql_db.Name = "frm_sql_db";
            this.frm_sql_db.Size = new System.Drawing.Size(224, 20);
            this.frm_sql_db.TabIndex = 3;
            this.frm_sql_db.Text = "textBox1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.frm_sql_user);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.frm_sql_server);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.frm_sql_password);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Location = new System.Drawing.Point(8, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 128);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "SQL Server";
            // 
            // frm_sql_server
            // 
            this.frm_sql_server.Location = new System.Drawing.Point(128, 28);
            this.frm_sql_server.Name = "frm_sql_server";
            this.frm_sql_server.Size = new System.Drawing.Size(224, 20);
            this.frm_sql_server.TabIndex = 3;
            this.frm_sql_server.Text = "textBox1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.frm_sql_db_table);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.frm_sql_db);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(8, 184);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 96);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Data";
            // 
            // frm_sql_db_table
            // 
            this.frm_sql_db_table.Location = new System.Drawing.Point(128, 60);
            this.frm_sql_db_table.Name = "frm_sql_db_table";
            this.frm_sql_db_table.Size = new System.Drawing.Size(224, 20);
            this.frm_sql_db_table.TabIndex = 5;
            this.frm_sql_db_table.Text = "textBox2";
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_ok.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_ok.Location = new System.Drawing.Point(160, 288);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(75, 23);
            this.btn_ok.TabIndex = 7;
            this.btn_ok.Text = "Uložit";
            this.btn_ok.Click += new System.EventHandler(this.Btn_okClick);
            // 
            // sqlcfg
            // 
            this.AcceptButton = this.btn_ok;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(384, 317);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "sqlcfg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nastavení připojení k MySQL";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.sqlcfg_closing);
            this.Load += new System.EventHandler(this.SqlcfgLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
		void SqlcfgLoad(object sender, System.EventArgs e)
		{
            frm_sql_server.Text = opener.ini.IniReadValue("sql", "sql_host");
            frm_sql_user.Text = opener.ini.IniReadValue("sql", "sql_user");
            //frm_sql_password.Text = opener.ini.IniReadEncValue("sql", "sql_pass");
            frm_sql_db.Text = opener.ini.IniReadValue("sql", "sql_db");
            frm_sql_db_table.Text = opener.ini.IniReadValue("sql", "sql_db_table");
		}
		
		void Btn_okClick(object sender, System.EventArgs e)
		{
			sqlcfg_SaveConfig();
		}
		
		void sqlcfg_closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cfg_saved == false){
				DialogResult result = MessageBox.Show("Nastavení nebylo uloženo.\nPokud okno zavřete, nastavení nebude uloženo.", "Nastavení nebylo uloženo", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
				switch(result) {
					case DialogResult.OK:
						break;
					case DialogResult.Cancel:
						e.Cancel = true;
						break;
				}
			}
		}
		
		void sqlcfg_SaveConfig() {
			// Saving config...
            opener.ini.IniWriteValue("sql", "sql_host", frm_sql_server.Text);
            opener.ini.IniWriteValue("sql", "sql_user", frm_sql_user.Text);
            if (frm_sql_password.Text != "")
            {
                opener.ini.IniWriteEncValue("sql", "sql_pass", frm_sql_password.Text);
            }
            opener.ini.IniWriteValue("sql", "sql_db", frm_sql_db.Text);
            opener.ini.IniWriteValue("sql", "sql_db_table", frm_sql_db_table.Text);
			opener.LogDebug("Configuration saved", "SYSTEM");
			cfg_saved = true;
			opener.ShowAppNotify("Nastavení SQL bylo uloženo.", 3);
			opener.MySQL_connect();
            opener.CTK_profileSave();
            opener.CTK_profileShowWnd();
			this.Close();
		}
		
	}
}
