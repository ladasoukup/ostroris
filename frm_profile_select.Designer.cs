namespace CTK_reader
{
    partial class frm_profile_select
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_profile_select));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_pass = new System.Windows.Forms.TextBox();
            this.lbl_error = new System.Windows.Forms.Label();
            this.timer_open = new System.Windows.Forms.Timer(this.components);
            this.txt_user = new System.Windows.Forms.TextBox();
            this.pict_logo = new System.Windows.Forms.PictureBox();
            this.timer_hide_app = new System.Windows.Forms.Timer(this.components);
            this.btn_login = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.loadingCircle_login = new MRG.Controls.UI.LoadingCircle();
            ((System.ComponentModel.ISupportInitialize)(this.pict_logo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_login)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label1.Location = new System.Drawing.Point(8, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 18);
            this.label1.TabIndex = 1;
            this.label1.Text = "Jm�no:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.label2.Location = new System.Drawing.Point(8, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Heslo:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txt_pass
            // 
            this.txt_pass.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_pass.Location = new System.Drawing.Point(68, 118);
            this.txt_pass.Name = "txt_pass";
            this.txt_pass.PasswordChar = '#';
            this.txt_pass.Size = new System.Drawing.Size(188, 21);
            this.txt_pass.TabIndex = 2;
            this.txt_pass.TextChanged += new System.EventHandler(this.txt_pass_textChanged);
            this.txt_pass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_pass_KeyDown);
            // 
            // lbl_error
            // 
            this.lbl_error.BackColor = System.Drawing.Color.Black;
            this.lbl_error.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_error.ForeColor = System.Drawing.Color.LightCoral;
            this.lbl_error.Location = new System.Drawing.Point(8, 145);
            this.lbl_error.Name = "lbl_error";
            this.lbl_error.Size = new System.Drawing.Size(140, 23);
            this.lbl_error.TabIndex = 5;
            this.lbl_error.Text = "Pracuji...";
            this.lbl_error.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // timer_open
            // 
            this.timer_open.Interval = 1000;
            this.timer_open.Tick += new System.EventHandler(this.timer_open_Tick);
            // 
            // txt_user
            // 
            this.txt_user.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.txt_user.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txt_user.Location = new System.Drawing.Point(68, 95);
            this.txt_user.Name = "txt_user";
            this.txt_user.Size = new System.Drawing.Size(188, 20);
            this.txt_user.TabIndex = 1;
            this.txt_user.TextChanged += new System.EventHandler(this.txt_user_TextChanged);
            this.txt_user.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txt_user_KeyDown);
            // 
            // pict_logo
            // 
            this.pict_logo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.pict_logo.Image = global::CTK_reader.Properties.Resources.OstroRis_logo;
            this.pict_logo.InitialImage = null;
            this.pict_logo.Location = new System.Drawing.Point(2, 1);
            this.pict_logo.Name = "pict_logo";
            this.pict_logo.Size = new System.Drawing.Size(500, 200);
            this.pict_logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pict_logo.TabIndex = 6;
            this.pict_logo.TabStop = false;
            // 
            // timer_hide_app
            // 
            this.timer_hide_app.Interval = 2500;
            this.timer_hide_app.Tick += new System.EventHandler(this.timer_hide_app_Tick);
            // 
            // btn_login
            // 
            this.btn_login.DirtyPaletteCounter = 9;
            this.btn_login.Location = new System.Drawing.Point(171, 143);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(85, 25);
            this.btn_login.TabIndex = 7;
            this.btn_login.Text = "P�ihl�sit";
            this.btn_login.Values.ExtraText = "";
            this.btn_login.Values.Image = null;
            this.btn_login.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_login.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_login.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_login.Values.Text = "P�ihl�sit";
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // loadingCircle_login
            // 
            this.loadingCircle_login.Active = false;
            this.loadingCircle_login.BackColor = System.Drawing.Color.Black;
            this.loadingCircle_login.Color = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.loadingCircle_login.InnerCircleRadius = 6;
            this.loadingCircle_login.Location = new System.Drawing.Point(142, 145);
            this.loadingCircle_login.Name = "loadingCircle_login";
            this.loadingCircle_login.NumberSpoke = 9;
            this.loadingCircle_login.OuterCircleRadius = 7;
            this.loadingCircle_login.RotationSpeed = 100;
            this.loadingCircle_login.Size = new System.Drawing.Size(23, 23);
            this.loadingCircle_login.SpokeThickness = 4;
            this.loadingCircle_login.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.Firefox;
            this.loadingCircle_login.TabIndex = 8;
            this.loadingCircle_login.Text = "Pracuji...";
            this.loadingCircle_login.Visible = false;
            // 
            // frm_profile_select
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(503, 202);
            this.Controls.Add(this.loadingCircle_login);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.txt_user);
            this.Controls.Add(this.lbl_error);
            this.Controls.Add(this.txt_pass);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pict_logo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frm_profile_select";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zvolte profil";
            this.Load += new System.EventHandler(this.frm_profile_select_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_profile_select_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pict_logo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_login)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_pass;
        private System.Windows.Forms.Label lbl_error;
        private System.Windows.Forms.Timer timer_open;
        private System.Windows.Forms.TextBox txt_user;
        private System.Windows.Forms.PictureBox pict_logo;
        private System.Windows.Forms.Timer timer_hide_app;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_login;
        private MRG.Controls.UI.LoadingCircle loadingCircle_login;
    }
}