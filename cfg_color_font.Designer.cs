namespace CTK_reader
{
    partial class cfg_color_font
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cfg_color_font));
            this.dialog_color = new System.Windows.Forms.ColorDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_active_bg = new System.Windows.Forms.Button();
            this.btn_active_text = new System.Windows.Forms.Button();
            this.txt_active = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_detail_bg = new System.Windows.Forms.Button();
            this.btn_detail_text = new System.Windows.Forms.Button();
            this.txt_detail = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_unreaded_bg = new System.Windows.Forms.Button();
            this.btn_unreaded_text = new System.Windows.Forms.Button();
            this.btn_readed_bg = new System.Windows.Forms.Button();
            this.btn_readed_text = new System.Windows.Forms.Button();
            this.txt_unreaded = new System.Windows.Forms.TextBox();
            this.txt_readed = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_default = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cfg_ctk_list_font = new System.Windows.Forms.ComboBox();
            this.cfg_ctk_list_font_size = new System.Windows.Forms.NumericUpDown();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cfg_ctk_detail_font = new System.Windows.Forms.ComboBox();
            this.cfg_ctk_detail_font_size = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cfg_ctk_list_font_size)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cfg_ctk_detail_font_size)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_active_bg);
            this.groupBox1.Controls.Add(this.btn_active_text);
            this.groupBox1.Controls.Add(this.txt_active);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.btn_detail_bg);
            this.groupBox1.Controls.Add(this.btn_detail_text);
            this.groupBox1.Controls.Add(this.txt_detail);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_unreaded_bg);
            this.groupBox1.Controls.Add(this.btn_unreaded_text);
            this.groupBox1.Controls.Add(this.btn_readed_bg);
            this.groupBox1.Controls.Add(this.btn_readed_text);
            this.groupBox1.Controls.Add(this.txt_unreaded);
            this.groupBox1.Controls.Add(this.txt_readed);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(319, 140);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Barvy";
            // 
            // btn_active_bg
            // 
            this.btn_active_bg.Location = new System.Drawing.Point(248, 75);
            this.btn_active_bg.Name = "btn_active_bg";
            this.btn_active_bg.Size = new System.Drawing.Size(57, 21);
            this.btn_active_bg.TabIndex = 19;
            this.btn_active_bg.Text = "pozad�";
            this.btn_active_bg.UseVisualStyleBackColor = true;
            this.btn_active_bg.Click += new System.EventHandler(this.btn_active_bg_Click);
            // 
            // btn_active_text
            // 
            this.btn_active_text.Location = new System.Drawing.Point(185, 75);
            this.btn_active_text.Name = "btn_active_text";
            this.btn_active_text.Size = new System.Drawing.Size(57, 21);
            this.btn_active_text.TabIndex = 18;
            this.btn_active_text.Text = "text";
            this.btn_active_text.UseVisualStyleBackColor = true;
            this.btn_active_text.Click += new System.EventHandler(this.btn_active_text_Click);
            // 
            // txt_active
            // 
            this.txt_active.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_active.Location = new System.Drawing.Point(87, 76);
            this.txt_active.Name = "txt_active";
            this.txt_active.ReadOnly = true;
            this.txt_active.Size = new System.Drawing.Size(92, 21);
            this.txt_active.TabIndex = 21;
            this.txt_active.Text = "Uk�zka textu";
            this.txt_active.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(6, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(75, 15);
            this.label4.TabIndex = 20;
            this.label4.Text = "Ozna�en�:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_detail_bg
            // 
            this.btn_detail_bg.Location = new System.Drawing.Point(248, 102);
            this.btn_detail_bg.Name = "btn_detail_bg";
            this.btn_detail_bg.Size = new System.Drawing.Size(57, 21);
            this.btn_detail_bg.TabIndex = 5;
            this.btn_detail_bg.Text = "pozad�";
            this.btn_detail_bg.UseVisualStyleBackColor = true;
            this.btn_detail_bg.Click += new System.EventHandler(this.btn_detail_bg_Click);
            // 
            // btn_detail_text
            // 
            this.btn_detail_text.Location = new System.Drawing.Point(185, 102);
            this.btn_detail_text.Name = "btn_detail_text";
            this.btn_detail_text.Size = new System.Drawing.Size(57, 21);
            this.btn_detail_text.TabIndex = 4;
            this.btn_detail_text.Text = "text";
            this.btn_detail_text.UseVisualStyleBackColor = true;
            this.btn_detail_text.Click += new System.EventHandler(this.btn_detail_text_Click);
            // 
            // txt_detail
            // 
            this.txt_detail.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_detail.Location = new System.Drawing.Point(87, 103);
            this.txt_detail.Name = "txt_detail";
            this.txt_detail.ReadOnly = true;
            this.txt_detail.Size = new System.Drawing.Size(92, 21);
            this.txt_detail.TabIndex = 17;
            this.txt_detail.Text = "Uk�zka textu";
            this.txt_detail.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(6, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "Detail:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_unreaded_bg
            // 
            this.btn_unreaded_bg.Location = new System.Drawing.Point(248, 49);
            this.btn_unreaded_bg.Name = "btn_unreaded_bg";
            this.btn_unreaded_bg.Size = new System.Drawing.Size(57, 21);
            this.btn_unreaded_bg.TabIndex = 3;
            this.btn_unreaded_bg.Text = "pozad�";
            this.btn_unreaded_bg.UseVisualStyleBackColor = true;
            this.btn_unreaded_bg.Click += new System.EventHandler(this.btn_unreaded_bg_Click);
            // 
            // btn_unreaded_text
            // 
            this.btn_unreaded_text.Location = new System.Drawing.Point(185, 49);
            this.btn_unreaded_text.Name = "btn_unreaded_text";
            this.btn_unreaded_text.Size = new System.Drawing.Size(57, 21);
            this.btn_unreaded_text.TabIndex = 2;
            this.btn_unreaded_text.Text = "text";
            this.btn_unreaded_text.UseVisualStyleBackColor = true;
            this.btn_unreaded_text.Click += new System.EventHandler(this.btn_unreaded_text_Click);
            // 
            // btn_readed_bg
            // 
            this.btn_readed_bg.Location = new System.Drawing.Point(248, 22);
            this.btn_readed_bg.Name = "btn_readed_bg";
            this.btn_readed_bg.Size = new System.Drawing.Size(57, 21);
            this.btn_readed_bg.TabIndex = 1;
            this.btn_readed_bg.Text = "pozad�";
            this.btn_readed_bg.UseVisualStyleBackColor = true;
            this.btn_readed_bg.Click += new System.EventHandler(this.btn_readed_bg_Click);
            // 
            // btn_readed_text
            // 
            this.btn_readed_text.Location = new System.Drawing.Point(185, 22);
            this.btn_readed_text.Name = "btn_readed_text";
            this.btn_readed_text.Size = new System.Drawing.Size(57, 21);
            this.btn_readed_text.TabIndex = 0;
            this.btn_readed_text.Text = "text";
            this.btn_readed_text.UseVisualStyleBackColor = true;
            this.btn_readed_text.Click += new System.EventHandler(this.btn_readed_text_Click);
            // 
            // txt_unreaded
            // 
            this.txt_unreaded.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_unreaded.Location = new System.Drawing.Point(87, 50);
            this.txt_unreaded.Name = "txt_unreaded";
            this.txt_unreaded.ReadOnly = true;
            this.txt_unreaded.Size = new System.Drawing.Size(92, 21);
            this.txt_unreaded.TabIndex = 16;
            this.txt_unreaded.Text = "Uk�zka textu";
            this.txt_unreaded.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // txt_readed
            // 
            this.txt_readed.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txt_readed.Location = new System.Drawing.Point(87, 23);
            this.txt_readed.Name = "txt_readed";
            this.txt_readed.ReadOnly = true;
            this.txt_readed.Size = new System.Drawing.Size(92, 21);
            this.txt_readed.TabIndex = 15;
            this.txt_readed.Text = "Uk�zka textu";
            this.txt_readed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nep�e�ten�:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(6, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "P�e�ten�:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btn_save
            // 
            this.btn_save.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_save.Location = new System.Drawing.Point(230, 244);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(101, 24);
            this.btn_save.TabIndex = 6;
            this.btn_save.Text = "Ulo�it a zav��t";
            this.btn_save.UseVisualStyleBackColor = true;
            this.btn_save.Click += new System.EventHandler(this.btn_save_Click);
            // 
            // btn_default
            // 
            this.btn_default.Location = new System.Drawing.Point(12, 244);
            this.btn_default.Name = "btn_default";
            this.btn_default.Size = new System.Drawing.Size(106, 24);
            this.btn_default.TabIndex = 7;
            this.btn_default.Text = "V�choz� nastaven�";
            this.btn_default.UseVisualStyleBackColor = true;
            this.btn_default.Click += new System.EventHandler(this.btn_default_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.groupBox6);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox3.Location = new System.Drawing.Point(12, 158);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(320, 80);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Nastaven� fontu a velikosti";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cfg_ctk_list_font);
            this.groupBox4.Controls.Add(this.cfg_ctk_list_font_size);
            this.groupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox4.Location = new System.Drawing.Point(8, 16);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(144, 56);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Seznam zpr�v";
            // 
            // cfg_ctk_list_font
            // 
            this.cfg_ctk_list_font.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cfg_ctk_list_font.Items.AddRange(new object[] {
            "Arial",
            "Comic Sans MS",
            "Tahoma",
            "Times New Roman",
            "Verdana"});
            this.cfg_ctk_list_font.Location = new System.Drawing.Point(8, 24);
            this.cfg_ctk_list_font.Name = "cfg_ctk_list_font";
            this.cfg_ctk_list_font.Size = new System.Drawing.Size(80, 21);
            this.cfg_ctk_list_font.Sorted = true;
            this.cfg_ctk_list_font.TabIndex = 9;
            // 
            // cfg_ctk_list_font_size
            // 
            this.cfg_ctk_list_font_size.Location = new System.Drawing.Point(96, 24);
            this.cfg_ctk_list_font_size.Maximum = new decimal(new int[] {
            28,
            0,
            0,
            0});
            this.cfg_ctk_list_font_size.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.cfg_ctk_list_font_size.Name = "cfg_ctk_list_font_size";
            this.cfg_ctk_list_font_size.Size = new System.Drawing.Size(40, 21);
            this.cfg_ctk_list_font_size.TabIndex = 10;
            this.cfg_ctk_list_font_size.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.cfg_ctk_detail_font);
            this.groupBox6.Controls.Add(this.cfg_ctk_detail_font_size);
            this.groupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox6.Location = new System.Drawing.Point(166, 16);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(146, 56);
            this.groupBox6.TabIndex = 11;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Detail zpr�vy";
            // 
            // cfg_ctk_detail_font
            // 
            this.cfg_ctk_detail_font.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cfg_ctk_detail_font.Location = new System.Drawing.Point(7, 24);
            this.cfg_ctk_detail_font.Name = "cfg_ctk_detail_font";
            this.cfg_ctk_detail_font.Size = new System.Drawing.Size(83, 21);
            this.cfg_ctk_detail_font.Sorted = true;
            this.cfg_ctk_detail_font.TabIndex = 9;
            // 
            // cfg_ctk_detail_font_size
            // 
            this.cfg_ctk_detail_font_size.Location = new System.Drawing.Point(99, 25);
            this.cfg_ctk_detail_font_size.Maximum = new decimal(new int[] {
            28,
            0,
            0,
            0});
            this.cfg_ctk_detail_font_size.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.cfg_ctk_detail_font_size.Name = "cfg_ctk_detail_font_size";
            this.cfg_ctk_detail_font_size.Size = new System.Drawing.Size(40, 21);
            this.cfg_ctk_detail_font_size.TabIndex = 10;
            this.cfg_ctk_detail_font_size.Value = new decimal(new int[] {
            12,
            0,
            0,
            0});
            // 
            // cfg_color_font
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(343, 276);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btn_default);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cfg_color_font";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nastaven� barev a p�sem";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.cfg_color_Closing);
            this.Load += new System.EventHandler(this.cfg_color_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cfg_ctk_list_font_size)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cfg_ctk_detail_font_size)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColorDialog dialog_color;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.TextBox txt_readed;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_unreaded;
        private System.Windows.Forms.Button btn_readed_bg;
        private System.Windows.Forms.Button btn_readed_text;
        private System.Windows.Forms.Button btn_unreaded_bg;
        private System.Windows.Forms.Button btn_unreaded_text;
        private System.Windows.Forms.Button btn_detail_bg;
        private System.Windows.Forms.Button btn_detail_text;
        private System.Windows.Forms.TextBox txt_detail;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_active_bg;
        private System.Windows.Forms.Button btn_active_text;
        private System.Windows.Forms.TextBox txt_active;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_default;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox cfg_ctk_list_font;
        private System.Windows.Forms.NumericUpDown cfg_ctk_list_font_size;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox cfg_ctk_detail_font;
        private System.Windows.Forms.NumericUpDown cfg_ctk_detail_font_size;
    }
}