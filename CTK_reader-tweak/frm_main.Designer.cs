namespace CTK_reader_tweak
{
    partial class frm_main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_main));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_SQL = new System.Windows.Forms.TabPage();
            this.cfg_sql_table = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cfg_sql_db = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cfg_sql_user = new System.Windows.Forms.TextBox();
            this.cfg_sql_host = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cfg_sql_password = new System.Windows.Forms.TextBox();
            this.tab_Login = new System.Windows.Forms.TabPage();
            this.cfg_autologin_enable = new System.Windows.Forms.CheckBox();
            this.cfg_autologin_password = new System.Windows.Forms.TextBox();
            this.cfg_autologin_user = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tab_fix = new System.Windows.Forms.TabPage();
            this.fix_debug_off = new System.Windows.Forms.Button();
            this.fix_debug_on = new System.Windows.Forms.Button();
            this.fix_info = new System.Windows.Forms.Label();
            this.fix_au = new System.Windows.Forms.Button();
            this.fix_window = new System.Windows.Forms.Button();
            this.tab_EULA = new System.Windows.Forms.TabPage();
            this.txt_EULA = new System.Windows.Forms.TextBox();
            this.btn_close = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tab_SQL.SuspendLayout();
            this.tab_Login.SuspendLayout();
            this.tab_fix.SuspendLayout();
            this.tab_EULA.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tab_SQL);
            this.tabControl1.Controls.Add(this.tab_Login);
            this.tabControl1.Controls.Add(this.tab_fix);
            this.tabControl1.Controls.Add(this.tab_EULA);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(544, 272);
            this.tabControl1.TabIndex = 0;
            // 
            // tab_SQL
            // 
            this.tab_SQL.Controls.Add(this.cfg_sql_table);
            this.tab_SQL.Controls.Add(this.label7);
            this.tab_SQL.Controls.Add(this.cfg_sql_db);
            this.tab_SQL.Controls.Add(this.label8);
            this.tab_SQL.Controls.Add(this.cfg_sql_user);
            this.tab_SQL.Controls.Add(this.cfg_sql_host);
            this.tab_SQL.Controls.Add(this.label1);
            this.tab_SQL.Controls.Add(this.label3);
            this.tab_SQL.Controls.Add(this.label2);
            this.tab_SQL.Controls.Add(this.label4);
            this.tab_SQL.Controls.Add(this.cfg_sql_password);
            this.tab_SQL.Location = new System.Drawing.Point(4, 22);
            this.tab_SQL.Name = "tab_SQL";
            this.tab_SQL.Padding = new System.Windows.Forms.Padding(3);
            this.tab_SQL.Size = new System.Drawing.Size(536, 246);
            this.tab_SQL.TabIndex = 0;
            this.tab_SQL.Text = "MySQL";
            this.tab_SQL.UseVisualStyleBackColor = true;
            // 
            // cfg_sql_table
            // 
            this.cfg_sql_table.Location = new System.Drawing.Point(108, 167);
            this.cfg_sql_table.Name = "cfg_sql_table";
            this.cfg_sql_table.Size = new System.Drawing.Size(224, 21);
            this.cfg_sql_table.TabIndex = 20;
            this.cfg_sql_table.Text = "textBox2";
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(14, 168);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Tabulka:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cfg_sql_db
            // 
            this.cfg_sql_db.Location = new System.Drawing.Point(108, 140);
            this.cfg_sql_db.Name = "cfg_sql_db";
            this.cfg_sql_db.Size = new System.Drawing.Size(224, 21);
            this.cfg_sql_db.TabIndex = 18;
            this.cfg_sql_db.Text = "textBox1";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(14, 141);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 16);
            this.label8.TabIndex = 17;
            this.label8.Text = "Datab�ze:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cfg_sql_user
            // 
            this.cfg_sql_user.Location = new System.Drawing.Point(108, 70);
            this.cfg_sql_user.Name = "cfg_sql_user";
            this.cfg_sql_user.Size = new System.Drawing.Size(224, 21);
            this.cfg_sql_user.TabIndex = 14;
            this.cfg_sql_user.Text = "textBox2";
            // 
            // cfg_sql_host
            // 
            this.cfg_sql_host.Location = new System.Drawing.Point(108, 43);
            this.cfg_sql_host.Name = "cfg_sql_host";
            this.cfg_sql_host.Size = new System.Drawing.Size(224, 21);
            this.cfg_sql_host.TabIndex = 9;
            this.cfg_sql_host.Text = "textBox1";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(3, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(443, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Nastaven� p�ipojen� k MySQL";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(4, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 16);
            this.label3.TabIndex = 13;
            this.label3.Text = "SQL u�ivatel:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(14, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 16);
            this.label2.TabIndex = 8;
            this.label2.Text = "SQL host:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(14, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 16);
            this.label4.TabIndex = 12;
            this.label4.Text = "SQL heslo:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cfg_sql_password
            // 
            this.cfg_sql_password.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cfg_sql_password.Location = new System.Drawing.Point(108, 97);
            this.cfg_sql_password.Name = "cfg_sql_password";
            this.cfg_sql_password.PasswordChar = '#';
            this.cfg_sql_password.Size = new System.Drawing.Size(224, 22);
            this.cfg_sql_password.TabIndex = 15;
            // 
            // tab_Login
            // 
            this.tab_Login.Controls.Add(this.cfg_autologin_enable);
            this.tab_Login.Controls.Add(this.cfg_autologin_password);
            this.tab_Login.Controls.Add(this.cfg_autologin_user);
            this.tab_Login.Controls.Add(this.label6);
            this.tab_Login.Controls.Add(this.label9);
            this.tab_Login.Controls.Add(this.label5);
            this.tab_Login.Location = new System.Drawing.Point(4, 22);
            this.tab_Login.Name = "tab_Login";
            this.tab_Login.Padding = new System.Windows.Forms.Padding(3);
            this.tab_Login.Size = new System.Drawing.Size(536, 246);
            this.tab_Login.TabIndex = 1;
            this.tab_Login.Text = "Auto Login";
            this.tab_Login.UseVisualStyleBackColor = true;
            // 
            // cfg_autologin_enable
            // 
            this.cfg_autologin_enable.AutoSize = true;
            this.cfg_autologin_enable.Location = new System.Drawing.Point(12, 29);
            this.cfg_autologin_enable.Name = "cfg_autologin_enable";
            this.cfg_autologin_enable.Size = new System.Drawing.Size(293, 17);
            this.cfg_autologin_enable.TabIndex = 20;
            this.cfg_autologin_enable.Text = "Povolit automatick� p�ihla�ov�n� n�sleduj�c�ho u�ivatele.";
            this.cfg_autologin_enable.UseVisualStyleBackColor = true;
            this.cfg_autologin_enable.CheckedChanged += new System.EventHandler(this.cfg_autologin_EnDis);
            // 
            // cfg_autologin_password
            // 
            this.cfg_autologin_password.Enabled = false;
            this.cfg_autologin_password.Location = new System.Drawing.Point(151, 84);
            this.cfg_autologin_password.Name = "cfg_autologin_password";
            this.cfg_autologin_password.PasswordChar = '#';
            this.cfg_autologin_password.Size = new System.Drawing.Size(224, 21);
            this.cfg_autologin_password.TabIndex = 18;
            // 
            // cfg_autologin_user
            // 
            this.cfg_autologin_user.Enabled = false;
            this.cfg_autologin_user.Location = new System.Drawing.Point(151, 57);
            this.cfg_autologin_user.Name = "cfg_autologin_user";
            this.cfg_autologin_user.Size = new System.Drawing.Size(224, 21);
            this.cfg_autologin_user.TabIndex = 16;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(12, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(133, 21);
            this.label6.TabIndex = 17;
            this.label6.Text = "U�ivatelsk� heslo:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(9, 58);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(136, 16);
            this.label9.TabIndex = 15;
            this.label9.Text = "U�ivatelks� jm�no:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(3, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(443, 23);
            this.label5.TabIndex = 7;
            this.label5.Text = "Automatick� p�ihla�ov�n� u�ivatele";
            // 
            // tab_fix
            // 
            this.tab_fix.Controls.Add(this.fix_debug_off);
            this.tab_fix.Controls.Add(this.fix_debug_on);
            this.tab_fix.Controls.Add(this.fix_info);
            this.tab_fix.Controls.Add(this.fix_au);
            this.tab_fix.Controls.Add(this.fix_window);
            this.tab_fix.Location = new System.Drawing.Point(4, 22);
            this.tab_fix.Name = "tab_fix";
            this.tab_fix.Padding = new System.Windows.Forms.Padding(3);
            this.tab_fix.Size = new System.Drawing.Size(536, 246);
            this.tab_fix.TabIndex = 3;
            this.tab_fix.Text = "Opravy";
            this.tab_fix.UseVisualStyleBackColor = true;
            // 
            // fix_debug_off
            // 
            this.fix_debug_off.Location = new System.Drawing.Point(275, 68);
            this.fix_debug_off.Name = "fix_debug_off";
            this.fix_debug_off.Size = new System.Drawing.Size(255, 25);
            this.fix_debug_off.TabIndex = 4;
            this.fix_debug_off.Text = "NEzobrazovat LOG po spu�t�n� aplikace";
            this.fix_debug_off.UseVisualStyleBackColor = true;
            this.fix_debug_off.Click += new System.EventHandler(this.fix_debug_off_Click);
            // 
            // fix_debug_on
            // 
            this.fix_debug_on.Location = new System.Drawing.Point(6, 68);
            this.fix_debug_on.Name = "fix_debug_on";
            this.fix_debug_on.Size = new System.Drawing.Size(255, 25);
            this.fix_debug_on.TabIndex = 3;
            this.fix_debug_on.Text = "Zobrazit LOG po spu�t�n� aplikace";
            this.fix_debug_on.UseVisualStyleBackColor = true;
            this.fix_debug_on.Click += new System.EventHandler(this.fix_debug_on_Click);
            // 
            // fix_info
            // 
            this.fix_info.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.fix_info.ForeColor = System.Drawing.Color.Red;
            this.fix_info.Location = new System.Drawing.Point(3, 227);
            this.fix_info.Name = "fix_info";
            this.fix_info.Size = new System.Drawing.Size(527, 16);
            this.fix_info.TabIndex = 2;
            this.fix_info.Text = "...";
            this.fix_info.Visible = false;
            // 
            // fix_au
            // 
            this.fix_au.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fix_au.Location = new System.Drawing.Point(6, 37);
            this.fix_au.Name = "fix_au";
            this.fix_au.Size = new System.Drawing.Size(524, 25);
            this.fix_au.TabIndex = 1;
            this.fix_au.Text = "St�hnout v�echny soubory z aktualiza�n�ho webu.";
            this.fix_au.UseVisualStyleBackColor = true;
            this.fix_au.Click += new System.EventHandler(this.fix_au_Click);
            // 
            // fix_window
            // 
            this.fix_window.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.fix_window.Location = new System.Drawing.Point(6, 6);
            this.fix_window.Name = "fix_window";
            this.fix_window.Size = new System.Drawing.Size(524, 25);
            this.fix_window.TabIndex = 0;
            this.fix_window.Text = "Opravit pozici okna";
            this.fix_window.UseVisualStyleBackColor = true;
            this.fix_window.Click += new System.EventHandler(this.fix_window_Click);
            // 
            // tab_EULA
            // 
            this.tab_EULA.Controls.Add(this.txt_EULA);
            this.tab_EULA.Location = new System.Drawing.Point(4, 22);
            this.tab_EULA.Margin = new System.Windows.Forms.Padding(0);
            this.tab_EULA.Name = "tab_EULA";
            this.tab_EULA.Size = new System.Drawing.Size(536, 246);
            this.tab_EULA.TabIndex = 2;
            this.tab_EULA.Text = "EULA";
            this.tab_EULA.UseVisualStyleBackColor = true;
            // 
            // txt_EULA
            // 
            this.txt_EULA.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txt_EULA.BackColor = System.Drawing.Color.White;
            this.txt_EULA.Location = new System.Drawing.Point(0, 0);
            this.txt_EULA.Margin = new System.Windows.Forms.Padding(0);
            this.txt_EULA.Multiline = true;
            this.txt_EULA.Name = "txt_EULA";
            this.txt_EULA.ReadOnly = true;
            this.txt_EULA.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_EULA.Size = new System.Drawing.Size(536, 246);
            this.txt_EULA.TabIndex = 0;
            // 
            // btn_close
            // 
            this.btn_close.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_close.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_close.Location = new System.Drawing.Point(445, 286);
            this.btn_close.Name = "btn_close";
            this.btn_close.Size = new System.Drawing.Size(107, 23);
            this.btn_close.TabIndex = 1;
            this.btn_close.Text = "Ulo�it a zav��t";
            this.btn_close.UseVisualStyleBackColor = true;
            this.btn_close.Click += new System.EventHandler(this.btn_close_Click);
            // 
            // frm_main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(568, 321);
            this.Controls.Add(this.btn_close);
            this.Controls.Add(this.tabControl1);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OstroRis - TWEAK";
            this.Load += new System.EventHandler(this.frm_main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tab_SQL.ResumeLayout(false);
            this.tab_SQL.PerformLayout();
            this.tab_Login.ResumeLayout(false);
            this.tab_Login.PerformLayout();
            this.tab_fix.ResumeLayout(false);
            this.tab_EULA.ResumeLayout(false);
            this.tab_EULA.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_SQL;
        private System.Windows.Forms.TabPage tab_Login;
        private System.Windows.Forms.Button btn_close;
        private System.Windows.Forms.TextBox cfg_sql_user;
        private System.Windows.Forms.TextBox cfg_sql_host;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox cfg_sql_password;
        private System.Windows.Forms.TextBox cfg_sql_table;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox cfg_sql_db;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TabPage tab_EULA;
        private System.Windows.Forms.TextBox txt_EULA;
        private System.Windows.Forms.TextBox cfg_autologin_password;
        private System.Windows.Forms.TextBox cfg_autologin_user;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cfg_autologin_enable;
        private System.Windows.Forms.TabPage tab_fix;
        private System.Windows.Forms.Button fix_window;
        private System.Windows.Forms.Button fix_au;
        private System.Windows.Forms.Label fix_info;
        private System.Windows.Forms.Button fix_debug_off;
        private System.Windows.Forms.Button fix_debug_on;
    }
}

