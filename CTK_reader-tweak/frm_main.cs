using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CTK_reader_tweak
{
    public partial class frm_main : Form
    {
        private Ini.IniFile cfg = new Ini.IniFile(Application.StartupPath + "\\OstroRis.ini");

        public frm_main()
        {
            InitializeComponent();
        }

        private void frm_main_Load(object sender, EventArgs e)
        {
            string S = null;

            //EULA tab
            txt_EULA.Text = Properties.Resources.license.ToString();

            // SQL tab
            cfg_sql_host.Text = cfg.IniReadValue("sql", "sql_host");
            cfg_sql_user.Text = cfg.IniReadValue("sql", "sql_user");
            cfg_sql_db.Text = cfg.IniReadValue("sql", "sql_db");
            cfg_sql_table.Text = cfg.IniReadValue("sql", "sql_db_table");

            //AutoLogin tab
            S = cfg.IniReadValue("AutoLogin", "enable");
            if (S != "") { if (S == "True") cfg_autologin_enable.Checked = true; }
            cfg_autologin_user.Text = cfg.IniReadEncValue("AutoLogin", "user");

        }

        private void btn_close_Click(object sender, EventArgs e)
        {
            // SQL tab
            cfg.IniWriteValue("sql", "sql_host", cfg_sql_host.Text);
            cfg.IniWriteValue("sql", "sql_user", cfg_sql_user.Text);
            cfg.IniWriteValue("sql", "sql_db", cfg_sql_db.Text);
            cfg.IniWriteValue("sql", "sql_db_table", cfg_sql_table.Text);
            if (cfg_sql_password.Text != "")
            {
                cfg.IniWriteEncValue("sql", "sql_pass", cfg_sql_password.Text);
            }

            //AutoLogin tab
            cfg.IniWriteValue("AutoLogin", "enable", cfg_autologin_enable.Checked.ToString());
            cfg.IniWriteEncValue("AutoLogin", "user", cfg_autologin_user.Text);
            if (cfg_autologin_password.Text != "")
            {
                cfg.IniWriteEncValue("AutoLogin", "password", cfg_autologin_password.Text);
            }

            // QUIT.
            this.Close();
            Application.DoEvents();
            Application.Exit();
        }

        private void cfg_autologin_EnDis(object sender, EventArgs e)
        {
            if (cfg_autologin_enable.Checked == true)
            {
                cfg_autologin_user.Enabled = true;
                cfg_autologin_password.Enabled = true;
            }
            else
            {
                cfg_autologin_user.Enabled = false;
                cfg_autologin_password.Enabled = false;
            }
        }

        private void fix_window_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Opravdu chcete obnovit v�choz� velikost a pozici okna aplikace?", "Obnovit polohu a velikost okna", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            if (res == DialogResult.Yes)
            {
                cfg.IniWriteValue("window", "width", "600");
                cfg.IniWriteValue("window", "height", "500");
                cfg.IniWriteValue("window", "left", "5");
                cfg.IniWriteValue("window", "top", "5");
                cfg.IniWriteValue("window", "state", "Normal");
            }
        }

        private void fix_au_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show("Opravdu chcete st�hnout v�echny soubory t�to aplikace z aktualiza�n�ho webu?\r\n\r\nTuto akci pou�ijte pouze v p��pad�, �e aplikaci nelze spustit.", "Vynucen� aktualizace", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);
            if (res == DialogResult.Yes)
            {
                btn_close.Enabled = false;
                fix_info.Text = "Prob�h� aktualizace V�ECH soubor�. Neukon�ujte tuto aplikaci!";
                fix_info.Visible = true;

                cfg.IniWriteValue("global", "LastVersion", "0.0.0.0");
                SB.BgAutoUpdate.BgAutoUpdate au = new SB.BgAutoUpdate.BgAutoUpdate();
                au.au_url = "http://au.ladasoukup.cz/OstroRis/";
                au.au_ico = Properties.Resources.ico_update;
                au.ForceAU = true;
                au.DoUpdate();
                
                Application.DoEvents();
                btn_close.Enabled = true;
                fix_info.Visible = false;
            }
        }

        private void fix_debug_on_Click(object sender, EventArgs e)
        {
            cfg.IniWriteValue("debug", "showlog", true.ToString());
        }

        private void fix_debug_off_Click(object sender, EventArgs e)
        {
            cfg.IniWriteValue("debug", "showlog", false.ToString());
        }
    }
}