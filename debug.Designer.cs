namespace CTK_reader
{
    partial class debug
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(debug));
            this.txt_debug = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txt_debug
            // 
            this.txt_debug.AcceptsReturn = true;
            this.txt_debug.BackColor = System.Drawing.SystemColors.Info;
            this.txt_debug.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txt_debug.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.txt_debug.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_debug.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_debug.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txt_debug.Location = new System.Drawing.Point(0, 0);
            this.txt_debug.Multiline = true;
            this.txt_debug.Name = "txt_debug";
            this.txt_debug.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txt_debug.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_debug.Size = new System.Drawing.Size(584, 346);
            this.txt_debug.TabIndex = 5;
            this.txt_debug.TabStop = false;
            this.txt_debug.TextChanged += new System.EventHandler(this.txt_debug_TextChanged);
            // 
            // debug
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 346);
            this.ControlBox = false;
            this.Controls.Add(this.txt_debug);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "debug";
            this.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Silver;
            this.Text = "debug";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.debug_FormClosing);
            this.Load += new System.EventHandler(this.debug_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.TextBox txt_debug;


    }
}