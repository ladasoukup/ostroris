using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
    public partial class debug : KryptonForm
    {
        MainForm opener;

        public debug(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void debug_Load(object sender, EventArgs e)
        {

        }

        private void debug_FormClosing(object sender, FormClosingEventArgs e)
        {
            // store window position
            try
            {
                opener.ini.IniWriteValue("debug", "width", this.Width.ToString());
                opener.ini.IniWriteValue("debug", "height", this.Height.ToString());
                opener.ini.IniWriteValue("debug", "left", this.Left.ToString());
                opener.ini.IniWriteValue("debug", "top", this.Top.ToString());

            }
            catch { }
            
            // Write LogFile
            try
            {
                StreamWriter LogFile = new StreamWriter(Application.StartupPath + "\\log_ctk_reader.log", false, System.Text.Encoding.GetEncoding("windows-1250"));
                LogFile.Write(txt_debug.Text);
                LogFile.Close();
            }
            catch
            {
                // Nepovedlo se zapsat log, ignoruj...
            }
        }

        private void txt_debug_TextChanged(object sender, EventArgs e)
        {
            int TrimLength = 20000;
            if (txt_debug.Text.Length > TrimLength)
            {
                txt_debug.Text = txt_debug.Text.Substring((txt_debug.Text.Length - TrimLength), TrimLength);
            }
        }
    }
}