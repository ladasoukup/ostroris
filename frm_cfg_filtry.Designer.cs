namespace CTK_reader
{
    partial class frm_cfg_filtry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_cfg_filtry));
            this.gb_filtr = new System.Windows.Forms.GroupBox();
            this.btn_saveFiltr = new System.Windows.Forms.Button();
            this.ctk_filtr_titulek = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.ctk_kategorie_and = new System.Windows.Forms.CheckBox();
            this.lbl_ctk_filtry_kraj = new System.Windows.Forms.Label();
            this.ctk_kraj = new System.Windows.Forms.ComboBox();
            this.ctk_filtry_and = new System.Windows.Forms.CheckBox();
            this.lbl_ctk_filtry_text = new System.Windows.Forms.Label();
            this.ctk_text = new System.Windows.Forms.TextBox();
            this.lbl_ctk_filtry_klicovaSlova = new System.Windows.Forms.Label();
            this.ctk_klicovaSlova = new System.Windows.Forms.TextBox();
            this.lbl_ctk_filtry_nadpis = new System.Windows.Forms.Label();
            this.ctk_nadpis = new System.Windows.Forms.TextBox();
            this.ctk_kategorie = new System.Windows.Forms.CheckedListBox();
            this.ctk_filtry = new System.Windows.Forms.ListBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_newFiltr = new System.Windows.Forms.Button();
            this.btn_DeleteFiltr = new System.Windows.Forms.Button();
            this.gb_filtr.SuspendLayout();
            this.SuspendLayout();
            // 
            // gb_filtr
            // 
            this.gb_filtr.Controls.Add(this.btn_saveFiltr);
            this.gb_filtr.Controls.Add(this.ctk_filtr_titulek);
            this.gb_filtr.Controls.Add(this.label1);
            this.gb_filtr.Controls.Add(this.ctk_kategorie_and);
            this.gb_filtr.Controls.Add(this.lbl_ctk_filtry_kraj);
            this.gb_filtr.Controls.Add(this.ctk_kraj);
            this.gb_filtr.Controls.Add(this.ctk_filtry_and);
            this.gb_filtr.Controls.Add(this.lbl_ctk_filtry_text);
            this.gb_filtr.Controls.Add(this.ctk_text);
            this.gb_filtr.Controls.Add(this.lbl_ctk_filtry_klicovaSlova);
            this.gb_filtr.Controls.Add(this.ctk_klicovaSlova);
            this.gb_filtr.Controls.Add(this.lbl_ctk_filtry_nadpis);
            this.gb_filtr.Controls.Add(this.ctk_nadpis);
            this.gb_filtr.Controls.Add(this.ctk_kategorie);
            this.gb_filtr.Location = new System.Drawing.Point(174, 12);
            this.gb_filtr.Name = "gb_filtr";
            this.gb_filtr.Size = new System.Drawing.Size(361, 342);
            this.gb_filtr.TabIndex = 1;
            this.gb_filtr.TabStop = false;
            this.gb_filtr.Text = "Nastaven� filtru";
            // 
            // btn_saveFiltr
            // 
            this.btn_saveFiltr.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_saveFiltr.Location = new System.Drawing.Point(244, 292);
            this.btn_saveFiltr.Name = "btn_saveFiltr";
            this.btn_saveFiltr.Size = new System.Drawing.Size(106, 25);
            this.btn_saveFiltr.TabIndex = 24;
            this.btn_saveFiltr.Text = "ULO�IT FILTR";
            this.btn_saveFiltr.UseVisualStyleBackColor = true;
            this.btn_saveFiltr.Click += new System.EventHandler(this.btn_saveFiltr_Click);
            // 
            // ctk_filtr_titulek
            // 
            this.ctk_filtr_titulek.Location = new System.Drawing.Point(74, 14);
            this.ctk_filtr_titulek.Name = "ctk_filtr_titulek";
            this.ctk_filtr_titulek.Size = new System.Drawing.Size(276, 21);
            this.ctk_filtr_titulek.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "N�zev filtru";
            // 
            // ctk_kategorie_and
            // 
            this.ctk_kategorie_and.Location = new System.Drawing.Point(6, 318);
            this.ctk_kategorie_and.Name = "ctk_kategorie_and";
            this.ctk_kategorie_and.Size = new System.Drawing.Size(80, 19);
            this.ctk_kategorie_and.TabIndex = 19;
            this.ctk_kategorie_and.Text = "z�rove�";
            // 
            // lbl_ctk_filtry_kraj
            // 
            this.lbl_ctk_filtry_kraj.Location = new System.Drawing.Point(190, 181);
            this.lbl_ctk_filtry_kraj.Name = "lbl_ctk_filtry_kraj";
            this.lbl_ctk_filtry_kraj.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_kraj.TabIndex = 18;
            this.lbl_ctk_filtry_kraj.Text = "Filtrovat podle kraje:";
            // 
            // ctk_kraj
            // 
            this.ctk_kraj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ctk_kraj.FormattingEnabled = true;
            this.ctk_kraj.Location = new System.Drawing.Point(190, 198);
            this.ctk_kraj.Name = "ctk_kraj";
            this.ctk_kraj.Size = new System.Drawing.Size(160, 21);
            this.ctk_kraj.TabIndex = 17;
            // 
            // ctk_filtry_and
            // 
            this.ctk_filtry_and.Checked = true;
            this.ctk_filtry_and.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ctk_filtry_and.Location = new System.Drawing.Point(190, 225);
            this.ctk_filtry_and.Name = "ctk_filtry_and";
            this.ctk_filtry_and.Size = new System.Drawing.Size(80, 24);
            this.ctk_filtry_and.TabIndex = 16;
            this.ctk_filtry_and.Text = "z�rove�";
            // 
            // lbl_ctk_filtry_text
            // 
            this.lbl_ctk_filtry_text.Location = new System.Drawing.Point(190, 134);
            this.lbl_ctk_filtry_text.Name = "lbl_ctk_filtry_text";
            this.lbl_ctk_filtry_text.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_text.TabIndex = 15;
            this.lbl_ctk_filtry_text.Text = "obsahuje v detailu textu";
            // 
            // ctk_text
            // 
            this.ctk_text.Location = new System.Drawing.Point(190, 150);
            this.ctk_text.Name = "ctk_text";
            this.ctk_text.Size = new System.Drawing.Size(160, 21);
            this.ctk_text.TabIndex = 14;
            // 
            // lbl_ctk_filtry_klicovaSlova
            // 
            this.lbl_ctk_filtry_klicovaSlova.Location = new System.Drawing.Point(190, 89);
            this.lbl_ctk_filtry_klicovaSlova.Name = "lbl_ctk_filtry_klicovaSlova";
            this.lbl_ctk_filtry_klicovaSlova.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_klicovaSlova.TabIndex = 13;
            this.lbl_ctk_filtry_klicovaSlova.Text = "obsahuje v kl��ov�ch slovech";
            // 
            // ctk_klicovaSlova
            // 
            this.ctk_klicovaSlova.Location = new System.Drawing.Point(190, 105);
            this.ctk_klicovaSlova.Name = "ctk_klicovaSlova";
            this.ctk_klicovaSlova.Size = new System.Drawing.Size(160, 21);
            this.ctk_klicovaSlova.TabIndex = 12;
            // 
            // lbl_ctk_filtry_nadpis
            // 
            this.lbl_ctk_filtry_nadpis.Location = new System.Drawing.Point(190, 41);
            this.lbl_ctk_filtry_nadpis.Name = "lbl_ctk_filtry_nadpis";
            this.lbl_ctk_filtry_nadpis.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_nadpis.TabIndex = 11;
            this.lbl_ctk_filtry_nadpis.Text = "obsahuje v nadpisu";
            // 
            // ctk_nadpis
            // 
            this.ctk_nadpis.Location = new System.Drawing.Point(190, 57);
            this.ctk_nadpis.Name = "ctk_nadpis";
            this.ctk_nadpis.Size = new System.Drawing.Size(160, 21);
            this.ctk_nadpis.TabIndex = 10;
            // 
            // ctk_kategorie
            // 
            this.ctk_kategorie.FormattingEnabled = true;
            this.ctk_kategorie.Location = new System.Drawing.Point(6, 41);
            this.ctk_kategorie.Name = "ctk_kategorie";
            this.ctk_kategorie.Size = new System.Drawing.Size(178, 276);
            this.ctk_kategorie.TabIndex = 0;
            // 
            // ctk_filtry
            // 
            this.ctk_filtry.FormattingEnabled = true;
            this.ctk_filtry.Location = new System.Drawing.Point(12, 12);
            this.ctk_filtry.Name = "ctk_filtry";
            this.ctk_filtry.Size = new System.Drawing.Size(156, 342);
            this.ctk_filtry.TabIndex = 20;
            this.ctk_filtry.SelectedValueChanged += new System.EventHandler(this.ctk_filtry_SelectionChanged);
            // 
            // btn_ok
            // 
            this.btn_ok.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_ok.Location = new System.Drawing.Point(429, 360);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(106, 25);
            this.btn_ok.TabIndex = 21;
            this.btn_ok.Text = "Zav��t okno";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_newFiltr
            // 
            this.btn_newFiltr.Location = new System.Drawing.Point(12, 361);
            this.btn_newFiltr.Name = "btn_newFiltr";
            this.btn_newFiltr.Size = new System.Drawing.Size(73, 23);
            this.btn_newFiltr.TabIndex = 22;
            this.btn_newFiltr.Text = "NOV�";
            this.btn_newFiltr.UseVisualStyleBackColor = true;
            this.btn_newFiltr.Click += new System.EventHandler(this.btn_newFiltr_Click);
            // 
            // btn_DeleteFiltr
            // 
            this.btn_DeleteFiltr.Location = new System.Drawing.Point(92, 361);
            this.btn_DeleteFiltr.Name = "btn_DeleteFiltr";
            this.btn_DeleteFiltr.Size = new System.Drawing.Size(76, 23);
            this.btn_DeleteFiltr.TabIndex = 23;
            this.btn_DeleteFiltr.Text = "SMAZAT";
            this.btn_DeleteFiltr.UseVisualStyleBackColor = true;
            this.btn_DeleteFiltr.Click += new System.EventHandler(this.btn_DeleteFiltr_Click);
            // 
            // frm_cfg_filtry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(544, 391);
            this.Controls.Add(this.btn_DeleteFiltr);
            this.Controls.Add(this.btn_newFiltr);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.ctk_filtry);
            this.Controls.Add(this.gb_filtr);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frm_cfg_filtry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Filtry";
            this.WindowActive = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frm_cfg_filtr_FormClosing);
            this.Load += new System.EventHandler(this.frm_cfg_filtry_Load);
            this.gb_filtr.ResumeLayout(false);
            this.gb_filtr.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gb_filtr;
        private System.Windows.Forms.CheckedListBox ctk_kategorie;
        private System.Windows.Forms.CheckBox ctk_kategorie_and;
        private System.Windows.Forms.Label lbl_ctk_filtry_kraj;
        private System.Windows.Forms.ComboBox ctk_kraj;
        private System.Windows.Forms.CheckBox ctk_filtry_and;
        private System.Windows.Forms.Label lbl_ctk_filtry_text;
        private System.Windows.Forms.TextBox ctk_text;
        private System.Windows.Forms.Label lbl_ctk_filtry_klicovaSlova;
        private System.Windows.Forms.TextBox ctk_klicovaSlova;
        private System.Windows.Forms.Label lbl_ctk_filtry_nadpis;
        private System.Windows.Forms.TextBox ctk_nadpis;
        private System.Windows.Forms.ListBox ctk_filtry;
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox ctk_filtr_titulek;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_newFiltr;
        private System.Windows.Forms.Button btn_DeleteFiltr;
        private System.Windows.Forms.Button btn_saveFiltr;
    }
}