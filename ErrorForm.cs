/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 4.8.2005
 * Time: 20:35
 * 
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Text;

namespace CTK_reader
{
	/// <summary>
	/// Description of ErrorForm.
	/// </summary>
	public class ErrorForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		public System.Windows.Forms.TextBox error_text;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label3;
		public ErrorForm()
		{
			InitializeComponent();
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ErrorForm));
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.error_text = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.label3.Location = new System.Drawing.Point(8, 275);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(624, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Aplikace pravděpodobně nebude moci pracovat normálně, proto je doporučeno aplikac" +
                "i ukončit a spustit znovu.";
            // 
            // button1
            // 
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.button1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.button1.Location = new System.Drawing.Point(8, 297);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(624, 21);
            this.button1.TabIndex = 4;
            this.button1.Text = "Ukončit aplikaci";
            this.button1.Click += new System.EventHandler(this.Button1Click);
            // 
            // error_text
            // 
            this.error_text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.error_text.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.error_text.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.error_text.Location = new System.Drawing.Point(8, 67);
            this.error_text.Multiline = true;
            this.error_text.Name = "error_text";
            this.error_text.ReadOnly = true;
            this.error_text.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.error_text.Size = new System.Drawing.Size(624, 200);
            this.error_text.TabIndex = 1;
            this.error_text.Text = "textBox1";
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(624, 22);
            this.label1.TabIndex = 0;
            this.label1.Text = "V aplikaci došlo k závažné chybě.";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 37);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(624, 22);
            this.label2.TabIndex = 2;
            this.label2.Text = "Prosím odešlete text chyby správci systému. Popis chyby byl uložen do souboru \"_e" +
                "rror.txt\" v adresáři aplikace.";
            // 
            // ErrorForm
            // 
            this.AcceptButton = this.button1;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(640, 330);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.error_text);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ErrorForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "V aplikaci došlo k závažné chybě";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ErrorFormLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		
		void Button1Click(object sender, System.EventArgs e)
		{
			Application.Exit();
		}
		
		void ErrorFormLoad(object sender, System.EventArgs e)
		{
			
		}
		
	}
}
