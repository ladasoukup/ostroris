using System.IO;
using MySQLDriverCS;
using System.Windows.Forms;
using System.Drawing;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace CTK_reader
{
    partial class MainForm
    {

        public void CTK_profileShowWnd()
        {
            CTK_profileShowWnd(false);
            LogDebug("-> CTK_profileShowWnd(false);", "SYSTEM");
        }

        public void CTK_profileShowWnd(bool AllowAutoLogin)
        {
            ctk_detail.Text = "Pros�m, p�ihla�te se do syst�mu...";
            ctk_list_timer.Enabled = false;
            top_menu_nastaveni__CTK.Enabled = false;
            top_menu_nastaveni__CTK_filters.Enabled = false;
            top_menu_nastaveni__fonts.Enabled = false;
            SBnews_panel.SelectTab(0);
            SBnews_panel.Enabled = false;

            frm_profile_select wnd = new frm_profile_select(this);
            wnd.AllowAutoLogin = AllowAutoLogin;
            wnd.Show();
        }

        public void CTK_profileLoad()
        {
            string S;
            ChangeStatusBar("Na��t�m u�ivatelsk� profil...", true);
            this.Cursor = Cursors.WaitCursor;
            Application.DoEvents();
            if (ActualProfile != null)
            {
                LogDebug("Loading user profil...", "SYSTEM");
                object[] ctk_config = null;
                object[] ctk_config_dataGrid = new object[21];
                object[] RIS_config_dataGrid = new object[21];
                object[] CustNews_config_dataGrid = new object[24];
                int counter = 0;
                int Traffic_startID = 0;

                ctk_list_Readed = new int[5000];
                ctk_list_Readed_idx = 0;
                UIcolors = new Color[8] { Color.Black, Color.White, Color.DarkRed, Color.White, Color.Black, Color.FromArgb(187, 217, 255), Color.Black, Color.Yellow };
                ctk_flag_titles = null;
                SBnews_ScratchBox.Text = "Zde si m��ete ps�t Va�e pozn�mky...";

                ctk_config = CTK_profile_LoadSerializedData();
                config_store = ctk_config; //Store config for later use...

                ctk_detail.ForeColor = UIcolors[4];
                ctk_detail.BackColor = UIcolors[5];
                SMS_detail.ForeColor = UIcolors[4];
                SMS_detail.BackColor = UIcolors[5];
                RIS_txt_detail.ForeColor = UIcolors[4];
                RIS_txt_detail.BackColor = UIcolors[5];
                CustNews_txt_detail.ForeColor = UIcolors[4];
                CustNews_txt_detail.BackColor = UIcolors[5];
                
                dg_ctk_list.RowsDefaultCellStyle.SelectionForeColor = UIcolors[6];
                dg_ctk_list.RowsDefaultCellStyle.SelectionBackColor = UIcolors[7];
                dg_ctk_list.BackgroundColor = UIcolors[1];
                dg_SMS.RowsDefaultCellStyle.SelectionForeColor = UIcolors[6];
                dg_SMS.RowsDefaultCellStyle.SelectionBackColor = UIcolors[7];
                dg_SMS.BackgroundColor = UIcolors[1];
                dg_RIS_list.RowsDefaultCellStyle.SelectionForeColor = UIcolors[6];
                dg_RIS_list.RowsDefaultCellStyle.SelectionBackColor = UIcolors[7];
                dg_RIS_list.BackgroundColor = UIcolors[1];
                dg_CustNews_list.RowsDefaultCellStyle.SelectionForeColor = UIcolors[6];
                dg_CustNews_list.RowsDefaultCellStyle.SelectionBackColor = UIcolors[7];
                dg_CustNews_list.BackgroundColor = UIcolors[1];


                // Nastav "dg_ctk_list"
                counter = 0;
                try
                {
                    ctk_config_dataGrid = (object[])ctk_config[0];
                    for (int loop = 0; loop < 7; loop++)
                    {
                        dg_ctk_list.Columns[loop].DisplayIndex = Convert.ToInt32((string)ctk_config_dataGrid[counter]);
                        counter++;
                        dg_ctk_list.Columns[loop].Width = Convert.ToInt32((string)ctk_config_dataGrid[counter]);
                        counter++;
                        S = (string)ctk_config_dataGrid[counter];
                        if ((S == "False") || (loop == 0)) { dg_ctk_list.Columns[loop].Visible = false; } else { dg_ctk_list.Columns[loop].Visible = true; }
                        counter++;
                    }

                }
                catch { }

                // Fonty
                try
                {
                    dg_ctk_list.Font = (Font)ctk_config[1];
                    dg_SMS.Font = (Font)ctk_config[1];
                    dg_RIS_list.Font = (Font)ctk_config[1];
                    dg_CustNews_list.Font = (Font)ctk_config[1];

                    ctk_detail.Font = (Font)ctk_config[2];
                    SMS_detail.Font = (Font)ctk_config[2];
                    RIS_txt_detail.Font = (Font)ctk_config[2];
                    CustNews_txt_detail.Font = (Font)ctk_config[2];
                }
                catch { }
                try
                {
                    ctk_reload_time = Convert.ToInt32((string)ctk_config[3]);
                    if (ctk_reload_time < 30) ctk_reload_time = 30; //Minimalne 30s.
                    CustNews_reload_time = ctk_reload_time;
                }
                catch
                {
                    LogDebug("\"refresh\" is not defined!", "INI");
                }
                try { ShowNewMsgNotify = (bool)ctk_config[4]; }
                catch { LogDebug("\"ShowNewMsgNotify\" is not defined!", "INI"); }

                // ctk_flag_titles
                // ctk_config[5] - REMOVED!!!


                // tab CTK Spliter
                try
                {
                    if (ctk_config[6] != null)
                    {
                        CTK_split_main.SplitterDistance = (int)ctk_config[6];
                    }
                }
                catch { LogDebug("Error loading CTK Spliter!", "PROFILE"); }

                // ScratchBox
                try
                {
                    if (ctk_config[7] != null)
                    {
                        SBnews_ScratchBox.Text = (string)ctk_config[7];
                    }
                }
                catch { LogDebug("Error loading ScratchBox text!", "PROFILE"); }

                // SBnews_panel.SelectTab
                try
                {
                    if (ctk_config[8] != null)
                    {
                        SBnews_panel.SelectTab((string)ctk_config[8]);
                    }
                }
                catch { LogDebug("Error setting \"SBnews_panel.SelectTab\" !", "PROFILE"); }

                // tab SMS Spliter
                try
                {
                    if (ctk_config[9] != null)
                    {
                        SMS_split1.SplitterDistance = (int)ctk_config[9];
                    }
                }
                catch { LogDebug("Error loading SMS Spliter!", "PROFILE"); }

                // Nastav "dg_RIS_list"
                counter = 0;
                try
                {
                    RIS_config_dataGrid = (object[])ctk_config[10];
                    for (int loop = 0; loop < 7; loop++)
                    {
                        dg_RIS_list.Columns[loop].DisplayIndex = Convert.ToInt32((string)RIS_config_dataGrid[counter]);
                        counter++;
                        dg_RIS_list.Columns[loop].Width = Convert.ToInt32((string)RIS_config_dataGrid[counter]);
                        counter++;
                        S = (string)RIS_config_dataGrid[counter];
                        if ((S == "False") || (loop == 0)) { dg_RIS_list.Columns[loop].Visible = false; } else { dg_RIS_list.Columns[loop].Visible = true; }
                        counter++;
                    }

                }
                catch { }

                // tab RIS Spliter
                try
                {
                    if (ctk_config[11] != null)
                    {
                        RIS_split_dg_detail.SplitterDistance = (int)ctk_config[11];
                    }
                }
                catch { LogDebug("Error loading RIS Spliter!", "PROFILE"); }

                // Nastav "dg_CustNews_list"
                counter = 0;
                try
                {
                    CustNews_config_dataGrid = (object[])ctk_config[12];
                    for (int loop = 0; loop < 8; loop++)
                    {
                        dg_CustNews_list.Columns[loop].DisplayIndex = Convert.ToInt32((string)CustNews_config_dataGrid[counter]);
                        counter++;
                        dg_CustNews_list.Columns[loop].Width = Convert.ToInt32((string)CustNews_config_dataGrid[counter]);
                        counter++;
                        S = (string)CustNews_config_dataGrid[counter];
                        if ((S == "False") || (loop == 0)) { dg_CustNews_list.Columns[loop].Visible = false; } else { dg_CustNews_list.Columns[loop].Visible = true; }
                        counter++;
                    }

                }
                catch { }

                // tab CustNews Spliter
                try
                {
                    if (ctk_config[13] != null)
                    {
                        CustNews_split_dg_detail.SplitterDistance = (int)ctk_config[13];
                    }
                }
                catch { LogDebug("Error loading CustNews Spliter!", "PROFILE"); }

                // CustNews_list_Readed
                try
                {
                    CustNews_list_Readed = (int[])ctk_config[14];
                    CustNews_list_Readed_idx = (int)ctk_config[15];
                }
                catch { LogDebug("Error loading CustNews readed/unreaded status!", "PROFILE"); }
                if (CustNews_list_Readed == null)
                {
                    CustNews_list_Readed = new int[5000];
                    CustNews_list_Readed_idx = 0;
                    LogDebug("reseting CustNews readed/unreaded status...", "PROFILE");
                }

                // Doprava - panels distance
                try
                {
                    if (ctk_config[16] != null)
                    {
                        int[] trafficSplit = (int[])ctk_config[16];
                        SBnews_doprava_split_main.SplitterDistance = trafficSplit[0];
                        SBnews_doprava_split_secondary.SplitterDistance = trafficSplit[1];
                    }
                }
                catch { LogDebug("Error loading traffic splitters!", "PROFILE"); }
                
                // SBnews_doprava_kraj.SelectedIndex
                try
                {
                    if (ctk_config[17] != null)
                    {
                        Traffic_startID = (int)ctk_config[17];
                    }
                }
                catch { }

                ctk_nacti_filtry();
                ctk_list_reload();
                SMS_init();
                RIS_init();
                SBnews_doprava_INIT(Traffic_startID);
                top_menu_nastaveni__CTK.Enabled = true;
                top_menu_nastaveni__CTK_filters.Enabled = true;
                top_menu_nastaveni__fonts.Enabled = true;
                SBnews_panel.Enabled = true;
            }
            ChangeStatusBar("P�ipraven", false);
            this.Cursor = Cursors.Default;
        }

        
        public void CTK_profileSave()
        {
            ChangeStatusBar("Ukl�d�m u�ivatelsk� profil...", true);
            this.Cursor = Cursors.WaitCursor;
            ctk_tab.Enabled = false;
            Application.DoEvents();
            if (ActualProfile != null)
            {
                // SAVE PROFILE
                object[] ctk_config = new object[50];
                object[] ctk_config_dataGrid = new object[21];
                object[] RIS_config_dataGrid = new object[21];
                object[] CustNews_config_dataGrid = new object[24];
                int counter = 0;
                LogDebug("Saving user profil...", "SYSTEM");
                // Add config values, that are from NEXT version!
                try
                {
                    for (int loop = 0; loop < ctk_config.Length; loop++)
                    {
                        ctk_config[loop] = config_store[loop];
                    }
                }
                catch { }
                // ctk_config_dataGrid
                counter = 0;
                for (int loop = 0; loop < 7; loop++)
                {
                    ctk_config_dataGrid[counter] = dg_ctk_list.Columns[loop].DisplayIndex.ToString();
                    counter++;
                    ctk_config_dataGrid[counter] = dg_ctk_list.Columns[loop].Width.ToString();
                    counter++;
                    ctk_config_dataGrid[counter] = dg_ctk_list.Columns[loop].Visible.ToString();
                    counter++;
                }
                // RIS_config_dataGrid
                counter = 0;
                for (int loop = 0; loop < 7; loop++)
                {
                    RIS_config_dataGrid[counter] = dg_RIS_list.Columns[loop].DisplayIndex.ToString();
                    counter++;
                    RIS_config_dataGrid[counter] = dg_RIS_list.Columns[loop].Width.ToString();
                    counter++;
                    RIS_config_dataGrid[counter] = dg_RIS_list.Columns[loop].Visible.ToString();
                    counter++;
                }
                // CustNews_config_dataGrid
                counter = 0;
                for (int loop = 0; loop < 8; loop++)
                {
                    CustNews_config_dataGrid[counter] = dg_CustNews_list.Columns[loop].DisplayIndex.ToString();
                    counter++;
                    CustNews_config_dataGrid[counter] = dg_CustNews_list.Columns[loop].Width.ToString();
                    counter++;
                    CustNews_config_dataGrid[counter] = dg_CustNews_list.Columns[loop].Visible.ToString();
                    counter++;
                }

                // Build ctk_config
                ctk_config[0] = ctk_config_dataGrid;
                ctk_config[1] = dg_ctk_list.Font;
                ctk_config[2] = ctk_detail.Font;
                ctk_config[3] = ctk_reload_time.ToString();
                ctk_config[4] = ShowNewMsgNotify;
                ctk_config[5] = ctk_flag_titles;
                ctk_config[6] = CTK_split_main.SplitterDistance;
                ctk_config[7] = SBnews_ScratchBox.Text;
                ctk_config[8] = SBnews_panel.SelectedTab.Name;
                ctk_config[9] = SMS_split1.SplitterDistance;
                ctk_config[10] = RIS_config_dataGrid;
                ctk_config[11] = RIS_split_dg_detail.SplitterDistance;
                ctk_config[12] = CustNews_config_dataGrid;
                ctk_config[13] = CustNews_split_dg_detail.SplitterDistance;
                ctk_config[14] = CustNews_list_Readed;
                ctk_config[15] = CustNews_list_Readed_idx;

                Application.DoEvents();
                ctk_config[16] = new int[2] { SBnews_doprava_split_main.SplitterDistance, SBnews_doprava_split_secondary.SplitterDistance };
                ctk_config[17] = SBnews_doprava_kraj.SelectedIndex;

                CTK_profile_SaveSerializedData(ctk_config);
            }
            ActualProfile = null;
            dg_ctk_list.Rows.Clear();
            ctk_ulozeny_filtr.Items.Clear();
            SMS_cleanup();
            RIS_cleanup();
            SBnews_doprava_DEINIT();
            ChangeStatusBar("P�ipraven", false);
            this.Cursor = Cursors.Default;
            Application.DoEvents();
            ShowAppNotify("V� profil byl �sp�n� ulo�en.", 1000, ToolTipIcon.Info);
        }

        private void CTK_profile_SaveSerializedData(object[] ctk_config)
        {
            // ULOZ CustNews_list_Readed, CustNews_list_Readed_idx, ctk_list_Readed, ctk_list_Readed_idx
            IFormatter formatter = new BinaryFormatter();

            string tmp_ComputerName = null;
            try { tmp_ComputerName = System.Net.Dns.GetHostEntry("localhost").HostName.ToString(); }
            catch { tmp_ComputerName = "-not allowed-"; }

            object[] ser_save = new object[6];
            ser_save[0] = null; // ex. FLAGS
            ser_save[1] = null; // ex. FLAGS
            ser_save[2] = ctk_list_Readed;
            ser_save[3] = ctk_list_Readed_idx;
            ser_save[4] = UIcolors;
            ser_save[5] = ctk_config;

            try
            {
                MemoryStream ser_data_MS = new MemoryStream();
                formatter.Serialize(ser_data_MS, ser_save);
                byte[] memory_array = ser_data_MS.ToArray();
                string ser_data = Convert.ToBase64String(memory_array, Base64FormattingOptions.InsertLineBreaks);
                ser_data_MS.Close();

                object[,] mysql_data = new object[,] {
                    { "ctk_profile_bin", ser_data },
                    { "ctk_last_version", Application.ProductVersion.ToString() },
                    { "ctk_last_logout", DateTime.Now.ToString() },
                    { "ctk_last_computer", tmp_ComputerName } };
                object[,] mysql_where = new object[,] { { "ctk_user", "=", ActualProfile } };

                try
                {
                    //TODO: Lepe osetrit nemoznost ulozeni profilu z duvodu nedostupnosti MySQL serveru.
                    new MySQLUpdateCommand(MYSQL, mysql_data, sql_db_table + "_profil", mysql_where, null);
                }
                catch
                {
                    try
                    {
                        MYSQL.Close();
                        Application.DoEvents();
                        MySQL_connect();
                        new MySQLUpdateCommand(MYSQL, mysql_data, sql_db_table + "_profil", mysql_where, null);
                    }
                    catch
                    {
                        LogDebug("Profil se nepoda�ilo ulo�it!", "ERROR");
                        MessageBox.Show("V� profil se nepoda�ilo ulo�it, proto�e server MySQL neodpov�d�l.", "Profil se nepoda�ilo ulo�it", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception e) { LogDebug(e.ToString(), "Serialization"); }

        }

        private object[] CTK_profile_LoadSerializedData()
        {
            IFormatter formatter = new BinaryFormatter();
            object[] ser_save = new object[5];
            object[] ctk_config = null;
            try
            {
                string ser_data = null;
                /*
                System.Data.DataTable dt = new MySQLSelectCommand(MYSQL, new string[] { "ctk_profile_bin" }, new string[] { sql_db_table + "_profil" }, new object[,] { { "ctk_user", "=", ActualProfile } }, null, null).Table;
                if (dt.Rows.Count != 0) { ser_data = dt.Rows[0]["ctk_profile_bin"] as string; }
                */


                string sql_query = "SELECT ctk_profile_bin FROM " + sql_db_table + "_profil WHERE ctk_user = '" + ActualProfile + "' LIMIT 1";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MYSQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    ser_data = sql_reader.GetString(0);
                }
                
                byte[] memory_array = (byte[])Convert.FromBase64String(ser_data);
                MemoryStream ser_data_MS = new MemoryStream();
                ser_data_MS.Write(memory_array, 0, memory_array.Length);
                ser_data_MS.Flush();
                ser_data_MS.Position = 0;
                ser_save = (object[])formatter.Deserialize(ser_data_MS);
                ser_data_MS.Close();

                //  (int[])ser_save[0];  - ex. FLAGS
                //  (int)ser_save[1]; - ex. FLAGS
                ctk_list_Readed = (int[])ser_save[2];
                ctk_list_Readed_idx = (int)ser_save[3];
                UIcolors = (Color[])ser_save[4];
                ctk_config = (object[])ser_save[5];

            }
            catch (Exception e) { LogDebug(e.ToString(), "DeSerialization"); }

            // UPDATE Last Client Version info in UserProfile
            try
            {
                string tmp_ComputerName = null;
                try { tmp_ComputerName = System.Net.Dns.GetHostEntry("localhost").HostName.ToString(); }
                catch { tmp_ComputerName = "-not allowed-"; }

                object[,] mysql_data = new object[,] {
                    { "ctk_last_version", Application.ProductVersion.ToString() },
                    { "ctk_last_computer", tmp_ComputerName } };
                object[,] mysql_where = new object[,] { { "ctk_user", "=", ActualProfile } };
                new MySQLUpdateCommand(MYSQL, mysql_data, sql_db_table + "_profil", mysql_where, null);
            }
            catch { }

            return ctk_config;
        }

    }
}