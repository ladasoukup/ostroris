namespace CTK_reader
{
    partial class CustNews_new_record
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustNews_new_record));
            this.group_custnews_main = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.group_custnews_repo = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.RIS_header_title = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_embargo_do = new System.Windows.Forms.DateTimePicker();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_nazev = new System.Windows.Forms.TextBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_kategorie = new System.Windows.Forms.ComboBox();
            this.group_custnews_misto = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_okres = new System.Windows.Forms.ComboBox();
            this.e_oblast = new System.Windows.Forms.ComboBox();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_mesto = new System.Windows.Forms.TextBox();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btn_MasterEditMode = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.btn_delete = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_vlozit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.e_id_repa = new System.Windows.Forms.TextBox();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_zacatek_repa = new System.Windows.Forms.TextBox();
            this.kryptonLabel9 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.e_konec_repa = new System.Windows.Forms.TextBox();
            this.kryptonLabel10 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.group_custnews_popis = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.e_groups = new System.Windows.Forms.CheckedListBox();
            this.e_popis = new System.Windows.Forms.TextBox();
            this.e_delka_repa = new System.Windows.Forms.MaskedTextBox();
            this.kryptonLabel11 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_main.Panel)).BeginInit();
            this.group_custnews_main.Panel.SuspendLayout();
            this.group_custnews_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_repo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_repo.Panel)).BeginInit();
            this.group_custnews_repo.Panel.SuspendLayout();
            this.group_custnews_repo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title.Panel)).BeginInit();
            this.RIS_header_title.Panel.SuspendLayout();
            this.RIS_header_title.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_misto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_misto.Panel)).BeginInit();
            this.group_custnews_misto.Panel.SuspendLayout();
            this.group_custnews_misto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_MasterEditMode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_vlozit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_popis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_popis.Panel)).BeginInit();
            this.group_custnews_popis.Panel.SuspendLayout();
            this.group_custnews_popis.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel11)).BeginInit();
            this.SuspendLayout();
            // 
            // group_custnews_main
            // 
            this.group_custnews_main.DirtyPaletteCounter = 2;
            this.group_custnews_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.group_custnews_main.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.group_custnews_main.Location = new System.Drawing.Point(0, 0);
            this.group_custnews_main.Name = "group_custnews_main";
            // 
            // group_custnews_main.Panel
            // 
            this.group_custnews_main.Panel.Controls.Add(this.group_custnews_popis);
            this.group_custnews_main.Panel.Controls.Add(this.group_custnews_repo);
            this.group_custnews_main.Panel.Controls.Add(this.RIS_header_title);
            this.group_custnews_main.Panel.Controls.Add(this.group_custnews_misto);
            this.group_custnews_main.Panel.Controls.Add(this.btn_MasterEditMode);
            this.group_custnews_main.Panel.Controls.Add(this.btn_delete);
            this.group_custnews_main.Panel.Controls.Add(this.btn_vlozit);
            this.group_custnews_main.Size = new System.Drawing.Size(633, 598);
            this.group_custnews_main.TabIndex = 0;
            this.group_custnews_main.Text = "Vlo�it novou zpr�vu";
            this.group_custnews_main.ValuesPrimary.Description = "Vlastn� zpr�vy";
            this.group_custnews_main.ValuesPrimary.Heading = "Vlo�it novou zpr�vu";
            this.group_custnews_main.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.copy;
            this.group_custnews_main.ValuesSecondary.Description = "kl��ov� slova";
            this.group_custnews_main.ValuesSecondary.Heading = "";
            this.group_custnews_main.ValuesSecondary.Image = null;
            // 
            // group_custnews_repo
            // 
            this.group_custnews_repo.DirtyPaletteCounter = 5;
            this.group_custnews_repo.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_custnews_repo.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.group_custnews_repo.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.group_custnews_repo.HeaderVisibleSecondary = false;
            this.group_custnews_repo.Location = new System.Drawing.Point(0, 185);
            this.group_custnews_repo.Name = "group_custnews_repo";
            // 
            // group_custnews_repo.Panel
            // 
            this.group_custnews_repo.Panel.Controls.Add(this.kryptonLabel11);
            this.group_custnews_repo.Panel.Controls.Add(this.e_delka_repa);
            this.group_custnews_repo.Panel.Controls.Add(this.e_konec_repa);
            this.group_custnews_repo.Panel.Controls.Add(this.kryptonLabel10);
            this.group_custnews_repo.Panel.Controls.Add(this.e_zacatek_repa);
            this.group_custnews_repo.Panel.Controls.Add(this.kryptonLabel9);
            this.group_custnews_repo.Panel.Controls.Add(this.kryptonLabel5);
            this.group_custnews_repo.Panel.Controls.Add(this.e_id_repa);
            this.group_custnews_repo.Panel.Controls.Add(this.kryptonLabel3);
            this.group_custnews_repo.Size = new System.Drawing.Size(631, 114);
            this.group_custnews_repo.TabIndex = 30;
            this.group_custnews_repo.Text = "Repo";
            this.group_custnews_repo.ValuesPrimary.Description = "";
            this.group_custnews_repo.ValuesPrimary.Heading = "Repo";
            this.group_custnews_repo.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.CustNews_repo;
            this.group_custnews_repo.ValuesSecondary.Description = "";
            this.group_custnews_repo.ValuesSecondary.Heading = "Description";
            this.group_custnews_repo.ValuesSecondary.Image = null;
            // 
            // RIS_header_title
            // 
            this.RIS_header_title.DirtyPaletteCounter = 5;
            this.RIS_header_title.Dock = System.Windows.Forms.DockStyle.Top;
            this.RIS_header_title.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.RIS_header_title.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.RIS_header_title.HeaderVisibleSecondary = false;
            this.RIS_header_title.Location = new System.Drawing.Point(0, 91);
            this.RIS_header_title.Name = "RIS_header_title";
            // 
            // RIS_header_title.Panel
            // 
            this.RIS_header_title.Panel.Controls.Add(this.e_embargo_do);
            this.RIS_header_title.Panel.Controls.Add(this.kryptonLabel2);
            this.RIS_header_title.Panel.Controls.Add(this.e_nazev);
            this.RIS_header_title.Panel.Controls.Add(this.kryptonLabel4);
            this.RIS_header_title.Panel.Controls.Add(this.kryptonLabel1);
            this.RIS_header_title.Panel.Controls.Add(this.e_kategorie);
            this.RIS_header_title.Size = new System.Drawing.Size(631, 94);
            this.RIS_header_title.TabIndex = 20;
            this.RIS_header_title.Text = "Informace o akci";
            this.RIS_header_title.ValuesPrimary.Description = "";
            this.RIS_header_title.ValuesPrimary.Heading = "Informace o akci";
            this.RIS_header_title.ValuesPrimary.Image = null;
            this.RIS_header_title.ValuesSecondary.Description = "";
            this.RIS_header_title.ValuesSecondary.Heading = "Description";
            this.RIS_header_title.ValuesSecondary.Image = null;
            // 
            // e_embargo_do
            // 
            this.e_embargo_do.CalendarTitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.e_embargo_do.CustomFormat = "dd.MMMM yyyy   -   HH:mm:ss";
            this.e_embargo_do.DropDownAlign = System.Windows.Forms.LeftRightAlignment.Right;
            this.e_embargo_do.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.e_embargo_do.Location = new System.Drawing.Point(411, 40);
            this.e_embargo_do.MinDate = new System.DateTime(2007, 1, 1, 0, 0, 0, 0);
            this.e_embargo_do.Name = "e_embargo_do";
            this.e_embargo_do.Size = new System.Drawing.Size(205, 20);
            this.e_embargo_do.TabIndex = 23;
            this.e_embargo_do.ValueChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.DirtyPaletteCounter = 6;
            this.kryptonLabel2.Location = new System.Drawing.Point(316, 41);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(89, 19);
            this.kryptonLabel2.TabIndex = 33;
            this.kryptonLabel2.Text = "Embargovat do:";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "Embargovat do:";
            // 
            // e_nazev
            // 
            this.e_nazev.Location = new System.Drawing.Point(85, 12);
            this.e_nazev.Name = "e_nazev";
            this.e_nazev.Size = new System.Drawing.Size(531, 20);
            this.e_nazev.TabIndex = 21;
            this.e_nazev.TextChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.DirtyPaletteCounter = 5;
            this.kryptonLabel4.Location = new System.Drawing.Point(10, 14);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(69, 19);
            this.kryptonLabel4.TabIndex = 0;
            this.kryptonLabel4.Text = "N�zev akce:";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "N�zev akce:";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.DirtyPaletteCounter = 5;
            this.kryptonLabel1.Location = new System.Drawing.Point(18, 41);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(61, 19);
            this.kryptonLabel1.TabIndex = 6;
            this.kryptonLabel1.Text = "Kategorie:";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "Kategorie:";
            // 
            // e_kategorie
            // 
            this.e_kategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.e_kategorie.FormattingEnabled = true;
            this.e_kategorie.Location = new System.Drawing.Point(85, 39);
            this.e_kategorie.Name = "e_kategorie";
            this.e_kategorie.Size = new System.Drawing.Size(222, 21);
            this.e_kategorie.TabIndex = 22;
            this.e_kategorie.SelectedIndexChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // group_custnews_misto
            // 
            this.group_custnews_misto.DirtyPaletteCounter = 5;
            this.group_custnews_misto.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_custnews_misto.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.group_custnews_misto.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.group_custnews_misto.HeaderVisibleSecondary = false;
            this.group_custnews_misto.Location = new System.Drawing.Point(0, 0);
            this.group_custnews_misto.Name = "group_custnews_misto";
            // 
            // group_custnews_misto.Panel
            // 
            this.group_custnews_misto.Panel.Controls.Add(this.e_okres);
            this.group_custnews_misto.Panel.Controls.Add(this.e_oblast);
            this.group_custnews_misto.Panel.Controls.Add(this.kryptonLabel8);
            this.group_custnews_misto.Panel.Controls.Add(this.kryptonLabel7);
            this.group_custnews_misto.Panel.Controls.Add(this.e_mesto);
            this.group_custnews_misto.Panel.Controls.Add(this.kryptonLabel6);
            this.group_custnews_misto.Size = new System.Drawing.Size(631, 91);
            this.group_custnews_misto.TabIndex = 10;
            this.group_custnews_misto.Text = "M�sto kon�n�";
            this.group_custnews_misto.ValuesPrimary.Description = "";
            this.group_custnews_misto.ValuesPrimary.Heading = "M�sto kon�n�";
            this.group_custnews_misto.ValuesPrimary.Image = null;
            this.group_custnews_misto.ValuesSecondary.Description = "";
            this.group_custnews_misto.ValuesSecondary.Heading = "Description";
            this.group_custnews_misto.ValuesSecondary.Image = null;
            // 
            // e_okres
            // 
            this.e_okres.FormattingEnabled = true;
            this.e_okres.Items.AddRange(new object[] {
            "-zvolte-"});
            this.e_okres.Location = new System.Drawing.Point(363, 11);
            this.e_okres.Name = "e_okres";
            this.e_okres.Size = new System.Drawing.Size(250, 21);
            this.e_okres.TabIndex = 12;
            this.e_okres.Text = "-zvolte-";
            this.e_okres.SelectedIndexChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // e_oblast
            // 
            this.e_oblast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.e_oblast.FormattingEnabled = true;
            this.e_oblast.Items.AddRange(new object[] {
            "-zvolte-"});
            this.e_oblast.Location = new System.Drawing.Point(57, 11);
            this.e_oblast.Name = "e_oblast";
            this.e_oblast.Size = new System.Drawing.Size(250, 21);
            this.e_oblast.TabIndex = 11;
            this.e_oblast.SelectedIndexChanged += new System.EventHandler(this.e_oblast_SelectedIndexChanged);
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.DirtyPaletteCounter = 5;
            this.kryptonLabel8.Location = new System.Drawing.Point(7, 11);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.Size = new System.Drawing.Size(45, 19);
            this.kryptonLabel8.TabIndex = 14;
            this.kryptonLabel8.Text = "Oblast:";
            this.kryptonLabel8.Values.ExtraText = "";
            this.kryptonLabel8.Values.Image = null;
            this.kryptonLabel8.Values.Text = "Oblast:";
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.DirtyPaletteCounter = 5;
            this.kryptonLabel7.Location = new System.Drawing.Point(316, 11);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.Size = new System.Drawing.Size(41, 19);
            this.kryptonLabel7.TabIndex = 7;
            this.kryptonLabel7.Text = "Okres:";
            this.kryptonLabel7.Values.ExtraText = "";
            this.kryptonLabel7.Values.Image = null;
            this.kryptonLabel7.Values.Text = "Okres:";
            // 
            // e_mesto
            // 
            this.e_mesto.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.e_mesto.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.e_mesto.Location = new System.Drawing.Point(57, 38);
            this.e_mesto.Name = "e_mesto";
            this.e_mesto.Size = new System.Drawing.Size(250, 20);
            this.e_mesto.TabIndex = 13;
            this.e_mesto.TextChanged += new System.EventHandler(this.data_changed__rebuild_keywords);
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.DirtyPaletteCounter = 5;
            this.kryptonLabel6.Location = new System.Drawing.Point(7, 38);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(44, 19);
            this.kryptonLabel6.TabIndex = 9;
            this.kryptonLabel6.Text = "M�sto:";
            this.kryptonLabel6.Values.ExtraText = "";
            this.kryptonLabel6.Values.Image = null;
            this.kryptonLabel6.Values.Text = "M�sto:";
            // 
            // btn_MasterEditMode
            // 
            this.btn_MasterEditMode.ButtonStyle = ComponentFactory.Krypton.Toolkit.ButtonStyle.Cluster;
            this.btn_MasterEditMode.DirtyPaletteCounter = 8;
            this.btn_MasterEditMode.Location = new System.Drawing.Point(8, 518);
            this.btn_MasterEditMode.Name = "btn_MasterEditMode";
            this.btn_MasterEditMode.Size = new System.Drawing.Size(157, 25);
            this.btn_MasterEditMode.TabIndex = 120;
            this.btn_MasterEditMode.Text = "Administr�torsk� m�d";
            this.btn_MasterEditMode.Values.ExtraText = "";
            this.btn_MasterEditMode.Values.Image = null;
            this.btn_MasterEditMode.Values.Text = "Administr�torsk� m�d";
            this.btn_MasterEditMode.Click += new System.EventHandler(this.btn_MasterEditMode_Click);
            // 
            // btn_delete
            // 
            this.btn_delete.DirtyPaletteCounter = 5;
            this.btn_delete.Enabled = false;
            this.btn_delete.Location = new System.Drawing.Point(374, 518);
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.Size = new System.Drawing.Size(100, 25);
            this.btn_delete.TabIndex = 110;
            this.btn_delete.Text = "Zru�it";
            this.btn_delete.Values.ExtraText = "";
            this.btn_delete.Values.Image = null;
            this.btn_delete.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_delete.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_delete.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_delete.Values.Text = "Zru�it";
            this.btn_delete.Click += new System.EventHandler(this.btn_delete_Click);
            // 
            // btn_vlozit
            // 
            this.btn_vlozit.DirtyPaletteCounter = 5;
            this.btn_vlozit.Location = new System.Drawing.Point(524, 518);
            this.btn_vlozit.Name = "btn_vlozit";
            this.btn_vlozit.Size = new System.Drawing.Size(100, 25);
            this.btn_vlozit.TabIndex = 100;
            this.btn_vlozit.Text = "Vlo�it";
            this.btn_vlozit.Values.ExtraText = "";
            this.btn_vlozit.Values.Image = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_vlozit.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_vlozit.Values.Text = "Vlo�it";
            this.btn_vlozit.Click += new System.EventHandler(this.btn_vlozit_Click);
            // 
            // e_id_repa
            // 
            this.e_id_repa.Location = new System.Drawing.Point(85, 10);
            this.e_id_repa.Name = "e_id_repa";
            this.e_id_repa.Size = new System.Drawing.Size(222, 20);
            this.e_id_repa.TabIndex = 31;
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.DirtyPaletteCounter = 6;
            this.kryptonLabel3.Location = new System.Drawing.Point(21, 11);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(58, 19);
            this.kryptonLabel3.TabIndex = 32;
            this.kryptonLabel3.Text = "K�d repa:";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "K�d repa:";
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.DirtyPaletteCounter = 7;
            this.kryptonLabel5.Location = new System.Drawing.Point(316, 11);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(66, 19);
            this.kryptonLabel5.TabIndex = 34;
            this.kryptonLabel5.Text = "D�lka repa:";
            this.kryptonLabel5.Values.ExtraText = "";
            this.kryptonLabel5.Values.Image = null;
            this.kryptonLabel5.Values.Text = "D�lka repa:";
            // 
            // e_zacatek_repa
            // 
            this.e_zacatek_repa.Location = new System.Drawing.Point(85, 36);
            this.e_zacatek_repa.Name = "e_zacatek_repa";
            this.e_zacatek_repa.Size = new System.Drawing.Size(531, 20);
            this.e_zacatek_repa.TabIndex = 33;
            // 
            // kryptonLabel9
            // 
            this.kryptonLabel9.DirtyPaletteCounter = 6;
            this.kryptonLabel9.Location = new System.Drawing.Point(28, 38);
            this.kryptonLabel9.Name = "kryptonLabel9";
            this.kryptonLabel9.Size = new System.Drawing.Size(51, 19);
            this.kryptonLabel9.TabIndex = 36;
            this.kryptonLabel9.Text = "Za��tek:";
            this.kryptonLabel9.Values.ExtraText = "";
            this.kryptonLabel9.Values.Image = null;
            this.kryptonLabel9.Values.Text = "Za��tek:";
            // 
            // e_konec_repa
            // 
            this.e_konec_repa.Location = new System.Drawing.Point(85, 62);
            this.e_konec_repa.Name = "e_konec_repa";
            this.e_konec_repa.Size = new System.Drawing.Size(531, 20);
            this.e_konec_repa.TabIndex = 34;
            // 
            // kryptonLabel10
            // 
            this.kryptonLabel10.DirtyPaletteCounter = 6;
            this.kryptonLabel10.Location = new System.Drawing.Point(36, 63);
            this.kryptonLabel10.Name = "kryptonLabel10";
            this.kryptonLabel10.Size = new System.Drawing.Size(43, 19);
            this.kryptonLabel10.TabIndex = 38;
            this.kryptonLabel10.Text = "Konec:";
            this.kryptonLabel10.Values.ExtraText = "";
            this.kryptonLabel10.Values.Image = null;
            this.kryptonLabel10.Values.Text = "Konec:";
            // 
            // group_custnews_popis
            // 
            this.group_custnews_popis.DirtyPaletteCounter = 5;
            this.group_custnews_popis.Dock = System.Windows.Forms.DockStyle.Top;
            this.group_custnews_popis.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.TabHighProfile;
            this.group_custnews_popis.HeaderStylePrimary = ComponentFactory.Krypton.Toolkit.HeaderStyle.Secondary;
            this.group_custnews_popis.HeaderVisibleSecondary = false;
            this.group_custnews_popis.Location = new System.Drawing.Point(0, 299);
            this.group_custnews_popis.Name = "group_custnews_popis";
            // 
            // group_custnews_popis.Panel
            // 
            this.group_custnews_popis.Panel.Controls.Add(this.e_groups);
            this.group_custnews_popis.Panel.Controls.Add(this.e_popis);
            this.group_custnews_popis.Size = new System.Drawing.Size(631, 213);
            this.group_custnews_popis.TabIndex = 40;
            this.group_custnews_popis.Text = "Text zpr�vy";
            this.group_custnews_popis.ValuesPrimary.Description = "Okruhy";
            this.group_custnews_popis.ValuesPrimary.Heading = "Text zpr�vy";
            this.group_custnews_popis.ValuesPrimary.Image = null;
            this.group_custnews_popis.ValuesSecondary.Description = "";
            this.group_custnews_popis.ValuesSecondary.Heading = "Description";
            this.group_custnews_popis.ValuesSecondary.Image = null;
            // 
            // e_groups
            // 
            this.e_groups.CheckOnClick = true;
            this.e_groups.FormattingEnabled = true;
            this.e_groups.Location = new System.Drawing.Point(479, 3);
            this.e_groups.Name = "e_groups";
            this.e_groups.Size = new System.Drawing.Size(144, 184);
            this.e_groups.TabIndex = 42;
            // 
            // e_popis
            // 
            this.e_popis.Location = new System.Drawing.Point(2, 3);
            this.e_popis.Multiline = true;
            this.e_popis.Name = "e_popis";
            this.e_popis.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.e_popis.Size = new System.Drawing.Size(471, 184);
            this.e_popis.TabIndex = 41;
            // 
            // e_delka_repa
            // 
            this.e_delka_repa.Location = new System.Drawing.Point(388, 11);
            this.e_delka_repa.Mask = "00:00";
            this.e_delka_repa.Name = "e_delka_repa";
            this.e_delka_repa.Size = new System.Drawing.Size(57, 20);
            this.e_delka_repa.TabIndex = 32;
            // 
            // kryptonLabel11
            // 
            this.kryptonLabel11.DirtyPaletteCounter = 8;
            this.kryptonLabel11.Location = new System.Drawing.Point(451, 11);
            this.kryptonLabel11.Name = "kryptonLabel11";
            this.kryptonLabel11.Size = new System.Drawing.Size(102, 19);
            this.kryptonLabel11.TabIndex = 41;
            this.kryptonLabel11.Text = "(minuty : sekundy)";
            this.kryptonLabel11.Values.ExtraText = "";
            this.kryptonLabel11.Values.Image = null;
            this.kryptonLabel11.Values.Text = "(minuty : sekundy)";
            // 
            // CustNews_new_record
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(633, 598);
            this.Controls.Add(this.group_custnews_main);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "CustNews_new_record";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vlastn� zpr�vy";
            this.TextExtra = "";
            this.Load += new System.EventHandler(this.CustNews_new_record_Load);
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_main.Panel)).EndInit();
            this.group_custnews_main.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_main)).EndInit();
            this.group_custnews_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_repo.Panel)).EndInit();
            this.group_custnews_repo.Panel.ResumeLayout(false);
            this.group_custnews_repo.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_repo)).EndInit();
            this.group_custnews_repo.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title.Panel)).EndInit();
            this.RIS_header_title.Panel.ResumeLayout(false);
            this.RIS_header_title.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_header_title)).EndInit();
            this.RIS_header_title.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_misto.Panel)).EndInit();
            this.group_custnews_misto.Panel.ResumeLayout(false);
            this.group_custnews_misto.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_misto)).EndInit();
            this.group_custnews_misto.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_MasterEditMode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_vlozit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_popis.Panel)).EndInit();
            this.group_custnews_popis.Panel.ResumeLayout(false);
            this.group_custnews_popis.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.group_custnews_popis)).EndInit();
            this.group_custnews_popis.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup group_custnews_main;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton btn_MasterEditMode;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_delete;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_vlozit;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup group_custnews_misto;
        private System.Windows.Forms.ComboBox e_okres;
        private System.Windows.Forms.ComboBox e_oblast;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private System.Windows.Forms.TextBox e_mesto;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_header_title;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private System.Windows.Forms.TextBox e_nazev;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private System.Windows.Forms.ComboBox e_kategorie;
        private System.Windows.Forms.DateTimePicker e_embargo_do;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup group_custnews_repo;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        private System.Windows.Forms.TextBox e_id_repa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private System.Windows.Forms.TextBox e_konec_repa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel10;
        private System.Windows.Forms.TextBox e_zacatek_repa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel9;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup group_custnews_popis;
        private System.Windows.Forms.CheckedListBox e_groups;
        private System.Windows.Forms.TextBox e_popis;
        private System.Windows.Forms.MaskedTextBox e_delka_repa;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel11;
    }
}