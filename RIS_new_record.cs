using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
using MySQLDriverCS;

namespace CTK_reader
{
    //public partial class RIS_new_record : Form
    public partial class RIS_new_record : KryptonForm
    {
        MainForm opener;
        public bool EditMode = false;
        public bool MasterEditMode = false;
        public string EditMode_id = "0";
        private string baseID = "";
        private int e_nazev_VER = 0;
        private int e_nazev_VER_new = 1;
        private string edit_base_nazev = "";
        private string edit_base_vlozil = "";

        public RIS_new_record(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void RIS_new_record_Load(object sender, EventArgs e)
        {
            string ctk_sql = "";
            btn_delete.Visible = false;
            btn_MasterEditMode.Visible = false;

            // kategorie
            e_kategorie.Items.Clear();
            e_kategorie.Items.Add("-zvolte-");
            for (int loop = 1; loop < opener.RIS_sel_kategorie.Items.Count; loop++)
            {
                e_kategorie.Items.Add(opener.RIS_sel_kategorie.Items[loop]);
            }
            try { e_kategorie.SelectedIndex = 0; }
            catch { }

            // okruhy
            e_groups.Items.Clear();
            for (int loop = 1; loop < opener.RIS_sel_group.Items.Count; loop++)
            {
                e_groups.Items.Add(opener.RIS_sel_group.Items[loop]);
            }

            // oblasti
            e_oblast.Items.Clear();
            e_oblast.Items.Add("-zvolte-");
            for (int loop = 1; loop < opener.RIS_sel_oblast.Items.Count; loop++)
            {
                e_oblast.Items.Add(opener.RIS_sel_oblast.Items[loop]);
            }
            e_oblast.Items.Add("-jin�-");
            try { e_oblast.SelectedIndex = 0; }
            catch { }

            // mesto - AutoComplete
            ctk_sql = "SELECT DISTINCT ris_mesto FROM " + opener.sql_db_table + "_ris ORDER BY ris_mesto ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                e_mesto.AutoCompleteCustomSource.Clear();
                while (ctk_reader.Read())
                {
                    e_mesto.AutoCompleteCustomSource.Add(ctk_reader[0].ToString());
                }
            }
            catch { }

            try
            {
                if (EditMode) RIS_new_record_Load_editmode();
            }
            catch (Exception ex)
            {
                opener.LogDebug(ex.ToString(), "Error");
                MessageBox.Show("Do�lo k nezn�m� chyb�\r\nEditace nem��e b�t provedena.", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Error);
                this.Close();
            }
        }

        private bool RIS_edit_checkRights()
        {
            bool ret = false;
            string sql_query = "";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            sql_query = "SELECT DISTINCT(vlozil) FROM " + opener.sql_db_table + "_ris WHERE (id=" + EditMode_id + " OR base_id=" + EditMode_id + ")";
            sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
            sql_reader = sql_cmd.ExecuteReaderEx();
            opener.LogDebug(sql_query, "SQL");
            while (sql_reader.Read())
            {
                if (sql_reader.GetString(0).ToString() == opener.ActualProfile)
                {
                    ret = true;
                }
            }
            if (ret == false) ret = RIS_edit_checkMasterRights();
            return ret;
        }

        private bool RIS_edit_checkMasterRights()
        {
            bool ret = false;
            string MasterEdit = opener.ini.IniReadEncValue("RIS", "MasterEdit");
            string[] A;

            if (MasterEdit != null)
            {
                A = MasterEdit.Split(',');
                foreach (string MasterEditor in A)
                {
                    if (MasterEditor == opener.ActualProfile)
                    {
                        ret = true;
                    }
                    
                    if (MasterEditor == "***ALL***")
                    {
                        ret = true;
                    }
                }
            }
            return ret;
        }


        private void RIS_new_record_Load_editmode()
        {
            bool HaltEditor = false;
            string S;
            string[] A;
            string sql_query = "";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            // set values and blosk fields...
            e_datum_do.Enabled = false;
            e_datum_od.Enabled = false;
            //e_groups.Enabled = false;
            e_kategorie.Enabled = false;
            e_mesto.Enabled = false;
            e_misto.Enabled = false;
            e_nazev.Enabled = false;
            e_oblast.Enabled = false;
            e_okres.Enabled = false;
            btn_delete.Enabled = true;
            btn_delete.Visible = true;
            RIS_new_header.ValuesPrimary.Heading = "Editace z�znamu";


            bool CanEdit = RIS_edit_checkRights();

            if (CanEdit == false)
            {
                MessageBox.Show("Pro editaci t�to zpr�vy nem�te dostate�n� pr�va.\r\nB�n� u�ivatel m��e editovat jen sv� vlastn� zpr�vy.", "Nem�te pr�va editovat tuto zpr�vu", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                Application.DoEvents();
                this.Close();
            }
            else
            {
                if (RIS_edit_checkMasterRights()) { btn_MasterEditMode.Visible = true; }

                sql_query = "SELECT base_id FROM " + opener.sql_db_table + "_ris WHERE id=" + EditMode_id + " LIMIT 1";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                opener.LogDebug(sql_query, "SQL");
                while (sql_reader.Read())
                {
                    try { baseID = sql_reader.GetInt32(0).ToString(); }
                    catch { baseID = EditMode_id; }
                }

                sql_query = "SELECT * FROM " + opener.sql_db_table + "_ris WHERE base_id=" + baseID + " ORDER BY verze DESC LIMIT 1";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();


                opener.LogDebug(sql_query, "SQL");
                while (sql_reader.Read())
                {
                    try { e_nazev_VER = sql_reader.GetInt32(21); }
                    catch { e_nazev_VER = 1; }
                    e_nazev_VER_new = e_nazev_VER + 1;

                    if (e_nazev_VER_new > 9999)
                    {
                        if (RIS_edit_checkMasterRights() == true)
                        {
                            MessageBox.Show("Pr�v� editujete zru�enou zpr�vu.\r\nAby byla editace �sp�n�, je vhodn� se p�epnout do m�du Administr�tora!", "Editace zru�en� ud�losti", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                            HaltEditor = false;
                        }
                        else
                        {
                            // AKCE ZRUSENA
                            MessageBox.Show("Tuto akci nem��ete editovat - akce byla zru�ena!", "Akce zru�ena", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                            HaltEditor = true;
                            this.Close();
                        }
                    }


                    if (HaltEditor == false)
                    {
                        // e_groups -1-
                        S = sql_reader.GetString(1);
                        A = S.Split('*');

                        int I = e_groups.Items.Count;
                        for (int loop = 0; loop < I; loop++)
                        {
                            foreach (string item in A)
                            {
                                if (item == e_groups.Items[loop].ToString()) e_groups.SetItemChecked(loop, true);
                            }
                        }


                        S = sql_reader.GetString(2);
                        e_datum_od.Value = new DateTime(Convert.ToInt32(S.Substring(0, 4)), Convert.ToInt32(S.Substring(4, 2)), Convert.ToInt32(S.Substring(6, 2)));
                        S = sql_reader.GetString(3);
                        e_datum_do.Value = new DateTime(Convert.ToInt32(S.Substring(0, 4)), Convert.ToInt32(S.Substring(4, 2)), Convert.ToInt32(S.Substring(6, 2)));

                        S = sql_reader.GetString(4);
                        e_oblast.SelectedItem = S;
                        if (e_oblast.SelectedItem.ToString() != S) e_oblast.Enabled = true;

                        if (e_oblast.SelectedItem.ToString() != "-jin�-")
                        {
                            S = sql_reader.GetString(5);
                            e_okres.SelectedItem = S;
                            if (e_okres.SelectedItem.ToString() != S)
                            {
                                e_okres.Items.Add(S);
                                e_okres.SelectedItem = S;
                            }
                        }
                        else
                        {
                            S = sql_reader.GetString(5);
                            e_okres.Items.Add(S);
                            e_okres.SelectedItem = S;
                            if (e_okres.SelectedItem.ToString() != S)
                            {
                                e_okres.Items.Add(S);
                                e_okres.SelectedItem = S;
                            }
                        }



                        e_mesto.Text = sql_reader.GetString(6);
                        e_misto.Text = sql_reader.GetString(7);

                        S = sql_reader.GetString(9);
                        e_kategorie.SelectedItem = S;
                        if (e_kategorie.SelectedItem.ToString() != S) e_kategorie.Enabled = true;

                        S = sql_reader.GetString(10);
                        A = S.Split('|');
                        edit_base_nazev = A[0].Trim();
                        S = edit_base_nazev + " | " + e_nazev_VER_new + ". verze";
                        e_nazev.Text = S;
                        
                        e_popis.Text = sql_reader.GetString(11);
                        if (sql_reader.GetInt32(13) > 0) e_medialni_partner.Checked = true;
                        if (sql_reader.GetInt32(14) > 0) e_promo_vysilani.Checked = true;

                        edit_base_vlozil = sql_reader.GetString(17);
                    }
                }
                sql_reader.Close();
            }
        }

        private void e_oblast_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ctk_sql;
            ctk_sql = "SELECT DISTINCT okres FROM " + opener.sql_db_table + "_ris_oblasti WHERE oblast='" + e_oblast.Text + "' ORDER BY okres ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                e_okres.Items.Clear();
                e_okres.Items.Add("-zvolte-");
                while (ctk_reader.Read())
                {
                    e_okres.Items.Add(ctk_reader[0].ToString());
                }
                try { e_okres.SelectedIndex = 0; }
                catch { }
            }
            catch { }
            Keywords_create();
        }

        private string Keywords_create()
        {
            string ret = "";
            string mp_anone = "";
            string datum = "";
            string S_nazev = "";

            if (e_medialni_partner.Checked == true) mp_anone = ", medi�ln� partner";
            datum = e_datum_od.Value.ToLongDateString();

            S_nazev = e_nazev.Text;
            string[] A = S_nazev.Split('|');
            S_nazev = A[0].Trim();

            ret = e_oblast.Text + ", " + e_mesto.Text + ", " + S_nazev + ", " + datum + ", " + e_kategorie.Text + mp_anone;


            RIS_new_header.ValuesSecondary.Heading = ret;
            return ret;
        }

        private string RIS_getDatum(int mode)
        {
            string ret = "";
            string datum1 = "";
            string datum2 = "";

            datum1 = e_datum_od.Value.Year.ToString("0000") + e_datum_od.Value.Month.ToString("00") + e_datum_od.Value.Day.ToString("00");
            datum2 = e_datum_do.Value.Year.ToString("0000") + e_datum_do.Value.Month.ToString("00") + e_datum_do.Value.Day.ToString("00");

            if (mode == 0)
            {
                if (datum1 != datum2)
                {
                    ret = datum1 + " - " + datum2;
                }
                else
                {
                    ret = datum1;
                }
            }
            else if (mode == 1)
            {
                ret = datum1;
            }
            else if (mode == 2)
            {
                ret = datum2;
            }
            else
            {
                ret = "----";
            }

            return ret;
        }

        private int CheckValues()
        {
            int ret = 0;
            string keywords = Keywords_create();
            string sql_query = "";
            MySQLCommand sql_cmd;
            MySQLDataReader sql_reader;

            e_groups.BackColor = Color.White;
            e_popis.BackColor = Color.White;
            e_kategorie.BackColor = Color.White;
            e_nazev.BackColor = Color.White;
            e_misto.BackColor = Color.White;
            e_mesto.BackColor = Color.White;
            e_okres.BackColor = Color.White;
            e_oblast.BackColor = Color.White;

            if (e_groups.CheckedItems.Count < 1) ret = 8;
            if (e_popis.Text == "") ret = 7;
            if (e_kategorie.Text == "-zvolte-") ret = 6;
            if (e_nazev.Text == "") ret = 5;
            if (e_misto.Text == "") ret = 4;
            if (e_mesto.Text == "") ret = 3;
            if (e_okres.Text == "-zvolte-") ret = 2;
            if (e_oblast.Text == "-zvolte-") ret = 1;

            if (EditMode == false)
            {
                // check KeyWords
                sql_query = "SELECT DISTINCT(ris_klicova_slova) FROM " + opener.sql_db_table + "_ris WHERE (id!=" + EditMode_id + " OR base_id!=" + EditMode_id + ")";
                sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                opener.LogDebug(sql_query, "SQL");
                while (sql_reader.Read())
                {
                    if (keywords == sql_reader.GetString(0))
                    {
                        DialogResult question = MessageBox.Show("V datab�zi byla nalezena ud�lost, kter� m� stejn� kl��ov� slova!\r\nMohlo by se jednat o tu samou ud�lost / akci.\r\n\r\nOpravdu chcete ud�lost vlo�it do datab�ze?", "Duplicita", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (question == DialogResult.No) ret = 10;
                    }
                }
            }

            return ret;
        }

        private void btn_vlozit_Click(object sender, EventArgs e)
        {
            int RetCode = RIS_saveRecord();
            if (RetCode == 0)
            {
                opener.ShowAppNotify("Akce byla �sp�n� vlo�ena do datab�ze RIS.", 3, ToolTipIcon.Info);
                opener.RIS_dg_Load_List();
                this.Close();
            }
            else
            {
                opener.LogDebug("check failed: " + RetCode.ToString(), "user error");
            }
        }

        private int RIS_saveRecord()
        {
            return RIS_saveRecord(false);
        }
        private int RIS_saveRecord(bool SkipChecks)
        {
            int ret = 9999;
            string keywords = Keywords_create();
            string datum_od = RIS_getDatum(1);
            string datum_do = RIS_getDatum(2);
            int ris_mistni_promo = 0;
            int ris_medialni_partner = 0;
            int ris_promo_vysilani = 0;
            string group_id = "*";
            string ris_vlozil = opener.ActualProfile;

            int ValueCheck = CheckValues();
            if ((ValueCheck == 0) || (SkipChecks == true))
            {
                if (e_medialni_partner.Checked == true) ris_medialni_partner = 1;
                if (e_promo_vysilani.Checked == true) ris_promo_vysilani = 1;

                for (int loop = 0; loop < e_groups.CheckedItems.Count; loop++)
                {
                    group_id += e_groups.CheckedItems[loop] + "*";
                }

                if (MasterEditMode)
                {
                    // EditMode_id
                    object[,] sql_delete = { { "id", "=", EditMode_id } };
                    new MySQLDeleteCommand(opener.MYSQL, opener.sql_db_table + "_ris", sql_delete, null);
                    ris_vlozil = edit_base_vlozil;
                    e_nazev_VER_new = e_nazev_VER;
                }
                else
                {
                    EditMode_id = null;
                }

                object[,] sql_insert = {
                        {"id", EditMode_id},
                        {"group_id", group_id},
                        {"datum_zacatek", datum_od},
                        {"datum_konec", datum_do},
                        {"ris_oblast", e_oblast.Text},
                        {"ris_okres", e_okres.Text},
                        {"ris_mesto", e_mesto.Text},
                        {"ris_misto", e_misto.Text},
                        {"ris_klicova_slova", keywords},
                        {"ris_kategorie", e_kategorie.SelectedItem.ToString()},
                        {"ris_nazev", e_nazev.Text},
                        {"ris_popis", e_popis.Text},
                        {"ris_mistni_promo", ris_mistni_promo},
                        {"ris_medialni_partner", ris_medialni_partner},
                        {"ris_promo_vysilani", ris_promo_vysilani},
                        {"vlozil", ris_vlozil},
                        {"vlozil_datum", DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")},
                        {"vlozil_cas", DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00")}, 
                        {"base_id", baseID}, 
                        {"verze", e_nazev_VER_new}
                    };
                try
                {
                    new MySQLDriverCS.MySQLInsertCommand(opener.MYSQL, sql_insert, opener.sql_db_table + "_ris");
                    RIS_FixEmptyBaseID();
                    opener.LogDebug("Akce vlo�ena...", "RIS");
                    ret = 0;
                }
                catch (Exception e)
                {
                    opener.LogDebug(e.ToString() , "Error");
                    ret = 9999;
                }
            }
            else
            {
                ret = ValueCheck;
                RIS_new_ShowCheckErrorMsg(ret);
            }
            return ret;
        }

        private void RIS_new_ShowCheckErrorMsg(int err_code)
        {
            string ErrorString = "";
            switch (err_code)
            {
                case 1:
                    ErrorString = "Pros�m zvolte oblast.";
                    e_oblast.BackColor = Color.Yellow;
                    e_oblast.Focus();
                    break;

                case 2:
                    ErrorString = "Pros�m zvolte okres.";
                    e_okres.BackColor = Color.Yellow;
                    e_okres.Focus();
                    break;

                case 3:
                    ErrorString = "Pros�m zadejte m�sto.";
                    e_mesto.BackColor = Color.Yellow;
                    e_mesto.Focus();
                    break;

                case 4:
                    ErrorString = "Pros�m zadejte m�sto (nap�. n�m�st�).";
                    e_misto.BackColor = Color.Yellow;
                    e_misto.Focus();
                    break;

                case 5:
                    ErrorString = "Pros�m zadejte n�zev akce.";
                    e_nazev.BackColor = Color.Yellow;
                    e_nazev.Focus();
                    break;

                case 6:
                    ErrorString = "Pros�m zvolte kategorie dann� akce.";
                    e_kategorie.BackColor = Color.Yellow;
                    e_kategorie.Focus();
                    break;

                case 7:
                    ErrorString = "Pros�m napi�te popis akce.";
                    e_popis.BackColor = Color.Yellow;
                    e_popis.Focus();
                    break;

                case 8:
                    ErrorString = "Pros�m zvolte alespo� jeden okruh (m��ete i v�ce).";
                    e_groups.BackColor = Color.Yellow;
                    e_groups.Focus();
                    break;


                case 10:
                    ErrorString = "Do�lo k duplicit� kl��ov�ch slov - akce ji� v datab�zi existuje.";
                    break;

                default:
                    ErrorString = "Nezn�m� chyba :(";
                    break;
            }
            opener.ShowAppNotify("Vlo�en� z�znamu se nezda�ilo.\r\n\r\n" + ErrorString, 5, ToolTipIcon.Warning);

        }

        private void RIS_FixEmptyBaseID()
        {
            try
            {
                string sql_query = "UPDATE " + opener.sql_db_table + "_ris SET base_id=id WHERE base_id=0";
                opener.LogDebug(sql_query, "SQL");
                MySQLDriverCS.MySQLCommand sql_cmd = new MySQLCommand(sql_query, opener.MYSQL);
                sql_cmd.ExecuteNonQuery();
            }
            catch (Exception err) { opener.LogDebug(err.ToString(), "ERROR"); }
        }

        private void data_changed__rebuild_keywords(object sender, EventArgs e)
        {
            if (e_datum_od.Value > e_datum_do.Value) e_datum_do.Value = e_datum_od.Value;
            e_datum_do.MinDate = e_datum_od.Value;
            Keywords_create();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            int RetCode = -1;
            
            DialogResult question;
            question = MessageBox.Show("Tato operace nastav� p��znak ZRU�ENO pro tuto akci.\r\nAkce nebude odstran�na z datab�ze.\r\n\r\nTuto akci NELZE vz�t zp�t.\r\nOpravdu chcete akci zru�it?", "Zru�it akci", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

            if (question == DialogResult.Yes)
            {
                string[] A;
                string S;
                
                e_nazev_VER_new = 9999;
                A = e_nazev.Text.Split('|');
                S = A[0].Trim() + " | ZRU�ENO";
                e_nazev.Text = S;
                Keywords_create();

                if (MasterEditMode)
                {
                    try
                    {
                        object[,] sql_delete = { { "id", "=", EditMode_id } };
                        new MySQLDeleteCommand(opener.MYSQL, opener.sql_db_table + "_ris", sql_delete, null);
                        RetCode = 0;
                    } catch {}
                }
                else
                {
                    RetCode = RIS_saveRecord(true);
                }
                
                if (RetCode == 0)
                {
                    opener.ShowAppNotify("Akce byla �sp�n� ZRU�ENA.", 3, ToolTipIcon.Info);
                    opener.RIS_dg_Load_List();
                    this.Close();
                }
                else
                {
                    opener.LogDebug("unknown error", "error");
                }
            }
        }

        private void btn_MasterEditMode_Click(object sender, EventArgs e)
        {
            if (RIS_edit_checkMasterRights())
            {

                if (btn_MasterEditMode.Checked == true)
                {
                    if (e_nazev_VER < 2)
                    {
                        e_nazev.Text = edit_base_nazev;
                    }
                    else
                    {
                        e_nazev.Text = edit_base_nazev + " | " + e_nazev_VER + ". verze";
                    }
                    MasterEditMode = true;
                    RIS_new_header.ValuesPrimary.Heading = "Administr�torsk� m�d";
                }
                else
                {
                    e_nazev.Text = edit_base_nazev + " | " + e_nazev_VER_new + ". verze";
                    MasterEditMode = false;
                    RIS_new_header.ValuesPrimary.Heading = "Editace z�znamu";
                }
            }
            else
            {
                MessageBox.Show("Nem�te pr�va na vstup do Administr�torsk�ho m�du", "P��stup odep�en", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}