NEOPRAVENE CHYBY a TODO:
* nastaveni delky timeru u SMS (automaticke nacitani novych zprav)
* odesilani emailu (?)
* lepe osetrit nemoznost ulozeni profilu z duvodu nedostupnosti MySQL serveru.
+ databaze vyher
+ DJ Info
+ lepsi poznamky ;)
+ automaticke dokoncovani u filtru

* je t�eba zv�raznit �zru�eno� ve vlastn�ch zpr�v�ch
* m�lo by b�t zobrazen� jen posledn� verze ve vlastn�ch zpr�v�ch
* u embargov�n� � p�i zm�n� data automaticky nastavit �as 00:01
* dot�hnout mo�nost editace u embargovan�ch...


OPRAVENE CHYBY:
1.7.3
* OstroRIS je vynucene 32bit only. "libmysql.dll " nepodporuje 64bit verzi (plati i pro Tweak)
* OPRAVIT:  scrollov�n� ve zpr�v�ch �TK v Ostrorisu se p�eru�� ve chv�li, kdy p�ijde nov� zpr�va

1.7.0
* aktualizovan OstroRIS-tweak.exe
* opraven status bar
* pridano ukladani sirky panelu na zalozce doprava
* upravy nacitani dopravy
* snad opravena chyba pri tisku SMS
* informace o doprave se aktualizuji jen pri zmene
* doprava - notifikace o neplatnosti dat
* opraveno mozne ulozeni pozice okna mimo obrazovku

1.6.9
* DOPRAVA ABA
* zmena AutoUpdatu na au.ladasoukup.cz

1.6.7
* opraven "PSTaskDialog.dll" - podpora pro Win Vista
* CustNews - podpora pro readed / unreaded

1.6.6
* odstraneny vlajecky z CTK (nepouzivana funkce a zpusobovala neumerne zpomalovani)
* za urcitych okolnosti mohla aplikace spadnou pri vstupu na zalozku Vlastnich zprav
* nektera tlacitka pod WinVista nemala text - opraveno
* zlepseny nektere dialogy (PSTaskDialog)
* pri chybe spojeni s MySQL jiz nezustava ve status baru text "Nacitam profil..."
* opraveno zobrazovani embargovanych vlastnich zprav
* CustNews - ***ALL*** umozni editaci vsem (zapsano sifrovane v INI - "CustNews"->"MasterEdit")
* RIS - ***ALL*** umozni editaci vsem (zapsano sifrovane v INI - "RIS"->"MasterEdit")

!!! pridana moznost zobrazit i embargovane zpravy (Vlastni Zpravy)
1.6.5
* CustNews - podpora pro "zobrazit i embargovane"
* pridana podpora pro "please wait, working...."
* omezen pocet znaku pro SMS na 155
* zmeneny knihovny pro komunikaci s MySQL!!!

1.6.4
* oprava editace Vlastnich Zprav (nacitani data embargovani)
* zruseno schovavani starsich verzi u Vlastnich zprav
* pridana podpora pro automaticke nacitani zprav (Vlastni Zpravy)

1.6.3
* doladeny vlastni zpravy
* podpora prace se schrankou u vlastnich zprav

1.6.2
* opravena prace se schrankou (+ reorganizace kodu)
* RIS - detekce duplicit
* RIS - opravena editace okresu  ;)
* pridana zalozka - Vlastni zpravy  (CustNews)

1.6.1
* opraven editor Pozn�mek (textbox p�esahoval mimo aplikaci)
* opraven doubleClick na stavovy radek
* lepsi verzovani (zobrazuji se data a casy vsech revizi + autor revize
* podpora pro MasterEdit

1.6.0
* RIS - oprava editace


1.5.9
* RIS - editace + pr�va

1.5.8
* podpora verzovan� u RISu

1.5.7
* ukladani rozlozeni datagridu RISu do profilu

1.5.6
* podpora tisku (RIS)
* podpora kop�rov�n� do schr�nky (RIS)
* podpora pro ulo�it jako... (RIS)
* m�sto pr�zdn�ch selectbox� se nov� objevuje text "-v�echny-" (RIS)
* dopln�no jm�no autora + datum a �as vlo�en� (RIS)


1.5.5 (beta verze)
* RIS


PREJMENOVANI APLIKACE NA "OstroRIS" + nove logo a nova ikonka!

1.2.0
* RIS
* tisk
* skin Office 2007 blue


1.1.9
* Office2007 style menu
* zablokovana podpora pro dluhe SMS - zmena na strane SMS serveru
* podpora CSV (zatim jen SMS)
* FIX: nastaveni fontu a barev u SMS
* notifikace o nove SMS
* nastin RISu (plus jeho bety)

1.1.8
* Ctrl+C vklada do schranky i vice SMS (pokud neni oznacen zadny text v detailu)
* Podpora pro "DLOUHE SMS" - CTK reader nove umi spojovat SMS rozdelene do vice (az 4) SMS

1.1.7
HOTFIX!!!
* pokud uzivatel nema pravo na zadnou SMS branu, neni mozne se prihlasit a dojde k neosetrene chybe

1.1.6
* ukladat posuvnik u SMS (oddelovac seznamu a detailu)
* oprava "neaktivniho" posuvniku u datagridu (vynuceny redraw)

1.1.5
*** RC2 ***
* podpora pro Tisk, ukladani a kopirovani (do schranky) pro vice SMS najednou!
* oprava nekolika drobnych chyb
* po prepnuti na zalozku dojde k refreshnuti vsech DataGridu na zalozce
* zm�na v nastaven�... Barvy a P�sma se nastavuj� spole�n�, Obecn� nastaven� se zm�nila na Nastaven� �TK

*** RC1 ***
* oprava pri obnove seznamu SMS - aplikace spadne, pokud neni navazano spojeni s MySQL
* podpora pro Tisk, Kopirovat vse za VSECH zalozek.
* Odstraneno zneaktivneni nekterych polozek menu (tisk, nahled, ...)

*** BETA3 ***
* zlepsena podpora pro presun vice SMS najednou (pouze JEDEN MySQL prikaz)
* opravena podpora pro Kopirovani textu do Clipboardu!
* podpora pro odesilani SMS

*** BETA 2 ***
* lepsi obslouzeni DragAndDrop
* oprava sirky praveho pruhu (kalendar, TreeView)
* podpora pro presun vice SMS najednou ;)

*** BETA 1 ***
* SMS  - vyzaduje SMS server !!!

1.1.4
* z�lo�ka "Internet" byla odstran�na - zp�sobovala p��li� probl�m� a nem�la ��dn� efekt
* notify okno nahrazeno za klasickou windows bublinu
* nov� DEBUG okno... a moznozt zobrazit ho ihned po spusteni aplikace
* po p�ihl�en� nebyl aktivn� posuvn�k u seznamu zpr�v
* PROMAZAVAT PROFIL - smazat vlajecky, pro ktere neni v DB zprava
* tisk - odrazeni --> nove se pouziva HardMargin z tiskarny.

1.1.3
*** HOTFIX ***
* "webBrowser1" mohl zpusobit pad aplikace pri jejim startu - funkce byla docasne zakazana.

1.1.2
* opraven prihlasovaci dialog - chybove hlasky se mohly "zatoulat" pod prihlasovaci dialog a aplikace zustane zatuhla
* centralni konfiguraci (tzn. primou editaci hodnot v MySQL tabulce pujde editovat INI aplikace)
* po odhlaseni se stale nacitaji nove zpravy (auto-refresh) a nektere polozky v menu jsou aktivni
* opraveno prihlasovani - chybove hlasky jsou nyni OK
* splashscreen je nyni jako globalni resource - exe soubor je pak mensi

1.1.1
*** TESTOVAC� BUILD ***
* login screen obsahuje logo aplikace

1.1.0
* "notify" okno se zobrazuje 35 pixelu od leveho okraje.
* BUG: pokud doslo k odpojeni od MySQL, pokusi aplikace pripojit nez ulozi profil.
* BUG: pri restartu MySQL serveru se CTKreader jiz nepripoji zpet (1.0.6)
* MYSQL !!zmena!! - "ctk_profile.ctk_profile_bin": TEXT, 65535 (16-bit length) -> MEDIUMTEXT, 16777215 (24-bit length)  (1.0.6)
* u CTK je nove mozna zmena velikosti seznamu zprav a detailu zpravy (splitter)  (1.0.6)
* pridana karta RIS (zatim prazdna) (1.0.6)
* pridana karta internet (MSIE)  (1.0.6)
* pridan "ScratchBox" (1.0.6)
* pridana podpora pro zalozky (moduly) (1.0.6)

1.0.5
* opravit anti-datov�n� zpr�v �TKou - zpr�vy se na�tou i kdy� budou vyd�ny pozd�ji ne� je ud�van� datum v hlavi�ce  (1.0.5)
* Mozna chyba prihlaseni  (1.0.5)
* Nova verze libmySQL.dll  (1.0.5)
* Ukladani / Nacitani popisku vlajecek (1.0.5)

1.0.4
* Uzavirani spojeni k MySQL  (1.0.4)

1.0.3
* BackgroundAutoUpdate (1.0.3)
* ignoruje se z�porn� hodnota pozice okna z INI (1.0.3)

1.0.2
* mo�nost nastavit notifikaci o nov� zpr�v� (1.0.2)
* AutoLogin (1.0.2)
* Force AutoUpdate - nedovolit uzivateli odmitnout update. (1.0.2)

1.0.1
* Pokud starsi verze otevre profil ulozeny novejsi verzi, muze dojit ke smazani novejsi konfigurace (nove indexy, ktere starsi verze nezna) - je potreba pri nacitani profilu nacist i tyto data a bezezmeny je pak ulozit... (1.0.1)
* P�i nastaven� filtru se VYPNE auto reload.... (1.0.1)

0.x
* Prehazet IKONY pro Menu (0.8.1)
* Aplikace muze nastartovat mimo obrazovku!!! (0.8.0)
* AutoUpdate usekne konec posledniho souboru (0.6.8)
* "DAUM" misto "DATUM"