/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 17.7.2005
 * Time: 19:32
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using Ini;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
	/// <summary>
	/// Description of sqlcfg.
	/// </summary>
    public class cfg_ctk : KryptonForm
    {
        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox cfg_ctk_refresh;
		private System.Windows.Forms.Label label4;
		// Global variables
		MainForm opener;
        private CheckBox dv_show_5;
        private CheckBox dv_show_4;
        private CheckBox dv_show_3;
        private CheckBox dv_show_2;
        private CheckBox dv_show_1;
        private CheckBox cfg_notify;
		bool cfg_saved = false;
		// Functions
		public cfg_ctk(MainForm sender)
		{
			// The InitializeComponent() call is required for Windows Forms designer support.
			InitializeComponent();
			opener = sender;
		}		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cfg_ctk));
            this.label4 = new System.Windows.Forms.Label();
            this.cfg_ctk_refresh = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cfg_notify = new System.Windows.Forms.CheckBox();
            this.dv_show_5 = new System.Windows.Forms.CheckBox();
            this.dv_show_4 = new System.Windows.Forms.CheckBox();
            this.dv_show_3 = new System.Windows.Forms.CheckBox();
            this.dv_show_2 = new System.Windows.Forms.CheckBox();
            this.dv_show_1 = new System.Windows.Forms.CheckBox();
            this.btn_ok = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(206, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 5;
            this.label4.Text = "sekundách";
            // 
            // cfg_ctk_refresh
            // 
            this.cfg_ctk_refresh.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.cfg_ctk_refresh.Location = new System.Drawing.Point(166, 16);
            this.cfg_ctk_refresh.Name = "cfg_ctk_refresh";
            this.cfg_ctk_refresh.Size = new System.Drawing.Size(32, 14);
            this.cfg_ctk_refresh.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(320, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nastavení ČTK";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 16);
            this.label3.TabIndex = 3;
            this.label3.Text = "Obnovovat seznam zpráv po";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cfg_notify);
            this.groupBox1.Controls.Add(this.dv_show_5);
            this.groupBox1.Controls.Add(this.dv_show_4);
            this.groupBox1.Controls.Add(this.dv_show_3);
            this.groupBox1.Controls.Add(this.dv_show_2);
            this.groupBox1.Controls.Add(this.dv_show_1);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cfg_ctk_refresh);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.groupBox1.Location = new System.Drawing.Point(8, 40);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(320, 182);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Přehled zpráv";
            // 
            // cfg_notify
            // 
            this.cfg_notify.AutoSize = true;
            this.cfg_notify.Location = new System.Drawing.Point(8, 35);
            this.cfg_notify.Name = "cfg_notify";
            this.cfg_notify.Size = new System.Drawing.Size(275, 17);
            this.cfg_notify.TabIndex = 11;
            this.cfg_notify.Text = "Upozornit na nové zprávy pomocí notifikačního okna";
            this.cfg_notify.UseVisualStyleBackColor = true;
            // 
            // dv_show_5
            // 
            this.dv_show_5.AutoSize = true;
            this.dv_show_5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dv_show_5.Location = new System.Drawing.Point(9, 156);
            this.dv_show_5.Name = "dv_show_5";
            this.dv_show_5.Size = new System.Drawing.Size(193, 17);
            this.dv_show_5.TabIndex = 10;
            this.dv_show_5.Text = "Zobrazovat pole \"KLÍČOVÁ SLOVA\"";
            this.dv_show_5.UseVisualStyleBackColor = true;
            // 
            // dv_show_4
            // 
            this.dv_show_4.AutoSize = true;
            this.dv_show_4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dv_show_4.Location = new System.Drawing.Point(9, 133);
            this.dv_show_4.Name = "dv_show_4";
            this.dv_show_4.Size = new System.Drawing.Size(155, 17);
            this.dv_show_4.TabIndex = 9;
            this.dv_show_4.Text = "Zobrazovat pole \"TITULEK\"";
            this.dv_show_4.UseVisualStyleBackColor = true;
            // 
            // dv_show_3
            // 
            this.dv_show_3.AutoSize = true;
            this.dv_show_3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dv_show_3.Location = new System.Drawing.Point(9, 110);
            this.dv_show_3.Name = "dv_show_3";
            this.dv_show_3.Size = new System.Drawing.Size(172, 17);
            this.dv_show_3.TabIndex = 8;
            this.dv_show_3.Text = "Zobrazovat pole \"KATEGORIE\"";
            this.dv_show_3.UseVisualStyleBackColor = true;
            // 
            // dv_show_2
            // 
            this.dv_show_2.AutoSize = true;
            this.dv_show_2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dv_show_2.Location = new System.Drawing.Point(9, 87);
            this.dv_show_2.Name = "dv_show_2";
            this.dv_show_2.Size = new System.Drawing.Size(135, 17);
            this.dv_show_2.TabIndex = 7;
            this.dv_show_2.Text = "Zobrazovat pole \"ČAS\"";
            this.dv_show_2.UseVisualStyleBackColor = true;
            // 
            // dv_show_1
            // 
            this.dv_show_1.AutoSize = true;
            this.dv_show_1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.dv_show_1.Location = new System.Drawing.Point(9, 64);
            this.dv_show_1.Name = "dv_show_1";
            this.dv_show_1.Size = new System.Drawing.Size(150, 17);
            this.dv_show_1.TabIndex = 6;
            this.dv_show_1.Text = "Zobrazovat pole \"DATUM\"";
            this.dv_show_1.UseVisualStyleBackColor = true;
            // 
            // btn_ok
            // 
            this.btn_ok.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btn_ok.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_ok.Location = new System.Drawing.Point(211, 228);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(117, 28);
            this.btn_ok.TabIndex = 7;
            this.btn_ok.Text = "Uložit a zavřít";
            this.btn_ok.Click += new System.EventHandler(this.Btn_okClick);
            // 
            // cfg_ctk
            // 
            this.AcceptButton = this.btn_ok;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(338, 263);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cfg_ctk";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Nastavení ČTK";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.cfg_closing);
            this.Load += new System.EventHandler(this.cfgLoad);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

		}
		#endregion
		void cfgLoad(object sender, System.EventArgs e)
		{
            try
            {
                cfg_ctk_refresh.Text = Convert.ToString(opener.ctk_reload_time);
            }
            catch { cfg_ctk_refresh.Text = "90"; }

            if (opener.ShowNewMsgNotify) { cfg_notify.Checked = true; } else { cfg_notify.Checked = false; }

            dv_show_1.Checked = opener.dg_ctk_list.Columns[1].Visible;
            dv_show_2.Checked = opener.dg_ctk_list.Columns[2].Visible;
            dv_show_3.Checked = opener.dg_ctk_list.Columns[3].Visible;
            dv_show_4.Checked = opener.dg_ctk_list.Columns[4].Visible;
            dv_show_5.Checked = opener.dg_ctk_list.Columns[5].Visible;
		}
		
		void Btn_okClick(object sender, System.EventArgs e)
		{
			sqlcfg_SaveConfig();
		}
		
		void cfg_closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if (cfg_saved == false){
				DialogResult result = MessageBox.Show("Nastavení nebylo uloženo.\nPokud okno zavřete, nastavení nebude uloženo.", "Nastavení nebylo uloženo", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
				switch(result) {
					case DialogResult.OK:
						break;
					case DialogResult.Cancel:
						e.Cancel = true;
						break;
				}
			}
		}
		
		void sqlcfg_SaveConfig() {
			// Saving config...
			string S = "";
			int i = 0;

            if (cfg_ctk_refresh.Text == "") cfg_ctk_refresh.Text = "120";
			i = Convert.ToInt32(cfg_ctk_refresh.Text);
			if (i >= 30) {
				S = i.ToString();
			} else {
				S = "30";
			}
			opener.ctk_reload_time = Convert.ToInt32(S);
            opener.CustNews_reload_time = Convert.ToInt32(S);
            opener.ShowNewMsgNotify = cfg_notify.Checked;

            opener.dg_ctk_list.Columns[1].Visible = dv_show_1.Checked;
            opener.dg_ctk_list.Columns[2].Visible = dv_show_2.Checked;
            opener.dg_ctk_list.Columns[3].Visible = dv_show_3.Checked;
            opener.dg_ctk_list.Columns[4].Visible = dv_show_4.Checked;
            opener.dg_ctk_list.Columns[5].Visible = dv_show_5.Checked;

			opener.ShowAppNotify("Nastavení bylo uloženo.", 3);
			opener.LogDebug("Configuration saved", "SYSTEM");
			opener.ctk_list_reload();
			cfg_saved = true;
			this.Close();
		}
	}
}
