using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using MySQLDriverCS;

namespace CTK_reader
{
    partial class MainForm
    {
        
        string ExportCtk2text()
        {
            string textExport = "";

            LogDebug("ExportCTK2text() - " + SBnews_panel.SelectedTab.Name, "USER");
            try
            {
                if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ctk")
                {
                    // CTK
                    int I = 0;
                    try
                    {
                        I = dg_ctk_list.CurrentRow.Index;
                        if (dg_ctk_list.Rows[I].Cells[0].Value.ToString() != "")
                        {
                            textExport = dg_ctk_list.Rows[I].Cells[1].Value.ToString();
                            textExport += "  " + dg_ctk_list.Rows[I].Cells[2].Value.ToString();
                            textExport += "\r\n" + ctk_detail.Text;
                        }
                    }
                    catch (Exception e)
                    {
                        LogDebug(e.ToString(), "ERROR");
                    }
                }
                else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
                {
                    // SMS
                    textExport = SMS_detail.Text.ToString();

                    try
                    {
                        if (dg_SMS.SelectedRows.Count > 1)
                        {
                            textExport = "";
                            foreach (DataGridViewRow SMS2Export in dg_SMS.SelectedRows)
                            {
                                textExport += "Datum a �as: ";
                                textExport += SMS2Export.Cells[3].Value.ToString();
                                textExport += " ";
                                textExport += SMS2Export.Cells[4].Value.ToString();
                                textExport += "\r\nOdes�latel: ";
                                textExport += SMS2Export.Cells[2].Value.ToString();
                                textExport += "\r\n\r\n";
                                textExport += SMS2Export.Cells[6].Value.ToString().Replace("\r", "").Replace("\n", "");
                                textExport += "\r\n--------------------------------------------------\r\n";
                            }
                        }

                    }
                    catch { }

                }
                else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ris")
                {
                    // RIS
                    int I = 0;
                    try
                    {
                        I = dg_RIS_list.CurrentRow.Index;
                        if (dg_RIS_list.Rows[I].Cells[0].Value.ToString() != "")
                        {
                            textExport += RIS_txt_detail.Text;
                        }
                    }
                    catch (Exception e)
                    {
                        LogDebug(e.ToString(), "ERROR");
                    }

                }

                else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_CustNews")
                {
                    // CustNews
                    int I = 0;
                    try
                    {
                        I = dg_CustNews_list.CurrentRow.Index;
                        if (dg_CustNews_list.Rows[I].Cells[0].Value.ToString() != "")
                        {
                            textExport += CustNews_txt_detail.Text;
                        }
                    }
                    catch (Exception e)
                    {
                        LogDebug(e.ToString(), "ERROR");
                    }

                }

                else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_scratch")
                {
                    // Poznamky
                    textExport = SBnews_ScratchBox.Text.ToString();
                }
            }
            catch
            {
                LogDebug("ExportCtk2text() unknown error", "ERROR");
            }
            return textExport;
        }

        string ExportCtk2csv(string csv_splitter)
        {
            string textExport = "";

            try
            {
                if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
                {
                    // SMS
                    textExport = SMS_detail.Text.ToString();

                    try
                    {
                        if (dg_SMS.SelectedRows.Count > 1)
                        {
                            textExport = "";
                            foreach (DataGridViewRow SMS2Export in dg_SMS.SelectedRows)
                            {
                                textExport += SMS2Export.Cells[3].Value.ToString();
                                textExport += csv_splitter;
                                textExport += SMS2Export.Cells[4].Value.ToString();
                                textExport += csv_splitter;
                                textExport += (SMS2Export.Cells[2].Value.ToString()).Replace("+420", "");
                                textExport += csv_splitter;
                                textExport += (((SMS2Export.Cells[6].Value.ToString()).Replace("\n", "")).Replace("\r", "")).Replace(csv_splitter, "_");
                                textExport += "\r\n";
                            }
                        }

                    }
                    catch { }

                }
            }
            catch
            {
                LogDebug("ExportCtk2csv() unknown error", "ERROR");
            }
            return textExport;
        }

        void mmenu_tisk_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_tisk_click()", "USER");
            string textToPrint = "";

            textToPrint = ExportCtk2text();

            MultipadPrintDocument _printdocument = new MultipadPrintDocument();
            PrintDialog pd = new PrintDialog();
            _printdocument.Text = textToPrint;
            _printdocument.Font = ctk_detail.Font;
            pd.Document = _printdocument;
            //pd.ShowDialog();
            if (pd.ShowDialog() == DialogResult.OK)
            {
                _printdocument.Print();
                ShowAppNotify("Tisk byl zah�jen...", 5);
            }
        }

        void mmenu_tisk_nahled_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_tisk_nahled_click()", "USER");
            string textToPrint;

            textToPrint = ExportCtk2text();

            MultipadPrintDocument _printdocument = new MultipadPrintDocument();
            PrintPreviewDialog ppd = new PrintPreviewDialog();
            _printdocument.Text = textToPrint;
            _printdocument.Font = ctk_detail.Font;

            ppd.Document = _printdocument;
            ppd.ShowDialog();
        }
        
        void mmenu_kopirovat_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_kopirovat_click() - " + SBnews_panel.SelectedTab.Name.ToString(), "USER");
            if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ctk")
            {
                // CTK
                Clipboard.SetDataObject(ctk_detail.SelectedText.ToString());
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
            {
                // SMS
                if (SMS_detail.SelectedText.ToString() != "")
                {
                    Clipboard.SetDataObject(SMS_detail.SelectedText.ToString());
                }
                else
                {
                    mmenu_kopirovat_celou_click(sender, e);
                }
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ris")
            {
                // RIS
                Clipboard.SetDataObject(RIS_txt_detail.SelectedText.ToString());
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_CustNews")
            {
                // CustNews
                Clipboard.SetDataObject(CustNews_txt_detail.SelectedText.ToString());
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_scratch")
            {
                // Poznamky
                Clipboard.SetDataObject(SBnews_ScratchBox.SelectedText.ToString());
            }
        }

        void mmenu_kopirovat_celou_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_kopirovat_celou_click() - " + SBnews_panel.SelectedTab.Name.ToString(), "USER");

            string ClipboardText = ExportCtk2text();
            Clipboard.SetDataObject(ClipboardText);

            if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ctk")
            {
                //CTK
                ShowAppNotify("Zpr�va byla �sp�n� vlo�ena do schr�nky.", 5);
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
            {
                // SMS
                ShowAppNotify("SMS zpr�va byla �sp�n� vlo�ena do schr�nky.", 5);
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_scratch")
            {
                // Poznamky
                ShowAppNotify("Text byl �sp�n� vlo�en do schr�nky.", 5);
            }

        }

        void mmenu_ulozit_click(object sender, System.EventArgs e)
        {
            string SaveText = ExportCtk2text();
            if (dialog_mmenu_ulozit.ShowDialog() == DialogResult.OK)
            {
                // Write File
                StreamWriter LogFile = new StreamWriter(dialog_mmenu_ulozit.FileName, false, System.Text.Encoding.GetEncoding("windows-1250"));
                LogFile.Write(SaveText);
                LogFile.Close();
                ShowAppNotify("Zpr�va byla �sp�n� ulo�ena.", 5);
            }
        }


        private void mmenu_kopirovat_csv_click(object sender, EventArgs e)
        {
            LogDebug("mmenu_kopirovat_csv_click() - " + SBnews_panel.SelectedTab.Name.ToString(), "USER");
            string ClipboardText = ExportCtk2csv("\t");

            if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
            {
                // SMS
                Clipboard.SetDataObject(ClipboardText);
                ShowAppNotify("SMS zpr�vy byly �sp�n� vlo�eny do schr�nky.", 5);
            }
        }

        private void mmenu_ulozit_csv_Click(object sender, EventArgs e)
        {
            string SaveText = ExportCtk2csv(";");
            if (dialog_mmenu_ulozit_csv.ShowDialog() == DialogResult.OK)
            {
                // Write File
                try
                {
                    StreamWriter CSVFile = new StreamWriter(dialog_mmenu_ulozit_csv.FileName, false, System.Text.Encoding.GetEncoding("windows-1250"));
                    CSVFile.Write(SaveText);
                    CSVFile.Close();
                    ShowAppNotify("Zpr�va byla �sp�n� ulo�ena.", 5);
                }
                catch
                {
                    string ErrorText = "Zpr�va NEbyla ulo�ena - soubor je pravd�podobn� pou��v�n jinou aplikac�.";
                    MessageBox.Show(ErrorText, "Soubor nebyl ulo�en", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //ShowAppNotify(ErrorText, 5);
                }
            }
        }
    }
}