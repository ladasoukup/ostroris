using System.IO;
using MySQLDriverCS;
using System.Windows.Forms;
using System.Drawing;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Net;

namespace CTK_reader
{
    partial class MainForm
    {
        private string html_empty = "<html><head></head><body style=\"background-color: #BBD9FF;\">Žádná aktuální data k zobrazení...</body></html>";
        private string[] trafficIDs = new string[250];
        private int SBNews_doprava_selected_krajID = 0;

        private void SBnews_doprava_INIT(int StartID)
        {
            LogDebug("SBnews_doprava_INIT()", "DOPRAVA");
            int LoopID = 0;

            doprava_web_stupne.DocumentText = html_empty;
            doprava_web_nehody.DocumentText = html_empty;

            try
            {
                SBnews_doprava_kraj.Items.Clear();
                string sql_query = "SELECT titulek, id FROM " + sql_db_table + "_traffic_url ORDER BY id_sort ASC";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MYSQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    SBnews_doprava_kraj.Items.Add(sql_reader.GetString(0));
                    if (sql_reader.GetString(0) != "")
                    {
                        trafficIDs[LoopID] = sql_reader.GetString(1);
                    }
                    else
                    {
                        trafficIDs[LoopID] = "-empty-";
                    }
                    LoopID++;
                }
            }
            catch { }

            Application.DoEvents();

            try
            {
                if (StartID <= SBnews_doprava_kraj.Items.Count)
                {
                    SBnews_doprava_kraj.SelectedIndex = StartID;
                }
                else
                {
                    SBnews_doprava_kraj.SelectedIndex = 0;
                }
            }
            catch { }
            timer_doprava.Enabled = true;
            doprava_reloadData_stupne();
        }

        private void SBnews_doprava_DEINIT()
        {
            LogDebug("SBnews_doprava_DEINIT()", "DOPRAVA");

            timer_doprava.Enabled = false;
            doprava_web_stupne.DocumentText = html_empty;
            doprava_web_nehody.DocumentText = html_empty;
        }

        private void SBnews_doprava_Zmen_Kraj(object sender, EventArgs e)
        {
            string SelValue = trafficIDs[SBnews_doprava_kraj.SelectedIndex];
            if (SelValue != "-empty-")
            {
                doprava_web_nehody.DocumentText = html_empty;
                doprava_reloadData_nehody();
                SBNews_doprava_selected_krajID = SBnews_doprava_kraj.SelectedIndex;
            }
            else
            {
                try
                {
                    SBnews_doprava_kraj.SelectedIndex = SBNews_doprava_selected_krajID;
                }
                catch { }
            }
        }

        private void btn_SBnews_doprava_reload_Click(object sender, EventArgs e)
        {
            doprava_reloadData();
        }

        private string HtmlRewriteFonts(string html)
        {
            /*
            #HEAD-FAMILY#
            #HEAD-SIZE#
			#TEXT-FAMILY#
            #TEXT-SIZE#
            */

            string FontName = ctk_detail.Font.Name.ToString();
            int FontSize = Convert.ToInt32(ctk_detail.Font.Size);


            html = html.Replace("#HEAD-FAMILY#", FontName);
            html = html.Replace("#HEAD-SIZE#", (FontSize + 2).ToString());
            html = html.Replace("#TEXT-FAMILY#", FontName);
            html = html.Replace("#TEXT-SIZE#", FontSize.ToString());

            return (html);
        }

        private void doprava_reloadData() {
            doprava_reloadData_stupne();
            doprava_reloadData_nehody();
        }

        private void doprava_reloadData_stupne()
        {
            LogDebug("doprava_reloadData_stupne()", "DOPRAVA");
            string html_doc = "";
            try
            {
                string sql_query = "SELECT html_data, data_update, data_update_bin FROM " + sql_db_table + "_traffic WHERE id='stupne' LIMIT 1";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MYSQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                sql_reader.Read();
                html_doc = sql_reader.GetString(0);
                html_doc = HtmlRewriteFonts(html_doc);
                if (doprava_web_stupne.DocumentText != html_doc) doprava_web_stupne.DocumentText = html_doc;

                SBnews_doprava_panel_left.ValuesSecondary.Heading = "Data OK";
                SBnews_doprava_panel_left.ValuesSecondary.Description = sql_reader.GetString(1);
                if (CheckDataTimeStamp(sql_reader.GetString(2)))
                {
                    SBnews_doprava_panel_left.ValuesSecondary.Image = null;
                }
                else
                {
                    SBnews_doprava_panel_left.ValuesSecondary.Image = Properties.Resources.wawn_32;
                    SBnews_doprava_panel_left.ValuesSecondary.Heading = "Neaktuální data!";
                }

            }
            catch
            {
                SBnews_doprava_panel_left.ValuesSecondary.Heading = "Chyba aktualizace dat!!!";
                SBnews_doprava_panel_left.ValuesSecondary.Image = Properties.Resources.wawn_32;
            }
        }

        private void doprava_reloadData_nehody()
        {
            LogDebug("doprava_reloadData_nehody()", "DOPRAVA");
            string html_doc = "";
            string SelValue = "";

            Application.DoEvents();

            try
            {
                SelValue = trafficIDs[SBnews_doprava_kraj.SelectedIndex];
                string sql_query = "SELECT html_data, data_update, data_update_bin FROM " + sql_db_table + "_traffic WHERE id='" + SelValue + "' LIMIT 1";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MYSQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                sql_reader.Read();
                html_doc = sql_reader.GetString(0);
                html_doc = HtmlRewriteFonts(html_doc);

                if (doprava_web_nehody.DocumentText != html_doc) doprava_web_nehody.DocumentText = html_doc;

                SBnews_Doprava_panel_main.ValuesSecondary.Heading = "Data OK";
                SBnews_Doprava_panel_main.ValuesSecondary.Description = sql_reader.GetString(1);
                if (CheckDataTimeStamp(sql_reader.GetString(2)))
                {
                    SBnews_Doprava_panel_main.ValuesSecondary.Image = null;
                }
                else
                {
                    SBnews_Doprava_panel_main.ValuesSecondary.Image = Properties.Resources.wawn_32;
                    SBnews_Doprava_panel_main.ValuesSecondary.Heading = "Neaktuální data!";
                }

            }
            catch
            {
                SBnews_Doprava_panel_main.ValuesSecondary.Heading = "Chyba aktualizace dat!!!";
                SBnews_Doprava_panel_main.ValuesSecondary.Image = Properties.Resources.wawn_32;
            }
        }

        private bool CheckDataTimeStamp(string DataTimeStamp)
        {
            bool ret = true;
            int DataLifeTime = 10; // in minutes

            try
            {
                IFormatter formatter = new BinaryFormatter();
                object dataTS;

                byte[] memory_array = (byte[])Convert.FromBase64String(DataTimeStamp);
                MemoryStream MemStream = new MemoryStream();
                MemStream.Write(memory_array, 0, memory_array.Length);
                MemStream.Flush();
                MemStream.Position = 0;
                dataTS = (object)formatter.Deserialize(MemStream);
                MemStream.Close();

                DateTime DataUpdate = (DateTime)dataTS;
                if (DateTime.Compare(DateTime.Now.AddMinutes(-DataLifeTime), DataUpdate) > 0)
                {
                    // outdated data!!!
                    ret = false;
                }
            }
            catch { }

            return (ret);
        }

        private void timer_doprava_Tick(object sender, EventArgs e)
        {
            LogDebug("timer_doprava_Tick()", "DOPRAVA");
            doprava_reloadData();
        }
    }
}