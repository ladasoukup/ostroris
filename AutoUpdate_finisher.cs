using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace CTK_reader
{
    class AutoUpdate_finisher
    {
        private bool DeleteFiles_mask(string str_Path, string str_FileMask, MainForm opener, bool NeedRestart)
        {
            bool b_ret = false;
            try
            {
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(str_Path);
                foreach (System.IO.FileInfo f in dir.GetFiles(str_FileMask))
                {
                    f.Delete();
                    opener.LogDebug("Deleting file \"" + f.Name.ToString() + "\"", "SYSTEM");
                    if (NeedRestart == true) b_ret = true;
                }
            }
            catch { /* IGNORE error... */ }
            return b_ret;
        }

        
        public AutoUpdate_finisher(MainForm opener, string OldVersion)
		{
            int OldVer = 0;
            string S = null;
            bool NeedRestart = false;

            if (OldVersion == "")
            {
                S = "070";
            }
            else
            {
                S = OldVersion.Substring(0, 1) + OldVersion.Substring(2, 1) + OldVersion.Substring(4, 1);
            }
            OldVer = Convert.ToInt32(S);
            opener.LogDebug("Updating from version: " + S, "AU finisher");

            // *********************
            // START UPDATE STEPS...
            // *********************

            // VERSION 0.7.9
            if (OldVer <= 079)
            {
                if (DeleteFiles_mask(Application.StartupPath, "*.manifest", opener, true)) NeedRestart = true;
            }

            // VERSION 0.8.8
            if (OldVer <= 093)
            {
                DeleteFiles_mask(Application.StartupPath, "MenuImageLib.dll", opener, false);
            }

            // VERSION 0.9.3
            if (OldVer <= 093)
            {
                DeleteFiles_mask(Application.StartupPath, "s_*.bin", opener, false);
            }

            // VERSION 1.0.3
            if (OldVer <= 103)
            {
                DeleteFiles_mask(Application.StartupPath, "ctk_reader_au.ini", opener, false);
            }

            // ALL VERSIONs...
            try
            {
                StreamWriter BugsFile = new StreamWriter(Application.StartupPath + "\\BUGS.txt", false, System.Text.Encoding.GetEncoding("windows-1250"));
                BugsFile.Write(Properties.Resources.BUGS);
                BugsFile.Close();
            }
            catch (Exception e)
            {
                opener.LogDebug(e.ToString(), "ERROR");
                // Nepovedlo se zapsat log, ignoruj...
            }

            // ALL DONE :D
            if (NeedRestart)
            {
                DialogResult result = MessageBox.Show("Proces aktualizace byl dokon�en.\r\nK pln�mu dokon�en� aktualizace je pot�eba aplikaci restartovat.\r\nBez restartu nemus� aplikace pracovat spr�vn�!\r\n\r\nRestartovat hned?", "Aktualizace dokon�ena", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (result == DialogResult.Yes)
                {
                    Application.Restart();
                }
            }
		}
    }
}
