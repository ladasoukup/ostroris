using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using MySQLDriverCS;

namespace CTK_reader
{
    partial class MainForm
    {
        public void RIS_init()
        {
            LogDebug("RIS_init()", "SYSTEM");
            RIS_cleanup();

            RIS_sel_all_versions.Checked = false;

            // Load RIS groups
            RIS_sel_group.Items.Add("-v�echy-");
            sql_query = "SELECT group_id FROM " + sql_db_table + "_ris_groups ORDER BY id";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    RIS_sel_group.Items.Add(sql_reader.GetString(0));
                }
                sql_reader.Close();
                if (RIS_sel_group.Items.Count > 0) RIS_sel_group.SelectedIndex = 0;
            }
            catch { LogDebug("Load RIS groups FAILED", "ERROR"); }

            // Load RIS categories
            RIS_sel_kategorie.Items.Add("-v�echy-");
            sql_query = "SELECT category_id FROM " + sql_db_table + "_ris_categories ORDER BY category_id";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    RIS_sel_kategorie.Items.Add(sql_reader.GetString(0));
                }
                sql_reader.Close();
                if (RIS_sel_kategorie.Items.Count > 0) RIS_sel_kategorie.SelectedIndex = 0;
            }
            catch { LogDebug("Load RIS categories FAILED", "ERROR"); }
            RIS_sel_kategorie.Items.Add("- ostatn� -");

            // Load RIS oblasti
            RIS_sel_oblast.Items.Add("-v�echy-");
            sql_query = "SELECT DISTINCT oblast FROM " + sql_db_table + "_ris_oblasti ORDER BY oblast ASC";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    RIS_sel_oblast.Items.Add(sql_reader[0].ToString());
                }
                if (RIS_sel_oblast.Items.Count > 0) RIS_sel_oblast.SelectedIndex = 0;
            }
            catch { LogDebug("Load RIS oblasti FAILED", "ERROR"); }

            RIS_dg_Load_List();
        }

        public void RIS_cleanup()
        {
            RIS_txt_detail.Text = "";
            dg_RIS_list.Rows.Clear();
            RIS_sel_group.Items.Clear();
            RIS_sel_oblast.Items.Clear();
            RIS_sel_kategorie.Items.Clear();
            RIS_sel_mesto.Text = "";
            RIS_sel_nazev.Text = "";
        }

        public void RIS_dg_Load_List()
        {
            LogDebug("RIS_dg_Load_List()", "SYSTEM");
            string datum_sel_start;
            string datum_sel_end;
            string datum_zacatek;
            string datum_konec;
            string[] new_row = new string[] { "", "", "", "", "" };
            Bitmap flag_img_MP = Properties.Resources.flag_empty;
            Bitmap flag_img_PV = Properties.Resources.flag_empty;
            int LastBaseID = -1;
            int baseID = 0;
            int I = 0;

            ChangeStatusBar("Na��t�m data...", true);
            try
            {
                datum_sel_start = RIS_calendar.SelectionStart.Year.ToString("0000") + RIS_calendar.SelectionStart.Month.ToString("00") + RIS_calendar.SelectionStart.Day.ToString("00");
                datum_sel_end = RIS_calendar.SelectionEnd.Year.ToString("0000") + RIS_calendar.SelectionEnd.Month.ToString("00") + RIS_calendar.SelectionEnd.Day.ToString("00");
                sql_query = "SELECT id, base_id, datum_zacatek, datum_konec, ris_okres, ris_klicova_slova, ris_kategorie, ris_nazev, ris_medialni_partner, ris_promo_vysilani";
                sql_query += " FROM " + sql_db_table + "_ris";
                sql_query += " WHERE (('" + datum_sel_start + "' BETWEEN datum_zacatek AND datum_konec) OR ('" + datum_sel_end + "' BETWEEN datum_zacatek AND datum_konec) OR (datum_zacatek BETWEEN '" + datum_sel_start + "' AND '" + datum_sel_end + "'))";
                if (RIS_sel_group.SelectedIndex != 0) sql_query += " AND group_id LIKE '%*" + RIS_sel_group.SelectedItem + "*%'";
                if (RIS_sel_kategorie.SelectedIndex != 0) sql_query += " AND ris_kategorie = '" + RIS_sel_kategorie.SelectedItem + "'";
                if (RIS_sel_oblast.SelectedIndex != 0) sql_query += " AND ris_oblast = '" + RIS_sel_oblast.SelectedItem + "'";
                if (RIS_sel_mesto.Text != "") sql_query += " AND (ris_mesto LIKE '%" + RIS_sel_mesto.Text + "%' OR ris_okres LIKE '%" + RIS_sel_mesto.Text + "%')";
                if (RIS_sel_nazev.Text != "") sql_query += " AND ris_nazev LIKE '%" + RIS_sel_nazev.Text + "%'";
                sql_query += " GROUP BY base_id, ris_nazev ORDER BY datum_zacatek ASC, ris_nazev ASC";

                dg_RIS_list.Rows.Clear();
                LogDebug(sql_query, "SQL");
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    datum_zacatek = sql_reader["datum_zacatek"].ToString().Substring(6, 2) + "." + sql_reader["datum_zacatek"].ToString().Substring(4, 2) + "." + sql_reader["datum_zacatek"].ToString().Substring(0, 4);
                    datum_konec = sql_reader["datum_konec"].ToString().Substring(6, 2) + "." + sql_reader["datum_konec"].ToString().Substring(4, 2) + "." + sql_reader["datum_konec"].ToString().Substring(0, 4);

                    flag_img_MP = Properties.Resources.flag_empty;
                    flag_img_PV = Properties.Resources.flag_empty;
                    if (sql_reader["ris_medialni_partner"].ToString() == "1") flag_img_MP = Properties.Resources.RIS_MP;
                    if (sql_reader["ris_promo_vysilani"].ToString() == "1") flag_img_PV = Properties.Resources.RIS_PV;

                    new_row = new string[] { sql_reader["id"].ToString(), datum_zacatek, sql_reader["ris_okres"].ToString(), sql_reader["ris_nazev"].ToString(), sql_reader["ris_klicova_slova"].ToString() };

                    try { baseID = sql_reader.GetInt32(1); }
                    catch { baseID = 0; }
                    if (baseID == 0) baseID = sql_reader.GetInt32(0);
                    if (RIS_sel_all_versions.Checked == false)
                    {
                        // Show only last version
                        if (baseID == LastBaseID)
                        {
                            // remove last inserted ROW
                            try
                            {
                                I = dg_RIS_list.Rows.GetFirstRow(DataGridViewElementStates.Visible);
                                if (I >= 0)
                                {
                                    dg_RIS_list.Rows.RemoveAt(I);
                                }
                            }
                            catch (Exception err)
                            {
                                LogDebug("idx: " + I.ToString() + "\r\n" + err.ToString(), "DEBUG");
                            }
                        }
                    }

                    dg_RIS_list.Rows.Insert(0, new_row);

                    LastBaseID = baseID;

                    dg_RIS_list.Rows[0].Cells[5].Value = flag_img_MP;
                    dg_RIS_list.Rows[0].Cells[6].Value = flag_img_PV;
                }
                sql_reader.Close();
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "ERROR");
                ctk_status_label.Text = "Nen� nav�z�no p�ipojen� k MySQL datab�zi.";
                MYSQL.Close();
                Application.DoEvents();
                MySQL_connect();
            }
            ChangeStatusBar("P�ipraven", false);
        }


        public void RIS_LoadDetail()
        {
            LogDebug("RIS_LoadDetail()", "SYSTEM");

            ChangeStatusBar("Na��t�m data...", true);
            int dg_currID = 0; // dg_RIS_list.CurrentRow.Index;
            string datum_zacatek;
            string datum_konec;
            string datum_vlozil;
            string MedialniPartner = "Ne";
            string PromoVeVysilani = "Ne";
            string VersionEditHistory = "";

            if (dg_RIS_list.Rows.Count > 0)
            {
                dg_currID = dg_RIS_list.CurrentRow.Index;
                try
                {
                    sql_query2 = "SELECT * FROM " + sql_db_table + "_ris WHERE id=" + dg_RIS_list.Rows[dg_currID].Cells[0].Value + " LIMIT 1";
                    sql_cmd2 = new MySQLCommand(sql_query2, MYSQL);
                    sql_reader2 = sql_cmd2.ExecuteReaderEx();
                    while (sql_reader2.Read())
                    {
                        string sql_query3 = "SELECT vlozil, vlozil_datum, vlozil_cas, verze FROM " + sql_db_table + "_ris WHERE base_id=" + sql_reader2.GetString(20) + " AND verze < '" + sql_reader2.GetString(21) + "' ORDER BY verze ASC";
                        MySQLCommand sql_cmd3 = new MySQLCommand(sql_query3, MYSQL);
                        MySQLDataReader sql_reader3 = sql_cmd3.ExecuteReaderEx();
                        while (sql_reader3.Read())
                        {
                            datum_vlozil = sql_reader3.GetString(1).Substring(6, 2) + "." + sql_reader3.GetString(1).Substring(4, 2) + "." + sql_reader3.GetString(1).Substring(0, 4);
                            VersionEditHistory += sql_reader3.GetString(3) + ". verze: " + sql_reader3.GetString(0); // vlozil
                            VersionEditHistory += " (" + datum_vlozil + " " + sql_reader3.GetString(2) + ")";
                            VersionEditHistory += "\r\n";
                        }


                        datum_zacatek = sql_reader2["datum_zacatek"].ToString().Substring(6, 2) + "." + sql_reader2["datum_zacatek"].ToString().Substring(4, 2) + "." + sql_reader2["datum_zacatek"].ToString().Substring(0, 4);
                        datum_konec = sql_reader2["datum_konec"].ToString().Substring(6, 2) + "." + sql_reader2["datum_konec"].ToString().Substring(4, 2) + "." + sql_reader2["datum_konec"].ToString().Substring(0, 4);
                        datum_vlozil = sql_reader2["vlozil_datum"].ToString().Substring(6, 2) + "." + sql_reader2["vlozil_datum"].ToString().Substring(4, 2) + "." + sql_reader2["vlozil_datum"].ToString().Substring(0, 4);


                        if (sql_reader2.GetInt32(13) != 0) MedialniPartner = "Ano"; // medialni partner
                        if (sql_reader2.GetInt32(14) != 0) PromoVeVysilani = "Ano"; // prromo ve vysilani


                        RIS_txt_detail.Text = sql_reader2.GetString(8) + "\r\n\r\n"; //keywords
                        RIS_txt_detail.Text += "N�zev: " + sql_reader2.GetString(10) + "\r\n";
                        RIS_txt_detail.Text += "Datum: " + datum_zacatek + " - " + datum_konec + "\r\n";

                        RIS_txt_detail.Text += "M�sto: " + sql_reader2.GetString(6) + "\r\n";
                        RIS_txt_detail.Text += "M�sto: " + sql_reader2.GetString(7) + "\r\n";
                        RIS_txt_detail.Text += "Medi�ln� partner: " + MedialniPartner + "\r\n";
                        RIS_txt_detail.Text += "Povinn� do vys�l�n�: " + PromoVeVysilani + "\r\n\r\n";

                        RIS_txt_detail.Text += sql_reader2.GetString(11) + "\r\n\r\n"; // popis
                        RIS_txt_detail.Text += "-------------------------------------------------------\r\n";
                        RIS_txt_detail.Text += VersionEditHistory;
                        RIS_txt_detail.Text += "Tato verze: " + sql_reader2.GetString(17); // vlozil
                        RIS_txt_detail.Text += " (" + datum_vlozil + " " + sql_reader2.GetString(19) + ")";
                    }
                    sql_reader2.Close();
                }
                catch (Exception ex)
                {
                    LogDebug(ex.ToString(), "ERROR");
                    ctk_status_label.Text = "Nen� nav�z�no p�ipojen� k MySQL datab�zi.";
                    MYSQL.Close();
                    Application.DoEvents();
                    MySQL_connect();
                }
            }
            else
            {
                RIS_txt_detail.Text = "";
            }
            ChangeStatusBar("P�ipraven", false);
        }

        private void btn_RIS_new_Click(object sender, EventArgs e)
        {
            LogDebug("btn_RIS_new_Click()", "USER");
            RIS_new_record wnd = new RIS_new_record(this);
            wnd.Show();

        }
        private void btn_RIS_edit_Click(object sender, EventArgs e)
        {
            LogDebug("btn_RIS_edit_Click()", "USER");
            RIS_new_record wnd = new RIS_new_record(this);
            wnd.EditMode = true;
            wnd.EditMode_id = dg_RIS_list.Rows[dg_RIS_list.CurrentRow.Index].Cells[0].Value.ToString();
            wnd.Show();
        }

        private void RIS_btn_reload_Click(object sender, EventArgs e)
        {
            RIS_dg_Load_List();
        }

        private void RIS_btn_reload_clean_Click(object sender, EventArgs e)
        {
            if (RIS_sel_group.Items.Count > 0) RIS_sel_group.SelectedIndex = 0;
            if (RIS_sel_kategorie.Items.Count > 0) RIS_sel_kategorie.SelectedIndex = 0;
            if (RIS_sel_oblast.Items.Count > 0) RIS_sel_oblast.SelectedIndex = 0;
            RIS_sel_nazev.Text = "";
            RIS_sel_mesto.Text = "";

            RIS_dg_Load_List();
        }

        private void RIS_sel_all_versions_Click(object sender, EventArgs e)
        {
            RIS_dg_Load_List();
        }

    }
}