using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
    public partial class SMS_sendMsg : KryptonForm
    {
        private MainForm opener;
        public string sms_gate = "";

        public SMS_sendMsg(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void SMS_sendMsg_Load(object sender, EventArgs e)
        {
            if (opener.SMS_folders.SelectedNode.Parent == null)
            {
                sms_gate = opener.SMS_folders.SelectedNode.Name;
            }
            else
            {
                sms_gate = opener.SMS_folders.SelectedNode.Parent.Name;
            }
        }

        private void btn_SMS_send_Click(object sender, EventArgs e)
        {
            try
            {
                if ((SMS_sendTo.Text.Length == 13) && (SMS_text.Text!=""))
                {
                    object[,] sql_insert = {
                        {"sms_gate", sms_gate},
                        {"sms_to", SMS_sendTo.Text},
                        {"sms_text", SMS_text.Text},
                        {"sms_send_after", "0000-00-00 00:00:00"}
                    };
                    new MySQLDriverCS.MySQLInsertCommand(opener.MYSQL, sql_insert, opener.sql_db_table + "_sms_outqueue");
                    opener.LogDebug("Send SMS to: " + SMS_sendTo.Text, "SMS");
                    opener.ShowAppNotify("SMS byla �sp�n� odesl�na.", 3, ToolTipIcon.Info);
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                opener.LogDebug("sending SMS: " + ex.ToString(), "ERROR");
                opener.ShowAppNotify("Chyba p�i odes�l�n� SMS.\r\n" + ex.ToString(), 3, ToolTipIcon.Error);
            }
        }

        private void SMS_text_TextChanged(object sender, EventArgs e)
        {
            int SMSChars = SMS_text.Text.Length;
            int SMSCharsLeft = 155 - SMSChars;
            
            SMS_charCount.Text = SMSChars.ToString();
            SMS_charCountLeft.Text = SMSCharsLeft.ToString();

            if (SMSCharsLeft > 60) SMS_charCountLeft.ForeColor = Color.DarkGreen;
            if (SMSCharsLeft < 60) SMS_charCountLeft.ForeColor = Color.DarkRed;
            if (SMSCharsLeft < 25) SMS_charCountLeft.ForeColor = Color.Red;
        }
    }
}