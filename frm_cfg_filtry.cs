using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySQLDriverCS;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
    public partial class frm_cfg_filtry : KryptonForm
    {
        MainForm opener = null;
        int LastEditID = -1;
        
        public frm_cfg_filtry(MainForm sender)
        {
            opener = sender;
            InitializeComponent();
        }

        private void frm_cfg_filtry_Load(object sender, EventArgs e)
        {
            btn_DeleteFiltr.Enabled = false;
            Load_CtkKategorie_Kraje();
            Load_Ctk_Filtry();
        }

        private void Load_CtkKategorie_Kraje()
        {
            ctk_kategorie.Items.Clear();
            for (int loop = 0; loop < opener.ctk_kat_list.Items.Count; loop++)
            {
                ctk_kategorie.Items.Add(opener.ctk_kat_list.Items[loop]);
            }

            ctk_kraj.Items.Clear();
            for (int loop = 0; loop < opener.filtry_ctk_kraj.Items.Count; loop++)
            {
                ctk_kraj.Items.Add(opener.filtry_ctk_kraj.Items[loop]);
            }
        }

        private void Load_Ctk_Filtry()
        {
            gb_filtr.Enabled = false;
            ctk_filtry.Items.Clear();
            btn_DeleteFiltr.Enabled = false;
            string ctk_sql = "";
            ctk_sql = "SELECT titulek FROM " + opener.sql_db_table_filter + " WHERE ctk_user = '" + opener.ActualProfile + "' ORDER BY titulek ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                while (ctk_reader.Read())
                {
                    ctk_filtry.Items.Add(ctk_reader["titulek"].ToString());
                }
            }
            catch (Exception e)
            {
                opener.LogDebug(e.ToString(), "ERROR");
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            this.Close();
            Application.DoEvents();
        }

        private void ctk_filtry_SelectionChanged(object sender, EventArgs e)
        {
            btn_DeleteFiltr.Enabled = true;
            Load_FILTR();
        }

        void Load_FILTR()
        {
            string ctk_sql = "";
            gb_filtr.Enabled = true;
            for (int loop = 0; loop < ctk_kategorie.Items.Count; loop++)
            {
                ctk_kategorie.SetItemCheckState(loop, CheckState.Unchecked);
            }
            ctk_sql = "SELECT * FROM " + opener.sql_db_table_filter + " WHERE ctk_user = '" + opener.ActualProfile + "' AND titulek='" + ctk_filtry.Text + "' LIMIT 1";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, opener.MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                opener.LogDebug(ctk_sql, "SQL");
                ctk_reader.Read();

                try { LastEditID = Convert.ToInt32(ctk_reader["id"].ToString()); } catch {}

                ctk_filtr_titulek.Text = ctk_reader["titulek"].ToString();
                ctk_nadpis.Text = ctk_reader["f_titulek"].ToString();
                ctk_klicovaSlova.Text = ctk_reader["f_klicova_slova"].ToString();
                ctk_text.Text = ctk_reader["f_text"].ToString();
                ctk_kraj.Text = ctk_reader["f_kraj"].ToString();

                if (ctk_reader["f_kategorie_and"].ToString() == "1")
                {
                    ctk_kategorie_and.Checked = true;
                } else {
                    ctk_kategorie_and.Checked = false;
                }

                if (ctk_reader["f_filtr_and"].ToString() == "1")
                {
                    ctk_filtry_and.Checked = true;
                } else {
                    ctk_filtry_and.Checked = false;
                }


                if (ctk_reader["f_kategorie"].ToString() != "")
                {
                    string[] f_kat = new string[100];
                    f_kat = ctk_reader["f_kategorie"].ToString().Split('*');
                    for (int extloop = 0; extloop < f_kat.Length; extloop++)
                    {
                        for (int loop = 0; loop < opener.xml_kat_id.Count; loop++)
                        {
                            if (opener.xml_kat_id[loop].Value == f_kat[extloop])
                            {
                                ctk_kategorie.SetItemCheckState(loop, CheckState.Checked);
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                opener.LogDebug(e.ToString(), "ERROR");
            }
        }

        private void btn_newFiltr_Click(object sender, EventArgs e)
        {
            gb_filtr.Enabled = true;
            btn_DeleteFiltr.Enabled = false;
            LastEditID = -1;
            ctk_filtr_titulek.Text = "Nov� filtr";
            ctk_filtr_titulek.Focus();
            ctk_filtr_titulek.SelectAll();
            ctk_nadpis.Text = "";
            ctk_klicovaSlova.Text = "";
            ctk_text.Text = "";
            ctk_kraj.SelectedIndex = 0;
            ctk_kategorie_and.Checked = false;
            ctk_filtry_and.Checked = true;
            for (int loop = 0; loop < ctk_kategorie.Items.Count; loop++)
            {
                ctk_kategorie.SetItemCheckState(loop, CheckState.Unchecked);
            }
        }

        private void btn_DeleteFiltr_Click(object sender, EventArgs e)
        {
            if (LastEditID > 0)
            {
                DialogResult res = MessageBox.Show("Opravdu chcete smazat tento filtr?", "Smazat filtr", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
                if (res == DialogResult.Yes)
                {
                    try
                    {
                        new MySQLDeleteCommand(opener.MYSQL, opener.sql_db_table_filter, new object[,] { { "ctk_user", "=", opener.ActualProfile }, { "id", "=", LastEditID } }, null);
                        opener.LogDebug("Delete filtr id: " + LastEditID.ToString(), "SQL");
                    }
                    catch { }
                    Load_Ctk_Filtry();
                }
            }
        }

        private void btn_saveFiltr_Click(object sender, EventArgs e)
        {
            string f_kategorie = null;
            string f_kategorie_and = "0";
            string f_filtr_and = "0";

            if (ctk_kategorie_and.Checked == true) f_kategorie_and = "1";
            if (ctk_filtry_and.Checked == true) f_filtr_and = "1";

            for (int loop = 0; loop < ctk_kategorie.Items.Count; loop++)
            {
                if (ctk_kategorie.GetItemCheckState(loop) == CheckState.Checked)
                {
                    if (f_kategorie != null) f_kategorie += "*";
                    f_kategorie += opener.xml_kat_id.Item(loop).Value;
                }
            }

            if (LastEditID == -1)
            {
                //NEW - Insert
                object[,] sql_insert = {
                    { "id", null },
                    { "ctk_user", opener.ActualProfile },
                    { "titulek", ctk_filtr_titulek.Text},
                    { "f_kategorie", f_kategorie},
                    { "f_kategorie_and", f_kategorie_and},
                    { "f_titulek", ctk_nadpis.Text},
                    { "f_klicova_slova", ctk_klicovaSlova.Text},
                    { "f_text", ctk_text.Text},
                    { "f_kraj", ctk_kraj.Text},
                    { "f_filtr_and", f_filtr_and}
                };
                try
                {
                    new MySQLInsertCommand(opener.MYSQL, sql_insert, opener.sql_db_table_filter);
                    opener.LogDebug("New filter saved", "SQL");
                }
                catch (Exception ex) { opener.LogDebug(ex.ToString(), "SQL"); }
            }
            else
            {
                //OLD - Update
                object[,] sql_update = {
                    { "ctk_user", opener.ActualProfile },
                    { "titulek", ctk_filtr_titulek.Text},
                    { "f_kategorie", f_kategorie},
                    { "f_kategorie_and", f_kategorie_and},
                    { "f_titulek", ctk_nadpis.Text},
                    { "f_klicova_slova", ctk_klicovaSlova.Text},
                    { "f_text", ctk_text.Text},
                    { "f_kraj", ctk_kraj.Text},
                    { "f_filtr_and", f_filtr_and}
                };
                object[,] sql_where = {
                    {"id", "=", LastEditID},
                    {"ctk_user", "=", opener.ActualProfile}
                };
                try
                {
                    new MySQLUpdateCommand(opener.MYSQL, sql_update, opener.sql_db_table_filter, sql_where, null);
                    opener.LogDebug("New filter saved", "SQL");
                }
                catch (Exception ex) { opener.LogDebug(ex.ToString(), "SQL"); }
            }

            Load_Ctk_Filtry();
        }

        private void frm_cfg_filtr_FormClosing(object sender, FormClosingEventArgs e)
        {
            opener.ctk_nacti_filtry();
        }
    }
}