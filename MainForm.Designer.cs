using System.Windows.Forms;
namespace CTK_reader
{
    partial class MainForm
    {
        private System.ComponentModel.IContainer components;

        #region Windows Forms Designer generated code
        /// <summary>
        /// This method is required for Windows Forms designer support.
        /// Do not change the method contents inside the source code editor. The Forms designer might
        /// not be able to load this method if it was changed manually.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Node");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Root", new System.Windows.Forms.TreeNode[] {
            treeNode1});
            this.ctk_filtry_spojeni = new System.Windows.Forms.CheckBox();
            this.ctk_zrus_filtry_icon = new System.Windows.Forms.PictureBox();
            this.ctk_kat_list = new System.Windows.Forms.CheckedListBox();
            this.filtry_ctk_klicovaSlova = new System.Windows.Forms.TextBox();
            this.ctk_kat_list_spojeni = new System.Windows.Forms.CheckBox();
            this.ctk_list_timer = new System.Windows.Forms.Timer(this.components);
            this.ctk_detail = new System.Windows.Forms.TextBox();
            this.mmenu_ctk_detail = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mmenu_ctk_detail_kopirovat = new System.Windows.Forms.ToolStripMenuItem();
            this.mmenu_ctk_detail_kopirovat_celou = new System.Windows.Forms.ToolStripMenuItem();
            this.mmenu_ctk_detail_kopirovat_csv = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.mmenu_ctk_detail_tisk = new System.Windows.Forms.ToolStripMenuItem();
            this.mmenu_ctk_detail_tisk_nahled = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.mmenu_ctk_detail_ulozit = new System.Windows.Forms.ToolStripMenuItem();
            this.mmenu_ctk_detail_ulozit_csv = new System.Windows.Forms.ToolStripMenuItem();
            this.mmenu_ctk_detail_email = new System.Windows.Forms.ToolStripMenuItem();
            this.ctk_ulozeny_filtr = new System.Windows.Forms.ComboBox();
            this.dialog_mmenu_ulozit = new System.Windows.Forms.SaveFileDialog();
            this.ctk_tabDatum = new System.Windows.Forms.TabPage();
            this.ctk_ulozeny_filtr_set = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.ctk_zrus_filtry = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.ctk_calendar = new System.Windows.Forms.MonthCalendar();
            this.filtry_ctk_text = new System.Windows.Forms.TextBox();
            this.lbl_ctk_filtry_klicovaSlova = new System.Windows.Forms.Label();
            this.ctk_tabKat = new System.Windows.Forms.TabPage();
            this.ctk_kat_nastav = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.filtry_ctk_nadpis = new System.Windows.Forms.TextBox();
            this.ctk_tabFiltry = new System.Windows.Forms.TabPage();
            this.btn_ctk_filtry_nastav = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.lbl_ctk_filtry_kraj = new System.Windows.Forms.Label();
            this.filtry_ctk_kraj = new System.Windows.Forms.ComboBox();
            this.lbl_ctk_filtry_text = new System.Windows.Forms.Label();
            this.lbl_ctk_filtry_nadpis = new System.Windows.Forms.Label();
            this.ctk_tab = new Dotnetrix.Controls.TabControlEX();
            this.dg_ctk_list = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.datum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.kategorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.titulek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.klicova_slova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.flag = new System.Windows.Forms.DataGridViewImageColumn();
            this.timer_ctkStart = new System.Windows.Forms.Timer(this.components);
            this.bg_AU = new System.ComponentModel.BackgroundWorker();
            this.SBnews_panel = new Dotnetrix.Controls.TabControlEX();
            this.SBnews_tab_ctk = new System.Windows.Forms.TabPage();
            this.CTK_split_main = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.CTK_split_top = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.CTK_panel_right = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.buttonSpecHeaderGroup1 = new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup();
            this.kryptonBorderEdge1 = new ComponentFactory.Krypton.Toolkit.KryptonBorderEdge();
            this.SBnews_tab_CustNews = new Dotnetrix.Controls.TabPageEX();
            this.CustNews_split_main = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.CustNews_split_dg_detail = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.dg_CustNews_list = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_datum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_cas = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_okres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_nazev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_klicova_slova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_CustNews_list_repo_ico = new System.Windows.Forms.DataGridViewImageColumn();
            this.dg_CustNews_list_id_repa = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CustNews_txt_detail = new System.Windows.Forms.TextBox();
            this.CustNews_main_panel_right = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.CustNews_main_panel_right_ShowHide = new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup();
            this.btn_CustNews_showAll = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.CustNews_sel_text = new System.Windows.Forms.TextBox();
            this.kryptonLabel9 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btn_CustNews_sel_drop = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CustNews_sel_set = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel6 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.CustNews_sel_oblast = new System.Windows.Forms.ComboBox();
            this.kryptonLabel7 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel8 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.CustNews_sel_group = new System.Windows.Forms.ComboBox();
            this.CustNews_sel_kategorie = new System.Windows.Forms.ComboBox();
            this.btn_CustNews_edit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_CustNews_new = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.CustNews_Calendar = new System.Windows.Forms.MonthCalendar();
            this.SBnews_tab_ris = new System.Windows.Forms.TabPage();
            this.RIS_panel_main = new ComponentFactory.Krypton.Toolkit.KryptonPanel();
            this.RIS_main_split_main = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.RIS_split_dg_detail = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.dg_RIS_list = new System.Windows.Forms.DataGridView();
            this.dg_RIS_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_RIS_datum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_RIS_okres = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_RIS_titulek = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_RIS_klicova_slova = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dg_RIS_medialni_partner = new System.Windows.Forms.DataGridViewImageColumn();
            this.dg_RIS_promo_vysilani = new System.Windows.Forms.DataGridViewImageColumn();
            this.RIS_txt_detail = new System.Windows.Forms.TextBox();
            this.RIS_main_panel_main_left = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.RIS_main_split_main_2_show_hide = new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup();
            this.RIS_sel_all_versions = new ComponentFactory.Krypton.Toolkit.KryptonCheckButton();
            this.RIS_btn_reload_clean = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.RIS_btn_reload = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.kryptonLabel5 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.RIS_sel_oblast = new System.Windows.Forms.ComboBox();
            this.kryptonLabel4 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel3 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel2 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.kryptonLabel1 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.btn_RIS_edit = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.RIS_calendar = new System.Windows.Forms.MonthCalendar();
            this.btn_RIS_new = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.RIS_sel_nazev = new System.Windows.Forms.TextBox();
            this.RIS_sel_group = new System.Windows.Forms.ComboBox();
            this.RIS_sel_mesto = new System.Windows.Forms.TextBox();
            this.RIS_sel_kategorie = new System.Windows.Forms.ComboBox();
            this.SBnews_tab_doprava = new Dotnetrix.Controls.TabPageEX();
            this.SBnews_doprava_split_main = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.SBnews_doprava_split_secondary = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.SBnews_doprava_panel_left = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.doprava_web_stupne = new System.Windows.Forms.WebBrowser();
            this.SBnews_Doprava_panel_main = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.doprava_web_nehody = new System.Windows.Forms.WebBrowser();
            this.SBnews_Doprava_panel_right = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.kryptonLabel10 = new ComponentFactory.Krypton.Toolkit.KryptonLabel();
            this.SBnews_doprava_kraj = new System.Windows.Forms.ListBox();
            this.SBnews_tab_sms = new System.Windows.Forms.TabPage();
            this.SMS_split_main = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.SMS_split1 = new ComponentFactory.Krypton.Toolkit.KryptonSplitContainer();
            this.dg_SMS = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_gate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_from = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_time = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_folder = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_text = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.sms_flag = new System.Windows.Forms.DataGridViewImageColumn();
            this.SMS_detail = new System.Windows.Forms.TextBox();
            this.SMS_panel_right = new ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup();
            this.SMS_panel_right_showhide = new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup();
            this.btn_SMS_new = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.btn_SMS_reply = new ComponentFactory.Krypton.Toolkit.KryptonButton();
            this.SMS_calendar = new System.Windows.Forms.MonthCalendar();
            this.SMS_folders = new System.Windows.Forms.TreeView();
            this.SBnews_tab_scratch = new System.Windows.Forms.TabPage();
            this.SBnews_ScratchBox = new System.Windows.Forms.TextBox();
            this.appNotify = new System.Windows.Forms.NotifyIcon(this.components);
            this.SMS_timer = new System.Windows.Forms.Timer(this.components);
            this.dialog_mmenu_ulozit_csv = new System.Windows.Forms.SaveFileDialog();
            this.ctk_status = new System.Windows.Forms.StatusStrip();
            this.LoadingCircle_statusbar = new MRG.Controls.UI.LoadingCircleToolStripMenuItem();
            this.ctk_status_label = new System.Windows.Forms.ToolStripStatusLabel();
            this.kryptonManager1 = new ComponentFactory.Krypton.Toolkit.KryptonManager(this.components);
            this.CustNews_timer_dg_reload = new System.Windows.Forms.Timer(this.components);
            this.top_menu = new System.Windows.Forms.MenuStrip();
            this.top_menu_soubor = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_soubor__profile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.top_menu_soubor__save = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_soubor__saveCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_soubor__sendEmail = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.top_menu_soubor__print = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_soubor__print_preview = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator11 = new System.Windows.Forms.ToolStripSeparator();
            this.top_menu_soubor__quit = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_editace = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_editace__copy = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_editace__copyAll = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_editace__copyCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_nastaveni = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_nastaveni__fonts = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_nastaveni__CTK = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_nastaveni__CTK_filters = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator12 = new System.Windows.Forms.ToolStripSeparator();
            this.top_menu_nastaveni__mysql = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu__about = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu__update = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_debug = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_debug__showLog = new System.Windows.Forms.ToolStripMenuItem();
            this.top_menu_debug__hideLog = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator13 = new System.Windows.Forms.ToolStripSeparator();
            this.top_menu_debug__raiseError = new System.Windows.Forms.ToolStripMenuItem();
            this.timer_doprava = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ctk_zrus_filtry_icon)).BeginInit();
            this.mmenu_ctk_detail.SuspendLayout();
            this.ctk_tabDatum.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctk_ulozeny_filtr_set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctk_zrus_filtry)).BeginInit();
            this.ctk_tabKat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ctk_kat_nastav)).BeginInit();
            this.ctk_tabFiltry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_ctk_filtry_nastav)).BeginInit();
            this.ctk_tab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_ctk_list)).BeginInit();
            this.SBnews_panel.SuspendLayout();
            this.SBnews_tab_ctk.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main.Panel1)).BeginInit();
            this.CTK_split_main.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main.Panel2)).BeginInit();
            this.CTK_split_main.Panel2.SuspendLayout();
            this.CTK_split_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top.Panel1)).BeginInit();
            this.CTK_split_top.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top.Panel2)).BeginInit();
            this.CTK_split_top.Panel2.SuspendLayout();
            this.CTK_split_top.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_panel_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_panel_right.Panel)).BeginInit();
            this.CTK_panel_right.Panel.SuspendLayout();
            this.CTK_panel_right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonBorderEdge1)).BeginInit();
            this.SBnews_tab_CustNews.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main.Panel1)).BeginInit();
            this.CustNews_split_main.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main.Panel2)).BeginInit();
            this.CustNews_split_main.Panel2.SuspendLayout();
            this.CustNews_split_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail.Panel1)).BeginInit();
            this.CustNews_split_dg_detail.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail.Panel2)).BeginInit();
            this.CustNews_split_dg_detail.Panel2.SuspendLayout();
            this.CustNews_split_dg_detail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_CustNews_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_main_panel_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_main_panel_right.Panel)).BeginInit();
            this.CustNews_main_panel_right.Panel.SuspendLayout();
            this.CustNews_main_panel_right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_showAll)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_sel_drop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_sel_set)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_new)).BeginInit();
            this.SBnews_tab_ris.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_panel_main)).BeginInit();
            this.RIS_panel_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main.Panel1)).BeginInit();
            this.RIS_main_split_main.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main.Panel2)).BeginInit();
            this.RIS_main_split_main.Panel2.SuspendLayout();
            this.RIS_main_split_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail.Panel1)).BeginInit();
            this.RIS_split_dg_detail.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail.Panel2)).BeginInit();
            this.RIS_split_dg_detail.Panel2.SuspendLayout();
            this.RIS_split_dg_detail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_RIS_list)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_panel_main_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_panel_main_left.Panel)).BeginInit();
            this.RIS_main_panel_main_left.Panel.SuspendLayout();
            this.RIS_main_panel_main_left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_sel_all_versions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_btn_reload_clean)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_btn_reload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RIS_edit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RIS_new)).BeginInit();
            this.SBnews_tab_doprava.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main.Panel1)).BeginInit();
            this.SBnews_doprava_split_main.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main.Panel2)).BeginInit();
            this.SBnews_doprava_split_main.Panel2.SuspendLayout();
            this.SBnews_doprava_split_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary.Panel1)).BeginInit();
            this.SBnews_doprava_split_secondary.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary.Panel2)).BeginInit();
            this.SBnews_doprava_split_secondary.Panel2.SuspendLayout();
            this.SBnews_doprava_split_secondary.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_panel_left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_panel_left.Panel)).BeginInit();
            this.SBnews_doprava_panel_left.Panel.SuspendLayout();
            this.SBnews_doprava_panel_left.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_main.Panel)).BeginInit();
            this.SBnews_Doprava_panel_main.Panel.SuspendLayout();
            this.SBnews_Doprava_panel_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_right.Panel)).BeginInit();
            this.SBnews_Doprava_panel_right.Panel.SuspendLayout();
            this.SBnews_Doprava_panel_right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel10)).BeginInit();
            this.SBnews_tab_sms.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main.Panel1)).BeginInit();
            this.SMS_split_main.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main.Panel2)).BeginInit();
            this.SMS_split_main.Panel2.SuspendLayout();
            this.SMS_split_main.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1.Panel1)).BeginInit();
            this.SMS_split1.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1.Panel2)).BeginInit();
            this.SMS_split1.Panel2.SuspendLayout();
            this.SMS_split1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dg_SMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_panel_right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_panel_right.Panel)).BeginInit();
            this.SMS_panel_right.Panel.SuspendLayout();
            this.SMS_panel_right.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_new)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_reply)).BeginInit();
            this.SBnews_tab_scratch.SuspendLayout();
            this.ctk_status.SuspendLayout();
            this.top_menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // ctk_filtry_spojeni
            // 
            this.ctk_filtry_spojeni.Checked = true;
            this.ctk_filtry_spojeni.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ctk_filtry_spojeni.Location = new System.Drawing.Point(16, 208);
            this.ctk_filtry_spojeni.Name = "ctk_filtry_spojeni";
            this.ctk_filtry_spojeni.Size = new System.Drawing.Size(80, 24);
            this.ctk_filtry_spojeni.TabIndex = 7;
            this.ctk_filtry_spojeni.Text = "z�rove�";
            // 
            // ctk_zrus_filtry_icon
            // 
            this.ctk_zrus_filtry_icon.Image = ((System.Drawing.Image)(resources.GetObject("ctk_zrus_filtry_icon.Image")));
            this.ctk_zrus_filtry_icon.Location = new System.Drawing.Point(8, 208);
            this.ctk_zrus_filtry_icon.Name = "ctk_zrus_filtry_icon";
            this.ctk_zrus_filtry_icon.Size = new System.Drawing.Size(24, 24);
            this.ctk_zrus_filtry_icon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ctk_zrus_filtry_icon.TabIndex = 1;
            this.ctk_zrus_filtry_icon.TabStop = false;
            this.ctk_zrus_filtry_icon.Visible = false;
            // 
            // ctk_kat_list
            // 
            this.ctk_kat_list.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_kat_list.CheckOnClick = true;
            this.ctk_kat_list.Location = new System.Drawing.Point(8, 8);
            this.ctk_kat_list.Name = "ctk_kat_list";
            this.ctk_kat_list.Size = new System.Drawing.Size(160, 180);
            this.ctk_kat_list.TabIndex = 0;
            this.ctk_kat_list.SelectedIndexChanged += new System.EventHandler(this.sql_filtr_text_changed);
            // 
            // filtry_ctk_klicovaSlova
            // 
            this.filtry_ctk_klicovaSlova.Location = new System.Drawing.Point(8, 72);
            this.filtry_ctk_klicovaSlova.Name = "filtry_ctk_klicovaSlova";
            this.filtry_ctk_klicovaSlova.Size = new System.Drawing.Size(160, 21);
            this.filtry_ctk_klicovaSlova.TabIndex = 2;
            this.filtry_ctk_klicovaSlova.TextChanged += new System.EventHandler(this.sql_filtr_text_changed);
            // 
            // ctk_kat_list_spojeni
            // 
            this.ctk_kat_list_spojeni.Location = new System.Drawing.Point(16, 208);
            this.ctk_kat_list_spojeni.Name = "ctk_kat_list_spojeni";
            this.ctk_kat_list_spojeni.Size = new System.Drawing.Size(80, 24);
            this.ctk_kat_list_spojeni.TabIndex = 2;
            this.ctk_kat_list_spojeni.Text = "z�rove�";
            // 
            // ctk_list_timer
            // 
            this.ctk_list_timer.Interval = 1000;
            this.ctk_list_timer.Tick += new System.EventHandler(this.Ctk_list_timerTick);
            // 
            // ctk_detail
            // 
            this.ctk_detail.AcceptsReturn = true;
            this.ctk_detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_detail.ContextMenuStrip = this.mmenu_ctk_detail;
            this.ctk_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctk_detail.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ctk_detail.Location = new System.Drawing.Point(0, 0);
            this.ctk_detail.Multiline = true;
            this.ctk_detail.Name = "ctk_detail";
            this.ctk_detail.ReadOnly = true;
            this.ctk_detail.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ctk_detail.Size = new System.Drawing.Size(857, 183);
            this.ctk_detail.TabIndex = 2;
            this.ctk_detail.Text = "ctk_detail";
            // 
            // mmenu_ctk_detail
            // 
            this.mmenu_ctk_detail.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.mmenu_ctk_detail.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mmenu_ctk_detail_kopirovat,
            this.mmenu_ctk_detail_kopirovat_celou,
            this.mmenu_ctk_detail_kopirovat_csv,
            this.toolStripSeparator6,
            this.mmenu_ctk_detail_tisk,
            this.mmenu_ctk_detail_tisk_nahled,
            this.toolStripSeparator7,
            this.mmenu_ctk_detail_ulozit,
            this.mmenu_ctk_detail_ulozit_csv,
            this.mmenu_ctk_detail_email});
            this.mmenu_ctk_detail.Name = "mmenu_ctk_detail";
            this.mmenu_ctk_detail.Size = new System.Drawing.Size(270, 192);
            // 
            // mmenu_ctk_detail_kopirovat
            // 
            this.mmenu_ctk_detail_kopirovat.Image = global::CTK_reader.Properties.Resources.copy;
            this.mmenu_ctk_detail_kopirovat.Name = "mmenu_ctk_detail_kopirovat";
            this.mmenu_ctk_detail_kopirovat.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.mmenu_ctk_detail_kopirovat.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_kopirovat.Text = "Kop�rovat";
            this.mmenu_ctk_detail_kopirovat.Click += new System.EventHandler(this.mmenu_kopirovat_click);
            // 
            // mmenu_ctk_detail_kopirovat_celou
            // 
            this.mmenu_ctk_detail_kopirovat_celou.Image = global::CTK_reader.Properties.Resources.copy_all;
            this.mmenu_ctk_detail_kopirovat_celou.Name = "mmenu_ctk_detail_kopirovat_celou";
            this.mmenu_ctk_detail_kopirovat_celou.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.C)));
            this.mmenu_ctk_detail_kopirovat_celou.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_kopirovat_celou.Text = "Kop�rovat celou zpr�vu";
            this.mmenu_ctk_detail_kopirovat_celou.Click += new System.EventHandler(this.mmenu_kopirovat_celou_click);
            // 
            // mmenu_ctk_detail_kopirovat_csv
            // 
            this.mmenu_ctk_detail_kopirovat_csv.Image = global::CTK_reader.Properties.Resources.csv_export;
            this.mmenu_ctk_detail_kopirovat_csv.Name = "mmenu_ctk_detail_kopirovat_csv";
            this.mmenu_ctk_detail_kopirovat_csv.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_kopirovat_csv.Text = "Kop�rovat jako CSV";
            this.mmenu_ctk_detail_kopirovat_csv.Click += new System.EventHandler(this.mmenu_kopirovat_csv_click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(266, 6);
            // 
            // mmenu_ctk_detail_tisk
            // 
            this.mmenu_ctk_detail_tisk.Image = global::CTK_reader.Properties.Resources.print;
            this.mmenu_ctk_detail_tisk.Name = "mmenu_ctk_detail_tisk";
            this.mmenu_ctk_detail_tisk.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_tisk.Text = "Tisk";
            this.mmenu_ctk_detail_tisk.Click += new System.EventHandler(this.mmenu_tisk_click);
            // 
            // mmenu_ctk_detail_tisk_nahled
            // 
            this.mmenu_ctk_detail_tisk_nahled.Image = global::CTK_reader.Properties.Resources.print_preview;
            this.mmenu_ctk_detail_tisk_nahled.Name = "mmenu_ctk_detail_tisk_nahled";
            this.mmenu_ctk_detail_tisk_nahled.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_tisk_nahled.Text = "N�hled tisku";
            this.mmenu_ctk_detail_tisk_nahled.Click += new System.EventHandler(this.mmenu_tisk_nahled_click);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(266, 6);
            // 
            // mmenu_ctk_detail_ulozit
            // 
            this.mmenu_ctk_detail_ulozit.Image = global::CTK_reader.Properties.Resources.save;
            this.mmenu_ctk_detail_ulozit.Name = "mmenu_ctk_detail_ulozit";
            this.mmenu_ctk_detail_ulozit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.mmenu_ctk_detail_ulozit.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_ulozit.Text = "Ulo�it jako text";
            this.mmenu_ctk_detail_ulozit.Click += new System.EventHandler(this.mmenu_ulozit_click);
            // 
            // mmenu_ctk_detail_ulozit_csv
            // 
            this.mmenu_ctk_detail_ulozit_csv.Image = global::CTK_reader.Properties.Resources.csv_export;
            this.mmenu_ctk_detail_ulozit_csv.Name = "mmenu_ctk_detail_ulozit_csv";
            this.mmenu_ctk_detail_ulozit_csv.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.mmenu_ctk_detail_ulozit_csv.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_ulozit_csv.Text = "Ulo�it jako  CSV";
            this.mmenu_ctk_detail_ulozit_csv.Click += new System.EventHandler(this.mmenu_ulozit_csv_Click);
            // 
            // mmenu_ctk_detail_email
            // 
            this.mmenu_ctk_detail_email.Enabled = false;
            this.mmenu_ctk_detail_email.Image = global::CTK_reader.Properties.Resources.mail;
            this.mmenu_ctk_detail_email.Name = "mmenu_ctk_detail_email";
            this.mmenu_ctk_detail_email.Size = new System.Drawing.Size(269, 22);
            this.mmenu_ctk_detail_email.Text = "Odeslat jako email";
            // 
            // ctk_ulozeny_filtr
            // 
            this.ctk_ulozeny_filtr.BackColor = System.Drawing.SystemColors.Window;
            this.ctk_ulozeny_filtr.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ctk_ulozeny_filtr.Location = new System.Drawing.Point(8, 176);
            this.ctk_ulozeny_filtr.Name = "ctk_ulozeny_filtr";
            this.ctk_ulozeny_filtr.Size = new System.Drawing.Size(160, 21);
            this.ctk_ulozeny_filtr.TabIndex = 3;
            // 
            // dialog_mmenu_ulozit
            // 
            this.dialog_mmenu_ulozit.DefaultExt = "*.txt";
            this.dialog_mmenu_ulozit.Filter = "Textov� soubor (*.txt)|*.txt";
            this.dialog_mmenu_ulozit.Title = "Ulo�it zpr�vu";
            // 
            // ctk_tabDatum
            // 
            this.ctk_tabDatum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_tabDatum.Controls.Add(this.ctk_ulozeny_filtr_set);
            this.ctk_tabDatum.Controls.Add(this.ctk_zrus_filtry);
            this.ctk_tabDatum.Controls.Add(this.ctk_ulozeny_filtr);
            this.ctk_tabDatum.Controls.Add(this.ctk_zrus_filtry_icon);
            this.ctk_tabDatum.Controls.Add(this.ctk_calendar);
            this.ctk_tabDatum.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ctk_tabDatum.Location = new System.Drawing.Point(4, 25);
            this.ctk_tabDatum.Name = "ctk_tabDatum";
            this.ctk_tabDatum.Size = new System.Drawing.Size(179, 363);
            this.ctk_tabDatum.TabIndex = 0;
            this.ctk_tabDatum.Text = "Datum";
            // 
            // ctk_ulozeny_filtr_set
            // 
            this.ctk_ulozeny_filtr_set.DirtyPaletteCounter = 97;
            this.ctk_ulozeny_filtr_set.Location = new System.Drawing.Point(128, 208);
            this.ctk_ulozeny_filtr_set.Name = "ctk_ulozeny_filtr_set";
            this.ctk_ulozeny_filtr_set.Size = new System.Drawing.Size(40, 25);
            this.ctk_ulozeny_filtr_set.TabIndex = 6;
            this.ctk_ulozeny_filtr_set.Text = "OK";
            this.ctk_ulozeny_filtr_set.Values.ExtraText = "";
            this.ctk_ulozeny_filtr_set.Values.Image = null;
            this.ctk_ulozeny_filtr_set.Values.ImageStates.ImageCheckedNormal = null;
            this.ctk_ulozeny_filtr_set.Values.ImageStates.ImageCheckedPressed = null;
            this.ctk_ulozeny_filtr_set.Values.ImageStates.ImageCheckedTracking = null;
            this.ctk_ulozeny_filtr_set.Values.Text = "OK";
            this.ctk_ulozeny_filtr_set.Click += new System.EventHandler(this.ctk_ulozeny_filtr_set_click);
            // 
            // ctk_zrus_filtry
            // 
            this.ctk_zrus_filtry.DirtyPaletteCounter = 97;
            this.ctk_zrus_filtry.Location = new System.Drawing.Point(42, 208);
            this.ctk_zrus_filtry.Name = "ctk_zrus_filtry";
            this.ctk_zrus_filtry.Size = new System.Drawing.Size(80, 25);
            this.ctk_zrus_filtry.TabIndex = 5;
            this.ctk_zrus_filtry.Text = "Zru�it filtry";
            this.ctk_zrus_filtry.Values.ExtraText = "";
            this.ctk_zrus_filtry.Values.Image = null;
            this.ctk_zrus_filtry.Values.ImageStates.ImageCheckedNormal = null;
            this.ctk_zrus_filtry.Values.ImageStates.ImageCheckedPressed = null;
            this.ctk_zrus_filtry.Values.ImageStates.ImageCheckedTracking = null;
            this.ctk_zrus_filtry.Values.Text = "Zru�it filtry";
            this.ctk_zrus_filtry.Click += new System.EventHandler(this.ctk_zrus_filtry_click);
            // 
            // ctk_calendar
            // 
            this.ctk_calendar.Location = new System.Drawing.Point(11, 8);
            this.ctk_calendar.MaxSelectionCount = 31;
            this.ctk_calendar.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.ctk_calendar.Name = "ctk_calendar";
            this.ctk_calendar.TabIndex = 0;
            this.ctk_calendar.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.ctk_calendar_date_selected);
            // 
            // filtry_ctk_text
            // 
            this.filtry_ctk_text.Location = new System.Drawing.Point(8, 117);
            this.filtry_ctk_text.Name = "filtry_ctk_text";
            this.filtry_ctk_text.Size = new System.Drawing.Size(160, 21);
            this.filtry_ctk_text.TabIndex = 4;
            this.filtry_ctk_text.TextChanged += new System.EventHandler(this.sql_filtr_text_changed);
            // 
            // lbl_ctk_filtry_klicovaSlova
            // 
            this.lbl_ctk_filtry_klicovaSlova.Location = new System.Drawing.Point(8, 56);
            this.lbl_ctk_filtry_klicovaSlova.Name = "lbl_ctk_filtry_klicovaSlova";
            this.lbl_ctk_filtry_klicovaSlova.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_klicovaSlova.TabIndex = 3;
            this.lbl_ctk_filtry_klicovaSlova.Text = "obsahuje v kl��ov�ch slovech";
            // 
            // ctk_tabKat
            // 
            this.ctk_tabKat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_tabKat.Controls.Add(this.ctk_kat_nastav);
            this.ctk_tabKat.Controls.Add(this.ctk_kat_list_spojeni);
            this.ctk_tabKat.Controls.Add(this.ctk_kat_list);
            this.ctk_tabKat.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ctk_tabKat.Location = new System.Drawing.Point(4, 25);
            this.ctk_tabKat.Name = "ctk_tabKat";
            this.ctk_tabKat.Size = new System.Drawing.Size(179, 363);
            this.ctk_tabKat.TabIndex = 3;
            this.ctk_tabKat.Text = "Kategorie";
            // 
            // ctk_kat_nastav
            // 
            this.ctk_kat_nastav.DirtyPaletteCounter = 97;
            this.ctk_kat_nastav.Location = new System.Drawing.Point(104, 208);
            this.ctk_kat_nastav.Name = "ctk_kat_nastav";
            this.ctk_kat_nastav.Size = new System.Drawing.Size(64, 25);
            this.ctk_kat_nastav.TabIndex = 3;
            this.ctk_kat_nastav.Text = "nastav";
            this.ctk_kat_nastav.Values.ExtraText = "";
            this.ctk_kat_nastav.Values.Image = null;
            this.ctk_kat_nastav.Values.ImageStates.ImageCheckedNormal = null;
            this.ctk_kat_nastav.Values.ImageStates.ImageCheckedPressed = null;
            this.ctk_kat_nastav.Values.ImageStates.ImageCheckedTracking = null;
            this.ctk_kat_nastav.Values.Text = "nastav";
            this.ctk_kat_nastav.Click += new System.EventHandler(this.ctk_kat_nastav_click);
            // 
            // filtry_ctk_nadpis
            // 
            this.filtry_ctk_nadpis.Location = new System.Drawing.Point(8, 24);
            this.filtry_ctk_nadpis.Name = "filtry_ctk_nadpis";
            this.filtry_ctk_nadpis.Size = new System.Drawing.Size(160, 21);
            this.filtry_ctk_nadpis.TabIndex = 0;
            this.filtry_ctk_nadpis.TextChanged += new System.EventHandler(this.sql_filtr_text_changed);
            // 
            // ctk_tabFiltry
            // 
            this.ctk_tabFiltry.Controls.Add(this.btn_ctk_filtry_nastav);
            this.ctk_tabFiltry.Controls.Add(this.lbl_ctk_filtry_kraj);
            this.ctk_tabFiltry.Controls.Add(this.filtry_ctk_kraj);
            this.ctk_tabFiltry.Controls.Add(this.ctk_filtry_spojeni);
            this.ctk_tabFiltry.Controls.Add(this.lbl_ctk_filtry_text);
            this.ctk_tabFiltry.Controls.Add(this.filtry_ctk_text);
            this.ctk_tabFiltry.Controls.Add(this.lbl_ctk_filtry_klicovaSlova);
            this.ctk_tabFiltry.Controls.Add(this.filtry_ctk_klicovaSlova);
            this.ctk_tabFiltry.Controls.Add(this.lbl_ctk_filtry_nadpis);
            this.ctk_tabFiltry.Controls.Add(this.filtry_ctk_nadpis);
            this.ctk_tabFiltry.Location = new System.Drawing.Point(4, 25);
            this.ctk_tabFiltry.Name = "ctk_tabFiltry";
            this.ctk_tabFiltry.Size = new System.Drawing.Size(179, 363);
            this.ctk_tabFiltry.TabIndex = 1;
            this.ctk_tabFiltry.Text = "Filtry";
            // 
            // btn_ctk_filtry_nastav
            // 
            this.btn_ctk_filtry_nastav.DirtyPaletteCounter = 97;
            this.btn_ctk_filtry_nastav.Location = new System.Drawing.Point(102, 206);
            this.btn_ctk_filtry_nastav.Name = "btn_ctk_filtry_nastav";
            this.btn_ctk_filtry_nastav.Size = new System.Drawing.Size(64, 25);
            this.btn_ctk_filtry_nastav.TabIndex = 10;
            this.btn_ctk_filtry_nastav.Text = "nastav";
            this.btn_ctk_filtry_nastav.Values.ExtraText = "";
            this.btn_ctk_filtry_nastav.Values.Image = null;
            this.btn_ctk_filtry_nastav.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_ctk_filtry_nastav.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_ctk_filtry_nastav.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_ctk_filtry_nastav.Values.Text = "nastav";
            this.btn_ctk_filtry_nastav.Click += new System.EventHandler(this.btn_ctk_filtry_nastav_click);
            // 
            // lbl_ctk_filtry_kraj
            // 
            this.lbl_ctk_filtry_kraj.Location = new System.Drawing.Point(8, 148);
            this.lbl_ctk_filtry_kraj.Name = "lbl_ctk_filtry_kraj";
            this.lbl_ctk_filtry_kraj.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_kraj.TabIndex = 9;
            this.lbl_ctk_filtry_kraj.Text = "Filtrovat podle kraje:";
            // 
            // filtry_ctk_kraj
            // 
            this.filtry_ctk_kraj.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.filtry_ctk_kraj.FormattingEnabled = true;
            this.filtry_ctk_kraj.Location = new System.Drawing.Point(8, 165);
            this.filtry_ctk_kraj.Name = "filtry_ctk_kraj";
            this.filtry_ctk_kraj.Size = new System.Drawing.Size(160, 21);
            this.filtry_ctk_kraj.TabIndex = 8;
            this.filtry_ctk_kraj.SelectedIndexChanged += new System.EventHandler(this.sql_filtr_kraj_changed);
            // 
            // lbl_ctk_filtry_text
            // 
            this.lbl_ctk_filtry_text.Location = new System.Drawing.Point(8, 101);
            this.lbl_ctk_filtry_text.Name = "lbl_ctk_filtry_text";
            this.lbl_ctk_filtry_text.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_text.TabIndex = 5;
            this.lbl_ctk_filtry_text.Text = "obsahuje v detailu textu";
            // 
            // lbl_ctk_filtry_nadpis
            // 
            this.lbl_ctk_filtry_nadpis.Location = new System.Drawing.Point(8, 8);
            this.lbl_ctk_filtry_nadpis.Name = "lbl_ctk_filtry_nadpis";
            this.lbl_ctk_filtry_nadpis.Size = new System.Drawing.Size(160, 16);
            this.lbl_ctk_filtry_nadpis.TabIndex = 1;
            this.lbl_ctk_filtry_nadpis.Text = "obsahuje v nadpisu";
            // 
            // ctk_tab
            // 
            this.ctk_tab.Appearance = Dotnetrix.Controls.TabAppearanceEX.FlatTab;
            this.ctk_tab.Controls.Add(this.ctk_tabDatum);
            this.ctk_tab.Controls.Add(this.ctk_tabKat);
            this.ctk_tab.Controls.Add(this.ctk_tabFiltry);
            this.ctk_tab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ctk_tab.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ctk_tab.HotColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ctk_tab.Location = new System.Drawing.Point(0, 0);
            this.ctk_tab.Name = "ctk_tab";
            this.ctk_tab.SelectedIndex = 0;
            this.ctk_tab.Size = new System.Drawing.Size(187, 392);
            this.ctk_tab.TabIndex = 5;
            this.ctk_tab.UseVisualStyles = false;
            this.ctk_tab.SelectedIndexChanged += new System.EventHandler(this.ctk_tab_SelectedIndexChanged);
            // 
            // dg_ctk_list
            // 
            this.dg_ctk_list.AllowUserToAddRows = false;
            this.dg_ctk_list.AllowUserToDeleteRows = false;
            this.dg_ctk_list.AllowUserToOrderColumns = true;
            this.dg_ctk_list.AllowUserToResizeRows = false;
            this.dg_ctk_list.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dg_ctk_list.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.dg_ctk_list.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dg_ctk_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_ctk_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.datum,
            this.cas,
            this.kategorie,
            this.titulek,
            this.klicova_slova,
            this.flag});
            this.dg_ctk_list.ContextMenuStrip = this.mmenu_ctk_detail;
            this.dg_ctk_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_ctk_list.Location = new System.Drawing.Point(0, 0);
            this.dg_ctk_list.Margin = new System.Windows.Forms.Padding(0);
            this.dg_ctk_list.MultiSelect = false;
            this.dg_ctk_list.Name = "dg_ctk_list";
            this.dg_ctk_list.ReadOnly = true;
            this.dg_ctk_list.RowHeadersVisible = false;
            this.dg_ctk_list.RowHeadersWidth = 20;
            this.dg_ctk_list.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_ctk_list.ShowCellErrors = false;
            this.dg_ctk_list.ShowCellToolTips = false;
            this.dg_ctk_list.ShowEditingIcon = false;
            this.dg_ctk_list.ShowRowErrors = false;
            this.dg_ctk_list.Size = new System.Drawing.Size(663, 424);
            this.dg_ctk_list.TabIndex = 1;
            this.dg_ctk_list.Sorted += new System.EventHandler(this.ctk_list_Sorted);
            this.dg_ctk_list.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dg_ctk_list_CellMouseClick);
            this.dg_ctk_list.SelectionChanged += new System.EventHandler(this.ctk_list_selectionChanged);
            // 
            // id
            // 
            this.id.HeaderText = "ID";
            this.id.MaxInputLength = 16000000;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Visible = false;
            this.id.Width = 68;
            // 
            // datum
            // 
            this.datum.HeaderText = "Datum";
            this.datum.Name = "datum";
            this.datum.ReadOnly = true;
            this.datum.Width = 68;
            // 
            // cas
            // 
            this.cas.HeaderText = "�as";
            this.cas.Name = "cas";
            this.cas.ReadOnly = true;
            this.cas.Width = 45;
            // 
            // kategorie
            // 
            this.kategorie.HeaderText = "Kategorie";
            this.kategorie.Name = "kategorie";
            this.kategorie.ReadOnly = true;
            this.kategorie.Width = 69;
            // 
            // titulek
            // 
            this.titulek.HeaderText = "Titulek";
            this.titulek.Name = "titulek";
            this.titulek.ReadOnly = true;
            this.titulek.Width = 150;
            // 
            // klicova_slova
            // 
            this.klicova_slova.HeaderText = "Kl��ov� slova";
            this.klicova_slova.Name = "klicova_slova";
            this.klicova_slova.ReadOnly = true;
            this.klicova_slova.Width = 200;
            // 
            // flag
            // 
            this.flag.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.flag.Description = "P��znak";
            this.flag.FillWeight = 16F;
            this.flag.HeaderText = "P";
            this.flag.Image = global::CTK_reader.Properties.Resources.flag_empty;
            this.flag.MinimumWidth = 16;
            this.flag.Name = "flag";
            this.flag.ReadOnly = true;
            this.flag.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.flag.Visible = false;
            this.flag.Width = 16;
            // 
            // timer_ctkStart
            // 
            this.timer_ctkStart.Tick += new System.EventHandler(this.timer_ctkStart_Tick);
            // 
            // bg_AU
            // 
            this.bg_AU.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bg_AU_DoWork);
            // 
            // SBnews_panel
            // 
            this.SBnews_panel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SBnews_panel.Appearance = Dotnetrix.Controls.TabAppearanceEX.FlatTab;
            this.SBnews_panel.Controls.Add(this.SBnews_tab_ctk);
            this.SBnews_panel.Controls.Add(this.SBnews_tab_CustNews);
            this.SBnews_panel.Controls.Add(this.SBnews_tab_ris);
            this.SBnews_panel.Controls.Add(this.SBnews_tab_doprava);
            this.SBnews_panel.Controls.Add(this.SBnews_tab_sms);
            this.SBnews_panel.Controls.Add(this.SBnews_tab_scratch);
            this.SBnews_panel.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SBnews_panel.Location = new System.Drawing.Point(0, 27);
            this.SBnews_panel.Name = "SBnews_panel";
            this.SBnews_panel.SelectedIndex = 0;
            this.SBnews_panel.Size = new System.Drawing.Size(871, 647);
            this.SBnews_panel.TabIndex = 8;
            this.SBnews_panel.UseVisualStyles = false;
            this.SBnews_panel.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.SBnews_panel_Selecting);
            this.SBnews_panel.Selected += new System.Windows.Forms.TabControlEventHandler(this.SBnews_panel_Selected);
            // 
            // SBnews_tab_ctk
            // 
            this.SBnews_tab_ctk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.SBnews_tab_ctk.Controls.Add(this.CTK_split_main);
            this.SBnews_tab_ctk.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_ctk.Name = "SBnews_tab_ctk";
            this.SBnews_tab_ctk.Padding = new System.Windows.Forms.Padding(3);
            this.SBnews_tab_ctk.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_ctk.TabIndex = 0;
            this.SBnews_tab_ctk.Text = "�TK";
            this.SBnews_tab_ctk.UseVisualStyleBackColor = true;
            // 
            // CTK_split_main
            // 
            this.CTK_split_main.Cursor = System.Windows.Forms.Cursors.Default;
            this.CTK_split_main.DirtyPaletteCounter = 160;
            this.CTK_split_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CTK_split_main.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CTK_split_main.Location = new System.Drawing.Point(3, 3);
            this.CTK_split_main.Name = "CTK_split_main";
            this.CTK_split_main.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.CTK_split_main.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            // 
            // CTK_split_main.Panel1
            // 
            this.CTK_split_main.Panel1.Controls.Add(this.CTK_split_top);
            this.CTK_split_main.Panel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.CTK_split_main.Panel1MinSize = 295;
            // 
            // CTK_split_main.Panel2
            // 
            this.CTK_split_main.Panel2.Controls.Add(this.kryptonBorderEdge1);
            this.CTK_split_main.Panel2.Controls.Add(this.ctk_detail);
            this.CTK_split_main.Panel2.Cursor = System.Windows.Forms.Cursors.Default;
            this.CTK_split_main.SeparatorStyle = ComponentFactory.Krypton.Toolkit.SeparatorStyle.HighProfile;
            this.CTK_split_main.Size = new System.Drawing.Size(857, 612);
            this.CTK_split_main.SplitterDistance = 424;
            this.CTK_split_main.TabIndex = 0;
            // 
            // CTK_split_top
            // 
            this.CTK_split_top.Cursor = System.Windows.Forms.Cursors.Default;
            this.CTK_split_top.DirtyPaletteCounter = 97;
            this.CTK_split_top.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CTK_split_top.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.CTK_split_top.IsSplitterFixed = true;
            this.CTK_split_top.Location = new System.Drawing.Point(0, 0);
            this.CTK_split_top.Name = "CTK_split_top";
            // 
            // CTK_split_top.Panel1
            // 
            this.CTK_split_top.Panel1.Controls.Add(this.dg_ctk_list);
            // 
            // CTK_split_top.Panel2
            // 
            this.CTK_split_top.Panel2.Controls.Add(this.CTK_panel_right);
            this.CTK_split_top.Size = new System.Drawing.Size(857, 424);
            this.CTK_split_top.SplitterDistance = 663;
            this.CTK_split_top.TabIndex = 0;
            // 
            // CTK_panel_right
            // 
            this.CTK_panel_right.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup[] {
            this.buttonSpecHeaderGroup1});
            this.CTK_panel_right.DirtyPaletteCounter = 97;
            this.CTK_panel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CTK_panel_right.HeaderVisibleSecondary = false;
            this.CTK_panel_right.Location = new System.Drawing.Point(0, 0);
            this.CTK_panel_right.Name = "CTK_panel_right";
            // 
            // CTK_panel_right.Panel
            // 
            this.CTK_panel_right.Panel.Controls.Add(this.ctk_tab);
            this.CTK_panel_right.Size = new System.Drawing.Size(189, 424);
            this.CTK_panel_right.TabIndex = 0;
            this.CTK_panel_right.Text = "Filtry";
            this.CTK_panel_right.ValuesPrimary.Description = "�TK";
            this.CTK_panel_right.ValuesPrimary.Heading = "Filtry";
            this.CTK_panel_right.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.cfg_filtry;
            this.CTK_panel_right.ValuesSecondary.Description = "";
            this.CTK_panel_right.ValuesSecondary.Heading = "Description";
            this.CTK_panel_right.ValuesSecondary.Image = null;
            // 
            // buttonSpecHeaderGroup1
            // 
            this.buttonSpecHeaderGroup1.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Near;
            this.buttonSpecHeaderGroup1.ExtraText = "";
            this.buttonSpecHeaderGroup1.Image = null;
            this.buttonSpecHeaderGroup1.Text = "";
            this.buttonSpecHeaderGroup1.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowRight;
            this.buttonSpecHeaderGroup1.UniqueName = "81FE327C54BC415E81FE327C54BC415E";
            // 
            // kryptonBorderEdge1
            // 
            this.kryptonBorderEdge1.DirtyPaletteCounter = 97;
            this.kryptonBorderEdge1.Location = new System.Drawing.Point(0, 0);
            this.kryptonBorderEdge1.Name = "kryptonBorderEdge1";
            this.kryptonBorderEdge1.Size = new System.Drawing.Size(50, 1);
            this.kryptonBorderEdge1.TabIndex = 3;
            this.kryptonBorderEdge1.Text = "kryptonBorderEdge1";
            // 
            // SBnews_tab_CustNews
            // 
            this.SBnews_tab_CustNews.Controls.Add(this.CustNews_split_main);
            this.SBnews_tab_CustNews.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_CustNews.Name = "SBnews_tab_CustNews";
            this.SBnews_tab_CustNews.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_CustNews.TabIndex = 4;
            this.SBnews_tab_CustNews.Text = "Vlastn� zpr�vy";
            this.SBnews_tab_CustNews.UseVisualStyleBackColor = true;
            // 
            // CustNews_split_main
            // 
            this.CustNews_split_main.Cursor = System.Windows.Forms.Cursors.Default;
            this.CustNews_split_main.DirtyPaletteCounter = 93;
            this.CustNews_split_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustNews_split_main.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.CustNews_split_main.IsSplitterFixed = true;
            this.CustNews_split_main.Location = new System.Drawing.Point(0, 0);
            this.CustNews_split_main.Name = "CustNews_split_main";
            // 
            // CustNews_split_main.Panel1
            // 
            this.CustNews_split_main.Panel1.Controls.Add(this.CustNews_split_dg_detail);
            // 
            // CustNews_split_main.Panel2
            // 
            this.CustNews_split_main.Panel2.Controls.Add(this.CustNews_main_panel_right);
            this.CustNews_split_main.Size = new System.Drawing.Size(863, 618);
            this.CustNews_split_main.SplitterDistance = 679;
            this.CustNews_split_main.TabIndex = 0;
            // 
            // CustNews_split_dg_detail
            // 
            this.CustNews_split_dg_detail.Cursor = System.Windows.Forms.Cursors.Default;
            this.CustNews_split_dg_detail.DirtyPaletteCounter = 93;
            this.CustNews_split_dg_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustNews_split_dg_detail.Location = new System.Drawing.Point(0, 0);
            this.CustNews_split_dg_detail.Name = "CustNews_split_dg_detail";
            this.CustNews_split_dg_detail.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // CustNews_split_dg_detail.Panel1
            // 
            this.CustNews_split_dg_detail.Panel1.Controls.Add(this.dg_CustNews_list);
            this.CustNews_split_dg_detail.Panel1MinSize = 295;
            // 
            // CustNews_split_dg_detail.Panel2
            // 
            this.CustNews_split_dg_detail.Panel2.Controls.Add(this.CustNews_txt_detail);
            this.CustNews_split_dg_detail.SeparatorStyle = ComponentFactory.Krypton.Toolkit.SeparatorStyle.HighProfile;
            this.CustNews_split_dg_detail.Size = new System.Drawing.Size(679, 618);
            this.CustNews_split_dg_detail.SplitterDistance = 423;
            this.CustNews_split_dg_detail.TabIndex = 0;
            // 
            // dg_CustNews_list
            // 
            this.dg_CustNews_list.AllowUserToAddRows = false;
            this.dg_CustNews_list.AllowUserToDeleteRows = false;
            this.dg_CustNews_list.AllowUserToOrderColumns = true;
            this.dg_CustNews_list.AllowUserToResizeRows = false;
            this.dg_CustNews_list.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dg_CustNews_list.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.dg_CustNews_list.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dg_CustNews_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_CustNews_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2,
            this.dg_CustNews_list_datum,
            this.dg_CustNews_list_cas,
            this.dg_CustNews_list_okres,
            this.dg_CustNews_list_nazev,
            this.dg_CustNews_list_klicova_slova,
            this.dg_CustNews_list_repo_ico,
            this.dg_CustNews_list_id_repa});
            this.dg_CustNews_list.ContextMenuStrip = this.mmenu_ctk_detail;
            this.dg_CustNews_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_CustNews_list.Location = new System.Drawing.Point(0, 0);
            this.dg_CustNews_list.Margin = new System.Windows.Forms.Padding(0);
            this.dg_CustNews_list.MultiSelect = false;
            this.dg_CustNews_list.Name = "dg_CustNews_list";
            this.dg_CustNews_list.ReadOnly = true;
            this.dg_CustNews_list.RowHeadersVisible = false;
            this.dg_CustNews_list.RowHeadersWidth = 20;
            this.dg_CustNews_list.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_CustNews_list.ShowCellErrors = false;
            this.dg_CustNews_list.ShowCellToolTips = false;
            this.dg_CustNews_list.ShowEditingIcon = false;
            this.dg_CustNews_list.ShowRowErrors = false;
            this.dg_CustNews_list.Size = new System.Drawing.Size(679, 423);
            this.dg_CustNews_list.TabIndex = 1;
            this.dg_CustNews_list.SelectionChanged += new System.EventHandler(this.dg_CustNews_list_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "ID";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 16000000;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.Visible = false;
            this.dataGridViewTextBoxColumn2.Width = 68;
            // 
            // dg_CustNews_list_datum
            // 
            this.dg_CustNews_list_datum.HeaderText = "Datum";
            this.dg_CustNews_list_datum.Name = "dg_CustNews_list_datum";
            this.dg_CustNews_list_datum.ReadOnly = true;
            this.dg_CustNews_list_datum.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_CustNews_list_datum.Width = 68;
            // 
            // dg_CustNews_list_cas
            // 
            this.dg_CustNews_list_cas.HeaderText = "�as";
            this.dg_CustNews_list_cas.Name = "dg_CustNews_list_cas";
            this.dg_CustNews_list_cas.ReadOnly = true;
            this.dg_CustNews_list_cas.Width = 45;
            // 
            // dg_CustNews_list_okres
            // 
            this.dg_CustNews_list_okres.HeaderText = "Okres";
            this.dg_CustNews_list_okres.Name = "dg_CustNews_list_okres";
            this.dg_CustNews_list_okres.ReadOnly = true;
            this.dg_CustNews_list_okres.Width = 70;
            // 
            // dg_CustNews_list_nazev
            // 
            this.dg_CustNews_list_nazev.HeaderText = "Titulek";
            this.dg_CustNews_list_nazev.Name = "dg_CustNews_list_nazev";
            this.dg_CustNews_list_nazev.ReadOnly = true;
            this.dg_CustNews_list_nazev.Width = 150;
            // 
            // dg_CustNews_list_klicova_slova
            // 
            this.dg_CustNews_list_klicova_slova.HeaderText = "Kl��ov� slova";
            this.dg_CustNews_list_klicova_slova.Name = "dg_CustNews_list_klicova_slova";
            this.dg_CustNews_list_klicova_slova.ReadOnly = true;
            this.dg_CustNews_list_klicova_slova.Width = 180;
            // 
            // dg_CustNews_list_repo_ico
            // 
            this.dg_CustNews_list_repo_ico.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dg_CustNews_list_repo_ico.Description = "Repo";
            this.dg_CustNews_list_repo_ico.FillWeight = 16F;
            this.dg_CustNews_list_repo_ico.HeaderText = " ";
            this.dg_CustNews_list_repo_ico.Image = global::CTK_reader.Properties.Resources.flag_empty;
            this.dg_CustNews_list_repo_ico.MinimumWidth = 16;
            this.dg_CustNews_list_repo_ico.Name = "dg_CustNews_list_repo_ico";
            this.dg_CustNews_list_repo_ico.ReadOnly = true;
            this.dg_CustNews_list_repo_ico.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_CustNews_list_repo_ico.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dg_CustNews_list_repo_ico.Width = 20;
            // 
            // dg_CustNews_list_id_repa
            // 
            this.dg_CustNews_list_id_repa.HeaderText = "Repo";
            this.dg_CustNews_list_id_repa.Name = "dg_CustNews_list_id_repa";
            this.dg_CustNews_list_id_repa.ReadOnly = true;
            this.dg_CustNews_list_id_repa.Width = 70;
            // 
            // CustNews_txt_detail
            // 
            this.CustNews_txt_detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.CustNews_txt_detail.ContextMenuStrip = this.mmenu_ctk_detail;
            this.CustNews_txt_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustNews_txt_detail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustNews_txt_detail.Location = new System.Drawing.Point(0, 0);
            this.CustNews_txt_detail.Multiline = true;
            this.CustNews_txt_detail.Name = "CustNews_txt_detail";
            this.CustNews_txt_detail.ReadOnly = true;
            this.CustNews_txt_detail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.CustNews_txt_detail.Size = new System.Drawing.Size(679, 190);
            this.CustNews_txt_detail.TabIndex = 2;
            this.CustNews_txt_detail.Text = "Vlastn� zpr�vy - detail";
            // 
            // CustNews_main_panel_right
            // 
            this.CustNews_main_panel_right.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup[] {
            this.CustNews_main_panel_right_ShowHide});
            this.CustNews_main_panel_right.DirtyPaletteCounter = 92;
            this.CustNews_main_panel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CustNews_main_panel_right.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.CustNews_main_panel_right.HeaderVisibleSecondary = false;
            this.CustNews_main_panel_right.Location = new System.Drawing.Point(0, 0);
            this.CustNews_main_panel_right.Name = "CustNews_main_panel_right";
            // 
            // CustNews_main_panel_right.Panel
            // 
            this.CustNews_main_panel_right.Panel.Controls.Add(this.btn_CustNews_showAll);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.CustNews_sel_text);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.kryptonLabel9);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.btn_CustNews_sel_drop);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.btn_CustNews_sel_set);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.kryptonLabel6);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.CustNews_sel_oblast);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.kryptonLabel7);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.kryptonLabel8);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.CustNews_sel_group);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.CustNews_sel_kategorie);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.btn_CustNews_edit);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.btn_CustNews_new);
            this.CustNews_main_panel_right.Panel.Controls.Add(this.CustNews_Calendar);
            this.CustNews_main_panel_right.Size = new System.Drawing.Size(179, 618);
            this.CustNews_main_panel_right.TabIndex = 0;
            this.CustNews_main_panel_right.Text = "Filtry";
            this.CustNews_main_panel_right.ValuesPrimary.Description = "Vlastn� zpr�vy";
            this.CustNews_main_panel_right.ValuesPrimary.Heading = "Filtry";
            this.CustNews_main_panel_right.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.cfg_filtry;
            this.CustNews_main_panel_right.ValuesSecondary.Description = "";
            this.CustNews_main_panel_right.ValuesSecondary.Heading = "Description";
            this.CustNews_main_panel_right.ValuesSecondary.Image = null;
            // 
            // CustNews_main_panel_right_ShowHide
            // 
            this.CustNews_main_panel_right_ShowHide.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Near;
            this.CustNews_main_panel_right_ShowHide.ExtraText = "";
            this.CustNews_main_panel_right_ShowHide.Image = null;
            this.CustNews_main_panel_right_ShowHide.Text = "";
            this.CustNews_main_panel_right_ShowHide.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowRight;
            this.CustNews_main_panel_right_ShowHide.UniqueName = "8FABC7FE1AAB44448FABC7FE1AAB4444";
            // 
            // btn_CustNews_showAll
            // 
            this.btn_CustNews_showAll.DirtyPaletteCounter = 98;
            this.btn_CustNews_showAll.Location = new System.Drawing.Point(9, 377);
            this.btn_CustNews_showAll.Name = "btn_CustNews_showAll";
            this.btn_CustNews_showAll.Size = new System.Drawing.Size(157, 25);
            this.btn_CustNews_showAll.TabIndex = 118;
            this.btn_CustNews_showAll.Text = "Zobrazit i embargovan�";
            this.btn_CustNews_showAll.Values.ExtraText = "";
            this.btn_CustNews_showAll.Values.Image = null;
            this.btn_CustNews_showAll.Values.Text = "Zobrazit i embargovan�";
            this.btn_CustNews_showAll.Click += new System.EventHandler(this.btn_CustNews_showAll_Click);
            // 
            // CustNews_sel_text
            // 
            this.CustNews_sel_text.Location = new System.Drawing.Point(9, 319);
            this.CustNews_sel_text.Name = "CustNews_sel_text";
            this.CustNews_sel_text.Size = new System.Drawing.Size(157, 21);
            this.CustNews_sel_text.TabIndex = 23;
            // 
            // kryptonLabel9
            // 
            this.kryptonLabel9.DirtyPaletteCounter = 99;
            this.kryptonLabel9.Location = new System.Drawing.Point(3, 297);
            this.kryptonLabel9.Name = "kryptonLabel9";
            this.kryptonLabel9.Size = new System.Drawing.Size(141, 20);
            this.kryptonLabel9.TabIndex = 116;
            this.kryptonLabel9.Text = "Obsahuje v textu zpr�vy";
            this.kryptonLabel9.Values.ExtraText = "";
            this.kryptonLabel9.Values.Image = null;
            this.kryptonLabel9.Values.Text = "Obsahuje v textu zpr�vy";
            // 
            // btn_CustNews_sel_drop
            // 
            this.btn_CustNews_sel_drop.DirtyPaletteCounter = 98;
            this.btn_CustNews_sel_drop.Location = new System.Drawing.Point(92, 346);
            this.btn_CustNews_sel_drop.Name = "btn_CustNews_sel_drop";
            this.btn_CustNews_sel_drop.Size = new System.Drawing.Size(74, 25);
            this.btn_CustNews_sel_drop.TabIndex = 31;
            this.btn_CustNews_sel_drop.Text = "Zru�it filtr";
            this.btn_CustNews_sel_drop.Values.ExtraText = "";
            this.btn_CustNews_sel_drop.Values.Image = null;
            this.btn_CustNews_sel_drop.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_CustNews_sel_drop.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_CustNews_sel_drop.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_CustNews_sel_drop.Values.Text = "Zru�it filtr";
            this.btn_CustNews_sel_drop.Click += new System.EventHandler(this.btn_CustNews_sel_drop_Click);
            // 
            // btn_CustNews_sel_set
            // 
            this.btn_CustNews_sel_set.DirtyPaletteCounter = 98;
            this.btn_CustNews_sel_set.Location = new System.Drawing.Point(9, 346);
            this.btn_CustNews_sel_set.Name = "btn_CustNews_sel_set";
            this.btn_CustNews_sel_set.Size = new System.Drawing.Size(74, 25);
            this.btn_CustNews_sel_set.TabIndex = 30;
            this.btn_CustNews_sel_set.Text = "Nastav filtr";
            this.btn_CustNews_sel_set.Values.ExtraText = "";
            this.btn_CustNews_sel_set.Values.Image = null;
            this.btn_CustNews_sel_set.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_CustNews_sel_set.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_CustNews_sel_set.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_CustNews_sel_set.Values.Text = "Nastav filtr";
            this.btn_CustNews_sel_set.Click += new System.EventHandler(this.btn_CustNews_sel_set_Click);
            // 
            // kryptonLabel6
            // 
            this.kryptonLabel6.DirtyPaletteCounter = 98;
            this.kryptonLabel6.Location = new System.Drawing.Point(3, 203);
            this.kryptonLabel6.Name = "kryptonLabel6";
            this.kryptonLabel6.Size = new System.Drawing.Size(129, 20);
            this.kryptonLabel6.TabIndex = 115;
            this.kryptonLabel6.Text = "Zobrazit pouze oblast";
            this.kryptonLabel6.Values.ExtraText = "";
            this.kryptonLabel6.Values.Image = null;
            this.kryptonLabel6.Values.Text = "Zobrazit pouze oblast";
            // 
            // CustNews_sel_oblast
            // 
            this.CustNews_sel_oblast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustNews_sel_oblast.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustNews_sel_oblast.FormattingEnabled = true;
            this.CustNews_sel_oblast.Location = new System.Drawing.Point(9, 223);
            this.CustNews_sel_oblast.Name = "CustNews_sel_oblast";
            this.CustNews_sel_oblast.Size = new System.Drawing.Size(157, 21);
            this.CustNews_sel_oblast.TabIndex = 21;
            // 
            // kryptonLabel7
            // 
            this.kryptonLabel7.DirtyPaletteCounter = 98;
            this.kryptonLabel7.Location = new System.Drawing.Point(3, 250);
            this.kryptonLabel7.Name = "kryptonLabel7";
            this.kryptonLabel7.Size = new System.Drawing.Size(147, 20);
            this.kryptonLabel7.TabIndex = 114;
            this.kryptonLabel7.Text = "Zobrazit pouze kategorie";
            this.kryptonLabel7.Values.ExtraText = "";
            this.kryptonLabel7.Values.Image = null;
            this.kryptonLabel7.Values.Text = "Zobrazit pouze kategorie";
            // 
            // kryptonLabel8
            // 
            this.kryptonLabel8.DirtyPaletteCounter = 98;
            this.kryptonLabel8.Location = new System.Drawing.Point(3, 176);
            this.kryptonLabel8.Name = "kryptonLabel8";
            this.kryptonLabel8.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel8.TabIndex = 113;
            this.kryptonLabel8.Text = "Okruh";
            this.kryptonLabel8.Values.ExtraText = "";
            this.kryptonLabel8.Values.Image = null;
            this.kryptonLabel8.Values.Text = "Okruh";
            // 
            // CustNews_sel_group
            // 
            this.CustNews_sel_group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustNews_sel_group.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustNews_sel_group.FormattingEnabled = true;
            this.CustNews_sel_group.Location = new System.Drawing.Point(46, 174);
            this.CustNews_sel_group.Name = "CustNews_sel_group";
            this.CustNews_sel_group.Size = new System.Drawing.Size(120, 21);
            this.CustNews_sel_group.TabIndex = 20;
            // 
            // CustNews_sel_kategorie
            // 
            this.CustNews_sel_kategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CustNews_sel_kategorie.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustNews_sel_kategorie.FormattingEnabled = true;
            this.CustNews_sel_kategorie.Location = new System.Drawing.Point(9, 270);
            this.CustNews_sel_kategorie.Name = "CustNews_sel_kategorie";
            this.CustNews_sel_kategorie.Size = new System.Drawing.Size(157, 21);
            this.CustNews_sel_kategorie.TabIndex = 22;
            // 
            // btn_CustNews_edit
            // 
            this.btn_CustNews_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CustNews_edit.DirtyPaletteCounter = 98;
            this.btn_CustNews_edit.Location = new System.Drawing.Point(92, 560);
            this.btn_CustNews_edit.Name = "btn_CustNews_edit";
            this.btn_CustNews_edit.Size = new System.Drawing.Size(74, 25);
            this.btn_CustNews_edit.TabIndex = 36;
            this.btn_CustNews_edit.Text = "Editace";
            this.btn_CustNews_edit.Values.ExtraText = "";
            this.btn_CustNews_edit.Values.Image = null;
            this.btn_CustNews_edit.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_CustNews_edit.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_CustNews_edit.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_CustNews_edit.Values.Text = "Editace";
            this.btn_CustNews_edit.Click += new System.EventHandler(this.btn_CustNews_edit_Click);
            // 
            // btn_CustNews_new
            // 
            this.btn_CustNews_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_CustNews_new.DirtyPaletteCounter = 98;
            this.btn_CustNews_new.Location = new System.Drawing.Point(9, 560);
            this.btn_CustNews_new.Name = "btn_CustNews_new";
            this.btn_CustNews_new.Size = new System.Drawing.Size(74, 25);
            this.btn_CustNews_new.TabIndex = 35;
            this.btn_CustNews_new.Text = "Nov�";
            this.btn_CustNews_new.Values.ExtraText = "";
            this.btn_CustNews_new.Values.Image = null;
            this.btn_CustNews_new.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_CustNews_new.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_CustNews_new.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_CustNews_new.Values.Text = "Nov�";
            this.btn_CustNews_new.Click += new System.EventHandler(this.btn_CustNews_new_Click);
            // 
            // CustNews_Calendar
            // 
            this.CustNews_Calendar.BackColor = System.Drawing.SystemColors.Window;
            this.CustNews_Calendar.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.CustNews_Calendar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CustNews_Calendar.Location = new System.Drawing.Point(9, 9);
            this.CustNews_Calendar.MaxSelectionCount = 31;
            this.CustNews_Calendar.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.CustNews_Calendar.Name = "CustNews_Calendar";
            this.CustNews_Calendar.TabIndex = 10;
            this.CustNews_Calendar.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.CustNews_Calendar.TitleForeColor = System.Drawing.Color.Black;
            this.CustNews_Calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.CustNews_Calendar_DateSelected);
            // 
            // SBnews_tab_ris
            // 
            this.SBnews_tab_ris.Controls.Add(this.RIS_panel_main);
            this.SBnews_tab_ris.Cursor = System.Windows.Forms.Cursors.Default;
            this.SBnews_tab_ris.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SBnews_tab_ris.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_ris.Name = "SBnews_tab_ris";
            this.SBnews_tab_ris.Padding = new System.Windows.Forms.Padding(3);
            this.SBnews_tab_ris.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_ris.TabIndex = 1;
            this.SBnews_tab_ris.Text = "RIS";
            this.SBnews_tab_ris.ToolTipText = "Redak�n� Informa�n� Syst�m";
            this.SBnews_tab_ris.UseVisualStyleBackColor = true;
            // 
            // RIS_panel_main
            // 
            this.RIS_panel_main.Controls.Add(this.RIS_main_split_main);
            this.RIS_panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_panel_main.Location = new System.Drawing.Point(3, 3);
            this.RIS_panel_main.Name = "RIS_panel_main";
            this.RIS_panel_main.Size = new System.Drawing.Size(857, 612);
            this.RIS_panel_main.TabIndex = 1;
            // 
            // RIS_main_split_main
            // 
            this.RIS_main_split_main.Cursor = System.Windows.Forms.Cursors.Default;
            this.RIS_main_split_main.DirtyPaletteCounter = 97;
            this.RIS_main_split_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_main_split_main.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.RIS_main_split_main.IsSplitterFixed = true;
            this.RIS_main_split_main.Location = new System.Drawing.Point(0, 0);
            this.RIS_main_split_main.Name = "RIS_main_split_main";
            // 
            // RIS_main_split_main.Panel1
            // 
            this.RIS_main_split_main.Panel1.Controls.Add(this.RIS_split_dg_detail);
            // 
            // RIS_main_split_main.Panel2
            // 
            this.RIS_main_split_main.Panel2.Controls.Add(this.RIS_main_panel_main_left);
            this.RIS_main_split_main.Size = new System.Drawing.Size(857, 612);
            this.RIS_main_split_main.SplitterDistance = 679;
            this.RIS_main_split_main.TabIndex = 0;
            // 
            // RIS_split_dg_detail
            // 
            this.RIS_split_dg_detail.Cursor = System.Windows.Forms.Cursors.Default;
            this.RIS_split_dg_detail.DirtyPaletteCounter = 160;
            this.RIS_split_dg_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_split_dg_detail.Location = new System.Drawing.Point(0, 0);
            this.RIS_split_dg_detail.Name = "RIS_split_dg_detail";
            this.RIS_split_dg_detail.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.RIS_split_dg_detail.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            // 
            // RIS_split_dg_detail.Panel1
            // 
            this.RIS_split_dg_detail.Panel1.Controls.Add(this.dg_RIS_list);
            // 
            // RIS_split_dg_detail.Panel2
            // 
            this.RIS_split_dg_detail.Panel2.Controls.Add(this.RIS_txt_detail);
            this.RIS_split_dg_detail.SeparatorStyle = ComponentFactory.Krypton.Toolkit.SeparatorStyle.HighProfile;
            this.RIS_split_dg_detail.Size = new System.Drawing.Size(679, 612);
            this.RIS_split_dg_detail.SplitterDistance = 273;
            this.RIS_split_dg_detail.TabIndex = 0;
            // 
            // dg_RIS_list
            // 
            this.dg_RIS_list.AllowUserToAddRows = false;
            this.dg_RIS_list.AllowUserToDeleteRows = false;
            this.dg_RIS_list.AllowUserToOrderColumns = true;
            this.dg_RIS_list.AllowUserToResizeRows = false;
            this.dg_RIS_list.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dg_RIS_list.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.dg_RIS_list.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dg_RIS_list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_RIS_list.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dg_RIS_id,
            this.dg_RIS_datum,
            this.dg_RIS_okres,
            this.dg_RIS_titulek,
            this.dg_RIS_klicova_slova,
            this.dg_RIS_medialni_partner,
            this.dg_RIS_promo_vysilani});
            this.dg_RIS_list.ContextMenuStrip = this.mmenu_ctk_detail;
            this.dg_RIS_list.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_RIS_list.Location = new System.Drawing.Point(0, 0);
            this.dg_RIS_list.Margin = new System.Windows.Forms.Padding(0);
            this.dg_RIS_list.MultiSelect = false;
            this.dg_RIS_list.Name = "dg_RIS_list";
            this.dg_RIS_list.ReadOnly = true;
            this.dg_RIS_list.RowHeadersVisible = false;
            this.dg_RIS_list.RowHeadersWidth = 20;
            this.dg_RIS_list.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_RIS_list.ShowCellErrors = false;
            this.dg_RIS_list.ShowCellToolTips = false;
            this.dg_RIS_list.ShowEditingIcon = false;
            this.dg_RIS_list.ShowRowErrors = false;
            this.dg_RIS_list.Size = new System.Drawing.Size(679, 273);
            this.dg_RIS_list.TabIndex = 1;
            this.dg_RIS_list.SelectionChanged += new System.EventHandler(this.dg_RIS_list_SelectionChanged);
            // 
            // dg_RIS_id
            // 
            this.dg_RIS_id.HeaderText = "ID";
            this.dg_RIS_id.MaxInputLength = 16000000;
            this.dg_RIS_id.Name = "dg_RIS_id";
            this.dg_RIS_id.ReadOnly = true;
            this.dg_RIS_id.Visible = false;
            this.dg_RIS_id.Width = 68;
            // 
            // dg_RIS_datum
            // 
            this.dg_RIS_datum.FillWeight = 70F;
            this.dg_RIS_datum.HeaderText = "Datum";
            this.dg_RIS_datum.Name = "dg_RIS_datum";
            this.dg_RIS_datum.ReadOnly = true;
            this.dg_RIS_datum.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dg_RIS_datum.Width = 70;
            // 
            // dg_RIS_okres
            // 
            this.dg_RIS_okres.FillWeight = 80F;
            this.dg_RIS_okres.HeaderText = "Okres";
            this.dg_RIS_okres.Name = "dg_RIS_okres";
            this.dg_RIS_okres.ReadOnly = true;
            this.dg_RIS_okres.Width = 80;
            // 
            // dg_RIS_titulek
            // 
            this.dg_RIS_titulek.FillWeight = 200F;
            this.dg_RIS_titulek.HeaderText = "N�zev akce";
            this.dg_RIS_titulek.Name = "dg_RIS_titulek";
            this.dg_RIS_titulek.ReadOnly = true;
            this.dg_RIS_titulek.Width = 200;
            // 
            // dg_RIS_klicova_slova
            // 
            this.dg_RIS_klicova_slova.FillWeight = 200F;
            this.dg_RIS_klicova_slova.HeaderText = "Kl��ov� slova";
            this.dg_RIS_klicova_slova.Name = "dg_RIS_klicova_slova";
            this.dg_RIS_klicova_slova.ReadOnly = true;
            this.dg_RIS_klicova_slova.Width = 200;
            // 
            // dg_RIS_medialni_partner
            // 
            this.dg_RIS_medialni_partner.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dg_RIS_medialni_partner.Description = "Medi�ln� partner";
            this.dg_RIS_medialni_partner.FillWeight = 16F;
            this.dg_RIS_medialni_partner.HeaderText = " ";
            this.dg_RIS_medialni_partner.Image = global::CTK_reader.Properties.Resources.flag_empty;
            this.dg_RIS_medialni_partner.MinimumWidth = 16;
            this.dg_RIS_medialni_partner.Name = "dg_RIS_medialni_partner";
            this.dg_RIS_medialni_partner.ReadOnly = true;
            this.dg_RIS_medialni_partner.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_RIS_medialni_partner.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dg_RIS_medialni_partner.Width = 20;
            // 
            // dg_RIS_promo_vysilani
            // 
            this.dg_RIS_promo_vysilani.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.dg_RIS_promo_vysilani.Description = "Promo ve vys�l�n�";
            this.dg_RIS_promo_vysilani.FillWeight = 16F;
            this.dg_RIS_promo_vysilani.HeaderText = " ";
            this.dg_RIS_promo_vysilani.Image = global::CTK_reader.Properties.Resources.flag_empty;
            this.dg_RIS_promo_vysilani.MinimumWidth = 16;
            this.dg_RIS_promo_vysilani.Name = "dg_RIS_promo_vysilani";
            this.dg_RIS_promo_vysilani.ReadOnly = true;
            this.dg_RIS_promo_vysilani.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dg_RIS_promo_vysilani.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.dg_RIS_promo_vysilani.Width = 20;
            // 
            // RIS_txt_detail
            // 
            this.RIS_txt_detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.RIS_txt_detail.ContextMenuStrip = this.mmenu_ctk_detail;
            this.RIS_txt_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_txt_detail.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_txt_detail.Location = new System.Drawing.Point(0, 0);
            this.RIS_txt_detail.Multiline = true;
            this.RIS_txt_detail.Name = "RIS_txt_detail";
            this.RIS_txt_detail.ReadOnly = true;
            this.RIS_txt_detail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.RIS_txt_detail.Size = new System.Drawing.Size(679, 334);
            this.RIS_txt_detail.TabIndex = 2;
            this.RIS_txt_detail.Text = "RIS - detail";
            // 
            // RIS_main_panel_main_left
            // 
            this.RIS_main_panel_main_left.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup[] {
            this.RIS_main_split_main_2_show_hide});
            this.RIS_main_panel_main_left.DirtyPaletteCounter = 97;
            this.RIS_main_panel_main_left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RIS_main_panel_main_left.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.RIS_main_panel_main_left.HeaderVisibleSecondary = false;
            this.RIS_main_panel_main_left.Location = new System.Drawing.Point(0, 0);
            this.RIS_main_panel_main_left.Name = "RIS_main_panel_main_left";
            // 
            // RIS_main_panel_main_left.Panel
            // 
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_all_versions);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_btn_reload_clean);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_btn_reload);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.kryptonLabel5);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_oblast);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.kryptonLabel4);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.kryptonLabel3);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.kryptonLabel2);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.kryptonLabel1);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.btn_RIS_edit);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_calendar);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.btn_RIS_new);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_nazev);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_group);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_mesto);
            this.RIS_main_panel_main_left.Panel.Controls.Add(this.RIS_sel_kategorie);
            this.RIS_main_panel_main_left.Size = new System.Drawing.Size(173, 612);
            this.RIS_main_panel_main_left.TabIndex = 0;
            this.RIS_main_panel_main_left.Text = "Filtry";
            this.RIS_main_panel_main_left.ValuesPrimary.Description = "RIS";
            this.RIS_main_panel_main_left.ValuesPrimary.Heading = "Filtry";
            this.RIS_main_panel_main_left.ValuesPrimary.Image = global::CTK_reader.Properties.Resources.cfg_filtry;
            this.RIS_main_panel_main_left.ValuesSecondary.Description = "";
            this.RIS_main_panel_main_left.ValuesSecondary.Heading = "Description";
            this.RIS_main_panel_main_left.ValuesSecondary.Image = null;
            // 
            // RIS_main_split_main_2_show_hide
            // 
            this.RIS_main_split_main_2_show_hide.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Near;
            this.RIS_main_split_main_2_show_hide.ExtraText = "";
            this.RIS_main_split_main_2_show_hide.Image = null;
            this.RIS_main_split_main_2_show_hide.Text = "";
            this.RIS_main_split_main_2_show_hide.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowRight;
            this.RIS_main_split_main_2_show_hide.UniqueName = "F060CD332AF045B3F060CD332AF045B3";
            // 
            // RIS_sel_all_versions
            // 
            this.RIS_sel_all_versions.DirtyPaletteCounter = 97;
            this.RIS_sel_all_versions.Location = new System.Drawing.Point(8, 426);
            this.RIS_sel_all_versions.Name = "RIS_sel_all_versions";
            this.RIS_sel_all_versions.Size = new System.Drawing.Size(154, 25);
            this.RIS_sel_all_versions.TabIndex = 20;
            this.RIS_sel_all_versions.Text = "Zobrazit v�echny verze";
            this.RIS_sel_all_versions.Values.ExtraText = "";
            this.RIS_sel_all_versions.Values.Image = null;
            this.RIS_sel_all_versions.Values.Text = "Zobrazit v�echny verze";
            this.RIS_sel_all_versions.Click += new System.EventHandler(this.RIS_sel_all_versions_Click);
            // 
            // RIS_btn_reload_clean
            // 
            this.RIS_btn_reload_clean.DirtyPaletteCounter = 97;
            this.RIS_btn_reload_clean.Location = new System.Drawing.Point(89, 395);
            this.RIS_btn_reload_clean.Name = "RIS_btn_reload_clean";
            this.RIS_btn_reload_clean.Size = new System.Drawing.Size(74, 25);
            this.RIS_btn_reload_clean.TabIndex = 10;
            this.RIS_btn_reload_clean.Text = "Zru�it filtr";
            this.RIS_btn_reload_clean.Values.ExtraText = "";
            this.RIS_btn_reload_clean.Values.Image = null;
            this.RIS_btn_reload_clean.Values.ImageStates.ImageCheckedNormal = null;
            this.RIS_btn_reload_clean.Values.ImageStates.ImageCheckedPressed = null;
            this.RIS_btn_reload_clean.Values.ImageStates.ImageCheckedTracking = null;
            this.RIS_btn_reload_clean.Values.Text = "Zru�it filtr";
            this.RIS_btn_reload_clean.Click += new System.EventHandler(this.RIS_btn_reload_clean_Click);
            // 
            // RIS_btn_reload
            // 
            this.RIS_btn_reload.DirtyPaletteCounter = 97;
            this.RIS_btn_reload.Location = new System.Drawing.Point(9, 395);
            this.RIS_btn_reload.Name = "RIS_btn_reload";
            this.RIS_btn_reload.Size = new System.Drawing.Size(74, 25);
            this.RIS_btn_reload.TabIndex = 9;
            this.RIS_btn_reload.Text = "Nastav filtr";
            this.RIS_btn_reload.Values.ExtraText = "";
            this.RIS_btn_reload.Values.Image = null;
            this.RIS_btn_reload.Values.ImageStates.ImageCheckedNormal = null;
            this.RIS_btn_reload.Values.ImageStates.ImageCheckedPressed = null;
            this.RIS_btn_reload.Values.ImageStates.ImageCheckedTracking = null;
            this.RIS_btn_reload.Values.Text = "Nastav filtr";
            this.RIS_btn_reload.Click += new System.EventHandler(this.RIS_btn_reload_Click);
            // 
            // kryptonLabel5
            // 
            this.kryptonLabel5.DirtyPaletteCounter = 97;
            this.kryptonLabel5.Location = new System.Drawing.Point(3, 206);
            this.kryptonLabel5.Name = "kryptonLabel5";
            this.kryptonLabel5.Size = new System.Drawing.Size(129, 20);
            this.kryptonLabel5.TabIndex = 20;
            this.kryptonLabel5.Text = "Zobrazit pouze oblast";
            this.kryptonLabel5.Values.ExtraText = "";
            this.kryptonLabel5.Values.Image = null;
            this.kryptonLabel5.Values.Text = "Zobrazit pouze oblast";
            // 
            // RIS_sel_oblast
            // 
            this.RIS_sel_oblast.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RIS_sel_oblast.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_sel_oblast.FormattingEnabled = true;
            this.RIS_sel_oblast.Location = new System.Drawing.Point(9, 226);
            this.RIS_sel_oblast.Name = "RIS_sel_oblast";
            this.RIS_sel_oblast.Size = new System.Drawing.Size(154, 21);
            this.RIS_sel_oblast.TabIndex = 5;
            // 
            // kryptonLabel4
            // 
            this.kryptonLabel4.DirtyPaletteCounter = 97;
            this.kryptonLabel4.Location = new System.Drawing.Point(2, 349);
            this.kryptonLabel4.Name = "kryptonLabel4";
            this.kryptonLabel4.Size = new System.Drawing.Size(135, 20);
            this.kryptonLabel4.TabIndex = 18;
            this.kryptonLabel4.Text = "Obsahuje v n�zvu akce";
            this.kryptonLabel4.Values.ExtraText = "";
            this.kryptonLabel4.Values.Image = null;
            this.kryptonLabel4.Values.Text = "Obsahuje v n�zvu akce";
            // 
            // kryptonLabel3
            // 
            this.kryptonLabel3.DirtyPaletteCounter = 97;
            this.kryptonLabel3.Location = new System.Drawing.Point(3, 300);
            this.kryptonLabel3.Name = "kryptonLabel3";
            this.kryptonLabel3.Size = new System.Drawing.Size(109, 20);
            this.kryptonLabel3.TabIndex = 17;
            this.kryptonLabel3.Text = "M�sto nebo okres";
            this.kryptonLabel3.Values.ExtraText = "";
            this.kryptonLabel3.Values.Image = null;
            this.kryptonLabel3.Values.Text = "M�sto nebo okres";
            // 
            // kryptonLabel2
            // 
            this.kryptonLabel2.DirtyPaletteCounter = 97;
            this.kryptonLabel2.Location = new System.Drawing.Point(3, 253);
            this.kryptonLabel2.Name = "kryptonLabel2";
            this.kryptonLabel2.Size = new System.Drawing.Size(147, 20);
            this.kryptonLabel2.TabIndex = 16;
            this.kryptonLabel2.Text = "Zobrazit pouze kategorie";
            this.kryptonLabel2.Values.ExtraText = "";
            this.kryptonLabel2.Values.Image = null;
            this.kryptonLabel2.Values.Text = "Zobrazit pouze kategorie";
            // 
            // kryptonLabel1
            // 
            this.kryptonLabel1.DirtyPaletteCounter = 97;
            this.kryptonLabel1.Location = new System.Drawing.Point(3, 179);
            this.kryptonLabel1.Name = "kryptonLabel1";
            this.kryptonLabel1.Size = new System.Drawing.Size(44, 20);
            this.kryptonLabel1.TabIndex = 15;
            this.kryptonLabel1.Text = "Okruh";
            this.kryptonLabel1.Values.ExtraText = "";
            this.kryptonLabel1.Values.Image = null;
            this.kryptonLabel1.Values.Text = "Okruh";
            // 
            // btn_RIS_edit
            // 
            this.btn_RIS_edit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_RIS_edit.DirtyPaletteCounter = 97;
            this.btn_RIS_edit.Location = new System.Drawing.Point(89, 551);
            this.btn_RIS_edit.Name = "btn_RIS_edit";
            this.btn_RIS_edit.Size = new System.Drawing.Size(74, 25);
            this.btn_RIS_edit.TabIndex = 105;
            this.btn_RIS_edit.Text = "Editace";
            this.btn_RIS_edit.Values.ExtraText = "";
            this.btn_RIS_edit.Values.Image = null;
            this.btn_RIS_edit.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_RIS_edit.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_RIS_edit.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_RIS_edit.Values.Text = "Editace";
            this.btn_RIS_edit.Click += new System.EventHandler(this.btn_RIS_edit_Click);
            // 
            // RIS_calendar
            // 
            this.RIS_calendar.BackColor = System.Drawing.SystemColors.Window;
            this.RIS_calendar.FirstDayOfWeek = System.Windows.Forms.Day.Monday;
            this.RIS_calendar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_calendar.Location = new System.Drawing.Point(9, 9);
            this.RIS_calendar.MaxSelectionCount = 31;
            this.RIS_calendar.MinDate = new System.DateTime(2000, 1, 1, 0, 0, 0, 0);
            this.RIS_calendar.Name = "RIS_calendar";
            this.RIS_calendar.TabIndex = 3;
            this.RIS_calendar.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.RIS_calendar.TitleForeColor = System.Drawing.Color.Black;
            this.RIS_calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.RIS_calendar_date_selected);
            // 
            // btn_RIS_new
            // 
            this.btn_RIS_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_RIS_new.DirtyPaletteCounter = 97;
            this.btn_RIS_new.Location = new System.Drawing.Point(9, 551);
            this.btn_RIS_new.Name = "btn_RIS_new";
            this.btn_RIS_new.Size = new System.Drawing.Size(74, 25);
            this.btn_RIS_new.TabIndex = 100;
            this.btn_RIS_new.Text = "Nov�";
            this.btn_RIS_new.Values.ExtraText = "";
            this.btn_RIS_new.Values.Image = null;
            this.btn_RIS_new.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_RIS_new.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_RIS_new.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_RIS_new.Values.Text = "Nov�";
            this.btn_RIS_new.Click += new System.EventHandler(this.btn_RIS_new_Click);
            // 
            // RIS_sel_nazev
            // 
            this.RIS_sel_nazev.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_sel_nazev.Location = new System.Drawing.Point(8, 368);
            this.RIS_sel_nazev.Name = "RIS_sel_nazev";
            this.RIS_sel_nazev.Size = new System.Drawing.Size(154, 21);
            this.RIS_sel_nazev.TabIndex = 8;
            // 
            // RIS_sel_group
            // 
            this.RIS_sel_group.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RIS_sel_group.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_sel_group.FormattingEnabled = true;
            this.RIS_sel_group.Location = new System.Drawing.Point(46, 177);
            this.RIS_sel_group.Name = "RIS_sel_group";
            this.RIS_sel_group.Size = new System.Drawing.Size(117, 21);
            this.RIS_sel_group.TabIndex = 4;
            // 
            // RIS_sel_mesto
            // 
            this.RIS_sel_mesto.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_sel_mesto.Location = new System.Drawing.Point(8, 320);
            this.RIS_sel_mesto.Name = "RIS_sel_mesto";
            this.RIS_sel_mesto.Size = new System.Drawing.Size(154, 21);
            this.RIS_sel_mesto.TabIndex = 7;
            // 
            // RIS_sel_kategorie
            // 
            this.RIS_sel_kategorie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RIS_sel_kategorie.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.RIS_sel_kategorie.FormattingEnabled = true;
            this.RIS_sel_kategorie.Location = new System.Drawing.Point(9, 273);
            this.RIS_sel_kategorie.Name = "RIS_sel_kategorie";
            this.RIS_sel_kategorie.Size = new System.Drawing.Size(154, 21);
            this.RIS_sel_kategorie.TabIndex = 6;
            // 
            // SBnews_tab_doprava
            // 
            this.SBnews_tab_doprava.Controls.Add(this.SBnews_doprava_split_main);
            this.SBnews_tab_doprava.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_doprava.Name = "SBnews_tab_doprava";
            this.SBnews_tab_doprava.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_doprava.TabIndex = 5;
            this.SBnews_tab_doprava.Text = "Doprava";
            // 
            // SBnews_doprava_split_main
            // 
            this.SBnews_doprava_split_main.Cursor = System.Windows.Forms.Cursors.Default;
            this.SBnews_doprava_split_main.DirtyPaletteCounter = 64;
            this.SBnews_doprava_split_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_doprava_split_main.Location = new System.Drawing.Point(0, 0);
            this.SBnews_doprava_split_main.Name = "SBnews_doprava_split_main";
            // 
            // SBnews_doprava_split_main.Panel1
            // 
            this.SBnews_doprava_split_main.Panel1.Controls.Add(this.SBnews_doprava_split_secondary);
            // 
            // SBnews_doprava_split_main.Panel2
            // 
            this.SBnews_doprava_split_main.Panel2.Controls.Add(this.SBnews_Doprava_panel_right);
            this.SBnews_doprava_split_main.Size = new System.Drawing.Size(863, 618);
            this.SBnews_doprava_split_main.SplitterDistance = 627;
            this.SBnews_doprava_split_main.TabIndex = 0;
            // 
            // SBnews_doprava_split_secondary
            // 
            this.SBnews_doprava_split_secondary.Cursor = System.Windows.Forms.Cursors.Default;
            this.SBnews_doprava_split_secondary.DirtyPaletteCounter = 63;
            this.SBnews_doprava_split_secondary.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_doprava_split_secondary.Location = new System.Drawing.Point(0, 0);
            this.SBnews_doprava_split_secondary.Name = "SBnews_doprava_split_secondary";
            // 
            // SBnews_doprava_split_secondary.Panel1
            // 
            this.SBnews_doprava_split_secondary.Panel1.Controls.Add(this.SBnews_doprava_panel_left);
            // 
            // SBnews_doprava_split_secondary.Panel2
            // 
            this.SBnews_doprava_split_secondary.Panel2.Controls.Add(this.SBnews_Doprava_panel_main);
            this.SBnews_doprava_split_secondary.Size = new System.Drawing.Size(627, 618);
            this.SBnews_doprava_split_secondary.SplitterDistance = 208;
            this.SBnews_doprava_split_secondary.TabIndex = 0;
            // 
            // SBnews_doprava_panel_left
            // 
            this.SBnews_doprava_panel_left.DirtyPaletteCounter = 61;
            this.SBnews_doprava_panel_left.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_doprava_panel_left.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.SBnews_doprava_panel_left.Location = new System.Drawing.Point(0, 0);
            this.SBnews_doprava_panel_left.Name = "SBnews_doprava_panel_left";
            // 
            // SBnews_doprava_panel_left.Panel
            // 
            this.SBnews_doprava_panel_left.Panel.Controls.Add(this.doprava_web_stupne);
            this.SBnews_doprava_panel_left.Size = new System.Drawing.Size(208, 618);
            this.SBnews_doprava_panel_left.TabIndex = 0;
            this.SBnews_doprava_panel_left.Text = "Stupn� provozu";
            this.SBnews_doprava_panel_left.ValuesPrimary.Description = "";
            this.SBnews_doprava_panel_left.ValuesPrimary.Heading = "Stupn� provozu";
            this.SBnews_doprava_panel_left.ValuesPrimary.Image = null;
            this.SBnews_doprava_panel_left.ValuesSecondary.Description = "";
            this.SBnews_doprava_panel_left.ValuesSecondary.Heading = "";
            this.SBnews_doprava_panel_left.ValuesSecondary.Image = null;
            // 
            // doprava_web_stupne
            // 
            this.doprava_web_stupne.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doprava_web_stupne.IsWebBrowserContextMenuEnabled = false;
            this.doprava_web_stupne.Location = new System.Drawing.Point(0, 0);
            this.doprava_web_stupne.MinimumSize = new System.Drawing.Size(20, 20);
            this.doprava_web_stupne.Name = "doprava_web_stupne";
            this.doprava_web_stupne.ScriptErrorsSuppressed = true;
            this.doprava_web_stupne.Size = new System.Drawing.Size(206, 583);
            this.doprava_web_stupne.TabIndex = 0;
            // 
            // SBnews_Doprava_panel_main
            // 
            this.SBnews_Doprava_panel_main.DirtyPaletteCounter = 60;
            this.SBnews_Doprava_panel_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_Doprava_panel_main.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.SBnews_Doprava_panel_main.Location = new System.Drawing.Point(0, 0);
            this.SBnews_Doprava_panel_main.Name = "SBnews_Doprava_panel_main";
            // 
            // SBnews_Doprava_panel_main.Panel
            // 
            this.SBnews_Doprava_panel_main.Panel.Controls.Add(this.doprava_web_nehody);
            this.SBnews_Doprava_panel_main.Size = new System.Drawing.Size(414, 618);
            this.SBnews_Doprava_panel_main.TabIndex = 0;
            this.SBnews_Doprava_panel_main.Text = "Aktu�ln� nehody a omezen�";
            this.SBnews_Doprava_panel_main.ValuesPrimary.Description = "";
            this.SBnews_Doprava_panel_main.ValuesPrimary.Heading = "Aktu�ln� nehody a omezen�";
            this.SBnews_Doprava_panel_main.ValuesPrimary.Image = null;
            this.SBnews_Doprava_panel_main.ValuesSecondary.Description = "";
            this.SBnews_Doprava_panel_main.ValuesSecondary.Heading = "";
            this.SBnews_Doprava_panel_main.ValuesSecondary.Image = null;
            // 
            // doprava_web_nehody
            // 
            this.doprava_web_nehody.Dock = System.Windows.Forms.DockStyle.Fill;
            this.doprava_web_nehody.Location = new System.Drawing.Point(0, 0);
            this.doprava_web_nehody.MinimumSize = new System.Drawing.Size(20, 20);
            this.doprava_web_nehody.Name = "doprava_web_nehody";
            this.doprava_web_nehody.ScriptErrorsSuppressed = true;
            this.doprava_web_nehody.Size = new System.Drawing.Size(412, 583);
            this.doprava_web_nehody.TabIndex = 0;
            // 
            // SBnews_Doprava_panel_right
            // 
            this.SBnews_Doprava_panel_right.DirtyPaletteCounter = 63;
            this.SBnews_Doprava_panel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_Doprava_panel_right.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.SBnews_Doprava_panel_right.Location = new System.Drawing.Point(0, 0);
            this.SBnews_Doprava_panel_right.Name = "SBnews_Doprava_panel_right";
            // 
            // SBnews_Doprava_panel_right.Panel
            // 
            this.SBnews_Doprava_panel_right.Panel.Controls.Add(this.kryptonLabel10);
            this.SBnews_Doprava_panel_right.Panel.Controls.Add(this.SBnews_doprava_kraj);
            this.SBnews_Doprava_panel_right.Size = new System.Drawing.Size(231, 618);
            this.SBnews_Doprava_panel_right.TabIndex = 0;
            this.SBnews_Doprava_panel_right.Text = "Kraje";
            this.SBnews_Doprava_panel_right.ValuesPrimary.Description = "Doprava";
            this.SBnews_Doprava_panel_right.ValuesPrimary.Heading = "Kraje";
            this.SBnews_Doprava_panel_right.ValuesPrimary.Image = null;
            this.SBnews_Doprava_panel_right.ValuesSecondary.Description = "";
            this.SBnews_Doprava_panel_right.ValuesSecondary.Heading = "";
            this.SBnews_Doprava_panel_right.ValuesSecondary.Image = null;
            // 
            // kryptonLabel10
            // 
            this.kryptonLabel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.kryptonLabel10.AutoSize = false;
            this.kryptonLabel10.DirtyPaletteCounter = 7;
            this.kryptonLabel10.LabelStyle = ComponentFactory.Krypton.Toolkit.LabelStyle.TitlePanel;
            this.kryptonLabel10.Location = new System.Drawing.Point(8, 539);
            this.kryptonLabel10.Name = "kryptonLabel10";
            this.kryptonLabel10.Size = new System.Drawing.Size(214, 41);
            this.kryptonLabel10.TabIndex = 2;
            this.kryptonLabel10.Text = "ABA 1240";
            this.kryptonLabel10.Values.ExtraText = "";
            this.kryptonLabel10.Values.Image = null;
            this.kryptonLabel10.Values.Text = "ABA 1240";
            // 
            // SBnews_doprava_kraj
            // 
            this.SBnews_doprava_kraj.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.SBnews_doprava_kraj.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(214)))), ((int)(((byte)(232)))), ((int)(((byte)(255)))));
            this.SBnews_doprava_kraj.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SBnews_doprava_kraj.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SBnews_doprava_kraj.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SBnews_doprava_kraj.FormattingEnabled = true;
            this.SBnews_doprava_kraj.ItemHeight = 14;
            this.SBnews_doprava_kraj.Location = new System.Drawing.Point(8, 6);
            this.SBnews_doprava_kraj.Name = "SBnews_doprava_kraj";
            this.SBnews_doprava_kraj.Size = new System.Drawing.Size(214, 518);
            this.SBnews_doprava_kraj.TabIndex = 1;
            this.SBnews_doprava_kraj.SelectedIndexChanged += new System.EventHandler(this.SBnews_doprava_Zmen_Kraj);
            // 
            // SBnews_tab_sms
            // 
            this.SBnews_tab_sms.Controls.Add(this.SMS_split_main);
            this.SBnews_tab_sms.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_sms.Name = "SBnews_tab_sms";
            this.SBnews_tab_sms.Padding = new System.Windows.Forms.Padding(3);
            this.SBnews_tab_sms.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_sms.TabIndex = 3;
            this.SBnews_tab_sms.Text = "SMS";
            this.SBnews_tab_sms.UseVisualStyleBackColor = true;
            // 
            // SMS_split_main
            // 
            this.SMS_split_main.Cursor = System.Windows.Forms.Cursors.Default;
            this.SMS_split_main.DirtyPaletteCounter = 97;
            this.SMS_split_main.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SMS_split_main.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.SMS_split_main.IsSplitterFixed = true;
            this.SMS_split_main.Location = new System.Drawing.Point(3, 3);
            this.SMS_split_main.Name = "SMS_split_main";
            // 
            // SMS_split_main.Panel1
            // 
            this.SMS_split_main.Panel1.Controls.Add(this.SMS_split1);
            // 
            // SMS_split_main.Panel2
            // 
            this.SMS_split_main.Panel2.Controls.Add(this.SMS_panel_right);
            this.SMS_split_main.Size = new System.Drawing.Size(857, 612);
            this.SMS_split_main.SplitterDistance = 677;
            this.SMS_split_main.TabIndex = 8;
            // 
            // SMS_split1
            // 
            this.SMS_split1.Cursor = System.Windows.Forms.Cursors.Default;
            this.SMS_split1.DirtyPaletteCounter = 160;
            this.SMS_split1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SMS_split1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SMS_split1.Location = new System.Drawing.Point(0, 0);
            this.SMS_split1.Name = "SMS_split1";
            this.SMS_split1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            this.SMS_split1.PaletteMode = ComponentFactory.Krypton.Toolkit.PaletteMode.Office2007Blue;
            // 
            // SMS_split1.Panel1
            // 
            this.SMS_split1.Panel1.Controls.Add(this.dg_SMS);
            this.SMS_split1.Panel1MinSize = 150;
            // 
            // SMS_split1.Panel2
            // 
            this.SMS_split1.Panel2.Controls.Add(this.SMS_detail);
            this.SMS_split1.Panel2MinSize = 80;
            this.SMS_split1.SeparatorStyle = ComponentFactory.Krypton.Toolkit.SeparatorStyle.HighProfile;
            this.SMS_split1.Size = new System.Drawing.Size(677, 612);
            this.SMS_split1.SplitterDistance = 489;
            this.SMS_split1.TabIndex = 7;
            // 
            // dg_SMS
            // 
            this.dg_SMS.AllowUserToAddRows = false;
            this.dg_SMS.AllowUserToDeleteRows = false;
            this.dg_SMS.AllowUserToOrderColumns = true;
            this.dg_SMS.AllowUserToResizeRows = false;
            this.dg_SMS.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dg_SMS.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.dg_SMS.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.dg_SMS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_SMS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.sms_gate,
            this.sms_from,
            this.sms_date,
            this.sms_time,
            this.sms_folder,
            this.sms_text,
            this.sms_flag});
            this.dg_SMS.ContextMenuStrip = this.mmenu_ctk_detail;
            this.dg_SMS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dg_SMS.Location = new System.Drawing.Point(0, 0);
            this.dg_SMS.Margin = new System.Windows.Forms.Padding(0);
            this.dg_SMS.Name = "dg_SMS";
            this.dg_SMS.ReadOnly = true;
            this.dg_SMS.RowHeadersVisible = false;
            this.dg_SMS.RowHeadersWidth = 20;
            this.dg_SMS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_SMS.ShowCellErrors = false;
            this.dg_SMS.ShowCellToolTips = false;
            this.dg_SMS.ShowEditingIcon = false;
            this.dg_SMS.ShowRowErrors = false;
            this.dg_SMS.Size = new System.Drawing.Size(677, 489);
            this.dg_SMS.TabIndex = 5;
            this.dg_SMS.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dg_SMS_MouseDown);
            this.dg_SMS.SelectionChanged += new System.EventHandler(this.dg_SMS_SelectionChanged);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 16000000;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Visible = false;
            this.dataGridViewTextBoxColumn1.Width = 68;
            // 
            // sms_gate
            // 
            this.sms_gate.HeaderText = "sms_gate";
            this.sms_gate.Name = "sms_gate";
            this.sms_gate.ReadOnly = true;
            this.sms_gate.Visible = false;
            // 
            // sms_from
            // 
            this.sms_from.HeaderText = "sms_from";
            this.sms_from.Name = "sms_from";
            this.sms_from.ReadOnly = true;
            this.sms_from.Visible = false;
            // 
            // sms_date
            // 
            this.sms_date.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.sms_date.HeaderText = "Datum";
            this.sms_date.Name = "sms_date";
            this.sms_date.ReadOnly = true;
            this.sms_date.Width = 63;
            // 
            // sms_time
            // 
            this.sms_time.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.sms_time.HeaderText = "�as";
            this.sms_time.Name = "sms_time";
            this.sms_time.ReadOnly = true;
            this.sms_time.Width = 50;
            // 
            // sms_folder
            // 
            this.sms_folder.HeaderText = "sms_folder";
            this.sms_folder.Name = "sms_folder";
            this.sms_folder.ReadOnly = true;
            this.sms_folder.Visible = false;
            // 
            // sms_text
            // 
            this.sms_text.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.sms_text.HeaderText = "Text";
            this.sms_text.Name = "sms_text";
            this.sms_text.ReadOnly = true;
            // 
            // sms_flag
            // 
            this.sms_flag.Description = "P��znak";
            this.sms_flag.FillWeight = 16F;
            this.sms_flag.HeaderText = "P";
            this.sms_flag.Image = global::CTK_reader.Properties.Resources.flag_empty;
            this.sms_flag.MinimumWidth = 16;
            this.sms_flag.Name = "sms_flag";
            this.sms_flag.ReadOnly = true;
            this.sms_flag.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.sms_flag.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.sms_flag.Visible = false;
            this.sms_flag.Width = 16;
            // 
            // SMS_detail
            // 
            this.SMS_detail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.SMS_detail.ContextMenuStrip = this.mmenu_ctk_detail;
            this.SMS_detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SMS_detail.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SMS_detail.Location = new System.Drawing.Point(0, 0);
            this.SMS_detail.Multiline = true;
            this.SMS_detail.Name = "SMS_detail";
            this.SMS_detail.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.SMS_detail.Size = new System.Drawing.Size(677, 118);
            this.SMS_detail.TabIndex = 6;
            // 
            // SMS_panel_right
            // 
            this.SMS_panel_right.ButtonSpecs.AddRange(new ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup[] {
            this.SMS_panel_right_showhide});
            this.SMS_panel_right.DirtyPaletteCounter = 97;
            this.SMS_panel_right.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SMS_panel_right.GroupBackStyle = ComponentFactory.Krypton.Toolkit.PaletteBackStyle.HeaderSecondary;
            this.SMS_panel_right.HeaderVisibleSecondary = false;
            this.SMS_panel_right.Location = new System.Drawing.Point(0, 0);
            this.SMS_panel_right.Name = "SMS_panel_right";
            // 
            // SMS_panel_right.Panel
            // 
            this.SMS_panel_right.Panel.Controls.Add(this.btn_SMS_new);
            this.SMS_panel_right.Panel.Controls.Add(this.btn_SMS_reply);
            this.SMS_panel_right.Panel.Controls.Add(this.SMS_calendar);
            this.SMS_panel_right.Panel.Controls.Add(this.SMS_folders);
            this.SMS_panel_right.Size = new System.Drawing.Size(175, 612);
            this.SMS_panel_right.TabIndex = 0;
            this.SMS_panel_right.Text = "SMS";
            this.SMS_panel_right.ValuesPrimary.Description = "zpr�vy";
            this.SMS_panel_right.ValuesPrimary.Heading = "SMS";
            this.SMS_panel_right.ValuesPrimary.Image = ((System.Drawing.Image)(resources.GetObject("SMS_panel_right.ValuesPrimary.Image")));
            this.SMS_panel_right.ValuesSecondary.Description = "";
            this.SMS_panel_right.ValuesSecondary.Heading = "Description";
            this.SMS_panel_right.ValuesSecondary.Image = null;
            // 
            // SMS_panel_right_showhide
            // 
            this.SMS_panel_right_showhide.Edge = ComponentFactory.Krypton.Toolkit.PaletteRelativeEdgeAlign.Near;
            this.SMS_panel_right_showhide.ExtraText = "";
            this.SMS_panel_right_showhide.Image = null;
            this.SMS_panel_right_showhide.Text = "";
            this.SMS_panel_right_showhide.Type = ComponentFactory.Krypton.Toolkit.PaletteButtonSpecStyle.ArrowRight;
            this.SMS_panel_right_showhide.UniqueName = "541B73E213584309541B73E213584309";
            // 
            // btn_SMS_new
            // 
            this.btn_SMS_new.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SMS_new.DirtyPaletteCounter = 97;
            this.btn_SMS_new.Location = new System.Drawing.Point(9, 554);
            this.btn_SMS_new.Name = "btn_SMS_new";
            this.btn_SMS_new.Size = new System.Drawing.Size(58, 25);
            this.btn_SMS_new.TabIndex = 4;
            this.btn_SMS_new.Text = "Nov�";
            this.btn_SMS_new.Values.ExtraText = "";
            this.btn_SMS_new.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SMS_new.Values.Image")));
            this.btn_SMS_new.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_SMS_new.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_SMS_new.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_SMS_new.Values.Text = "Nov�";
            this.btn_SMS_new.Click += new System.EventHandler(this.btn_SMS_new_Click);
            // 
            // btn_SMS_reply
            // 
            this.btn_SMS_reply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_SMS_reply.DirtyPaletteCounter = 97;
            this.btn_SMS_reply.Location = new System.Drawing.Point(73, 554);
            this.btn_SMS_reply.Name = "btn_SMS_reply";
            this.btn_SMS_reply.Size = new System.Drawing.Size(88, 25);
            this.btn_SMS_reply.TabIndex = 5;
            this.btn_SMS_reply.Text = "Odpov��";
            this.btn_SMS_reply.Values.ExtraText = "";
            this.btn_SMS_reply.Values.Image = ((System.Drawing.Image)(resources.GetObject("btn_SMS_reply.Values.Image")));
            this.btn_SMS_reply.Values.ImageStates.ImageCheckedNormal = null;
            this.btn_SMS_reply.Values.ImageStates.ImageCheckedPressed = null;
            this.btn_SMS_reply.Values.ImageStates.ImageCheckedTracking = null;
            this.btn_SMS_reply.Values.Text = "Odpov��";
            this.btn_SMS_reply.Click += new System.EventHandler(this.btn_SMS_reply_Click);
            // 
            // SMS_calendar
            // 
            this.SMS_calendar.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SMS_calendar.Location = new System.Drawing.Point(9, 9);
            this.SMS_calendar.MaxSelectionCount = 30;
            this.SMS_calendar.MinDate = new System.DateTime(2006, 1, 1, 0, 0, 0, 0);
            this.SMS_calendar.Name = "SMS_calendar";
            this.SMS_calendar.TabIndex = 0;
            this.SMS_calendar.TitleBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.SMS_calendar.DateSelected += new System.Windows.Forms.DateRangeEventHandler(this.SMS_calendar_DateSelected);
            // 
            // SMS_folders
            // 
            this.SMS_folders.AllowDrop = true;
            this.SMS_folders.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SMS_folders.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.SMS_folders.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.SMS_folders.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SMS_folders.FullRowSelect = true;
            this.SMS_folders.HideSelection = false;
            this.SMS_folders.Location = new System.Drawing.Point(9, 176);
            this.SMS_folders.Name = "SMS_folders";
            treeNode1.Name = "Node2";
            treeNode1.Text = "Node";
            treeNode2.Name = "Node1";
            treeNode2.Text = "Root";
            this.SMS_folders.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode2});
            this.SMS_folders.PathSeparator = "@";
            this.SMS_folders.Size = new System.Drawing.Size(152, 374);
            this.SMS_folders.TabIndex = 1;
            this.SMS_folders.DragDrop += new System.Windows.Forms.DragEventHandler(this.SMS_folders_DragDrop);
            this.SMS_folders.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.SMS_folders_AfterSelect);
            this.SMS_folders.DragEnter += new System.Windows.Forms.DragEventHandler(this.SMS_folders_DragEnter);
            // 
            // SBnews_tab_scratch
            // 
            this.SBnews_tab_scratch.Controls.Add(this.SBnews_ScratchBox);
            this.SBnews_tab_scratch.Location = new System.Drawing.Point(4, 25);
            this.SBnews_tab_scratch.Name = "SBnews_tab_scratch";
            this.SBnews_tab_scratch.Size = new System.Drawing.Size(863, 618);
            this.SBnews_tab_scratch.TabIndex = 2;
            this.SBnews_tab_scratch.Text = "Pozn�mky";
            this.SBnews_tab_scratch.UseVisualStyleBackColor = true;
            // 
            // SBnews_ScratchBox
            // 
            this.SBnews_ScratchBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.SBnews_ScratchBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SBnews_ScratchBox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SBnews_ScratchBox.Location = new System.Drawing.Point(0, 0);
            this.SBnews_ScratchBox.Multiline = true;
            this.SBnews_ScratchBox.Name = "SBnews_ScratchBox";
            this.SBnews_ScratchBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.SBnews_ScratchBox.Size = new System.Drawing.Size(863, 618);
            this.SBnews_ScratchBox.TabIndex = 0;
            this.SBnews_ScratchBox.Text = "ScratchBox.";
            // 
            // appNotify
            // 
            this.appNotify.Icon = ((System.Drawing.Icon)(resources.GetObject("appNotify.Icon")));
            this.appNotify.Text = "OstroRIS";
            this.appNotify.Visible = true;
            this.appNotify.BalloonTipClicked += new System.EventHandler(this.appNotify_BalloonTipClicked);
            this.appNotify.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.appNotify_MouseDoubleClick);
            // 
            // SMS_timer
            // 
            this.SMS_timer.Interval = 30000;
            this.SMS_timer.Tick += new System.EventHandler(this.SMS_timer_Tick);
            // 
            // dialog_mmenu_ulozit_csv
            // 
            this.dialog_mmenu_ulozit_csv.DefaultExt = "*.csv";
            this.dialog_mmenu_ulozit_csv.Filter = "Tabulka CSV (*.csv)|*.csv";
            this.dialog_mmenu_ulozit_csv.Title = "Ulo�it zpr�vu";
            // 
            // ctk_status
            // 
            this.ctk_status.BackColor = System.Drawing.SystemColors.Control;
            this.ctk_status.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.ctk_status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LoadingCircle_statusbar,
            this.ctk_status_label});
            this.ctk_status.Location = new System.Drawing.Point(0, 677);
            this.ctk_status.Name = "ctk_status";
            this.ctk_status.RenderMode = System.Windows.Forms.ToolStripRenderMode.ManagerRenderMode;
            this.ctk_status.Size = new System.Drawing.Size(871, 22);
            this.ctk_status.TabIndex = 9;
            this.ctk_status.Text = "CTK";
            this.ctk_status.DoubleClick += new System.EventHandler(this.ctk_status_dClick);
            // 
            // LoadingCircle_statusbar
            // 
            this.LoadingCircle_statusbar.BackColor = System.Drawing.Color.Transparent;
            // 
            // LoadingCircle_statusbar
            // 
            this.LoadingCircle_statusbar.LoadingCircleControl.AccessibleName = "LoadingCircle_statusbar";
            this.LoadingCircle_statusbar.LoadingCircleControl.Active = false;
            this.LoadingCircle_statusbar.LoadingCircleControl.BackColor = System.Drawing.Color.Transparent;
            this.LoadingCircle_statusbar.LoadingCircleControl.Color = System.Drawing.Color.DarkGray;
            this.LoadingCircle_statusbar.LoadingCircleControl.InnerCircleRadius = 6;
            this.LoadingCircle_statusbar.LoadingCircleControl.Location = new System.Drawing.Point(1, 2);
            this.LoadingCircle_statusbar.LoadingCircleControl.Name = "LoadingCircle_statusbar";
            this.LoadingCircle_statusbar.LoadingCircleControl.NumberSpoke = 9;
            this.LoadingCircle_statusbar.LoadingCircleControl.OuterCircleRadius = 7;
            this.LoadingCircle_statusbar.LoadingCircleControl.RotationSpeed = 100;
            this.LoadingCircle_statusbar.LoadingCircleControl.Size = new System.Drawing.Size(22, 20);
            this.LoadingCircle_statusbar.LoadingCircleControl.SpokeThickness = 4;
            this.LoadingCircle_statusbar.LoadingCircleControl.StylePreset = MRG.Controls.UI.LoadingCircle.StylePresets.Firefox;
            this.LoadingCircle_statusbar.LoadingCircleControl.TabIndex = 1;
            this.LoadingCircle_statusbar.LoadingCircleControl.Text = "loading";
            this.LoadingCircle_statusbar.Name = "LoadingCircle_statusbar";
            this.LoadingCircle_statusbar.Size = new System.Drawing.Size(22, 20);
            this.LoadingCircle_statusbar.Text = "loading";
            // 
            // ctk_status_label
            // 
            this.ctk_status_label.Name = "ctk_status_label";
            this.ctk_status_label.Size = new System.Drawing.Size(16, 17);
            this.ctk_status_label.Text = "...";
            // 
            // CustNews_timer_dg_reload
            // 
            this.CustNews_timer_dg_reload.Interval = 5000;
            this.CustNews_timer_dg_reload.Tick += new System.EventHandler(this.CustNews_timer_dg_reload_Tick);
            // 
            // top_menu
            // 
            this.top_menu.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.top_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.top_menu_soubor,
            this.top_menu_editace,
            this.top_menu_nastaveni,
            this.top_menu__about,
            this.top_menu__update,
            this.top_menu_debug});
            this.top_menu.Location = new System.Drawing.Point(0, 0);
            this.top_menu.Name = "top_menu";
            this.top_menu.Size = new System.Drawing.Size(871, 24);
            this.top_menu.TabIndex = 10;
            this.top_menu.Text = "menuStrip1";
            // 
            // top_menu_soubor
            // 
            this.top_menu_soubor.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.top_menu_soubor__profile,
            this.toolStripSeparator8,
            this.top_menu_soubor__save,
            this.top_menu_soubor__saveCSV,
            this.top_menu_soubor__sendEmail,
            this.toolStripSeparator10,
            this.top_menu_soubor__print,
            this.top_menu_soubor__print_preview,
            this.toolStripSeparator11,
            this.top_menu_soubor__quit});
            this.top_menu_soubor.Name = "top_menu_soubor";
            this.top_menu_soubor.Size = new System.Drawing.Size(57, 20);
            this.top_menu_soubor.Text = "Soubor";
            // 
            // top_menu_soubor__profile
            // 
            this.top_menu_soubor__profile.Image = global::CTK_reader.Properties.Resources.user_16;
            this.top_menu_soubor__profile.Name = "top_menu_soubor__profile";
            this.top_menu_soubor__profile.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__profile.Text = "P�epnout profil";
            this.top_menu_soubor__profile.Click += new System.EventHandler(this.mmneu_profile_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(222, 6);
            // 
            // top_menu_soubor__save
            // 
            this.top_menu_soubor__save.Image = global::CTK_reader.Properties.Resources.save;
            this.top_menu_soubor__save.Name = "top_menu_soubor__save";
            this.top_menu_soubor__save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.top_menu_soubor__save.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__save.Text = "Ulo�it jako text";
            this.top_menu_soubor__save.Click += new System.EventHandler(this.mmenu_ulozit_click);
            // 
            // top_menu_soubor__saveCSV
            // 
            this.top_menu_soubor__saveCSV.Image = global::CTK_reader.Properties.Resources.csv_export;
            this.top_menu_soubor__saveCSV.Name = "top_menu_soubor__saveCSV";
            this.top_menu_soubor__saveCSV.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.S)));
            this.top_menu_soubor__saveCSV.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__saveCSV.Text = "Ulo�it jako CSV";
            this.top_menu_soubor__saveCSV.Click += new System.EventHandler(this.mmenu_ulozit_csv_Click);
            // 
            // top_menu_soubor__sendEmail
            // 
            this.top_menu_soubor__sendEmail.Enabled = false;
            this.top_menu_soubor__sendEmail.Image = global::CTK_reader.Properties.Resources.mail;
            this.top_menu_soubor__sendEmail.Name = "top_menu_soubor__sendEmail";
            this.top_menu_soubor__sendEmail.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__sendEmail.Text = "Odeslat jako email";
            // 
            // toolStripSeparator10
            // 
            this.toolStripSeparator10.Name = "toolStripSeparator10";
            this.toolStripSeparator10.Size = new System.Drawing.Size(222, 6);
            // 
            // top_menu_soubor__print
            // 
            this.top_menu_soubor__print.Image = global::CTK_reader.Properties.Resources.print;
            this.top_menu_soubor__print.Name = "top_menu_soubor__print";
            this.top_menu_soubor__print.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.top_menu_soubor__print.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__print.Text = "Tisk";
            this.top_menu_soubor__print.Click += new System.EventHandler(this.mmenu_tisk_click);
            // 
            // top_menu_soubor__print_preview
            // 
            this.top_menu_soubor__print_preview.Image = global::CTK_reader.Properties.Resources.print_preview;
            this.top_menu_soubor__print_preview.Name = "top_menu_soubor__print_preview";
            this.top_menu_soubor__print_preview.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.P)));
            this.top_menu_soubor__print_preview.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__print_preview.Text = "N�hled tisku";
            this.top_menu_soubor__print_preview.Click += new System.EventHandler(this.mmenu_tisk_nahled_click);
            // 
            // toolStripSeparator11
            // 
            this.toolStripSeparator11.Name = "toolStripSeparator11";
            this.toolStripSeparator11.Size = new System.Drawing.Size(222, 6);
            // 
            // top_menu_soubor__quit
            // 
            this.top_menu_soubor__quit.Image = global::CTK_reader.Properties.Resources.quit;
            this.top_menu_soubor__quit.Name = "top_menu_soubor__quit";
            this.top_menu_soubor__quit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.top_menu_soubor__quit.Size = new System.Drawing.Size(225, 22);
            this.top_menu_soubor__quit.Text = "Konec";
            this.top_menu_soubor__quit.Click += new System.EventHandler(this.mmenu_konec_click);
            // 
            // top_menu_editace
            // 
            this.top_menu_editace.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.top_menu_editace__copy,
            this.top_menu_editace__copyAll,
            this.top_menu_editace__copyCSV});
            this.top_menu_editace.Name = "top_menu_editace";
            this.top_menu_editace.Size = new System.Drawing.Size(57, 20);
            this.top_menu_editace.Text = "Editace";
            // 
            // top_menu_editace__copy
            // 
            this.top_menu_editace__copy.Image = global::CTK_reader.Properties.Resources.copy;
            this.top_menu_editace__copy.Name = "top_menu_editace__copy";
            this.top_menu_editace__copy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.top_menu_editace__copy.Size = new System.Drawing.Size(269, 22);
            this.top_menu_editace__copy.Text = "Kop�rovat";
            this.top_menu_editace__copy.Click += new System.EventHandler(this.mmenu_kopirovat_click);
            // 
            // top_menu_editace__copyAll
            // 
            this.top_menu_editace__copyAll.Image = global::CTK_reader.Properties.Resources.copy_all;
            this.top_menu_editace__copyAll.Name = "top_menu_editace__copyAll";
            this.top_menu_editace__copyAll.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift)
                        | System.Windows.Forms.Keys.C)));
            this.top_menu_editace__copyAll.Size = new System.Drawing.Size(269, 22);
            this.top_menu_editace__copyAll.Text = "Kop�rovat celou zpr�vu";
            this.top_menu_editace__copyAll.Click += new System.EventHandler(this.mmenu_kopirovat_celou_click);
            // 
            // top_menu_editace__copyCSV
            // 
            this.top_menu_editace__copyCSV.Image = global::CTK_reader.Properties.Resources.csv_export;
            this.top_menu_editace__copyCSV.Name = "top_menu_editace__copyCSV";
            this.top_menu_editace__copyCSV.Size = new System.Drawing.Size(269, 22);
            this.top_menu_editace__copyCSV.Text = "Kop�rovat jako CSV";
            this.top_menu_editace__copyCSV.Click += new System.EventHandler(this.mmenu_kopirovat_csv_click);
            // 
            // top_menu_nastaveni
            // 
            this.top_menu_nastaveni.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.top_menu_nastaveni__fonts,
            this.top_menu_nastaveni__CTK,
            this.top_menu_nastaveni__CTK_filters,
            this.toolStripSeparator12,
            this.top_menu_nastaveni__mysql});
            this.top_menu_nastaveni.Name = "top_menu_nastaveni";
            this.top_menu_nastaveni.Size = new System.Drawing.Size(71, 20);
            this.top_menu_nastaveni.Text = "Nastaven�";
            // 
            // top_menu_nastaveni__fonts
            // 
            this.top_menu_nastaveni__fonts.Image = global::CTK_reader.Properties.Resources.cfg_color;
            this.top_menu_nastaveni__fonts.Name = "top_menu_nastaveni__fonts";
            this.top_menu_nastaveni__fonts.Size = new System.Drawing.Size(202, 22);
            this.top_menu_nastaveni__fonts.Text = "Nastaven� barev a p�sem";
            this.top_menu_nastaveni__fonts.Click += new System.EventHandler(this.mmenu_cfg_color_font_Click);
            // 
            // top_menu_nastaveni__CTK
            // 
            this.top_menu_nastaveni__CTK.Image = global::CTK_reader.Properties.Resources.cfg;
            this.top_menu_nastaveni__CTK.Name = "top_menu_nastaveni__CTK";
            this.top_menu_nastaveni__CTK.Size = new System.Drawing.Size(202, 22);
            this.top_menu_nastaveni__CTK.Text = "Nastaven� �TK";
            this.top_menu_nastaveni__CTK.Click += new System.EventHandler(this.mmenu_cfg_ctk_Click);
            // 
            // top_menu_nastaveni__CTK_filters
            // 
            this.top_menu_nastaveni__CTK_filters.Image = global::CTK_reader.Properties.Resources.cfg_filtry;
            this.top_menu_nastaveni__CTK_filters.Name = "top_menu_nastaveni__CTK_filters";
            this.top_menu_nastaveni__CTK_filters.Size = new System.Drawing.Size(202, 22);
            this.top_menu_nastaveni__CTK_filters.Text = "Nastaven� filtr� �TK";
            this.top_menu_nastaveni__CTK_filters.Click += new System.EventHandler(this.mmenu_cfg_filtry_Click);
            // 
            // toolStripSeparator12
            // 
            this.toolStripSeparator12.Name = "toolStripSeparator12";
            this.toolStripSeparator12.Size = new System.Drawing.Size(199, 6);
            // 
            // top_menu_nastaveni__mysql
            // 
            this.top_menu_nastaveni__mysql.Image = global::CTK_reader.Properties.Resources.cfg_sql;
            this.top_menu_nastaveni__mysql.Name = "top_menu_nastaveni__mysql";
            this.top_menu_nastaveni__mysql.Size = new System.Drawing.Size(202, 22);
            this.top_menu_nastaveni__mysql.Text = "P�ipojen� k MySQL";
            this.top_menu_nastaveni__mysql.Click += new System.EventHandler(this.mmenu_cfgsql_click);
            // 
            // top_menu__about
            // 
            this.top_menu__about.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.top_menu__about.Image = global::CTK_reader.Properties.Resources.about;
            this.top_menu__about.Name = "top_menu__about";
            this.top_menu__about.Size = new System.Drawing.Size(87, 20);
            this.top_menu__about.Text = "O aplikaci";
            this.top_menu__about.Click += new System.EventHandler(this.mmenu_about_click);
            // 
            // top_menu__update
            // 
            this.top_menu__update.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.top_menu__update.Name = "top_menu__update";
            this.top_menu__update.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.top_menu__update.Size = new System.Drawing.Size(79, 20);
            this.top_menu__update.Text = "Aktualizace";
            this.top_menu__update.Click += new System.EventHandler(this.mmenu_update_click);
            // 
            // top_menu_debug
            // 
            this.top_menu_debug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.top_menu_debug__showLog,
            this.top_menu_debug__hideLog,
            this.toolStripSeparator13,
            this.top_menu_debug__raiseError});
            this.top_menu_debug.Name = "top_menu_debug";
            this.top_menu_debug.Size = new System.Drawing.Size(56, 20);
            this.top_menu_debug.Text = "DEBUG";
            this.top_menu_debug.Visible = false;
            // 
            // top_menu_debug__showLog
            // 
            this.top_menu_debug__showLog.Name = "top_menu_debug__showLog";
            this.top_menu_debug__showLog.Size = new System.Drawing.Size(137, 22);
            this.top_menu_debug__showLog.Text = "Zobrazit log";
            this.top_menu_debug__showLog.Click += new System.EventHandler(this.mmenu_debug_show_click);
            // 
            // top_menu_debug__hideLog
            // 
            this.top_menu_debug__hideLog.Name = "top_menu_debug__hideLog";
            this.top_menu_debug__hideLog.Size = new System.Drawing.Size(137, 22);
            this.top_menu_debug__hideLog.Text = "Skr�t log";
            this.top_menu_debug__hideLog.Click += new System.EventHandler(this.mmenu_debug_hide_click);
            // 
            // toolStripSeparator13
            // 
            this.toolStripSeparator13.Name = "toolStripSeparator13";
            this.toolStripSeparator13.Size = new System.Drawing.Size(134, 6);
            // 
            // top_menu_debug__raiseError
            // 
            this.top_menu_debug__raiseError.Name = "top_menu_debug__raiseError";
            this.top_menu_debug__raiseError.Size = new System.Drawing.Size(137, 22);
            this.top_menu_debug__raiseError.Text = "RaiseError";
            this.top_menu_debug__raiseError.Click += new System.EventHandler(this.mmenu_debug_error_click);
            // 
            // timer_doprava
            // 
            this.timer_doprava.Interval = 60000;
            this.timer_doprava.Tick += new System.EventHandler(this.timer_doprava_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(187)))), ((int)(((byte)(217)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(871, 699);
            this.Controls.Add(this.ctk_status);
            this.Controls.Add(this.top_menu);
            this.Controls.Add(this.SBnews_panel);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.HelpButton = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(700, 540);
            this.Name = "MainForm";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OstroRIS";
            this.TextExtra = "";
            this.Load += new System.EventHandler(this.MainFormLoad);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_closing);
            this.ResizeEnd += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.ctk_zrus_filtry_icon)).EndInit();
            this.mmenu_ctk_detail.ResumeLayout(false);
            this.ctk_tabDatum.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctk_ulozeny_filtr_set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ctk_zrus_filtry)).EndInit();
            this.ctk_tabKat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ctk_kat_nastav)).EndInit();
            this.ctk_tabFiltry.ResumeLayout(false);
            this.ctk_tabFiltry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btn_ctk_filtry_nastav)).EndInit();
            this.ctk_tab.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_ctk_list)).EndInit();
            this.SBnews_panel.ResumeLayout(false);
            this.SBnews_tab_ctk.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main.Panel1)).EndInit();
            this.CTK_split_main.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main.Panel2)).EndInit();
            this.CTK_split_main.Panel2.ResumeLayout(false);
            this.CTK_split_main.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_main)).EndInit();
            this.CTK_split_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top.Panel1)).EndInit();
            this.CTK_split_top.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top.Panel2)).EndInit();
            this.CTK_split_top.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_split_top)).EndInit();
            this.CTK_split_top.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_panel_right.Panel)).EndInit();
            this.CTK_panel_right.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CTK_panel_right)).EndInit();
            this.CTK_panel_right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonBorderEdge1)).EndInit();
            this.SBnews_tab_CustNews.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main.Panel1)).EndInit();
            this.CustNews_split_main.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main.Panel2)).EndInit();
            this.CustNews_split_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_main)).EndInit();
            this.CustNews_split_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail.Panel1)).EndInit();
            this.CustNews_split_dg_detail.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail.Panel2)).EndInit();
            this.CustNews_split_dg_detail.Panel2.ResumeLayout(false);
            this.CustNews_split_dg_detail.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_split_dg_detail)).EndInit();
            this.CustNews_split_dg_detail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_CustNews_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_main_panel_right.Panel)).EndInit();
            this.CustNews_main_panel_right.Panel.ResumeLayout(false);
            this.CustNews_main_panel_right.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CustNews_main_panel_right)).EndInit();
            this.CustNews_main_panel_right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_showAll)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_sel_drop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_sel_set)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_CustNews_new)).EndInit();
            this.SBnews_tab_ris.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_panel_main)).EndInit();
            this.RIS_panel_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main.Panel1)).EndInit();
            this.RIS_main_split_main.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main.Panel2)).EndInit();
            this.RIS_main_split_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_split_main)).EndInit();
            this.RIS_main_split_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail.Panel1)).EndInit();
            this.RIS_split_dg_detail.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail.Panel2)).EndInit();
            this.RIS_split_dg_detail.Panel2.ResumeLayout(false);
            this.RIS_split_dg_detail.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_split_dg_detail)).EndInit();
            this.RIS_split_dg_detail.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_RIS_list)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_panel_main_left.Panel)).EndInit();
            this.RIS_main_panel_main_left.Panel.ResumeLayout(false);
            this.RIS_main_panel_main_left.Panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_main_panel_main_left)).EndInit();
            this.RIS_main_panel_main_left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.RIS_sel_all_versions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_btn_reload_clean)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RIS_btn_reload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RIS_edit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_RIS_new)).EndInit();
            this.SBnews_tab_doprava.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main.Panel1)).EndInit();
            this.SBnews_doprava_split_main.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main.Panel2)).EndInit();
            this.SBnews_doprava_split_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_main)).EndInit();
            this.SBnews_doprava_split_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary.Panel1)).EndInit();
            this.SBnews_doprava_split_secondary.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary.Panel2)).EndInit();
            this.SBnews_doprava_split_secondary.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_split_secondary)).EndInit();
            this.SBnews_doprava_split_secondary.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_panel_left.Panel)).EndInit();
            this.SBnews_doprava_panel_left.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_doprava_panel_left)).EndInit();
            this.SBnews_doprava_panel_left.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_main.Panel)).EndInit();
            this.SBnews_Doprava_panel_main.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_main)).EndInit();
            this.SBnews_Doprava_panel_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_right.Panel)).EndInit();
            this.SBnews_Doprava_panel_right.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SBnews_Doprava_panel_right)).EndInit();
            this.SBnews_Doprava_panel_right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.kryptonLabel10)).EndInit();
            this.SBnews_tab_sms.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main.Panel1)).EndInit();
            this.SMS_split_main.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main.Panel2)).EndInit();
            this.SMS_split_main.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split_main)).EndInit();
            this.SMS_split_main.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1.Panel1)).EndInit();
            this.SMS_split1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1.Panel2)).EndInit();
            this.SMS_split1.Panel2.ResumeLayout(false);
            this.SMS_split1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_split1)).EndInit();
            this.SMS_split1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dg_SMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SMS_panel_right.Panel)).EndInit();
            this.SMS_panel_right.Panel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SMS_panel_right)).EndInit();
            this.SMS_panel_right.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_new)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_SMS_reply)).EndInit();
            this.SBnews_tab_scratch.ResumeLayout(false);
            this.SBnews_tab_scratch.PerformLayout();
            this.ctk_status.ResumeLayout(false);
            this.ctk_status.PerformLayout();
            this.top_menu.ResumeLayout(false);
            this.top_menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private System.Windows.Forms.Label lbl_ctk_filtry_nadpis;
        private System.Windows.Forms.Label lbl_ctk_filtry_text;
        private System.Windows.Forms.TabPage ctk_tabFiltry;
        private System.Windows.Forms.TextBox filtry_ctk_nadpis;
        private System.Windows.Forms.TabPage ctk_tabKat;
        private System.Windows.Forms.Label lbl_ctk_filtry_klicovaSlova;
        private System.Windows.Forms.TextBox filtry_ctk_text;
        private System.Windows.Forms.MonthCalendar ctk_calendar;
        private System.Windows.Forms.TabPage ctk_tabDatum;
        private System.Windows.Forms.SaveFileDialog dialog_mmenu_ulozit;
        public System.Windows.Forms.TextBox ctk_detail;
        private System.Windows.Forms.Timer ctk_list_timer;
        private System.Windows.Forms.CheckBox ctk_kat_list_spojeni;
        private System.Windows.Forms.TextBox filtry_ctk_klicovaSlova;
        private System.Windows.Forms.PictureBox ctk_zrus_filtry_icon;
        private System.Windows.Forms.CheckBox ctk_filtry_spojeni;

        private ContextMenuStrip mmenu_ctk_detail;
        private ToolStripMenuItem mmenu_ctk_detail_kopirovat;
        private ToolStripMenuItem mmenu_ctk_detail_kopirovat_celou;
        private ToolStripSeparator toolStripSeparator6;
        private ToolStripMenuItem mmenu_ctk_detail_ulozit;
        private ToolStripMenuItem mmenu_ctk_detail_email;
        private ToolStripMenuItem mmenu_ctk_detail_tisk;
        private ToolStripMenuItem mmenu_ctk_detail_tisk_nahled;
        private ToolStripSeparator toolStripSeparator7;
        private Label lbl_ctk_filtry_kraj;
        private System.Windows.Forms.Timer timer_ctkStart;
        private System.ComponentModel.BackgroundWorker bg_AU;
        private TabPage SBnews_tab_ctk;
        private TabPage SBnews_tab_ris;
        //private SplitContainer SBnews_tab_ctk_spliter;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer CTK_split_main;
        private TabPage SBnews_tab_scratch;
        private TextBox SBnews_ScratchBox;
        private NotifyIcon appNotify;
        private TabPage SBnews_tab_sms;
        private MonthCalendar SMS_calendar;
        public TreeView SMS_folders;
        public DataGridView dg_SMS;
        //private SplitContainer SMS_split1;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer SMS_split1;
        private System.Windows.Forms.Timer SMS_timer;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private DataGridViewTextBoxColumn sms_gate;
        private DataGridViewTextBoxColumn sms_from;
        private DataGridViewTextBoxColumn sms_date;
        private DataGridViewTextBoxColumn sms_time;
        private DataGridViewTextBoxColumn sms_folder;
        private DataGridViewTextBoxColumn sms_text;
        private DataGridViewImageColumn sms_flag;
        public DataGridView dg_ctk_list;
        //private SplitContainer RIS_split_sub;
        private SaveFileDialog dialog_mmenu_ulozit_csv;
        public TextBox SMS_detail;
        private StatusStrip ctk_status;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SMS_reply;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_SMS_new;
        private ComponentFactory.Krypton.Toolkit.KryptonManager kryptonManager1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton ctk_zrus_filtry;
        private ComponentFactory.Krypton.Toolkit.KryptonBorderEdge kryptonBorderEdge1;
        private ComponentFactory.Krypton.Toolkit.KryptonButton ctk_ulozeny_filtr_set;
        private ComponentFactory.Krypton.Toolkit.KryptonButton ctk_kat_nastav;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_ctk_filtry_nastav;
        private ComponentFactory.Krypton.Toolkit.KryptonPanel RIS_panel_main;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer RIS_main_split_main;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup RIS_main_panel_main_left;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup RIS_main_split_main_2_show_hide;
        public DataGridView dg_RIS_list;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_RIS_edit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_RIS_new;
        private TextBox RIS_sel_nazev;
        private TextBox RIS_sel_mesto;
        public ComboBox RIS_sel_kategorie;
        private MonthCalendar RIS_calendar;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer RIS_split_dg_detail;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel4;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel3;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel2;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel1;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer SMS_split_main;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup SMS_panel_right;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup SMS_panel_right_showhide;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer CTK_split_top;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup CTK_panel_right;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup buttonSpecHeaderGroup1;
        private Dotnetrix.Controls.TabControlEX SBnews_panel;
        private Dotnetrix.Controls.TabControlEX ctk_tab;
        public ComboBox RIS_sel_group;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel5;
        public ComboBox RIS_sel_oblast;
        public TextBox RIS_txt_detail;
        private DataGridViewTextBoxColumn dg_RIS_id;
        private DataGridViewTextBoxColumn dg_RIS_datum;
        private DataGridViewTextBoxColumn dg_RIS_okres;
        private DataGridViewTextBoxColumn dg_RIS_titulek;
        private DataGridViewTextBoxColumn dg_RIS_klicova_slova;
        private DataGridViewImageColumn dg_RIS_medialni_partner;
        private DataGridViewImageColumn dg_RIS_promo_vysilani;
        private ComponentFactory.Krypton.Toolkit.KryptonButton RIS_btn_reload;
        private ComponentFactory.Krypton.Toolkit.KryptonButton RIS_btn_reload_clean;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton RIS_sel_all_versions;
        private ToolStripMenuItem mmenu_ctk_detail_kopirovat_csv;
        private ToolStripMenuItem mmenu_ctk_detail_ulozit_csv;
        private Dotnetrix.Controls.TabPageEX SBnews_tab_CustNews;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer CustNews_split_main;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup CustNews_main_panel_right;
        private ComponentFactory.Krypton.Toolkit.ButtonSpecHeaderGroup CustNews_main_panel_right_ShowHide;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer CustNews_split_dg_detail;
        public TextBox CustNews_txt_detail;
        private MonthCalendar CustNews_Calendar;
        public DataGridView dg_CustNews_list;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CustNews_edit;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CustNews_new;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CustNews_sel_drop;
        private ComponentFactory.Krypton.Toolkit.KryptonButton btn_CustNews_sel_set;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel6;
        public ComboBox CustNews_sel_oblast;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel7;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel8;
        public ComboBox CustNews_sel_group;
        public ComboBox CustNews_sel_kategorie;
        private DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private DataGridViewTextBoxColumn dg_CustNews_list_datum;
        private DataGridViewTextBoxColumn dg_CustNews_list_cas;
        private DataGridViewTextBoxColumn dg_CustNews_list_okres;
        private DataGridViewTextBoxColumn dg_CustNews_list_nazev;
        private DataGridViewTextBoxColumn dg_CustNews_list_klicova_slova;
        private DataGridViewImageColumn dg_CustNews_list_repo_ico;
        private DataGridViewTextBoxColumn dg_CustNews_list_id_repa;
        private TextBox CustNews_sel_text;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel9;
        private Timer CustNews_timer_dg_reload;
        private ComponentFactory.Krypton.Toolkit.KryptonCheckButton btn_CustNews_showAll;
        private DataGridViewTextBoxColumn id;
        private DataGridViewTextBoxColumn datum;
        private DataGridViewTextBoxColumn cas;
        private DataGridViewTextBoxColumn kategorie;
        private DataGridViewTextBoxColumn titulek;
        private DataGridViewTextBoxColumn klicova_slova;
        private DataGridViewImageColumn flag;
        private Dotnetrix.Controls.TabPageEX SBnews_tab_doprava;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer SBnews_doprava_split_main;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup SBnews_Doprava_panel_right;
        private ComponentFactory.Krypton.Toolkit.KryptonSplitContainer SBnews_doprava_split_secondary;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup SBnews_doprava_panel_left;
        private ComponentFactory.Krypton.Toolkit.KryptonHeaderGroup SBnews_Doprava_panel_main;
        private WebBrowser doprava_web_stupne;
        private WebBrowser doprava_web_nehody;
        private MenuStrip top_menu;
        private ToolStripMenuItem top_menu_soubor;
        private ToolStripMenuItem top_menu_editace;
        private ToolStripMenuItem top_menu_nastaveni;
        private ToolStripMenuItem top_menu__about;
        private ToolStripMenuItem top_menu__update;
        private ToolStripMenuItem top_menu_debug;
        private ToolStripMenuItem top_menu_soubor__profile;
        private ToolStripSeparator toolStripSeparator8;
        private ToolStripMenuItem top_menu_soubor__save;
        private ToolStripMenuItem top_menu_soubor__saveCSV;
        private ToolStripMenuItem top_menu_soubor__sendEmail;
        private ToolStripSeparator toolStripSeparator10;
        private ToolStripMenuItem top_menu_soubor__print;
        private ToolStripMenuItem top_menu_soubor__print_preview;
        private ToolStripSeparator toolStripSeparator11;
        private ToolStripMenuItem top_menu_soubor__quit;
        private ToolStripMenuItem top_menu_editace__copy;
        private ToolStripMenuItem top_menu_editace__copyAll;
        private ToolStripMenuItem top_menu_editace__copyCSV;
        private ToolStripMenuItem top_menu_nastaveni__fonts;
        private ToolStripMenuItem top_menu_nastaveni__CTK;
        private ToolStripMenuItem top_menu_nastaveni__CTK_filters;
        private ToolStripSeparator toolStripSeparator12;
        private ToolStripMenuItem top_menu_nastaveni__mysql;
        private ToolStripMenuItem top_menu_debug__showLog;
        private ToolStripMenuItem top_menu_debug__hideLog;
        private ToolStripSeparator toolStripSeparator13;
        private ToolStripMenuItem top_menu_debug__raiseError;
        private ListBox SBnews_doprava_kraj;
        private Timer timer_doprava;
        private MRG.Controls.UI.LoadingCircleToolStripMenuItem LoadingCircle_statusbar;
        private ToolStripStatusLabel ctk_status_label;
        private ComponentFactory.Krypton.Toolkit.KryptonLabel kryptonLabel10;
    }
}