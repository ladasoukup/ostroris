namespace CTK_reader
{
    partial class MainForm
    {

        public void LogDebug(string dbgText, string dbgType)
        {
            try
            {
                wnd_debug.txt_debug.AppendText("\r\n" + System.DateTime.Now.ToLongTimeString() + ": " + dbgType + ": " + dbgText);
            }
            catch { }
        }
    }
}