using System;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;
using System.Drawing.Printing;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using MySQLDriverCS;
using Ini;
using ComponentFactory.Krypton.Toolkit;
using PSTaskDialog;

namespace CTK_reader
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
    //public partial class MainForm : System.Windows.Forms.Form
    public partial class MainForm : KryptonForm
    {
        [STAThread]
        public static void Main(string[] args)
        {
            SplashScreen SPLASH = new SplashScreen();
            SPLASH.Show();
            Application.Run(new MainForm(SPLASH));
        }

        public MainForm(SplashScreen SPLASH)
        {
            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            mSPLASH = SPLASH;
            // The InitializeComponent() call is required for Windows Forms designer support.
            InitializeComponent();
            RIS_main_panel_main_left.ButtonSpecs[0].Click += new EventHandler(RIS_main_split_main_doShowHide);
            SMS_panel_right.ButtonSpecs[0].Click += new EventHandler(SMS_panel_right_doShowHide);
            CTK_panel_right.ButtonSpecs[0].Click += new EventHandler(CTK_panel_right_doShowHide);
            CustNews_main_panel_right.ButtonSpecs[0].Click += new EventHandler(CustNews_main_panel_right_doShowHide);
            
            wnd_debug = new debug(this); //create debug window
            wnd_debug.txt_debug.Text = Application.ProductName + " (version " + Application.ProductVersion + ")";
            wnd_debug.Text += " - " + Application.ProductName + " (version " + Application.ProductVersion + ")";
            LogDebug("MainForm ready.", "SYSTEM");
        }

        public void Application_ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            string ErrorLogFile = Application.StartupPath + "\\_error.txt";
            string ErrorDebug = "";
            ErrorDebug += Application.ProductName + " (" + Application.ProductVersion.ToString() + ")";
            ErrorDebug += "\r\n\r\n";
            ErrorDebug += e.Exception.ToString();
            ErrorDebug += "\r\n\r\n------------------------------------------------------------------------------------------------------------------------\r\n\r\n";
            ErrorDebug += wnd_debug.txt_debug.Text;
            StreamWriter LogFile = new StreamWriter(ErrorLogFile, false, System.Text.Encoding.GetEncoding("windows-1250"));
            LogFile.Write(ErrorDebug);
            LogFile.Close();
            Clipboard.SetDataObject(ErrorDebug);
            ErrorForm errFrm = new ErrorForm();
            errFrm.error_text.Text = ErrorDebug;
            errFrm.Show();
        }
        
        
        // Global variables
        private SplashScreen mSPLASH;
        public ComboBox ctk_ulozeny_filtr;
        public CheckedListBox ctk_kat_list;
        public IniFile ini = new IniFile(Application.StartupPath.ToString() + "\\OstroRis.ini");
        public MySQLConnection MYSQL;
        string sql_query, sql_query2;
        MySQLCommand sql_cmd, sql_cmd2;
        MySQLDataReader sql_reader, sql_reader2;
        string sql_db;
        public string sql_db_table;
        public string sql_db_table_filter;
        string sql_text_filtr = ""; // filtr dle textu
        string sql_kat_filtr = ""; // filtr kategorii
        bool sql_filtr_reload_needed = false;
        public XmlNodeList xml_kat_id; // kategorie - IDcka
        public XmlNodeList xml_kat_popis; // kategorie - Popisy
        XmlNodeList xml_kraj_id;
        XmlNodeList xml_kraj_popis;
        int ctk_tab_lastSelected = 0;
        int dg_lastID = 0;
        public string cfg_auDisable;
        // string ctk_last_time = "00:00"; //TODO: remove...
        string ctk_last_id = "0";
        public int ctk_reload_time = 120;
        public bool ShowNewMsgNotify = true;
        int ctk_reload_time_fail = 60;
        int dg_ctk_list_last_row_clicked = 0;
        int[] ctk_list_Readed = new int[5000];
        int ctk_list_Readed_idx = 0;
        public Color[] UIcolors = new Color[8] { Color.Black, Color.White, Color.DarkRed, Color.White, Color.Black, Color.White, Color.Black, Color.Yellow };
        public string ActualProfile = null;
        private object[] config_store = null;
        public string[] ctk_flag_titles;
        private FormWindowState ctk_last_window_state;
        public debug wnd_debug;
        public int SMS_lastMsgID = 0;
        protected bool SMS_CanSendSMS = false;
        protected bool SMS_CanDeleteSMS = false;
        public ComboBox filtry_ctk_kraj;
        private int RIS_main_panel_main_left_widthLeftRight = 0;
        private int SMS_panel_right_left_widthLeftRight = 0;
        private int CTK_panel_right_left_widthLeftRight = 0;
        private int CustNews_panel_right_left_widthLeftRight = 0;
        public string CustNews_LastDate = "20070101";
        public string CustNews_LastTime = "00:00:00";
        public int CustNews_reload_time = 120;
        private string StatusBar_lastText = "";
        private string StatusBar_defaultText = "Připraven";
        int[] CustNews_list_Readed = new int[5000];
        int CustNews_list_Readed_idx = 0;


        // Functions

        void MainFormLoad(object sender, System.EventArgs e)
        {
            string S;
            ctk_tab.Enabled = false;
            ChangeStatusBar("Aplikace startuje...", true);
            
            if (ini.IniReadValue("debug", "showlog") == "True")
            {
                top_menu_debug.Visible = true;
                wnd_debug.Visible = true;
                // load DebugWnd position...
                S = ini.IniReadValue("debug", "width");
                if (S != "") wnd_debug.Width = Convert.ToInt32(S);
                S = ini.IniReadValue("debug", "height");
                if (S != "") wnd_debug.Height = Convert.ToInt32(S);
                S = ini.IniReadValue("debug", "left");
                if (S != "") wnd_debug.Left = Convert.ToInt32(S);
                S = ini.IniReadValue("debug", "top");
                if (S != "") wnd_debug.Top = Convert.ToInt32(S);
            }
            // App INIT
            LogDebug("MainFormLoad()", "SYSTEM");
            
            Application.DoEvents();
            this.TextExtra = "verze: " + Application.ProductVersion.ToString();
            S = ini.IniReadValue("global", "LastVersion");
            if (S != Application.ProductVersion.ToString())
            {
                AutoUpdate_finisher AU_finish = new AutoUpdate_finisher(this, S);
            }

            ini.IniWriteValue("global", "LastVersion", Application.ProductVersion.ToString());
            ini.IniWriteValue("global", "LastRun", DateTime.Now.ToString());

            //Nacti pozici okna...
            S = ini.IniReadValue("window", "state");
            if (S == "Maximized")
            {
                this.WindowState = FormWindowState.Maximized;
            }
            S = ini.IniReadValue("window", "width");
            if (S != "") this.Width = Convert.ToInt32(S);
            S = ini.IniReadValue("window", "height");
            if (S != "") this.Height = Convert.ToInt32(S);
            S = ini.IniReadValue("window", "left");
            if (S != "")
            {
                if ((Convert.ToInt32(S) >= 0) && (Convert.ToInt32(S) + this.Width) < (Screen.PrimaryScreen.WorkingArea.Width))
                {
                    this.Left = Convert.ToInt32(S);
                }
            }
            S = ini.IniReadValue("window", "top");
            if (S != "")
            {
                if ((Convert.ToInt32(S) >= 0) && (Convert.ToInt32(S) + this.Height) < (Screen.PrimaryScreen.WorkingArea.Height))
                {
                    this.Top = Convert.ToInt32(S);
                }
            }

            // Nastav "dg_ctk_list"
            // defualt...
            dg_ctk_list.Columns[0].Visible = false;
            dg_ctk_list.Columns[3].Visible = false;
            dg_ctk_list.Columns[3].Width = 50;
            dg_ctk_list.Columns[4].Width = 350;
            dg_ctk_list.Columns[5].Width = 250;
            dg_ctk_list.Columns[6].Width = 16;
            dg_ctk_list.Columns[6].DisplayIndex = 0;

            ctk_nacti_kategorie();
            ctk_nacti_kraje();
            Application.DoEvents();
            MySQL_connect();
            config_ReadGlobalConfig();

            Application.DoEvents();
            timer_ctkStart.Interval = 500;
            timer_ctkStart.Enabled = true;

            Application.DoEvents();
            ctk_DoAutoUpdate(false);

            if (mSPLASH != null)
            mSPLASH.Close();
        }

        public void ChangeStatusBar(string TextToShow, bool isWorking)
        {
            if (isWorking == true)
            {
                Cursor = Cursors.WaitCursor;
                if (ctk_status_label.Text != StatusBar_defaultText) StatusBar_lastText = ctk_status_label.Text;
                ctk_status_label.Text = TextToShow;
            }
            else
            {
                if (StatusBar_lastText != "")
                {
                    ctk_status_label.Text = StatusBar_lastText;
                    StatusBar_lastText = "";
                    isWorking = true;
                }
                else
                {
                    ctk_status_label.Text = StatusBar_defaultText;
                    Cursor = Cursors.Default;
                }

            }
            LoadingCircle_statusbar.Visible = isWorking;
            LoadingCircle_statusbar.LoadingCircleControl.Active = isWorking;
            Application.DoEvents();
        }

        private void config_ReadGlobalConfig()
        {
            // Read global config from DB and store it to Local INI file.
            LogDebug("Proccessing global config", "G-CONFIG");

            try
            {
                string now_DT = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00");
                string gconfig_query = "SELECT * FROM " + sql_db_table + "_global_cfg WHERE cfg_valid_after < '" + now_DT + "'";
                MySQLCommand gconfig_cmd = new MySQLCommand(gconfig_query, MYSQL);
                MySQLDataReader gconfig_reader = gconfig_cmd.ExecuteReaderEx();
                while (gconfig_reader.Read())
                {
                    if (gconfig_reader["cfg_enc"].ToString() == "1")
                    {
                        ini.IniWriteEncValue(gconfig_reader["cfg_section"].ToString(), gconfig_reader["cfg_key"].ToString(), gconfig_reader["cfg_value"].ToString());
                    }
                    else
                    {
                        ini.IniWriteValue(gconfig_reader["cfg_section"].ToString(), gconfig_reader["cfg_key"].ToString(), gconfig_reader["cfg_value"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "G-CONFIG");
            }
        }

        private void timer_ctkStart_Tick(object sender, EventArgs e)
        {
            timer_ctkStart.Enabled = false;
            Application.DoEvents();
            this.Visible = false;
            CTK_profileShowWnd(true);
        }

        private void ctk_DoAutoUpdate(bool force)
        {
            // AUTO UPDATE (silent)
            bool AUrun = false;
            string cfg_auLastrun = ini.IniReadValue("AutoUpdate", "lastrun");
            if (cfg_auLastrun != "")
            {
                // TODO:  - SET HERE  "5"
                if (DateTime.Compare(DateTime.Today.AddDays(-1), Convert.ToDateTime(cfg_auLastrun)) > 0)
                {
                    AUrun = true;
                }
            }
            else { AUrun = true; }

            if ((AUrun == true) || (force == true))
            {
                LogDebug("Looking for new version", "AU");
                bg_AU.RunWorkerAsync();
                top_menu__update.Enabled = false;
                ini.IniWriteValue("AutoUpdate", "lastrun", System.DateTime.Today.ToShortDateString());
            }
        }

        private void bg_AU_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            SB.BgAutoUpdate.BgAutoUpdate au = new SB.BgAutoUpdate.BgAutoUpdate();
            au.au_url = "http://au.ladasoukup.cz/OstroRis/";
            au.au_ico = Properties.Resources.ico_update;
            au.DoUpdate();
        }

        public void MySQL_connect()
        {
            string sql_server = ini.IniReadValue("sql", "sql_host");
            string sql_user = ini.IniReadValue("sql", "sql_user");
            string sql_password = ini.IniReadEncValue("sql", "sql_pass");
            //string sql_charset = "windows-1250";

            sql_db = ini.IniReadValue("sql", "sql_db");
            sql_db_table = ini.IniReadValue("sql", "sql_db_table");
            sql_db_table_filter = sql_db_table + "_filtry";
            MYSQL = new MySQLConnection(new MySQLConnectionString(sql_server, sql_db, sql_user, sql_password).AsString);

            try
            {
                MYSQL.Open();
                ChangeStatusBar("Připraven", false);
                LogDebug("Connected to MySQL.", "SQL");
            }

            catch (Exception ex)
            {
                LogDebug("Unable to connect with MySQL.", "SQL");
                LogDebug(ex.ToString(), "ERROR");
                //MessageBox.Show("Připojení k MySQL databázi se nezdařilo.\nZkontrolujte nastavení a restartujte aplikaci.\nPokud potíže přetrvají, kontaktujte správce systému.", "Chyba MySQL", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                ChangeStatusBar("Připojení k MySQL databázi se nezdařilo...", false);
            }
        }

        void ctk_nacti_kategorie()
        {
            // XmlTextReader ctk_kat_reader = new XmlTextReader(System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("ctk_kategorie.xml"));
            XmlDocument ctk_kat = new XmlDocument();
            ctk_kat.LoadXml(Properties.Resources.ctk_kategorie);
            //ctk_kat.Load(ctk_kat_reader);
            XmlNode ctk_kat_element = ctk_kat.DocumentElement;
            xml_kat_id = ctk_kat_element.SelectNodes("/ctk_kategorie/kategorie/id/text()");
            xml_kat_popis = ctk_kat_element.SelectNodes("/ctk_kategorie/kategorie/popis/text()");
            for (int loop = 0; loop < xml_kat_id.Count; loop++)
            {
                if (xml_kat_popis[loop].Value != "*")
                {
                    ctk_kat_list.Items.Add(xml_kat_popis[loop].Value);
                }
                else
                {
                    ctk_kat_list.Items.Add(xml_kat_id[loop].Value);
                }
            }
        }

        void ctk_nacti_kraje()
        {
            XmlDocument ctk_kraje = new XmlDocument();
            ctk_kraje.LoadXml(Properties.Resources.ctk_kraje);
            XmlNode ctk_kraje_element = ctk_kraje.DocumentElement;
            xml_kraj_id = ctk_kraje_element.SelectNodes("/ctk_kraje/kraj/id/text()");
            xml_kraj_popis = ctk_kraje_element.SelectNodes("/ctk_kraje/kraj/popis/text()");
            for (int loop = 0; loop < xml_kraj_id.Count; loop++)
            {
                if (xml_kraj_popis[loop].Value != "*")
                {
                    filtry_ctk_kraj.Items.Add(xml_kraj_popis[loop].Value);
                }
                else
                {
                    filtry_ctk_kraj.Items.Add(xml_kraj_id[loop].Value);
                }
            }
            filtry_ctk_kraj.SelectedIndex = 0;
        }

        public void ctk_nacti_filtry()
        {
            LogDebug("Loading predefined filters...", "SQL");
            ctk_ulozeny_filtr.Items.Clear();
            string ctk_sql = "";
            ctk_sql = "SELECT titulek FROM " + sql_db_table_filter + " WHERE ctk_user = '" + ActualProfile + "' ORDER BY titulek ASC";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                LogDebug(ctk_sql, "SQL");
                while (ctk_reader.Read())
                {
                    ctk_ulozeny_filtr.Items.Add(ctk_reader["titulek"].ToString());
                }
            }
            catch (Exception e)
            {
                LogDebug(e.ToString(), "ERROR");
            }
        }

        void filtr_set_icon(bool showicon)
        {
            ctk_zrus_filtry_icon.Visible = showicon;
            ctk_zrus_filtry.Enabled = showicon;
            /* */
            if (showicon == false)
            {
                if (ctk_calendar.SelectionEnd == ctk_calendar.TodayDate)
                {
                    ctk_calendar.SetDate(ctk_calendar.SelectionEnd);
                }
                else
                {
                    ctk_calendar.SetDate(ctk_calendar.SelectionStart);
                }
            }
        }

        void ctk_tab_HighLight_active()
        {
            //ctk_tabDatum.Text = "Datum";
            if (sql_kat_filtr != "")
            {
                ctk_tabKat.ForeColor = Color.Red;
                ctk_kat_list.ForeColor = Color.Black;
                ctk_kat_list_spojeni.ForeColor = Color.Black;
            }
            else
            {
                ctk_tabKat.ForeColor = Color.Black;
            }
            if (sql_text_filtr != "")
            {
                ctk_tabFiltry.ForeColor = Color.Red;
                lbl_ctk_filtry_nadpis.ForeColor = Color.Black;
                filtry_ctk_nadpis.ForeColor = Color.Black;
                lbl_ctk_filtry_klicovaSlova.ForeColor = Color.Black;
                filtry_ctk_klicovaSlova.ForeColor = Color.Black;
                lbl_ctk_filtry_text.ForeColor = Color.Black;
                filtry_ctk_text.ForeColor = Color.Black;
                lbl_ctk_filtry_kraj.ForeColor = Color.Black;
                filtry_ctk_kraj.ForeColor = Color.Black;
                ctk_filtry_spojeni.ForeColor = Color.Black;
            }
            else
            {
                ctk_tabFiltry.ForeColor = Color.Black;
            }

            ctk_tab.Refresh();
            Application.DoEvents();
        }

        void ctk_ulozeny_filtr_set_click(object sender, System.EventArgs e)
        {
            LogDebug("ctk_ulozeny_filtr_set_click()", "USER");
            string ctk_sql = "";
            ctk_sql = "SELECT * FROM " + sql_db_table_filter + " WHERE titulek='" + ctk_ulozeny_filtr.Text + "' AND ctk_user = '" + ActualProfile + "' LIMIT 1";
            try
            {
                MySQLCommand ctk_cmd = new MySQLCommand(ctk_sql, MYSQL);
                MySQLDataReader ctk_reader = ctk_cmd.ExecuteReaderEx();
                LogDebug(ctk_sql, "SQL");
                ctk_reader.Read();
                // Filtr - text
                sql_filtr_reload_needed = false;
                if (ctk_reader["f_filtr_and"].ToString() == "1")
                {
                    ctk_filtry_spojeni.Checked = true;
                }
                else
                {
                    ctk_filtry_spojeni.Checked = false;
                }
                filtry_ctk_nadpis.Text = ctk_reader["f_titulek"].ToString();
                filtry_ctk_klicovaSlova.Text = ctk_reader["f_klicova_slova"].ToString();
                filtry_ctk_text.Text = ctk_reader["f_text"].ToString();
                if (sql_filtr_reload_needed == true)
                {
                    btn_ctk_filtry_nastav_click(sender, e);
                    sql_filtr_reload_needed = false;
                    filtr_set_icon(true);
                    Application.DoEvents();
                }
                // Filtr - kategorie
                sql_filtr_reload_needed = true; // Vzdy TRUE!!!
                for (int loop = 0; loop < ctk_kat_list.Items.Count; loop++)
                {
                    ctk_kat_list.SetItemCheckState(loop, CheckState.Unchecked);
                }
                if (ctk_reader["f_kategorie"].ToString() != "")
                {
                    if (ctk_reader["f_kategorie_and"].ToString() == "1")
                    {
                        ctk_kat_list_spojeni.Checked = true;
                    }
                    else
                    {
                        ctk_kat_list_spojeni.Checked = false;
                    }
                    string[] f_kat = new string[100];
                    f_kat = ctk_reader["f_kategorie"].ToString().Split('*');
                    for (int extloop = 0; extloop < f_kat.Length; extloop++)
                    {
                        for (int loop = 0; loop < xml_kat_id.Count; loop++)
                        {
                            if (xml_kat_id[loop].Value == f_kat[extloop])
                            {
                                ctk_kat_list.SetItemCheckState(loop, CheckState.Checked);
                            }
                        }
                    }
                }
                // Filtr - KRAJE
                if (ctk_reader["f_kraj"].ToString() != "")
                {
                    filtry_ctk_kraj.Text = ctk_reader["f_kraj"].ToString();
                }
                else
                {
                    filtry_ctk_kraj.SelectedIndex = 0;
                }

                if (sql_filtr_reload_needed == true)
                {
                    ctk_kat_nastav_click(sender, e);
                    sql_filtr_reload_needed = false;
                    filtr_set_icon(true);
                    Application.DoEvents();
                }
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "ERROR");
            }
        }

        public void ShowAppNotify(string msgTitle, string msgText, int msgTime)
        {
            ShowAppNotify(msgTitle + "\r\n" + msgText, msgTime, ToolTipIcon.Info);
        }
        public void ShowAppNotify(string msgText, int msgTime)
        {
            ShowAppNotify(msgText, msgTime, ToolTipIcon.Info);
        }
        public void ShowAppNotify(string msgText, int msgTime, ToolTipIcon msgIcon)
        {
            if (msgTime > 999) msgTime = msgTime / 1000;
            appNotify.BalloonTipTitle = "OstroRIS";
            appNotify.BalloonTipText = msgText;
            appNotify.BalloonTipIcon = msgIcon;
            appNotify.ShowBalloonTip(msgTime);
        }

        public void ctk_list_reload()
        {
            LogDebug("ctk_list_reload(); -> ctk_list_reload(\"reload\");", "SYSTEM");
            ctk_list_reload("reload");
        }

        public void ctk_list_reload(string action)
        {
            LogDebug("ctk_list_reload(\"" + action + "\");", "SYSTEM");
            ChangeStatusBar("Načítám seznam zpráv...", true);
            dg_ctk_list.Enabled = false;

            string[] new_row = null;
            string S = null;
            string Qdate_year = ctk_calendar.SelectionStart.Year.ToString("####");
            string Qdate_month = ctk_calendar.SelectionStart.Month.ToString("0#");
            string Qdate_day = ctk_calendar.SelectionStart.Day.ToString("0#");
            string Qdate_year2 = ctk_calendar.SelectionEnd.Year.ToString("####");
            string Qdate_month2 = ctk_calendar.SelectionEnd.Month.ToString("0#");
            string Qdate_day2 = ctk_calendar.SelectionEnd.Day.ToString("0#");
            string Qdate = "";
            string ctk_list_sql_filtr = "";
            string ctk_list_sql = "";
            string ctk_sql_filtr = "";
            bool MsgAdded = false;
            ctk_tab.Enabled = false;
            Application.DoEvents();

            if ((Qdate_day != Qdate_day2) || (Qdate_month != Qdate_month2))
            {
                Qdate = "(datum BETWEEN '" + Qdate_year + Qdate_month + Qdate_day + "' AND '" + Qdate_year2 + Qdate_month2 + Qdate_day2 + "')";
            }
            else
            {
                Qdate = "datum='" + Qdate_year + Qdate_month + Qdate_day + "'";
            }
            if (sql_kat_filtr != "")
            {
                ctk_list_sql_filtr = " AND (" + sql_kat_filtr + ")";
            }
            if (sql_text_filtr != "")
            {
                ctk_sql_filtr = " AND (" + sql_text_filtr + ")";
            }

            ctk_list_sql = "SELECT id, datum, cas, kategorie, titulek, klicova_slova FROM " + sql_db_table + " WHERE " + Qdate + ctk_list_sql_filtr + ctk_sql_filtr + " ORDER BY datum, cas ASC";
            if (action == "add")
            {
                if (ctk_calendar.SelectionStart.Date == ctk_calendar.TodayDate.Date)
                {
                    LogDebug("Only add new - last ID: " + ctk_last_id + ".", "SYSTEM");
                    ctk_list_sql = "SELECT id, datum, cas, kategorie, titulek, klicova_slova FROM " + sql_db_table + " WHERE " + Qdate + ctk_list_sql_filtr + ctk_sql_filtr + " AND id>'" + ctk_last_id + "' ORDER BY id ASC";
                }
                else
                {
                    ctk_last_id = "0";
                    dg_ctk_list.Rows.Clear();
                }

            }
            else
            {
                dg_ctk_list.Rows.Clear();
            }

            Application.DoEvents();
            if (ctk_list_sql != null)
            {
                try
                {
                    LogDebug(ctk_list_sql, "SQL");
                    MySQLCommand ctk_list_cmd = new MySQLCommand(ctk_list_sql, MYSQL);
                    MySQLDataReader ctk_list_reader = ctk_list_cmd.ExecuteReaderEx();
                    while (ctk_list_reader.Read())
                    {
                        S = ctk_list_reader["datum"].ToString().Substring(6, 2) + "." + ctk_list_reader["datum"].ToString().Substring(4, 2) + "." + ctk_list_reader["datum"].ToString().Substring(0, 4);
                        new_row = new string[] { ctk_list_reader["id"].ToString(), S, ctk_list_reader["cas"].ToString(), ctk_list_reader["kategorie"].ToString(), ctk_list_reader["titulek"].ToString(), ctk_list_reader["klicova_slova"].ToString() };
                        dg_ctk_list.Rows.Insert(0, new_row);
                        ctk_list_MarkUnReaded(0, ctk_list_reader["id"].ToString());
                        if (Convert.ToInt32(ctk_list_reader["id"].ToString()) > Convert.ToInt32(ctk_last_id))
                        {
                            ctk_last_id = ctk_list_reader["id"].ToString();
                        }
                        MsgAdded = true;
                    }
                }
                catch (Exception e)
                {
                    LogDebug(e.ToString(), "ERROR");
                    LogDebug("error - update in 60 secondes...", "SQL");
                    ChangeStatusBar("Není navázáno připojení k MySQL databázi.", false);
                    ctk_list_timer.Interval = (ctk_reload_time_fail * 1000);
                    ctk_list_timer.Enabled = true;
                    MYSQL.Close();
                    Application.DoEvents();
                    MySQL_connect();
                }
            }

            if ((MsgAdded) && (action == "add") && (ShowNewMsgNotify == true))
            {
                ShowAppNotify("Ze serveru byly staženy nové zprávy.", 10);
            }
            if (action != "show_flag")
            {
                if (ctk_calendar.SelectionStart.Date == ctk_calendar.TodayDate.Date)
                {
                    LogDebug("Start Timer - " + ctk_reload_time.ToString() + " seconds...", "SYSTEM");
                    ctk_list_timer.Interval = (ctk_reload_time * 1000);
                    ctk_list_timer.Enabled = true;
                }
            }
            else
            {
                LogDebug("Stop Timer", "SYSTEM");
                ctk_list_timer.Enabled = false;
            }

            try
            {
                if (dg_ctk_list.Rows.Count < 1)
                {
                    ctk_detail.Text = "";
                }
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "ERROR");
            }

            ctk_tab.Enabled = true;
            if (dg_ctk_list.Rows.Count > 0)
            {
                dg_lastID = dg_ctk_list.CurrentRow.Index;
            }
            else
            {
                dg_lastID = 0;
            }

            dg_ctk_list.Enabled = true;
            dg_ctk_list.Update();
            Application.DoEvents();
            ChangeStatusBar("Připraven", false);
            ctk_load_detail();

        }

        void ctk_load_detail()
        {
            string S = null;
            LogDebug("ctk_load_detail();", "SYSTEM");
            ChangeStatusBar("Načítám zprávu...", true);
            ctk_list_MarkReaded();

            if (dg_ctk_list.CurrentRow != null)
            {
                int dg_currID = dg_ctk_list.CurrentRow.Index;
                dg_lastID = dg_currID;
                if (dg_ctk_list.Rows[dg_currID].Cells[0].Value != null)
                {
                    string ctkDetailID = dg_ctk_list.Rows[dg_currID].Cells[0].Value.ToString();
                    string ctk_detail_sql = "SELECT * FROM " + sql_db_table + " WHERE id = '" + ctkDetailID + "' LIMIT 1";
                    try
                    {
                        MySQLCommand ctk_detail_cmd = new MySQLCommand(ctk_detail_sql, MYSQL);
                        MySQLDataReader ctk_detail_reader = ctk_detail_cmd.ExecuteReaderEx();
                        LogDebug(ctk_detail_sql, "SQL");
                        try
                        {
                            ctk_detail_reader.Read();
                            S = ctk_detail_reader.GetString(6) + "\r\n";
                            S += ctk_detail_reader.GetString(4) + "\r\n\r\n";
                            S += ctk_detail_reader.GetString(5);
                            string[] Sa = ctk_detail_reader.GetString(5).Split(new char[] { '\r' });
                            Sa = ctk_detail_reader.GetString(5).Split(new char[] { ' ', '\r' });
                            ctk_detail_reader.Close();
                        }
                        catch
                        {
                            LogDebug("ctk_load_detail() - Empty result...", "SYSTEM");
                        }
                        ctk_detail.Text = S.Replace("<br />", "\r");
                        ChangeStatusBar("Připraven", false);
                    }
                    catch (Exception e)
                    {
                        LogDebug(e.ToString(), "ERROR");
                        ChangeStatusBar("Není navázáno připojení k MySQL databázi.", false);
                        MYSQL.Close();
                        Application.DoEvents();
                        MySQL_connect();
                    }
                }
            }
            ctk_last_window_state = this.WindowState; //save window state...
        }

        private void ctk_list_selectionChanged(object sender, EventArgs e)
        {
            LogDebug("ctk_list_selectionChanged", "SYSTEM");
            ctk_load_detail();
        }

        private void ctk_list_MarkReaded()
        {
            try
            {
                dg_ctk_list.Rows[dg_ctk_list.CurrentRow.Index].DefaultCellStyle.ForeColor = UIcolors[0];
                dg_ctk_list.Rows[dg_ctk_list.CurrentRow.Index].DefaultCellStyle.BackColor = UIcolors[1];

                int IdxVal = Convert.ToInt32(dg_ctk_list.Rows[dg_ctk_list.CurrentRow.Index].Cells[0].Value.ToString());
                object obj1 = IdxVal;
                int IntRet = Array.IndexOf(ctk_list_Readed, obj1);
                if (IntRet < 0)
                {
                    ctk_list_Readed[ctk_list_Readed_idx] = IdxVal;
                    ctk_list_Readed_idx++;
                    if (ctk_list_Readed_idx >= ctk_list_Readed.Length) ctk_list_Readed_idx = 0;
                }

            }
            catch
            {
            }
        }

        private void ctk_list_MarkUnReaded(int RowId, string IdxVal)
        {
            object obj1 = Convert.ToInt32(IdxVal);
            int IntRet = Array.IndexOf(ctk_list_Readed, obj1);
            if (IntRet < 0)
            {
                dg_ctk_list.Rows[RowId].DefaultCellStyle.ForeColor = UIcolors[2];
                dg_ctk_list.Rows[RowId].DefaultCellStyle.BackColor = UIcolors[3];
            }
            else
            {
                dg_ctk_list.Rows[RowId].DefaultCellStyle.ForeColor = UIcolors[0];
                dg_ctk_list.Rows[RowId].DefaultCellStyle.BackColor = UIcolors[1];
            }
        }

        void ctk_calendar_date_selected(object sender, System.Windows.Forms.DateRangeEventArgs e)
        {
            LogDebug("ctk_calendar_date_selected()", "USER");
            LogDebug("Stop Timer", "SYSTEM");
            ctk_list_timer.Enabled = false;
            ctk_list_reload();
        }

        void mmenu_cfgsql_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_cfgsql_click()", "USER");
            cfg_OpenMySQL_config();
        }

        public void cfg_OpenMySQL_config()
        {
            sqlcfg wnd_sqlcfg = new sqlcfg(this);
            wnd_sqlcfg.Show();
        }

        bool ctk_quit()
        {
            bool HaltQuit = false;
            //DialogResult result = MessageBox.Show("Opravdu si přejete ukončit OstroRIS?", "Ukončit OstroRIS?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);
            DialogResult result = cTaskDialog.ShowTaskDialogBox(this.Text, "Ukončit OstroRIS?", "Opravdu si přejete ukončit OstroRIS?", "", "", "", "", "", eTaskDialogButtons.YesNo, eSysIcons.Question, eSysIcons.Question);
            switch (result)
            {
                case DialogResult.Yes:
                    LogDebug("QUIT", "SYSTEM");
                    // Ulozit pozici okna...
                    if (this.WindowState.ToString() != "Maximized")
                    {
                        ini.IniWriteValue("window", "width", this.Width.ToString());
                        ini.IniWriteValue("window", "height", this.Height.ToString());
                        if (this.Left >= 0) ini.IniWriteValue("window", "left", this.Left.ToString());
                        if (this.Top >= 0) ini.IniWriteValue("window", "top", this.Top.ToString());
                    }
                    ini.IniWriteValue("window", "state", this.WindowState.ToString());

                    CTK_profileSave();
                    ChangeStatusBar("Ukončování aplikace...", true);
                    // Exit
                    Application.DoEvents();
                    MYSQL.Close();
                    Application.DoEvents();
                    MYSQL.Dispose();
                    appNotify.Visible = false;
                    appNotify.Dispose();
                    Application.Exit();

                    break;
                case DialogResult.No:
                    HaltQuit = true;
                    break;
            }
            return HaltQuit;
        }

        void mmenu_konec_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_konec_click()", "USER");
            ctk_quit();
        }

        void MainForm_closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            LogDebug("MainForm_closing()", "SYSTEM");
            if (ctk_quit()) e.Cancel = true; // Vraci se prikaz pro zastaveni ukonceni...
        }

        void mmenu_about_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_about_click()", "USER");
            about wnd_about = new about();
            wnd_about.Show();
        }

        void Ctk_list_timerTick(object sender, System.EventArgs e)
        {
            LogDebug("Ctk_list_timerTick()", "Timer");
            ctk_list_timer.Enabled = false;
            ctk_list_reload("add");
        }

        void mmenu_debug_show_click(object sender, System.EventArgs e)
        {
            wnd_debug.Visible = true;
        }

        void mmenu_debug_hide_click(object sender, System.EventArgs e)
        {
            wnd_debug.Visible = false;
        }

        void mmenu_update_click(object sender, System.EventArgs e)
        {
            LogDebug("mmenu_update_click()", "USER");
            ctk_DoAutoUpdate(true);
        }

        void ctk_kat_nastav_click(object sender, System.EventArgs e)
        {
            LogDebug("ctk_kat_nastav_click()", "USER");
            string sql_kat_filtr_spojeni = "OR";
            if (ctk_kat_list_spojeni.Checked == true)
            {
                sql_kat_filtr_spojeni = "AND";
            }
            sql_kat_filtr = "";
            for (int loop = 0; loop < ctk_kat_list.Items.Count; loop++)
            {
                if (ctk_kat_list.GetItemCheckState(loop) == CheckState.Checked)
                {
                    if (sql_kat_filtr != "")
                    {
                        sql_kat_filtr += " " + sql_kat_filtr_spojeni + " ";
                    }

                    sql_kat_filtr += "kategorie LIKE '%" + xml_kat_id.Item(loop).Value + "%'";

                }
            }
            LogDebug("filtr: " + sql_kat_filtr, "SQL");
            ctk_list_reload();
            if ((sql_kat_filtr == "") && (sql_text_filtr == ""))
            {
                if (ctk_calendar.SelectionStart.Date == ctk_calendar.TodayDate.Date)
                {
                    ctk_list_timer.Enabled = true;
                    ctk_list_timer.Interval = ctk_reload_time * 1000;
                    LogDebug("Start Timer - " + ctk_reload_time.ToString() + " seconds...", "SYSTEM");
                }
                filtr_set_icon(false);
            }
            else
            {
                //ctk_list_timer.Enabled = false;
                //LogDebug("Stop Timer", "SYSTEM");
                // Two lines above are deprecated.

                filtr_set_icon(true);
            }
            ctk_tab_HighLight_active();
        }

        void btn_ctk_filtry_nastav_click(object sender, System.EventArgs e)
        {
            LogDebug("btn_ctk_filtry_nastav_click()", "USER");
            string sql_filtr_spojeni = "OR";
            sql_text_filtr = "";
            if (ctk_filtry_spojeni.Checked == true)
            {
                sql_filtr_spojeni = "AND";
            }

            if (filtry_ctk_nadpis.Text != "")
            {
                sql_text_filtr = "titulek LIKE '%" + filtry_ctk_nadpis.Text + "%'";
            }

            if (filtry_ctk_klicovaSlova.Text != "")
            {
                if (sql_text_filtr != "")
                {
                    sql_text_filtr += " " + sql_filtr_spojeni + " ";
                }
                sql_text_filtr += "klicova_slova LIKE '%" + filtry_ctk_klicovaSlova.Text + "%'";
            }

            if (filtry_ctk_text.Text != "")
            {
                if (sql_text_filtr != "")
                {
                    sql_text_filtr += " " + sql_filtr_spojeni + " ";
                }
                sql_text_filtr += "text LIKE '%" + filtry_ctk_text.Text + "%'";
            }

            string kraj_filtr = xml_kraj_id.Item(filtry_ctk_kraj.SelectedIndex).Value;
            if (kraj_filtr != "ALL")
            {
                if (sql_text_filtr != "")
                {
                    sql_text_filtr += " AND ";
                }
                sql_text_filtr += "kategorie LIKE '%" + kraj_filtr + "%'";
            }

            LogDebug("filtr: " + sql_text_filtr, "SQL");
            ctk_list_reload();
            if ((sql_kat_filtr == "") && (sql_text_filtr == ""))
            {
                if (ctk_calendar.SelectionStart.Date == ctk_calendar.TodayDate.Date)
                {
                    ctk_list_timer.Enabled = true;
                    ctk_list_timer.Interval = ctk_reload_time * 1000;
                    LogDebug("Start Timer - " + ctk_reload_time.ToString() + " seconds...", "SYSTEM");
                }
                filtr_set_icon(false);
            }
            else
            {
                //ctk_list_timer.Enabled = false;
                //LogDebug("Stop Timer", "SYSTEM");
                // Two lines above are deprecated.

                filtr_set_icon(true);
            }
            ctk_tab_HighLight_active();
        }

        void ctk_zrus_filtry_click(object sender, System.EventArgs e)
        {
            LogDebug("ctk_zrus_filtry_click()", "USER");
            for (int loop = 0; loop < ctk_kat_list.Items.Count; loop++)
            {
                ctk_kat_list.SetItemCheckState(loop, CheckState.Unchecked);
            }
            filtry_ctk_nadpis.Text = "";
            filtry_ctk_klicovaSlova.Text = "";
            filtry_ctk_text.Text = "";
            filtry_ctk_kraj.SelectedIndex = 0;
            sql_text_filtr = "";
            sql_kat_filtr = "";
            ctk_kat_list_spojeni.Checked = false;
            ctk_filtry_spojeni.Checked = true;

            filtr_set_icon(false);
            ctk_ulozeny_filtr.SelectedIndex = -1;
            ctk_list_reload();
            if (ctk_calendar.SelectionStart.Date == ctk_calendar.TodayDate.Date)
            {
                ctk_list_timer.Enabled = true;
                ctk_list_timer.Interval = ctk_reload_time * 1000;
                LogDebug("Start Timer - " + ctk_reload_time.ToString() + " seconds...", "SYSTEM");
            }
            ctk_tab_HighLight_active();
        }

        void sql_filtr_text_changed(object sender, System.EventArgs e)
        {
            if (sql_filtr_reload_needed == false)
            {
                LogDebug("sql_filtr_text_changed()", "SYSTEM");
            }
            sql_filtr_reload_needed = true;
        }

        private void sql_filtr_kraj_changed(object sender, EventArgs e)
        {
            if (sql_filtr_reload_needed == false)
            {
                LogDebug("sql_filtr_kraj_changed()", "SYSTEM");
            }
            sql_filtr_reload_needed = true;
        }

        void ctk_tab_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (sql_filtr_reload_needed == true)
            {
                switch (ctk_tab_lastSelected)
                {
                    case 1:
                        // Filtr kategorii
                        LogDebug("ctk_tab_TabIndexChanged() -> ctk_kat_nastav_click()", "SYSTEM");
                        ctk_kat_nastav_click(sender, e);
                        break;
                    case 2:
                        // Filtr slov
                        LogDebug("ctk_tab_TabIndexChanged() -> btn_ctk_filtry_nastav_click()", "SYSTEM");
                        btn_ctk_filtry_nastav_click(sender, e);
                        break;
                }
            }
            sql_filtr_reload_needed = false;
            ctk_tab_lastSelected = ctk_tab.SelectedIndex;
            ctk_tab_HighLight_active();
        }

        private void ctk_list_Sorted(object sender, EventArgs e)
        {
            LogDebug("ctk_list_Sorted()", "SYSTEM");
            if (dg_ctk_list.Rows.Count > 0)
            {
                dg_lastID = dg_ctk_list.CurrentRow.Index;
            }
            else
            {
                dg_lastID = 0;
            }
        }

        void ctk_status_dClick(object sender, System.EventArgs e)
        {
            top_menu_debug.Visible = true;
        }

        void mmenu_cfg_ctk_Click(object sender, System.EventArgs e)
        {
            cfg_ctk wnd_cfg = new cfg_ctk(this);
            wnd_cfg.Show();
        }

        void mmenu_debug_error_click(object sender, System.EventArgs e)
        {
            throw new InvalidOperationException("Invalid operation.");
        }

        private void dg_ctk_list_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            dg_ctk_list_last_row_clicked = e.RowIndex;
        }

        private void mmenu_cfg_color_font_Click(object sender, EventArgs e)
        {
            cfg_color_font wnd_color = new cfg_color_font(this);
            wnd_color.Show();
        }

        private void mmneu_profile_Click(object sender, EventArgs e)
        {
            LogDebug("mmneu_profile_Click()", "USER");
            CTK_profileSave();
            CTK_profileShowWnd();
        }

        private void mmenu_cfg_filtry_Click(object sender, EventArgs e)
        {
            LogDebug("mmenu_cfg_filtry_Click", "USER");
            frm_cfg_filtry wnd = new frm_cfg_filtry(this);
            wnd.Show();
        }


        private void MainForm_Resize(object sender, EventArgs e)
        {
            /*
             * DEAD CODE
            string ResizeLog;
            ResizeLog = "\r\nCTK wnd: " + this.Width.ToString() + "x" + this.Height.ToString();
            ResizeLog += "\r\ndg_ctk_list: " + dg_ctk_list.Width.ToString() + "x" + dg_ctk_list.Height.ToString();
            ResizeLog += "\r\nCTK_detail: " + ctk_detail.Width.ToString() + "x" + ctk_detail.Height.ToString();
            LogDebug(ResizeLog, "APPLICATION RESIZE");
            */
        }
                
        private void SBnews_panel_Selecting(object sender, TabControlCancelEventArgs e)
        {
            // TODO: SOME CODE HERE...
            /*
            if (e.TabPage.Name == "SBnews_tab_ris")
            {
                e.Cancel = true;
            }
            */
        }

        private void ShowHideCSVexport(bool visible)
        {
            top_menu_editace__copyCSV.Enabled = visible;
            top_menu_soubor__saveCSV.Enabled = visible;

            mmenu_ctk_detail_kopirovat_csv.Enabled = visible;
            mmenu_ctk_detail_ulozit_csv.Enabled = visible;
        }

        private void SBnews_panel_Selected(object sender, TabControlEventArgs e)
        {
            ShowHideCSVexport(false);
            if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ctk")
            {
                // FIX for the "disabled scrollbar" bug
                CTK_split_main.SplitterDistance += 1;
                dg_ctk_list.Refresh();
                CTK_split_main.SplitterDistance -= 1;
                dg_ctk_list.Focus();
            }

            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_CustNews")
            {
                // FIX for the "disabled scrollbar" bug
                CustNews_split_dg_detail.SplitterDistance += 1;
                dg_CustNews_list.Refresh();
                CustNews_split_dg_detail.SplitterDistance -= 1;
                dg_CustNews_list.Focus();
                CustNews_init();
            }
            
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_ris")
            {
                // FIX for the "disabled scrollbar" bug
                RIS_split_dg_detail.SplitterDistance += 1;
                dg_RIS_list.Refresh();
                RIS_split_dg_detail.SplitterDistance -= 1;
                dg_RIS_list.Focus();
                RIS_init();
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_sms")
            {
                // FIX for the "disabled scrollbar" bug
                SMS_split1.SplitterDistance += 1;
                dg_SMS.Refresh();
                SMS_split1.SplitterDistance -= 1;
                dg_SMS.Focus();
                SMS_init();
                ShowHideCSVexport(true);
            }
            else if (SBnews_panel.SelectedTab.Name == "SBnews_tab_scratch")
            {
                
            }
        }

        private void appNotify_BalloonTipClicked(object sender, EventArgs e)
        {
            ctk_restore_application();
        }

        private void appNotify_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ctk_restore_application();
        }

        private void ctk_restore_application()
        {
            try
            {
                if ((this.Visible == true) && (this.WindowState == FormWindowState.Minimized))
                {
                    this.WindowState = ctk_last_window_state;
                    LogDebug("restoring window state", "USER");
                }
            }
            catch { LogDebug("can't restore aplication state!", "ERROR"); }
            this.Activate();
        }

        public void SMS_init()
        {
            LogDebug("SMS_init()", "SYSTEM");
            SMS_cleanup();
            // Load SMS gates and Folders...
            sql_query = "SELECT sms_gate FROM " + sql_db_table + "_sms_rights WHERE ctk_user = '" + ActualProfile + "'";
            LogDebug(sql_query, "SQL");
            try
            {
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    sql_query2 = "SELECT id, sms_folder FROM " + sql_db_table + "_sms_folders WHERE sms_gate = '" + sql_reader["sms_gate"] + "' ORDER BY sms_order ASC";
                    LogDebug(sql_query2, "SQL");
                    SMS_folders.Nodes.Add(sql_reader["sms_gate"].ToString(), sql_reader["sms_gate"].ToString());
                    sql_cmd2 = new MySQLCommand(sql_query2, MYSQL);
                    sql_reader2 = sql_cmd2.ExecuteReaderEx();
                    while (sql_reader2.Read())
                    {
                        SMS_folders.Nodes[sql_reader["sms_gate"].ToString()].Nodes.Add(sql_reader2["id"].ToString(), sql_reader2["sms_folder"].ToString());
                    }
                    sql_reader2.Close();
                }
                sql_reader.Close();
            }
            catch { LogDebug("load SMS gates and FOLDERs FAILED", "ERROR"); }
            try
            {
                if (SMS_folders.Nodes.Count > 0)
                {
                    SMS_folders.ExpandAll();
                    SMS_folders.SelectedNode = SMS_folders.Nodes[0];
                }
                SMS_folders.Refresh();
            }
            catch { }
            SMS_reload_list(true);
        }

        private void SMS_cleanup()
        {
            LogDebug("SMS_cleanup()", "SYSTEM");
            dg_SMS.Rows.Clear();
            SMS_calendar.SetDate(DateTime.Now);
            SMS_detail.Text = "";
            SMS_folders.Nodes.Clear();
            SMS_CanDeleteSMS = false;
            SMS_CanSendSMS = false;
            btn_SMS_new.Enabled = SMS_CanSendSMS;
            btn_SMS_reply.Enabled = SMS_CanSendSMS;
        }


        public void SMS_reload_list()
        {
            SMS_reload_list(true);
        }
        public void SMS_reload_list(bool FullReload)
        {
            string DateFrom = SMS_calendar.SelectionStart.Year.ToString("0000") + SMS_calendar.SelectionStart.Month.ToString("00") + SMS_calendar.SelectionStart.Day.ToString("00");
            string DateTo = SMS_calendar.SelectionEnd.Year.ToString("0000") + SMS_calendar.SelectionEnd.Month.ToString("00") + SMS_calendar.SelectionEnd.Day.ToString("00");
            string smsgate = "";
            string smsfolder = "";
            bool MsgAdded = false;

            LogDebug("SMS_reload_list(" + FullReload.ToString() + ")", "SYSTEM");
            ChangeStatusBar("Načítám data...", true);
            SMS_timer.Enabled = false;
            try
            {
                if (SMS_folders.SelectedNode != null)
                {
                    if (SMS_folders.SelectedNode.Parent == null)
                    {
                        smsgate = SMS_folders.SelectedNode.Name;
                        smsfolder = "0";
                    }
                    else
                    {
                        smsgate = SMS_folders.SelectedNode.Parent.Name;
                        smsfolder = SMS_folders.SelectedNode.Name;
                    }

                    if (FullReload == true)
                    {
                        sql_query = "SELECT * FROM " + sql_db_table + "_sms WHERE (sms_date BETWEEN '" + DateFrom + "' AND '" + DateTo + "') AND sms_gate='" + smsgate + "' AND sms_folder='" + smsfolder + "' ORDER BY sms_date ASC, sms_time ASC";
                        SMS_lastMsgID = 0;
                        dg_SMS.Rows.Clear();
                    }
                    else
                    {
                        sql_query = "SELECT * FROM " + sql_db_table + "_sms WHERE id > '" + SMS_lastMsgID + "' AND (sms_date BETWEEN '" + DateFrom + "' AND '" + DateTo + "') AND sms_gate='" + smsgate + "' AND sms_folder='" + smsfolder + "' ORDER BY sms_date ASC, sms_time ASC";
                    }
                    LogDebug(sql_query, "SQL");
                    sql_cmd = new MySQLCommand(sql_query, MYSQL);
                    sql_reader = sql_cmd.ExecuteReaderEx();
                    while (sql_reader.Read())
                    {
                        int SMS_ID = sql_reader.GetInt32(0); //id
                        string SMS_TEXT = sql_reader.GetString(6); //sms_text
                        string[] NewSMS = new string[] {
                            SMS_ID.ToString(),
                            sql_reader["sms_gate"].ToString(),
                            sql_reader["sms_from"].ToString(),
                            sql_reader["sms_date"].ToString().Substring(6,2) + "." +sql_reader["sms_date"].ToString().Substring(4,2) + "."+sql_reader["sms_date"].ToString().Substring(0,4) ,
                            sql_reader["sms_time"].ToString(),
                            sql_reader["sms_folder"].ToString(),
                            SMS_TEXT
                        };
                        dg_SMS.Rows.Insert(0, NewSMS);
                        if (SMS_lastMsgID < SMS_ID) SMS_lastMsgID = SMS_ID;
                        MsgAdded = true;
                    }
                    sql_reader.Close();
                    // if ((MsgAdded) && (FullReload == false) && (ShowNewMsgNotify == true))
                    if ((MsgAdded) && (FullReload == false))
                    {
                        ShowAppNotify("Nová SMS zpráva.", 10);
                    }
                }
            }
            catch (Exception ex) { LogDebug("SMS_reload_list - " + ex.ToString(), "ERROR"); }
            if (SMS_calendar.SelectionEnd.ToLongDateString() == DateTime.Now.ToLongDateString())
            {
                LogDebug("Start timer...", "SMS");
                SMS_timer.Enabled = true;
            }
            // Update SMS rights
            try
            {
                sql_query2 = "SELECT sms_delete, sms_send FROM " + sql_db_table + "_sms_rights WHERE ctk_user = '" + ActualProfile + "' AND sms_gate = '" + smsgate + "'";
                LogDebug(sql_query2, "SQL");
                sql_cmd2 = new MySQLCommand(sql_query2, MYSQL);
                sql_reader2 = sql_cmd2.ExecuteReaderEx();
                sql_reader2.Read();
                if (sql_reader2["sms_delete"].ToString() == "1") { SMS_CanDeleteSMS = true; } else { SMS_CanDeleteSMS = false; }
                if (sql_reader2["sms_send"].ToString() == "1") { SMS_CanSendSMS = true; } else { SMS_CanSendSMS = false; }
            }
            catch { LogDebug("nepodařilo se načíst uživatelská práva na složku.", "SMS ERROR"); }
            btn_SMS_new.Enabled = SMS_CanSendSMS;
            btn_SMS_reply.Enabled = SMS_CanSendSMS;
            ChangeStatusBar("Připraven", false);
        }

        public void SMS_reload_detail()
        {
            LogDebug("SMS_reload_detail()", "SYSTEM");
            ChangeStatusBar("Načítám data...", true);
            int dg_currID = dg_SMS.CurrentRow.Index;
            if (dg_SMS.Rows[dg_currID].Cells[3].Value != null)
            {
                SMS_detail.Text = "Datum a čas: ";
                SMS_detail.Text += dg_SMS.Rows[dg_currID].Cells[3].Value.ToString();
                SMS_detail.Text += " ";
                SMS_detail.Text += dg_SMS.Rows[dg_currID].Cells[4].Value.ToString();
                SMS_detail.Text += "\r\nOdesílatel: ";
                SMS_detail.Text += dg_SMS.Rows[dg_currID].Cells[2].Value.ToString();
                SMS_detail.Text += "\r\n\r\n";
                SMS_detail.Text += dg_SMS.Rows[dg_currID].Cells[6].Value.ToString();
            }
            ChangeStatusBar("Připraven", false);
        }

        private void SMS_folders_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SMS_reload_list(true);
        }

        private void SMS_calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            SMS_reload_list(true);
        }

        private void dg_SMS_SelectionChanged(object sender, EventArgs e)
        {
            SMS_reload_detail();
        }


        private void dg_SMS_MouseDown(object sender, MouseEventArgs e)
        {

            /* Make sure it's an action of the right button because by default, a click of the left button will change the cell selection */
            if (e.Button.Equals(MouseButtons.Left))
            {

                /* If there are selected cells, assume they are the cells to be copied and proceed with he DoDragDrop method */
                if (dg_SMS.SelectedRows.Count > 0)
                {
                    SMS_folders.DoDragDrop(dg_SMS.SelectedRows, DragDropEffects.Move);
                }
            }
        }

        private void SMS_folders_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void SMS_folders_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                Type type = typeof(DataGridViewSelectedRowCollection);
                if (e.Data.GetDataPresent(type) == true)
                {
                    DataGridViewSelectedRowCollection DropData = (DataGridViewSelectedRowCollection)e.Data.GetData(type);

                    Point pt = SMS_folders.PointToClient(new Point(e.X, e.Y));
                    TreeNode DropTo = SMS_folders.GetNodeAt(pt);

                    string New_sms_folder;
                    string New_sms_gate;

                    if (DropTo.Parent == null)
                    {
                        New_sms_folder = "0";
                        New_sms_gate = DropTo.Name;
                    }
                    else
                    {
                        New_sms_folder = DropTo.Name;
                        New_sms_gate = DropTo.Parent.Name;
                    }
                    SMS_MoveToFolder(DropData, New_sms_gate, New_sms_folder);
                    LogDebug("Done.", "DragAndDrop");
                }
                else { LogDebug("Unsupported data source.", "DragAndDrop"); }
            }
            catch (Exception ex) { LogDebug(ex.ToString(), "Error"); }
        }

        private void SMS_MoveToFolder(DataGridViewSelectedRowCollection DropData, string New_sms_gate, string New_sms_folder)
        {
            string sql_InID = "";
            try
            {
                foreach (DataGridViewRow DropDataItem in DropData)
                {
                    LogDebug("Moving to folder... SMS: " + DropDataItem.Cells[0].Value.ToString() + "; Gate: " + New_sms_gate + "; Folder: " + New_sms_folder, "SMS");
                    if (sql_InID != "") sql_InID += " ,";
                    sql_InID += DropDataItem.Cells[0].Value.ToString();
                    if (dg_SMS.Rows.Count > 1) { dg_SMS.Rows.RemoveAt(DropDataItem.Index); }
                    else { dg_SMS.Rows.Clear(); }
                }
                sql_query = "UPDATE " + sql_db_table + "_sms SET sms_gate = '" + New_sms_gate + "', sms_folder = '" + New_sms_folder + "' WHERE id IN(" + sql_InID + ")";
                LogDebug(sql_query, "SQL");
                sql_cmd = new MySQLCommand(sql_query, MYSQL);
                sql_cmd.ExecuteNonQuery();
            }
            catch (Exception ex) { LogDebug("SMS_MoveToFolder unknown error - " + ex.ToString(), "ERROR"); }
        }

        private void SMS_timer_Tick(object sender, EventArgs e)
        {
            LogDebug("SMS_timer_Tick", "TIMER");
            SMS_timer.Enabled = false;
            SMS_reload_list(false);
        }

        private void btn_SMS_new_Click(object sender, EventArgs e)
        {
            SMS_sendMsg wnd_sms = new SMS_sendMsg(this);
            wnd_sms.Show();
        }

        private void btn_SMS_reply_Click(object sender, EventArgs e)
        {
            SMS_sendMsg wnd_sms = new SMS_sendMsg(this);
            wnd_sms.Show();
            if (dg_SMS.CurrentRow != null)
            {
                wnd_sms.SMS_sendTo.Text = dg_SMS.Rows[dg_SMS.CurrentRow.Index].Cells[2].Value.ToString(); ;
                wnd_sms.SMS_text.Focus();
            }
        }

        private void RIS_calendar_date_selected(object sender, DateRangeEventArgs e)
        {
            LogDebug("RIS_calendar_date_selected", "USER");
            RIS_dg_Load_List();
        }

        private void RIS_sel_group_SelectedValueChanged(object sender, EventArgs e)
        {
            LogDebug("RIS_sel_group_SelectedValueChanged", "USER");
            RIS_dg_Load_List();
        }

        private void dg_RIS_list_SelectionChanged(object sender, EventArgs e)
        {
            LogDebug("dg_RIS_list_SelectionChanged", "USER");
            RIS_LoadDetail();
        }

        private void CustNews_Calendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            LogDebug("CustNews_Calendar_DateSelected", "USER");
            CustNews_dg_Load_List();
        }

        private void CustNews_sel_all_versions_Click(object sender, EventArgs e)
        {
            LogDebug("CustNews_sel_all_versions_Click", "USER");
            CustNews_dg_Load_List();
        }

        private void RIS_main_split_main_doShowHide(object sender, EventArgs e)
        {
            RIS_main_panel_main_left.SuspendLayout();
            if (RIS_main_panel_main_left_widthLeftRight == 0)
            {
                // hide pannel...
                LogDebug("RIS panel - hide", "debug");
                int newWidth = RIS_main_panel_main_left.PreferredSize.Height;
                RIS_main_panel_main_left_widthLeftRight = RIS_main_split_main.Width - RIS_main_split_main.SplitterDistance;
                RIS_main_split_main.SplitterDistance = RIS_main_split_main.Width - newWidth - 5;
                RIS_main_panel_main_left.HeaderPositionPrimary = VisualOrientation.Right;
            }
            else
            {
                LogDebug("RIS panel - show", "debug");
                RIS_main_split_main.SplitterDistance = RIS_main_split_main.Width - RIS_main_panel_main_left_widthLeftRight;
                RIS_main_panel_main_left.HeaderPositionPrimary = VisualOrientation.Top;
                RIS_main_panel_main_left_widthLeftRight = 0;
            }
            RIS_main_panel_main_left.ResumeLayout();
        }
        private void SMS_panel_right_doShowHide(object sender, EventArgs e)
        {
            SMS_split_main.SuspendLayout();
            if (SMS_panel_right_left_widthLeftRight == 0)
            {
                // hide pannel...
                LogDebug("SMS panel - hide", "debug");
                int newWidth = SMS_panel_right.PreferredSize.Height;
                SMS_panel_right_left_widthLeftRight = SMS_split_main.Width - SMS_split_main.SplitterDistance;
                SMS_split_main.SplitterDistance = SMS_split_main.Width - newWidth - 5;
                SMS_panel_right.HeaderPositionPrimary = VisualOrientation.Right;
            }
            else
            {
                LogDebug("SMS panel - show", "debug");
                SMS_split_main.SplitterDistance = SMS_split_main.Width - SMS_panel_right_left_widthLeftRight;
                SMS_panel_right.HeaderPositionPrimary = VisualOrientation.Top;
                SMS_panel_right_left_widthLeftRight = 0;
            }
            SMS_split_main.ResumeLayout();
        }
        private void CTK_panel_right_doShowHide(object sender, EventArgs e)
        {
            CTK_split_top.SuspendLayout();
            if (CTK_panel_right_left_widthLeftRight == 0)
            {
                // hide pannel...
                LogDebug("SMS panel - hide", "debug");
                int newWidth = CTK_panel_right.PreferredSize.Height;
                CTK_panel_right_left_widthLeftRight = CTK_split_top.Width - CTK_split_top.SplitterDistance;
                CTK_split_top.SplitterDistance = CTK_split_top.Width - newWidth - 5;
                CTK_panel_right.HeaderPositionPrimary = VisualOrientation.Right;
            }
            else
            {
                LogDebug("SMS panel - show", "debug");
                CTK_split_top.SplitterDistance = CTK_split_top.Width - CTK_panel_right_left_widthLeftRight;
                CTK_panel_right.HeaderPositionPrimary = VisualOrientation.Top;
                CTK_panel_right_left_widthLeftRight = 0;
            }
            CTK_split_top.ResumeLayout();
        }


        private void CustNews_main_panel_right_doShowHide(object sender, EventArgs e)
        {
            CustNews_split_main.SuspendLayout();
            if (CustNews_panel_right_left_widthLeftRight == 0)
            {
                // hide pannel...
                LogDebug("CustNews panel - hide", "debug");
                int newWidth = CustNews_main_panel_right.PreferredSize.Height;
                CustNews_panel_right_left_widthLeftRight = CustNews_split_main.Width - CustNews_split_main.SplitterDistance;
                CustNews_split_main.SplitterDistance = CustNews_split_main.Width - newWidth - 5;
                CustNews_main_panel_right.HeaderPositionPrimary = VisualOrientation.Right;
            }
            else
            {
                LogDebug("CustNews panel - show", "debug");
                CustNews_split_main.SplitterDistance = CustNews_split_main.Width - CustNews_panel_right_left_widthLeftRight;
                CustNews_main_panel_right.HeaderPositionPrimary = VisualOrientation.Top;
                CustNews_panel_right_left_widthLeftRight = 0;
            }
            CustNews_split_main.ResumeLayout();
        }

    }
}
