using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using ComponentFactory.Krypton.Toolkit;

namespace CTK_reader
{
    public partial class cfg_color_font : KryptonForm
    {
        private MainForm opener;
        private bool cfg_saved = false;

        public cfg_color_font(MainForm sender)
        {
            InitializeComponent();
            opener = sender;
        }

        private void cfg_color_Load(object sender, EventArgs e)
        {
            txt_readed.ForeColor = opener.UIcolors[0];
            txt_readed.BackColor = opener.UIcolors[1];
            txt_unreaded.ForeColor = opener.UIcolors[2];
            txt_unreaded.BackColor = opener.UIcolors[3];
            txt_detail.ForeColor = opener.UIcolors[4];
            txt_detail.BackColor = opener.UIcolors[5];
            txt_active.ForeColor = opener.UIcolors[6];
            txt_active.BackColor = opener.UIcolors[7];

            cfg_nacti_fonty();

            cfg_ctk_list_font.Text = opener.dg_ctk_list.Font.Name.ToString();
            cfg_ctk_list_font_size.Value = Convert.ToInt32(opener.dg_ctk_list.Font.Size);
            cfg_ctk_detail_font.Text = opener.ctk_detail.Font.Name.ToString();
            cfg_ctk_detail_font_size.Value = Convert.ToInt32(opener.ctk_detail.Font.Size);
        }

        void cfg_nacti_fonty()
        {
            XmlNodeList xml_font;

            cfg_ctk_list_font.Items.Clear();
            cfg_ctk_detail_font.Items.Clear();

            XmlDocument ctk_font = new XmlDocument();
            ctk_font.LoadXml(Properties.Resources.ctk_fonty);
            XmlNode ctk_font_element = ctk_font.DocumentElement;
            xml_font = ctk_font_element.SelectNodes("/ctk_fonty/font/text()");

            for (int loop = 0; loop < xml_font.Count; loop++)
            {
                if (xml_font[loop].Value != "*")
                {
                    cfg_ctk_list_font.Items.Add(xml_font[loop].Value);
                    cfg_ctk_detail_font.Items.Add(xml_font[loop].Value);
                }
            }
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            opener.UIcolors[0] = txt_readed.ForeColor;
            opener.UIcolors[1] = txt_readed.BackColor;
            opener.UIcolors[2] = txt_unreaded.ForeColor;
            opener.UIcolors[3] = txt_unreaded.BackColor;
            opener.UIcolors[4] = txt_detail.ForeColor;
            opener.UIcolors[5] = txt_detail.BackColor;
            opener.UIcolors[6] = txt_active.ForeColor;
            opener.UIcolors[7] = txt_active.BackColor;
            
            opener.ctk_detail.ForeColor = txt_detail.ForeColor;
            opener.ctk_detail.BackColor = txt_detail.BackColor;
            opener.SMS_detail.ForeColor = txt_detail.ForeColor;
            opener.SMS_detail.BackColor = txt_detail.BackColor;
            opener.RIS_txt_detail.ForeColor = txt_detail.ForeColor;
            opener.RIS_txt_detail.BackColor = txt_detail.BackColor;
            opener.CustNews_txt_detail.ForeColor = txt_detail.ForeColor;
            opener.CustNews_txt_detail.BackColor = txt_detail.BackColor;

            opener.dg_ctk_list.BackgroundColor = txt_readed.BackColor;
            opener.dg_ctk_list.RowsDefaultCellStyle.SelectionForeColor = txt_active.ForeColor;
            opener.dg_ctk_list.RowsDefaultCellStyle.SelectionBackColor = txt_active.BackColor;
            opener.dg_SMS.BackgroundColor = txt_readed.BackColor;
            opener.dg_SMS.RowsDefaultCellStyle.SelectionForeColor = txt_active.ForeColor;
            opener.dg_SMS.RowsDefaultCellStyle.SelectionBackColor = txt_active.BackColor;
            opener.dg_RIS_list.BackgroundColor = txt_readed.BackColor;
            opener.dg_RIS_list.RowsDefaultCellStyle.SelectionForeColor = txt_active.ForeColor;
            opener.dg_RIS_list.RowsDefaultCellStyle.SelectionBackColor = txt_active.BackColor;
            opener.dg_CustNews_list.BackgroundColor = txt_readed.BackColor;
            opener.dg_CustNews_list.RowsDefaultCellStyle.SelectionForeColor = txt_active.ForeColor;
            opener.dg_CustNews_list.RowsDefaultCellStyle.SelectionBackColor = txt_active.BackColor;
            

            opener.dg_ctk_list.Font = new Font(cfg_ctk_list_font.Text, Convert.ToInt32(cfg_ctk_list_font_size.Value));
            opener.dg_SMS.Font = new Font(cfg_ctk_list_font.Text, Convert.ToInt32(cfg_ctk_list_font_size.Value));
            opener.dg_RIS_list.Font = new Font(cfg_ctk_list_font.Text, Convert.ToInt32(cfg_ctk_list_font_size.Value));
            opener.dg_CustNews_list.Font = new Font(cfg_ctk_list_font.Text, Convert.ToInt32(cfg_ctk_list_font_size.Value));
            opener.ctk_detail.Font = new Font(cfg_ctk_detail_font.Text, Convert.ToInt32(cfg_ctk_detail_font_size.Value));
            opener.SMS_detail.Font = new Font(cfg_ctk_detail_font.Text, Convert.ToInt32(cfg_ctk_detail_font_size.Value));
            opener.RIS_txt_detail.Font = new Font(cfg_ctk_detail_font.Text, Convert.ToInt32(cfg_ctk_detail_font_size.Value));
            opener.CustNews_txt_detail.Font = new Font(cfg_ctk_detail_font.Text, Convert.ToInt32(cfg_ctk_detail_font_size.Value));

            opener.ctk_list_reload();
            cfg_saved = true;
            opener.ShowAppNotify("Nastaven� barev bylo ulo�eno.", 3);
            this.Close();
        }


        private Color OpenColorDialog(Color actColor)
        {
            dialog_color.Color = actColor;
            dialog_color.ShowDialog();
            actColor = dialog_color.Color;
            return actColor;
        }

        private void btn_readed_text_Click(object sender, EventArgs e)
        {
            txt_readed.ForeColor = OpenColorDialog(txt_readed.ForeColor);
        }

        private void btn_readed_bg_Click(object sender, EventArgs e)
        {
            txt_readed.BackColor = OpenColorDialog(txt_readed.BackColor);
        }

        private void btn_unreaded_text_Click(object sender, EventArgs e)
        {
            txt_unreaded.ForeColor = OpenColorDialog(txt_unreaded.ForeColor);
        }

        private void btn_unreaded_bg_Click(object sender, EventArgs e)
        {
            txt_unreaded.BackColor = OpenColorDialog(txt_unreaded.BackColor);
        }

        private void btn_detail_text_Click(object sender, EventArgs e)
        {
            txt_detail.ForeColor = OpenColorDialog(txt_detail.ForeColor);
        }

        private void btn_detail_bg_Click(object sender, EventArgs e)
        {
            txt_detail.BackColor = OpenColorDialog(txt_detail.BackColor);
        }

        private void btn_active_text_Click(object sender, EventArgs e)
        {
            txt_active.ForeColor = OpenColorDialog(txt_active.ForeColor);
        }

        private void btn_active_bg_Click(object sender, EventArgs e)
        {
            txt_active.BackColor = OpenColorDialog(txt_active.BackColor);
        }

        private void btn_default_Click(object sender, EventArgs e)
        {
            DialogResult res  = MessageBox.Show("Opravdu chcete obnovit v�choz� nastaven� barev a p�sem?", "V�choz� nastaven�", MessageBoxButtons.YesNo,MessageBoxIcon.Question,MessageBoxDefaultButton.Button2);
            if (res == DialogResult.Yes)
            {
                txt_readed.ForeColor = Color.Black;
                txt_readed.BackColor = Color.White;
                txt_unreaded.ForeColor = Color.DarkRed;
                txt_unreaded.BackColor = Color.White;
                txt_detail.ForeColor = Color.Black;
                txt_detail.BackColor = Color.FromArgb(187, 217, 255);
                txt_active.ForeColor = Color.Black;
                txt_active.BackColor = Color.Yellow;

                cfg_ctk_list_font.Text = "Tahoma";
                cfg_ctk_list_font_size.Value = 10;
                cfg_ctk_detail_font.Text = "Tahoma";
                cfg_ctk_detail_font_size.Value = 12;

            }
        }

        private void cfg_color_Closing(object sender, FormClosingEventArgs e)
        {
            if (cfg_saved == false)
            {
                DialogResult result = MessageBox.Show("Nastaven� nebylo ulo�eno.\nPokud okno zav�ete, nastaven� nebude ulo�eno.", "Nastaven� nebylo ulo�eno", MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                switch (result)
                {
                    case DialogResult.OK:
                        break;
                    case DialogResult.Cancel:
                        e.Cancel = true;
                        break;
                }
            }
        }
    }
}